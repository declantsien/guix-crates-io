(define-module (crates-io so rt) #:use-module (crates-io))

(define-public crate-sort-0.0.3 (crate (name "sort") (vers "0.0.3") (hash "1wkjl8xv8afr1lqkb8prs0rnbm1hplf7czrzdm7j4c309d3ca3dz")))

(define-public crate-sort-0.0.1 (crate (name "sort") (vers "0.0.1") (hash "09ipzfcrvr2fkvihr50yrfq3px1wvxr65wbb8qml6i7694fnvcw5")))

(define-public crate-sort-0.0.4 (crate (name "sort") (vers "0.0.4") (hash "094r2aamsiwzywminybnifr7zvpg1x6f0h3v5m913nspp7vl141j")))

(define-public crate-sort-0.1 (crate (name "sort") (vers "0.1.0") (hash "081vxj9mmf2v5czzr3ahggiad7fci2hxlyh9yvjxqqblpmddsx0m")))

(define-public crate-sort-0.2 (crate (name "sort") (vers "0.2.0") (hash "0lrk465s3g9c1csvmw55gjsn26973vdpck6r9rw05ixhijbpxay4")))

(define-public crate-sort-0.3 (crate (name "sort") (vers "0.3.0") (hash "0rhhsf1ym3ca8gkzqsny5l5vxbi7fj4fz2xayxn20h90qxzdghxn")))

(define-public crate-sort-0.4 (crate (name "sort") (vers "0.4.0") (hash "02h1nx3bws289knq820a9lcm3whzmj72nlqp0b8im37kf5kqy0jc")))

(define-public crate-sort-0.5 (crate (name "sort") (vers "0.5.0") (hash "0b7safy8gfbnja4q3yv0gd36s2lc6igm7nbwbcdlz8bkhdsrwnyw")))

(define-public crate-sort-0.6 (crate (name "sort") (vers "0.6.0") (hash "0xln0pdgrrs8qhxkrl0213zjypg6gnxq6k0bzv0pysknklzsxqyf")))

(define-public crate-sort-0.7 (crate (name "sort") (vers "0.7.0") (hash "1sfx7f8gw074rs64i0s2z7vncdhky823yzvn471arkb6rls2n2jl")))

(define-public crate-sort-0.8 (crate (name "sort") (vers "0.8.0") (hash "1m3n9rgk0jinw83xwlg1fx9iajx742bq1vrqzdl8vqr1hxycr9mp")))

(define-public crate-sort-0.8 (crate (name "sort") (vers "0.8.1") (hash "1d32xxm4wf121wc2civ9klbwzk38p4hy35y0fbg2rvqihnklhz6c")))

(define-public crate-sort-0.8 (crate (name "sort") (vers "0.8.2") (hash "0ii3mv1rgmnvjha1qk291y8vd8akdrmw4935b49mf1ryiwaq4qy3")))

(define-public crate-sort-0.8 (crate (name "sort") (vers "0.8.3") (hash "1cdf9cmlilza0q4dmfjkd1f75vs98h26d81s907id1pl8yp9pcwg")))

(define-public crate-sort-0.8 (crate (name "sort") (vers "0.8.4") (hash "0k10cawc8bswrzf6dg74npkamc8vpirykvhh5plbd87775qzqbdp")))

(define-public crate-sort-0.8 (crate (name "sort") (vers "0.8.5") (hash "1idh3iak2y2xdy8fj76qk875gcprmw62mhcxcr2x5fmw9hwkddvr")))

(define-public crate-sort-by-borrowed-key-1 (crate (name "sort-by-borrowed-key") (vers "1.0.0") (hash "1pcssbzak1fbwd0drdp6xclj5np1z9kx12v31590zrdicld0s6wy")))

(define-public crate-sort-cargo-errors-1 (crate (name "sort-cargo-errors") (vers "1.0.0") (deps (list (crate-dep (name "supports-color") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1qlsh4yds1mgpc4cx4q4i5sjr7i73bjbzxwy8c3v5yb6y4nwpx95")))

(define-public crate-sort-it-0.1 (crate (name "sort-it") (vers "0.1.0") (hash "1z3gmbmzbycvsw8pd21jrvn1psz7h88s7b9lmwwp26qclpda4ill")))

(define-public crate-sort-it-0.1 (crate (name "sort-it") (vers "0.1.1") (hash "1k1hsmi8haxlzd6zmsqpmq0x974mrf78ybbycddrb52r9pfl9d3h")))

(define-public crate-sort-it-0.1 (crate (name "sort-it") (vers "0.1.2") (hash "0k89cgw70ri8p7ir7k0v54jccb0ja8l03cj18i3i23nsanqcsm9c")))

(define-public crate-sort-it-0.1 (crate (name "sort-it") (vers "0.1.3") (hash "1n1y14xjqfi9msm84dwaxb6ylybjli7ycyglq7m95szfqcz6j1zk")))

(define-public crate-sort-it-0.1 (crate (name "sort-it") (vers "0.1.4") (hash "1zy8623hmb3i0ymwdpc6hmjxcna8wjmh9acg7ikapxxyffd9pk9q")))

(define-public crate-sort-it-0.2 (crate (name "sort-it") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "18i1lypnzrdmbxz5ds5qf0zz64kj7bzcf7w71hz8b9mgfi09fm05")))

(define-public crate-sort-it-0.2 (crate (name "sort-it") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1h0vjn62nmhg0l9i6wxl409hdd4szpiispfpbjacmxiwjikck9p1")))

(define-public crate-sort-it-0.2 (crate (name "sort-it") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1i9pyi32l1zgbhbghnr79bq20f2ck1kwq3di367g2rpcswc5w06h")))

(define-public crate-sort-path-length-0.1 (crate (name "sort-path-length") (vers "0.1.0") (hash "0n5avgrwzyhc763rmi781nc58g5n7hnqshzahy26fspzlixzahr7")))

(define-public crate-sort-path-length-0.1 (crate (name "sort-path-length") (vers "0.1.1") (hash "0gc4mi2n11aj4liypb7qxh2bnvqzgibqjaby8s3a630c9b20i2qh")))

(define-public crate-sort-path-length-0.2 (crate (name "sort-path-length") (vers "0.2.0") (hash "0cxlmsil7pfjaj2c912cgf3nqn1g0jj9xk28fbihxn815dx9s7bg")))

(define-public crate-sort-path-length-0.2 (crate (name "sort-path-length") (vers "0.2.1") (hash "0aqjd2hf50ympvy3hpvvk3nzhi1260xla180d8p3lhdjmj6b6zza")))

(define-public crate-sort-path-length-0.3 (crate (name "sort-path-length") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1xcay22i8wkjxp2imj7v53jp0990qzd9qixcgwy69c553q3i91dd")))

(define-public crate-sort-path-length-0.3 (crate (name "sort-path-length") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0z4hix1f6brwxai3dki8znb4by9b302ric6akvzbbgp6wjwb9py3")))

(define-public crate-sort-rs-0.1 (crate (name "sort-rs") (vers "0.1.0") (hash "0qbq2c4na88lshpwwaj6b4nc5xlqya0a742z1gzmvvg1gxwqz9q2")))

(define-public crate-sort-rs-0.1 (crate (name "sort-rs") (vers "0.1.1") (hash "1kl8ls6mwd59izirnjbxlqgrkfq60ll91ycslpclji0p5ylaawqh")))

(define-public crate-sort-rs-0.1 (crate (name "sort-rs") (vers "0.1.2") (hash "0ilgg0wam22n7xkq6cxrsl4c5j5zrll0vip6pjqz3gfkkchibyk3")))

(define-public crate-sort-steps-0.0.0 (crate (name "sort-steps") (vers "0.0.0") (deps (list (crate-dep (name "gen-iter") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 0)))) (hash "0cs4qhyzz6sdn6kndfd8rphmj3c2lyb6p5vrnlw19z39a44w0jy1") (features (quote (("default"))))))

(define-public crate-sort-steps-0.1 (crate (name "sort-steps") (vers "0.1.0") (deps (list (crate-dep (name "gen-iter") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 0)))) (hash "0byfg23p4ck5p9bs7m5ypmqmkxcj73pihg0nvp8ia3ss0z24nkbc") (features (quote (("default"))))))

(define-public crate-sort-steps-0.2 (crate (name "sort-steps") (vers "0.2.0") (deps (list (crate-dep (name "gen-iter") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 0)))) (hash "067bv9q6k86mlz06qyvllbh3r5dg6sk6v2abdciv10y9y1k7zin5") (features (quote (("default"))))))

(define-public crate-sort-steps-0.2 (crate (name "sort-steps") (vers "0.2.1") (deps (list (crate-dep (name "gen-iter") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 0)))) (hash "0k6hxrhxwmjz2zhnp81wzpi1x9j7z73c4kh80bj0y3vrgsfvjm36") (features (quote (("default"))))))

(define-public crate-sort-steps-0.2 (crate (name "sort-steps") (vers "0.2.2") (deps (list (crate-dep (name "gen-iter") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 0)))) (hash "1lvd4pj4ibd9zr6mz4g1vp0diqnii32dfy410lvbj7lnx67kbgin") (features (quote (("default"))))))

(define-public crate-sort-steps-0.2 (crate (name "sort-steps") (vers "0.2.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 0)))) (hash "1b09f8c905p0cn1i9jqns9pwpif40i0pajqrffj4fx3cf1gz3g23") (features (quote (("default"))))))

(define-public crate-sort-steps-0.2 (crate (name "sort-steps") (vers "0.2.4") (deps (list (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 0)))) (hash "09pqd6mnrx860qj6waz1dfrjjr5kg3i0mcrgprn6vjiw1fqagik0") (features (quote (("default"))))))

(define-public crate-sort-visual-0.0.0 (crate (name "sort-visual") (vers "0.0.0") (deps (list (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)) (crate-dep (name "sort-steps") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "16c8lly51dnkk5f7ijzsp2gm8z34fyk2qcjridrhy71ijrv7krfj") (features (quote (("default"))))))

(define-public crate-sort_algorithms-0.1 (crate (name "sort_algorithms") (vers "0.1.1") (hash "142jkiawz7mz4ga3vbkmalnma3ixjdq12996mmgkn4xl1d22dyf8")))

(define-public crate-sort_algorithms-0.1 (crate (name "sort_algorithms") (vers "0.1.2") (hash "0q57f17lfypz8n42aq737yalflvq0l3s3lbl9pnr150zbqlfvmlx")))

(define-public crate-sort_algorithms-0.1 (crate (name "sort_algorithms") (vers "0.1.3") (hash "1119b6q4bg5zh6qdi4x7d5qsziccfrirjyra4gdv63mr999wd23s")))

(define-public crate-sort_algorithms-0.2 (crate (name "sort_algorithms") (vers "0.2.0") (hash "18cwc16iyznrp9cq35c3539hcjavg3crqfq360xy33myjym2k14g")))

(define-public crate-sort_algorithms-0.2 (crate (name "sort_algorithms") (vers "0.2.5") (hash "17jjv0xf5m0vjk3fg936903hq1p01k2p5kr477flrg838p0fqv2w")))

(define-public crate-sort_algorithms-0.2 (crate (name "sort_algorithms") (vers "0.2.6") (hash "00037slcdyahjdzz6019gaknw8dhicknvzm80zjbsfa7j9n1mi9r")))

(define-public crate-sort_algorithms-0.2 (crate (name "sort_algorithms") (vers "0.2.7") (hash "1sfdhmli40195ksi2yngd59yijbchrk7zc0z8axnvbrszw78178c")))

(define-public crate-sort_algorithms-0.2 (crate (name "sort_algorithms") (vers "0.2.8") (hash "1fx1qg341prqyqgl840bc3d5n14df8n16qhr708iidgbhzxpadxf")))

(define-public crate-sort_algorithms-0.2 (crate (name "sort_algorithms") (vers "0.2.9") (hash "0xn99n6mmfc26k4w6v549xlc211zdpq0nwinlkbxq45jnwwq2hhp")))

(define-public crate-sort_algorithms-0.3 (crate (name "sort_algorithms") (vers "0.3.0") (hash "0xmxmnyalcgk0325n1cr0k2lzrcd55pscppdxwi16ydp4vfzza93")))

(define-public crate-sort_algorithms-0.3 (crate (name "sort_algorithms") (vers "0.3.1") (hash "0aifk1lsaz5xya8k8wf641819qmim07dc3w62xq0f86987vi0kaj")))

(define-public crate-sort_algos-0.1 (crate (name "sort_algos") (vers "0.1.0") (hash "0hpmk2q2whq4c7g2ic99sni472lys8nqkb6ifnsmf1c556nv5x6q") (yanked #t)))

(define-public crate-sort_algos-0.1 (crate (name "sort_algos") (vers "0.1.1") (hash "110bp3v3k3hsjmliapvxw7m7wvyjzggkrr5mq0w9z2bhbylllgd4") (yanked #t)))

(define-public crate-sort_algos-0.1 (crate (name "sort_algos") (vers "0.1.2") (hash "1rjx62r1f5n72x44w2ah559c45d6jnfb8asan0kw0acd588njvlz")))

(define-public crate-sort_alogorithms-0.1 (crate (name "sort_alogorithms") (vers "0.1.1") (hash "1pjkm9ka23m79hbkfkl7r3hnkzvjx0dxyhycw3i2il7zzwxgwjm6")))

(define-public crate-sort_alogorithms_v010-0.1 (crate (name "sort_alogorithms_v010") (vers "0.1.0") (hash "14aw6hwk1wb52nmsjk71qmyfz9ncy4hgfhz3795zd57wq0lzbj8g")))

(define-public crate-sort_by_derive-0.1 (crate (name "sort_by_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "rust-format") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.99") (default-features #t) (kind 0)))) (hash "0l4n09493q2gk8v85p8s5gn7i358g93iwf7577ykd1lp0akmkjvn")))

(define-public crate-sort_by_derive-0.1 (crate (name "sort_by_derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "rust-format") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.99") (default-features #t) (kind 0)))) (hash "0wwc2qpylm97b50bwq0vy9blr72zydm6lr4iyqr0nj668j8ks17x")))

(define-public crate-sort_by_derive-0.1 (crate (name "sort_by_derive") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "rust-format") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.99") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0s76gcxvwj4n6az2bcmrkvd2y2mrj3f3ah5hlqf77b5kw179m9ri")))

(define-public crate-sort_by_derive-0.1 (crate (name "sort_by_derive") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "rust-format") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.99") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1mzh69m31bapb92qw94bh9inaalld4bimpyrnyilwf6dr9vxmdxp")))

(define-public crate-sort_by_derive-0.1 (crate (name "sort_by_derive") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "rust-format") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.99") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "08fn6nqaqvdws14pl53qq6hf5n2y5xgc6185c81ah80idyb7l0dr")))

(define-public crate-sort_by_derive-0.1 (crate (name "sort_by_derive") (vers "0.1.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "rust-format") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.99") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "158fqywc4bvmlkjy0xcsb6km0cqfj3xql9ivl18sw3x2gxmlbkq3")))

(define-public crate-sort_by_derive-0.1 (crate (name "sort_by_derive") (vers "0.1.6") (deps (list (crate-dep (name "either") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "rust-format") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.99") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1cagwgnp1dziylli58hvihjhrw8xv3zrim66vmz6jkc8nz8v54yg")))

(define-public crate-sort_by_derive-0.1 (crate (name "sort_by_derive") (vers "0.1.7") (deps (list (crate-dep (name "either") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "rust-format") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.99") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1p3frpszlcs1xj92a8zqqvm95k0bjgybzwn3mwgsgdpjr535q958")))

(define-public crate-sort_by_derive-0.1 (crate (name "sort_by_derive") (vers "0.1.8") (deps (list (crate-dep (name "either") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "rust-format") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.99") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "028p1jhri20rpj0kz5lqcmhpx34hkjlzmx9x0lf2mcynjpvzp57r")))

(define-public crate-sort_by_derive-0.1 (crate (name "sort_by_derive") (vers "0.1.9") (deps (list (crate-dep (name "either") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "rust-format") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.99") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ncymxccvd30rzf48lzwzyj89nzq24dwdhda3k3f663gkc9ll3ax")))

(define-public crate-sort_by_derive-0.1 (crate (name "sort_by_derive") (vers "0.1.10") (deps (list (crate-dep (name "either") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "rust-format") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.99") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0gg9mmnj2ci3dr2r3jcw9bgyyayd4jz50v0yn85dglpldfk8kzps")))

(define-public crate-sort_by_derive-0.1 (crate (name "sort_by_derive") (vers "0.1.11") (deps (list (crate-dep (name "either") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "rust-format") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.99") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1yil7bh6v24cznybz04v00pqcindw6yz12vh5c1fx345g00chh12")))

(define-public crate-sort_by_derive-0.1 (crate (name "sort_by_derive") (vers "0.1.12") (deps (list (crate-dep (name "either") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "rust-format") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.99") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1nnz1wg3a9bifpzfkr3h765nyxyzs2k38xqq7740pr936z9zylci")))

(define-public crate-sort_by_derive-0.1 (crate (name "sort_by_derive") (vers "0.1.13") (deps (list (crate-dep (name "either") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "rust-format") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.99") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0dg4zb9facz0w5gvvq760645bcwiwnqajcc4v1y2sdhqaiwxzsi1")))

(define-public crate-sort_by_derive-0.1 (crate (name "sort_by_derive") (vers "0.1.14") (deps (list (crate-dep (name "either") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "rust-format") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.99") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "07xh9n7ns0bqg5g8ndg5alqaiqdrf45qrzpcrl0qwi9bcjj8vs3v")))

(define-public crate-sort_by_derive-0.1 (crate (name "sort_by_derive") (vers "0.1.15") (deps (list (crate-dep (name "either") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "rust-format") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.99") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ldzr9l7f7m93xqx5a5gjnrhrvay4nigg9yn0nswgsvx9sp0i9x4")))

(define-public crate-sort_by_derive-0.1 (crate (name "sort_by_derive") (vers "0.1.16") (deps (list (crate-dep (name "either") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "rust-format") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2.0.28") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "139r7p9jfqicck7vkh8hyxb0y1q06641f0bqnzm8xqx68fg57v4y")))

(define-public crate-sort_by_derive-0.1 (crate (name "sort_by_derive") (vers "0.1.17") (deps (list (crate-dep (name "either") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "rust-format") (req ">=0.2, <1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1z60cdkwa5fzw2civ0z7jvbif4smn2xqhq53qy8vhnhw7i9vq0yg")))

(define-public crate-sort_library-0.1 (crate (name "sort_library") (vers "0.1.0") (hash "08g5azjsy7403r3awdw76qa0i7sicqp7j9cn4yqrgm71v3xmwq86")))

(define-public crate-sort_library-0.1 (crate (name "sort_library") (vers "0.1.1") (hash "1dj78x64w8cfcn0rfpp827gxfzpg13ha91clzr5kgsyym03n6905")))

(define-public crate-sort_method-0.1 (crate (name "sort_method") (vers "0.1.0") (hash "178dm7w7m5rva7rxv0b3av7klqksxgm0a94sbwi27x8w0d3ma4wi")))

(define-public crate-sort_nvim_spell_file-0.1 (crate (name "sort_nvim_spell_file") (vers "0.1.0") (deps (list (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "1aa0yxd4rh45j3vyabyd3y86d1z047ci0inc5avbd7xfdc7bwp2h")))

(define-public crate-sort_rust-0.1 (crate (name "sort_rust") (vers "0.1.0") (hash "1pbawwghpvs2jgdfwy70sy2rs9bmj6g0d7py2kv4z6fim2xc4ax9")))

(define-public crate-sort_rust-0.2 (crate (name "sort_rust") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "04iivlaldpkfdba4fw1w3kpqjn0axrhrw7lr233s18rpklvpfgzh")))

(define-public crate-sort_rust3-0.1 (crate (name "sort_rust3") (vers "0.1.0") (deps (list (crate-dep (name "rust_keien") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rust_wbc") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rust_wek") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "sort_rust") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0py5sbb4bcixm8n0cmbrncjpkzdgnsyjgfrlf4phw3niqklda0pv")))

(define-public crate-sort_solves-0.1 (crate (name "sort_solves") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.6.1") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0jsg2nzfacwa0yfhczcylsq5j2jiixgnqpg1cbacv3vsi6p4lrc1") (yanked #t)))

(define-public crate-sort_solves-0.2 (crate (name "sort_solves") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.6.1") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "00l7hcbmbsn4z82sygdb5v4idqgacjrv8cynby59mibimkzrw536")))

(define-public crate-sort_str_to_sql-0.0.1 (crate (name "sort_str_to_sql") (vers "0.0.1") (hash "1cm9frnpc1z07ppgq4wbf3przp4ynyjzlckcr4r83zwcrwg80whc")))

(define-public crate-sort_str_to_sql-0.0.2 (crate (name "sort_str_to_sql") (vers "0.0.2") (hash "03wn4y0igrbijjn44mss4b3lb1lyj61ygjvdk7grnwmhyqlym74v")))

(define-public crate-sort_str_to_sql-0.0.3 (crate (name "sort_str_to_sql") (vers "0.0.3") (deps (list (crate-dep (name "regex") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0nnxcsksnawi4ch8ypx3sx1aimnsp6wbxjijnfihjsbli8z4x8wm")))

(define-public crate-sort_str_to_sql-0.0.4 (crate (name "sort_str_to_sql") (vers "0.0.4") (deps (list (crate-dep (name "regex") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "regex_macros") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0r9r8klsyv0clw01swywg7i2wbza4f69axb2sp67xp057x4jdgcv")))

(define-public crate-sort_str_to_sql-0.0.5 (crate (name "sort_str_to_sql") (vers "0.0.5") (deps (list (crate-dep (name "regex") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "regex_macros") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "127m6kjdrx77n7fsl5zn53zlvnc4nmyyzqhpcz3xd5nplgh0gh9a")))

(define-public crate-sort_str_to_sql-1 (crate (name "sort_str_to_sql") (vers "1.0.0") (deps (list (crate-dep (name "regex") (req "^0.1.27") (default-features #t) (kind 0)))) (hash "1jxxvkajm5pc6261y2i2mb6zjs7h13jhbk8mx483lrnd6bh4r5a0")))

(define-public crate-sort_zh-0.1 (crate (name "sort_zh") (vers "0.1.0") (deps (list (crate-dep (name "chinese-number") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "rust_icu_ucol") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "1d45fgp8nnrdm7jjkg4pmpsn4g46sgxijpkg2zzn9hi2w10fp1z2")))

(define-public crate-sort_zh-0.1 (crate (name "sort_zh") (vers "0.1.1") (deps (list (crate-dep (name "chinese-number") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "rust_icu_ucol") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "1dm29w3g85jswayypyka0k4j0yz1y3z68ck82xipxr43m19w9w8l")))

(define-public crate-sortable-js-0.1 (crate (name "sortable-js") (vers "0.1.0+js.1.15.0") (deps (list (crate-dep (name "js-sys") (req "^0.3.60") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.83") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.60") (features (quote ("Element" "HtmlElement"))) (default-features #t) (kind 0)))) (hash "1lf92v5cxvjjz0vlp136749zsfk7dychhm8gh2djyl7v77ad6yli")))

(define-public crate-sortable-js-0.1 (crate (name "sortable-js") (vers "0.1.1+js.1.15.0") (deps (list (crate-dep (name "js-sys") (req "^0.3.60") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.83") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.60") (features (quote ("Element" "HtmlElement"))) (default-features #t) (kind 0)))) (hash "0w292v56ifm19k8yr3m897rychhp8a8hpv5yzybsy5sn5j4ghk0x")))

(define-public crate-sortable-js-0.1 (crate (name "sortable-js") (vers "0.1.2+js.1.15.0") (deps (list (crate-dep (name "js-sys") (req "^0.3.60") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.83") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.60") (features (quote ("Element" "HtmlElement"))) (default-features #t) (kind 0)))) (hash "105wxsnvpn6wbhhpqpz3a00q9hdlzjigmxkya4l89x85cvd3sgfz")))

(define-public crate-sortable-js-0.1 (crate (name "sortable-js") (vers "0.1.3+js.1.15.0") (deps (list (crate-dep (name "js-sys") (req "^0.3.60") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.83") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.60") (features (quote ("DomRect" "Element" "HtmlElement"))) (default-features #t) (kind 0)))) (hash "00xqa2qw0myw7nwv52ycm6gi5gjn6chrgfbsv2jaqi5w161hlnds")))

(define-public crate-sortable-js-0.1 (crate (name "sortable-js") (vers "0.1.4+js.1.15.0.patched.1") (deps (list (crate-dep (name "js-sys") (req "^0.3.60") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.83") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.60") (features (quote ("DomRect" "Element" "HtmlElement"))) (default-features #t) (kind 0)))) (hash "1zd9k54br5mh4apsnhc95hn2bs49n75y8nasf51ncjyxpzlyyq6z")))

(define-public crate-sortable-js-0.1 (crate (name "sortable-js") (vers "0.1.5+js.1.15.0.patched.1") (deps (list (crate-dep (name "js-sys") (req "^0.3.60") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.83") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.60") (features (quote ("DomRect" "Element" "HtmlElement"))) (default-features #t) (kind 0)))) (hash "1idlzkjbvinymgm4hq5x6720lx7mphidrh4slnb6240yhgpl0b5r")))

(define-public crate-sortable-js-0.1 (crate (name "sortable-js") (vers "0.1.6+js.1.15.2.patched.1") (deps (list (crate-dep (name "js-sys") (req "^0.3.60") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.83") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.60") (features (quote ("DomRect" "Element" "HtmlElement"))) (default-features #t) (kind 0)))) (hash "05y6s8rn4wnjllvka2hp768dy5d7ywrvsg9q345c332c7i1nd4z4")))

(define-public crate-sortable-quick-unique-id-1 (crate (name "sortable-quick-unique-id") (vers "1.1.0") (deps (list (crate-dep (name "machine_uuid") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0hr43gw2wvcz6j2s96xri0l644x1wifylxcrp8k0p5d38hpavc1c")))

(define-public crate-sortbuf-0.1 (crate (name "sortbuf") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.3") (default-features #t) (kind 2)))) (hash "1aryx9dj87yr0mqdsixwdk9gyza6n5id1k71xzv6hd0cs8j3vg8p")))

(define-public crate-sortby-0.1 (crate (name "sortby") (vers "0.1.0") (hash "0ca92kyaziycs87fiavwk4lkhgxhf5zvba5fgwyn7xjjxwm5cv8a")))

(define-public crate-sortby-0.1 (crate (name "sortby") (vers "0.1.1") (hash "0faxga2y715dgbdyvpnwbgzn8r7b0xz2xrgqckhjsp8pns4q8v70")))

(define-public crate-sortby-0.1 (crate (name "sortby") (vers "0.1.2") (hash "1vxrdd8i45b0887wfy1j8yab8rpzsdpp4g1p2n6s8719fl4bza4i")))

(define-public crate-sortby-0.1 (crate (name "sortby") (vers "0.1.3") (hash "0chbqy0bsvnc23znsrd58m6yj2aa4n4r4biraa85y5rmn45nq28v")))

(define-public crate-sorted-0.1 (crate (name "sorted") (vers "0.1.0") (hash "1zdqivzdn67ka58i283yi0pd76h0n99b03pyyr3yrw63b4907k9a") (features (quote (("unstable") ("default"))))))

(define-public crate-sorted-0.2 (crate (name "sorted") (vers "0.2.0") (hash "1lkgcj054z6dq5rjv7cldz279p3vmbsqqkl1gn7smmif4avqhhjl") (features (quote (("unstable") ("default"))))))

(define-public crate-sorted-channel-0.1 (crate (name "sorted-channel") (vers "0.1.0") (deps (list (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)))) (hash "180m5s2djvzblvq5v95ccq1zsgkljrn2xpp04vgygb48km0s8as5")))

(define-public crate-sorted-channel-0.1 (crate (name "sorted-channel") (vers "0.1.1") (deps (list (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)))) (hash "1f29n26vls28k63grk907cva67agbfjp9jzlwxl6jqf270kcfids")))

(define-public crate-sorted-collections-0.0.1 (crate (name "sorted-collections") (vers "0.0.1") (hash "1c97y84cycy0nysxg6x1dwpq0qyc48s0zdrn3pfa5b4graxcy1i2")))

(define-public crate-sorted-collections-0.0.2 (crate (name "sorted-collections") (vers "0.0.2") (hash "145x94kz7zypq7z9gckmhqsklqvax9jp89wsic9qsz7wws0mz12y")))

(define-public crate-sorted-collections-0.0.3 (crate (name "sorted-collections") (vers "0.0.3") (hash "0gip5pzib9588gayd0c7vb51g837zni43kzd0ps25nc78mvw2fha")))

(define-public crate-sorted-collections-0.0.4 (crate (name "sorted-collections") (vers "0.0.4") (hash "17m01jn1pmy6lmf94hqjc1mb7x6vcgb30djlg94br4j5yig5l70m")))

(define-public crate-sorted-collections-0.0.5 (crate (name "sorted-collections") (vers "0.0.5") (hash "0v9i7gd6gfaiqaiajilb5bvfkha355m8v6nx7x2zazp5bq302762")))

(define-public crate-sorted-collections-0.0.6 (crate (name "sorted-collections") (vers "0.0.6") (hash "15bgf1a92aclg054jmzvarm1bi3vhlapcinq3y3j5zwp3nnm4rw8")))

(define-public crate-sorted-collections-0.0.7 (crate (name "sorted-collections") (vers "0.0.7") (hash "1vs7iaihw2fs3bj9q1fbk1w5c8sk7afvyjm92vswymnm9dmgrnbn")))

(define-public crate-sorted-collections-0.0.8 (crate (name "sorted-collections") (vers "0.0.8") (hash "0n1a2k06k2dgh2gxdixg9iw6an1l01066631fx96k2hvgz9ccc0s")))

(define-public crate-sorted-insert-0.1 (crate (name "sorted-insert") (vers "0.1.0") (hash "0xzk143vcav5axvll2h55dsiwlh187h8la5j715f3db92h65y205") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-sorted-insert-0.2 (crate (name "sorted-insert") (vers "0.2.0") (hash "16rrzk23sdx54z36jz8hxkai888m0jj40bkfwjwcnzhcjsfmbsi3") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-sorted-insert-0.2 (crate (name "sorted-insert") (vers "0.2.1") (hash "1rd3irjqwi9v148k4884rsv5cb71hbr0rh2afl5pl9kr1djbpar5") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-sorted-insert-0.2 (crate (name "sorted-insert") (vers "0.2.2") (hash "1qzhb9jq8l2cmngrpkph4fwdpr4pnsv86a3g8x6zs0xa11nzq7bg") (features (quote (("std") ("default" "std"))))))

(define-public crate-sorted-insert-0.2 (crate (name "sorted-insert") (vers "0.2.3") (hash "0c20ijjpgn0jq30bpy7x3n4dms405by1dmxlagiqlwjasw1j5mjx") (features (quote (("std") ("default" "std"))))))

(define-public crate-sorted-iter-0.1 (crate (name "sorted-iter") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "0rrgc0w8027m5w7w592qq1f93fq8r49cy7iah6g9cpsjdrp4j3f6")))

(define-public crate-sorted-iter-0.1 (crate (name "sorted-iter") (vers "0.1.1") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "0gp9vfr8h1pg41l50r7f3dv9av4cmq496059d1hcf81lsv316ka2")))

(define-public crate-sorted-iter-0.1 (crate (name "sorted-iter") (vers "0.1.2") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "16ifamnlmfyvrj7afqy4mchvccrx0rri93fmlf6nkra8xvrghrh8")))

(define-public crate-sorted-iter-0.1 (crate (name "sorted-iter") (vers "0.1.3") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "152jh2ghf028hcmlnqj26gixj1cw550n5cpm5l1lj02rnr3g9rw3")))

(define-public crate-sorted-iter-0.1 (crate (name "sorted-iter") (vers "0.1.4") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "1fq1ln0j3b7r9ka5vayvqzs05hi6hq6k2rxz9xr4r4c8s8qvacmg")))

(define-public crate-sorted-iter-0.1 (crate (name "sorted-iter") (vers "0.1.5") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "0s8rbjynn1zdykjnn0izkcbsz8hcsvgsmjz0kvva8bml9bn6nrsy")))

(define-public crate-sorted-iter-0.1 (crate (name "sorted-iter") (vers "0.1.6") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "17i23jlsl31mqvc5wn69x2i4f96nkccqc0bbylv9nnpr8wzl5184")))

(define-public crate-sorted-iter-0.1 (crate (name "sorted-iter") (vers "0.1.7") (deps (list (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)))) (hash "12rcihj6jiklfqzvzrbb7vs5sky3ilq3crh08wx821gxsmg17sfb")))

(define-public crate-sorted-iter-0.1 (crate (name "sorted-iter") (vers "0.1.8") (deps (list (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)))) (hash "1pjknlxnjf62wclfqrvlp3a4i2xljiyzn2jrx6si7frl7w2l24wz")))

(define-public crate-sorted-iter-0.1 (crate (name "sorted-iter") (vers "0.1.9") (deps (list (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)))) (hash "1z21jx3wh5zbn8nm69a3rchad16x4ahjghprhwdys7wk0z5a5jxr")))

(define-public crate-sorted-iter-0.1 (crate (name "sorted-iter") (vers "0.1.10") (deps (list (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)))) (hash "02dmg39pzaz8i43r91zi8kyymd1g1a04vvh4dps8slmpfysz2qf3")))

(define-public crate-sorted-iter-0.1 (crate (name "sorted-iter") (vers "0.1.11") (deps (list (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)))) (hash "1xxiw29lid5hpwny4mbcwcps9apcjbxb69sv1zkdlb690zf5gsxw")))

(define-public crate-sorted-json-0.1 (crate (name "sorted-json") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "~0.7.8") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1.0.9") (default-features #t) (kind 0)))) (hash "02ncindg2jdpiiblgzagqckv1k2kba9ik75piwiz68nl7vyxq2z8")))

(define-public crate-sorted-list-0.1 (crate (name "sorted-list") (vers "0.1.0") (hash "19ixw8mff51804r6ai5lbxbyp1yw007v0c3bm8bjlssghnivg9s0") (features (quote (("nightly") ("default"))))))

(define-public crate-sorted-list-0.2 (crate (name "sorted-list") (vers "0.2.0") (hash "1bblyx2h7f9j0n9ab5vragicrsf6przc1wp2ianybpf727l733i5") (features (quote (("nightly") ("default"))))))

(define-public crate-sorted-rotated-0.1 (crate (name "sorted-rotated") (vers "0.1.0") (hash "1vi3aar6lnjc7q2prv12mr0g6gm4lk4wr9bqpk9qnyv5prffz811")))

(define-public crate-sorted-rotated-0.1 (crate (name "sorted-rotated") (vers "0.1.1") (hash "0ja2lsdjlycws7g9wrc6wq5mf590hyj0k8x083ypzwcmdn8dhq8i")))

(define-public crate-sorted-rs-0.1 (crate (name "sorted-rs") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.4") (kind 0)))) (hash "1fxq4sdim3il8hmlz65sl4ichr7as74zq00a19202zpyc9r2s2fy") (features (quote (("use-sse") ("use-avx2") ("std" "num/std") ("default" "use-sse" "std"))))))

(define-public crate-sorted-vec-0.3 (crate (name "sorted-vec") (vers "0.3.1") (deps (list (crate-dep (name "serde") (req "1.*") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0i4mk9wb2rd0var0iplk45n5d0psc1m07qrdd0vvvl2d0d5qy2sn")))

(define-public crate-sorted-vec-0.3 (crate (name "sorted-vec") (vers "0.3.2") (deps (list (crate-dep (name "serde") (req "1.*") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1qlkmqiazba05ll23b0545r665icsys84w06vvwk52g3ampnqrdf")))

(define-public crate-sorted-vec-0.3 (crate (name "sorted-vec") (vers "0.3.3") (deps (list (crate-dep (name "serde") (req "1.*") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1ndsihd6bc2nmvffj9a6klkrjvg7cp89kis057v0p67d603h3nkl")))

(define-public crate-sorted-vec-0.3 (crate (name "sorted-vec") (vers "0.3.4") (deps (list (crate-dep (name "serde") (req "1.*") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "13xw7g89mk18ccl5iqkvv0iia8h1s1dy8amzdpj3bi8gpm23avfc")))

(define-public crate-sorted-vec-0.3 (crate (name "sorted-vec") (vers "0.3.5") (deps (list (crate-dep (name "serde") (req "1.*") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1zkykgvg9bfmfzz2x1zksgmxs1zhnfranr9vq5v4ikqqn5vi0lsf")))

(define-public crate-sorted-vec-0.3 (crate (name "sorted-vec") (vers "0.3.6") (deps (list (crate-dep (name "serde") (req "1.*") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1gzzjn0vyc17jmb8z6ppvn4hwf0irq1lg9g6yhh9726phwx791bf")))

(define-public crate-sorted-vec-0.3 (crate (name "sorted-vec") (vers "0.3.7") (deps (list (crate-dep (name "serde") (req "1.*") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1lrlfy5rz006f4x40pfg63x504zzsjsbyz1zr8v5zm4srmcbxkfk")))

(define-public crate-sorted-vec-0.4 (crate (name "sorted-vec") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "1.*") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "185nrws78zmw2bazrrdqw386l5yjiy36d62xvafg03106rinvksm")))

(define-public crate-sorted-vec-0.4 (crate (name "sorted-vec") (vers "0.4.1") (deps (list (crate-dep (name "serde") (req "1.*") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1pkl33n0f9229v71mya9bbv17ln3b892gzyl83c5vrj4qvyz5arn")))

(define-public crate-sorted-vec-0.5 (crate (name "sorted-vec") (vers "0.5.0") (deps (list (crate-dep (name "serde") (req "1.*") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1gl71qsw2wsdzildrvf9y7sbjdxa59y0s02lz2zmyjz5db89ml5h")))

(define-public crate-sorted-vec-0.5 (crate (name "sorted-vec") (vers "0.5.1") (deps (list (crate-dep (name "serde") (req "1.*") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1i1l0x83yy2xrxghs63wrd65lhhssgldx3z3sdr41asg50qn1c7i")))

(define-public crate-sorted-vec-0.5 (crate (name "sorted-vec") (vers "0.5.2") (deps (list (crate-dep (name "serde") (req "1.*") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0cy2rkzh9ignnqs0v7xzaa81qm7405w9rq7bz6b93vg42rrlc9n0")))

(define-public crate-sorted-vec-0.6 (crate (name "sorted-vec") (vers "0.6.2") (deps (list (crate-dep (name "serde") (req "1.*") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "1.*") (default-features #t) (kind 2)))) (hash "1wl5xxzqbb8g5lgn3hk1jql810sv4gw7w9ywpyyw13fb28knhg30") (features (quote (("serde-transparent" "serde"))))))

(define-public crate-sorted-vec-0.7 (crate (name "sorted-vec") (vers "0.7.0") (deps (list (crate-dep (name "serde") (req "1.*") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "1.*") (default-features #t) (kind 2)))) (hash "1an2z3sm8df7saj6f9q4b49ipckgngiz2fi398fhqnhngnhy2422") (features (quote (("serde-nontransparent" "serde"))))))

(define-public crate-sorted-vec-0.8 (crate (name "sorted-vec") (vers "0.8.0") (deps (list (crate-dep (name "serde") (req "1.*") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "1.*") (default-features #t) (kind 2)))) (hash "0rnk120vdyg4lr81mbs28xjqd6b7xxakfbshkp8a8xs1q8vxp3ns") (features (quote (("serde-nontransparent" "serde"))))))

(define-public crate-sorted-vec-0.8 (crate (name "sorted-vec") (vers "0.8.1") (deps (list (crate-dep (name "serde") (req "1.*") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "1.*") (default-features #t) (kind 2)))) (hash "02h36w5znm8vsimjyakc00a2qlgiwahvdpj9p4czbygah7657f1f") (features (quote (("serde-nontransparent" "serde"))))))

(define-public crate-sorted-vec-0.8 (crate (name "sorted-vec") (vers "0.8.2") (deps (list (crate-dep (name "serde") (req "1.*") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "1.*") (default-features #t) (kind 2)))) (hash "1a1rlnj9kf2w7pb0r9whn7pwi3798x0lp2r2dy33h1k60y17n17h") (features (quote (("serde-nontransparent" "serde"))))))

(define-public crate-sorted-vec-0.8 (crate (name "sorted-vec") (vers "0.8.3") (deps (list (crate-dep (name "serde") (req "1.*") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "1.*") (default-features #t) (kind 2)))) (hash "0n3l8v05b2d7yd1arwylx0a6qb4vncps3k7abvfsslbg1fplqwy6") (features (quote (("serde-nontransparent" "serde"))))))

(define-public crate-sorted-vec2-0.1 (crate (name "sorted-vec2") (vers "0.1.0") (deps (list (crate-dep (name "is_sorted") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.*") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "1.*") (default-features #t) (kind 2)))) (hash "1sfr9s6qzdl9iphj442zb5g99kf5pv3w1krrira2bhbyz5v9547a") (features (quote (("serde-nontransparent" "serde") ("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde" "dep:is_sorted"))))))

(define-public crate-sorted_array-0.1 (crate (name "sorted_array") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "slice_search") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0zbd1qk76im6lak096wdnyhbk5jl7lifpbjm1zqzrkikji0vmgqw") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-sorted_array-0.1 (crate (name "sorted_array") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "slice_search") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1dc7rxfq71bi5cxxxrvq9mqzksqgwqlx2v1h2jqnfqpd2jly5q24") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-sorted_array-0.1 (crate (name "sorted_array") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "slice_search") (req "^0.1") (default-features #t) (kind 0)))) (hash "1m4a24x4whv4q22cvjw97a49qyi7f4wsbs4zbxxcipqws845iml9") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-sorted_array-0.1 (crate (name "sorted_array") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "slice_search") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1ckw1lilyp5iskx308z50yydcb8ldah6blb4x7g1kv0mqgmzn1rz") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-sorted_containers-0.1 (crate (name "sorted_containers") (vers "0.1.0") (hash "1s3rcmamnxp0nadz3q71syhv8q8hskqmpypbhl4xxc8h3bic876h") (features (quote (("default"))))))

(define-public crate-sorted_containers-0.1 (crate (name "sorted_containers") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0q9ih76a3gc4js21ndnl88sqg0p3nmn8wa0qyr1jngs3yw9bhk17") (features (quote (("default"))))))

(define-public crate-sorted_intersection-0.1 (crate (name "sorted_intersection") (vers "0.1.0") (hash "0i2acg2llwq5jd7qys4rvkb8w60g7hwn4vdibkvyv1qj2i17w4bd")))

(define-public crate-sorted_intersection-1 (crate (name "sorted_intersection") (vers "1.0.0") (hash "0kqapml3mgqgf2jgypyms39f97p8s4wwaj7s6chdvxj58dm9kyld")))

(define-public crate-sorted_intersection-1 (crate (name "sorted_intersection") (vers "1.0.1") (hash "10g6dchclk1bg420c7jszby9sbgvbzf7jraz4gdg1vl7y462cd09")))

(define-public crate-sorted_intersection-1 (crate (name "sorted_intersection") (vers "1.0.2") (hash "1h9y9scqrnpja204qg9mpp738q6amxwhwcmg0sf1iil0fm5nd1k2")))

(define-public crate-sorted_intersection-1 (crate (name "sorted_intersection") (vers "1.1.0") (hash "07znyc3cq19112lsrjp55sjcwp6g6c1nic0rlkfjq4w7qspz7svv")))

(define-public crate-sorted_intersection-1 (crate (name "sorted_intersection") (vers "1.2.0") (hash "12x2cmldv4b61kwh9zwzhx8b5lqnl7jzbcf5x0jwabcgr378si23")))

(define-public crate-sorted_locks_derive-0.1 (crate (name "sorted_locks_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "0wld1civhgsrn27aa49410v31d9b6xn48p702804dfyz0b6v6d67")))

(define-public crate-sorted_vector_map-0.1 (crate (name "sorted_vector_map") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 0)))) (hash "0ljxfl7dwzrwcn7gb0hcl26nc90p9q3wvc3nrw55j5skpjh4wkl3")))

(define-public crate-sorted_vector_map-0.2 (crate (name "sorted_vector_map") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 0)))) (hash "0s0k8rs0fhjsg9isg429fpymardm19xpzzllabjdpaxbq947c5nr")))

(define-public crate-sortedcontainers-0.0.1 (crate (name "sortedcontainers") (vers "0.0.1") (deps (list (crate-dep (name "more-asserts") (req "^0.2.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "1800af84bkagzx2aq98r30djsjis2zspn3hlx7r2ldv6sgkfxpwp") (yanked #t)))

(define-public crate-sortedcontainers-0.1 (crate (name "sortedcontainers") (vers "0.1.0-alpha.0") (deps (list (crate-dep (name "more-asserts") (req "^0.2.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0z8r2a3abb5khbczzl1gxkkc788hdk546x5ijygb1zhmhxvhww9c") (yanked #t)))

(define-public crate-sortedcontainers-0.1 (crate (name "sortedcontainers") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "more-asserts") (req "^0.2.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "00bw5xiz24fmxs247w9837wfi3i6qmwhisd412pgnrczkmlmgbj9") (yanked #t)))

(define-public crate-sortedcontainers-0.1 (crate (name "sortedcontainers") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "more-asserts") (req "^0.2.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0dvq19m786x00xlpvk02r5kwazkpw6pw2ffka66lzaamqvj2rgw8")))

(define-public crate-sortedcontainers-0.1 (crate (name "sortedcontainers") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "more-asserts") (req "^0.2.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "16h2zs67wj5v5zgcv2dfp81kdm1i9wdsy9w8ljcxkpf688p9xjza")))

(define-public crate-sortedcontainers-0.2 (crate (name "sortedcontainers") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "more-asserts") (req "^0.2.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "02yfnmhg9kl3zw7bkan62w0hqja7lxvq3mhli3h9q2d4y9yb6pdj")))

(define-public crate-sortedcontainers-0.2 (crate (name "sortedcontainers") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "more-asserts") (req "^0.2.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0hs9si4jy71qjhcv7vf6m42c2jbgbjpzv4kkk7ysdfy5jgrwdgml")))

(define-public crate-sortedcontainers-0.3 (crate (name "sortedcontainers") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "more-asserts") (req "^0.2.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "10pn48v6aaxs6m6dcg9p3n9xavshvcik6cr1xwafbkykd01aldhm")))

(define-public crate-sortedlist-rs-0.1 (crate (name "sortedlist-rs") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "14s0zlpr1ilqq81h2v2v19qx394pzyjgzakncvf4cz3j2gfxs0ck") (yanked #t)))

(define-public crate-sortedlist-rs-0.1 (crate (name "sortedlist-rs") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0jrip195acr0729lmlh9abh5mgwy2bv6kslfa89h6qgpkgbn6g5b") (yanked #t)))

(define-public crate-sortedlist-rs-0.1 (crate (name "sortedlist-rs") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0nmjms6h8rlnym8jscl575a6rc5sg08mhm6xnxfrpl2325nkl6i4") (yanked #t)))

(define-public crate-sortedlist-rs-0.2 (crate (name "sortedlist-rs") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0kf8lqppnnvyxjx3yp816diirsvqv8l1lvxvjbg4l9iq7cq3g3i1")))

(define-public crate-sortedlist-rs-0.1 (crate (name "sortedlist-rs") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0dfb6rapm7ld06lzywc8pr4lzkf7i537yzyh2ffb81awip2zsb95") (yanked #t)))

(define-public crate-sortedlist-rs-0.2 (crate (name "sortedlist-rs") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "1972gqzymlnqk9aaz8p2yp4n5zilfcrqviw6pg8cx30r5c0qmgcl")))

(define-public crate-sortedlist-rs-0.2 (crate (name "sortedlist-rs") (vers "0.2.2") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "1238rs2sjmw9xh2c5pqp4659pvvamz4zz0hjp8gsb3jwpch2fpv4")))

(define-public crate-sortedvec-0.1 (crate (name "sortedvec") (vers "0.1.0") (hash "0c84ca8nlkafa3ax2llrcsjll6105sz7mzlxkyyf4kd931b757y6")))

(define-public crate-sortedvec-0.2 (crate (name "sortedvec") (vers "0.2.0") (hash "06wpwb7bph3hf6vi6qqxflwywsm18zl276ksi7bpf4ji56g2kv1r")))

(define-public crate-sortedvec-0.3 (crate (name "sortedvec") (vers "0.3.0") (hash "17j6qxmxi1cb896i0x0d7nkahqj2m94b1rwr0fp8i8pkxjmz9gkd")))

(define-public crate-sortedvec-0.3 (crate (name "sortedvec") (vers "0.3.1") (hash "1rpdkvlpfci2d6vpw0v0xb55l67fk57whvakia2vzrrnwdsyjlms")))

(define-public crate-sortedvec-0.3 (crate (name "sortedvec") (vers "0.3.2") (hash "0hn4sgzdhbdw9bv8xr6d7im2d1dx9nrvhlbc0p4hagb7ymmrkz9k")))

(define-public crate-sortedvec-0.4 (crate (name "sortedvec") (vers "0.4.0") (hash "0335bnq844lq07ks34pf9i6n8f5ldz3hp58knrnpg4qdlmy72y53")))

(define-public crate-sortedvec-0.4 (crate (name "sortedvec") (vers "0.4.1") (hash "0p197zal84j6b22nl3ll2kzrs7kijjan8ma3sh0dy1gxw74pb714")))

(define-public crate-sortedvec-0.5 (crate (name "sortedvec") (vers "0.5.0") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (features (quote ("small_rng"))) (default-features #t) (kind 2)))) (hash "0nagzj9kfj9wnbfj3q0nxnmql36qmmbihyhnqa3555l7p9347vsv")))

(define-public crate-sorter-0.2 (crate (name "sorter") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "1w7hvgg816lyk1b99r1rsc3d0xpg72rc9xaa73hgr8ay98jdvi45")))

(define-public crate-sorter-0.3 (crate (name "sorter") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "0zkrx080j97r95qxj37wgqiwj7aby4lwl6bqzxlzi22agx2hxlhi")))

(define-public crate-sorter-0.4 (crate (name "sorter") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "1ykilmk5nvh4y8n324gq7cxq1yx3hvjic4dqnm3qjp9wdk9qmi5v")))

(define-public crate-sortery-1 (crate (name "sortery") (vers "1.4.2") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.34.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "1p14ayccmgdrnra6y5pcir3nb9hsngzhpqgjifwaqc0hsb1wmgp3")))

(define-public crate-sorterylib-0.1 (crate (name "sorterylib") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "05892bzqiwdg3c1ym7mrdjiz7mkxi6sjg9hdisaj6sdc5hz8ghin")))

(define-public crate-sorterylib-0.1 (crate (name "sorterylib") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "1dym5sias6id6kv40215yibly96d9i7hb4935211gccyf4mkfhal")))

(define-public crate-sorterylib-0.2 (crate (name "sorterylib") (vers "0.2.2") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "filetime") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "1cibqmgwqpb6siqyp8f26h0qbhr1bha0p59xvvkniywi4i2n1m9l")))

(define-public crate-sorterylib-0.3 (crate (name "sorterylib") (vers "0.3.2") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "filetime") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "0fmjbl2s0iyizfyb82n02pm13is89sn2sgm1cy6g9bhba2dwzn44")))

(define-public crate-sorterylib-0.3 (crate (name "sorterylib") (vers "0.3.3") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "filetime") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "15ybk2gl4k148k315rh765bh3pwpg0syl5ylhasxiq8x1760c8qx")))

(define-public crate-sorth-0.1 (crate (name "sorth") (vers "0.1.0") (hash "0zvibsa80l8vzp4lsdm0jmbvx6ncdgkcrg2k24dg5nc0ai2qp9la")))

(define-public crate-sorth-0.1 (crate (name "sorth") (vers "0.1.1") (hash "0vkfnmigji4pm9f54ffs6x5drzvrc63x09n6dd3s4q0rwynqmqzj")))

(define-public crate-sorth-0.2 (crate (name "sorth") (vers "0.2.0") (hash "003qc2ig0aq3zr8fkrr035jbrg90jblzgpbib8vc0fz2yl2f2lwd")))

(define-public crate-sorth-0.2 (crate (name "sorth") (vers "0.2.1") (hash "0ik2xsck8k7ssfy2j22amwsa28vrw0zspi80j9j7lasyclrl33ap")))

(define-public crate-sorth-0.2 (crate (name "sorth") (vers "0.2.2") (hash "0vwya7xijlbavs3wqznlmr14imj21k8npqv11q0zkkwc6yiknznm")))

(define-public crate-sorting-1 (crate (name "sorting") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0i8rqhahhjkc80kbkylhfqykbzfzikd3kc3n51mvzl6isqg1fwy6")))

(define-public crate-sorting-1 (crate (name "sorting") (vers "1.1.0") (deps (list (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "07v3371n5g9a5k0wj3nqpd474ckdwkg3wvyjzzvxp2agzw1fn89k")))

(define-public crate-sorting-1 (crate (name "sorting") (vers "1.2.0") (deps (list (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "1hz2iw0blxszx4vkkzjr18wwnyqbjgdhkwwf14nlw5grjcw38xm3")))

(define-public crate-sorting-algorithms-0.1 (crate (name "sorting-algorithms") (vers "0.1.0") (hash "1akwwdk0i3vxy4vz6dal7iq0rlg277d04dlfmnwhhb7q6l2h8w27")))

(define-public crate-sorting-vec-0.1 (crate (name "sorting-vec") (vers "0.1.0") (hash "13jnlr1ffn15ppjmmk8y0wdn643jr8bln7i1a4pavqfjsn8mgly6")))

(define-public crate-sorting-vec-0.1 (crate (name "sorting-vec") (vers "0.1.1") (hash "0h9m7ndybijralxi9y4ai8q8b50134b1vn98q4j5w0xf2pw9y2jl")))

(define-public crate-sorting_explorer-0.1 (crate (name "sorting_explorer") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "17bncnzxg7gcjjgjh4h2hmwla92aq1pqga4gyvrrk04gq85whhym")))

(define-public crate-sorting_networks-0.1 (crate (name "sorting_networks") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.4.2") (default-features #t) (kind 1)) (crate-dep (name "syn") (req "^0.12.13") (default-features #t) (kind 1)))) (hash "1diwsamgwnxv76pzvdx2pjzdki52jg0qdx1syv60z158dn5gadax") (features (quote (("std") ("default" "std"))))))

(define-public crate-sorting_rs-1 (crate (name "sorting_rs") (vers "1.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0dj5nwyphwjp79clhq2iaszj629l217zvv52zxhx41h1gsb1r61b")))

(define-public crate-sorting_rs-1 (crate (name "sorting_rs") (vers "1.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "1nksdk16y74ird2gnib3vx1k2947kmxk6jk02pkn1fdh4kx5vd4w")))

(define-public crate-sorting_rs-1 (crate (name "sorting_rs") (vers "1.2.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "04zam1l9689f6vw2j3agvlyibfyww2lzsk9jjwnbp7njd8adavqs")))

(define-public crate-sorting_rs-1 (crate (name "sorting_rs") (vers "1.2.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0zcsik9msr46niyhdyawl68p75cagj1db1wkjbjg0qdagqk38454")))

(define-public crate-sorting_rs-1 (crate (name "sorting_rs") (vers "1.2.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0ps776pqpx242kp2344q1cd7l7wvfvlaic1hy7icjkbyy0fqa7pv")))

(define-public crate-sorting_rs-1 (crate (name "sorting_rs") (vers "1.2.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0hb4yg6snncwjb7blgf45qs867zd8mlrlmb1m3jx9m4xrd1b0vb6")))

(define-public crate-sorting_rs-1 (crate (name "sorting_rs") (vers "1.2.5") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0i6wbh1rhx6lzl0b45bmc1isk5i4src830ld3b9s4zmym4xv9hys")))

(define-public crate-sorting_rs-1 (crate (name "sorting_rs") (vers "1.2.6") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0mm9ixya2qpkk811kq89w225nv29gljjgnpnq10s6vzpbhwzyl2k")))

(define-public crate-sorting_rs-1 (crate (name "sorting_rs") (vers "1.2.7") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0w4rq8zzhk70hka7k5a9gk6g17zswq032bv674sdhd2cavnqzp11")))

(define-public crate-sorting_rs-1 (crate (name "sorting_rs") (vers "1.2.8") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0i69xcl4mlwsa694k17y34z2sd5xivqhxr05izb9cga2j7ahdnc1")))

(define-public crate-sorting_rs-1 (crate (name "sorting_rs") (vers "1.2.9") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "1cjnvrc94n1lynaqpwpqkip2q9gs78nq1jba35xls2639bk7rvb4")))

(define-public crate-sorting_rs-1 (crate (name "sorting_rs") (vers "1.2.10") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "03a7ghrnsjr6wb0nhj6jxq0ggpwv7kbcq4wm9p0k4sr095r6jrrx")))

(define-public crate-sortlock-0.0.0 (crate (name "sortlock") (vers "0.0.0") (hash "0qii2qqcxjlm81b5fik4p7ydkpl7ydi1gbylr6n328r8ywvxzvw4")))

(define-public crate-sortnet-0.1 (crate (name "sortnet") (vers "0.1.0") (deps (list (crate-dep (name "arrayref") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 0)))) (hash "1bgkig1pfbcmqmfrribvylzni1k34cphfz7ncykh9f6p8288k4ly")))

(define-public crate-sortpar-0.1 (crate (name "sortpar") (vers "0.1.0") (deps (list (crate-dep (name "assert_cmd") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "caseless") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32") (features (quote ("color"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "natord") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "slurp") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "version-compare") (req "^0.0") (default-features #t) (kind 0)))) (hash "0nh3i4ly5x52a8lzmskphci3ak9ll89gcs5abcw7xlfx0v7zs9qg")))

(define-public crate-sortpar-0.1 (crate (name "sortpar") (vers "0.1.1") (deps (list (crate-dep (name "assert_cmd") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "caseless") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32") (features (quote ("color"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "natord") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "slurp") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "version-compare") (req "^0.0") (default-features #t) (kind 0)))) (hash "1392qfzpvxa4200lxny7cyyxq05s4rj0s6536hspwhl4d98c6pl2")))

(define-public crate-sortr-0.1 (crate (name "sortr") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0bpkybk0ksimqwwgzqaxs9gnvsab5pcn8yb1wd81kriy5fk8x34b")))

(define-public crate-sortr-0.1 (crate (name "sortr") (vers "0.1.1") (deps (list (crate-dep (name "cfg-if") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (optional #t) (default-features #t) (kind 0)))) (hash "0jx9chjsp7ynya3h6i1q2a4vy87y1wvzn9mychcf51g76fdgk2r9") (features (quote (("merge") ("insertion") ("heap") ("default" "bogo" "bubble" "heap" "insertion" "merge") ("bubble") ("bogo" "rand"))))))

(define-public crate-sortr-0.1 (crate (name "sortr") (vers "0.1.2") (deps (list (crate-dep (name "cfg-if") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (optional #t) (default-features #t) (kind 0)))) (hash "10s3kj8hb9w849yh8fi2gl263dzki3dh1ngv1f8n9w74qwkh6ig1") (features (quote (("merge") ("insertion") ("heap") ("default" "bogo" "bubble" "heap" "insertion" "merge") ("bubble") ("bogo" "rand"))))))

(define-public crate-sortrs-0.0.1 (crate (name "sortrs") (vers "0.0.1") (hash "0fzbx3bjplr70d3bbvwp0s980ains46wlmrssvmlfhsjpys6v3bb")))

(define-public crate-sortrs-0.0.2 (crate (name "sortrs") (vers "0.0.2") (hash "1ibkvz61b8ybya4968djnxhkbgdwiz3srjcncgjp4q245v2693n3")))

(define-public crate-sortrs-0.0.3 (crate (name "sortrs") (vers "0.0.3") (hash "10qqffa40sa0ivr3anq2cyi4r0v25fyy1aakc5nzl989kfpq4466")))

(define-public crate-sortrs-0.0.4 (crate (name "sortrs") (vers "0.0.4") (deps (list (crate-dep (name "rand") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "1fp37yhlx528kzbhddlha4x53ds22fwnf38q17b5v035sw3xq2fg")))

(define-public crate-sortrs-0.0.5 (crate (name "sortrs") (vers "0.0.5") (deps (list (crate-dep (name "num") (req "^0.1.28") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.12") (default-features #t) (kind 2)))) (hash "06dlfv9m7sfr365dzks45yw6mi0hff7b8acrap2hla0q58qr3dhg")))

(define-public crate-sorts-0.1 (crate (name "sorts") (vers "0.1.0") (hash "0sya7h1m7sl6ljxi0jgc8r0blbijvz3lv9apmvzs3wyrpxisdnfx")))

(define-public crate-sorts-0.2 (crate (name "sorts") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)))) (hash "1zn4iwn6y9yy485dr7x0kslazzidchcy35gp8mdnj3ljcpdxfvp7")))

(define-public crate-sorts-0.3 (crate (name "sorts") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)))) (hash "13sh807ysrnjyiq0ad56i5w8c25860xdif4d2ayzgjpfig29wrgy")))

(define-public crate-sorts-0.4 (crate (name "sorts") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "08yxnpm85g38b65x192qzw7vnyr23vjgjn0pyym4g3gwh8slvzgn")))

(define-public crate-sorts-0.5 (crate (name "sorts") (vers "0.5.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "0hwphf7zgnar5y6xm8bmw4a9zbm0jnmd54gh0idw98xi9lsnfjdw")))

(define-public crate-sorts-0.6 (crate (name "sorts") (vers "0.6.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "1x32b75hfy10c3w1ngd00gf6ljc66nprwkpihnp3pafnr75michw")))

(define-public crate-sorts-0.6 (crate (name "sorts") (vers "0.6.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "1f5g7njgyg1wsashpi73rv6h2sagc3p8sc2qfv39ddvs0i311sxa")))

(define-public crate-sortuniq-0.1 (crate (name "sortuniq") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2") (kind 0)) (crate-dep (name "lru") (req "^0.1") (default-features #t) (kind 0)))) (hash "1dablkay4vl06cr6qkm5ifdz37jxmj7qp5p8p2kffl1282ac4q2q")))

(define-public crate-sortuniq-0.2 (crate (name "sortuniq") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.9") (default-features #t) (kind 0)))) (hash "1b1slga13w7lkxywki85m9n2hbmjbzj3aifjhvrii8hfgy0yaqjm")))

(define-public crate-sorty-0.1 (crate (name "sorty") (vers "0.1.0") (hash "0c50wnn59ivipny5sy4zjjmmbxplkqg4sn71p55fpjsxm4fw07d0")))

(define-public crate-sorty-0.1 (crate (name "sorty") (vers "0.1.1") (hash "0k76q089l4rspja9biy7cagxhdh8iv2j88dxvm2630qnq0vm22ak")))

(define-public crate-sorty-0.1 (crate (name "sorty") (vers "0.1.2") (hash "11hai4cly8hxgjvrv6j811z50jayjbn3k65cp8r2yji6ys6rcycp")))

