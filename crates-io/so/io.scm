(define-module (crates-io so io) #:use-module (crates-io))

(define-public crate-soio-0.1 (crate (name "soio") (vers "0.1.0") (hash "0h0q5yqhvlvhkhs6l1nyjx59vd7pan7i2x50w2rrxm62yba1hpb5")))

(define-public crate-soio-0.1 (crate (name "soio") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "net2") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "slab") (req "^0.3") (default-features #t) (kind 0)))) (hash "1pkzg33ql23s7fbhwfgvp9iic0svf221pgqm58fwhgp73b1fvd7k")))

(define-public crate-soio-0.1 (crate (name "soio") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "net2") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "slab") (req "^0.3") (default-features #t) (kind 0)))) (hash "1nm7d1k6gj9d2zvj2yiycdbcshnb9lrsf0cha1sw05fyqb5m9gg8")))

(define-public crate-soio-0.1 (crate (name "soio") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "net2") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "slab") (req "^0.3") (default-features #t) (kind 0)))) (hash "1pai26awg2zfn63clyibvm4fmarkg1bsa9kchmx3n1sp0ad5gfcx")))

(define-public crate-soio-0.1 (crate (name "soio") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "net2") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "slab") (req "^0.3") (default-features #t) (kind 0)))) (hash "0395zj309vl0dz3xk802909a4bk68j53wh0wnn4r2r56iycw84mj")))

(define-public crate-soio-0.1 (crate (name "soio") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "net2") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "slab") (req "^0.3") (default-features #t) (kind 0)))) (hash "1q9qncslazvj7042r2a52ssjassic95kdsc5yx86qzc70xv7adyi")))

(define-public crate-soio-0.1 (crate (name "soio") (vers "0.1.6") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "net2") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "slab") (req "^0.3") (default-features #t) (kind 0)))) (hash "08gkin2rzmk5yhljykphxr51bplvrp4x8yfyacljh9ia51cr8xb6")))

(define-public crate-soio-0.1 (crate (name "soio") (vers "0.1.7") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "net2") (req "^0.2") (default-features #t) (kind 0)))) (hash "1gb1bnr3zcz59ad4zbgx08xsr3734cl9ykbghfg00jfd91n0j1f4")))

(define-public crate-soio-0.1 (crate (name "soio") (vers "0.1.8") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "136dszddlv7svsh0a1g081a36ry629f0qphmxwsj7vb5argqsik1")))

(define-public crate-soio-0.1 (crate (name "soio") (vers "0.1.9") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1i0l5112rckgsk24z7rmq8z7czlp7x2q2608vnc7s9ch3c2pds1f")))

(define-public crate-soio-0.2 (crate (name "soio") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1a2ff6mrg6lyh8s37g54qlpc2n19sxyb1r0dnnx1yvfhlcs5z04b")))

(define-public crate-soio-0.2 (crate (name "soio") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1a7xnvkbgac7r4ay1whvqa3l23b91lkcycpaacrk9anqhwds17xj")))

(define-public crate-soio-0.2 (crate (name "soio") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1prmw4279ppb14hm54hrvims6llfgwm2bmaypi19a93zn57zyp3k")))

(define-public crate-soio-0.2 (crate (name "soio") (vers "0.2.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "07kx40byxq9aaj472kc24235lbsk7a7386n4nyp6r8pbk3gq5928")))

