(define-module (crates-io so ca) #:use-module (crates-io))

(define-public crate-socarel-0.4 (crate (name "socarel") (vers "0.4.0") (hash "0a3789j6vqlqlx6zf071wi8krlh9189x7xay0y4sygrqg1rzdv80")))

(define-public crate-socat-0.0.0 (crate (name "socat") (vers "0.0.0") (hash "0jry6bdi8d77b6f5aiwjf0hh8ivniblj96iqr4v1mq1dgk04l5a5")))

