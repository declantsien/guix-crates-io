(define-module (crates-io so o_) #:use-module (crates-io))

(define-public crate-soo_macro-0.0.0 (crate (name "soo_macro") (vers "0.0.0") (hash "105iy0qx7754km6lmys7lq0amjv8vdiwk2dvklj5v6k56yb60f0b")))

(define-public crate-soo_macro_impl-0.0.0 (crate (name "soo_macro_impl") (vers "0.0.0") (hash "04cxygbcrx0g5fa99qazffkn7p0v7wcbchq0xkifnf2mnh7qgaqz")))

