(define-module (crates-io so nd) #:use-module (crates-io))

(define-public crate-sonde-0.1 (crate (name "sonde") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 0)))) (hash "1hqn8vjdjvppabydsv2nbl0yc5fdjz38ww3lkw7vwpc7mgpgrcdq")))

(define-public crate-sonde-0.1 (crate (name "sonde") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 0)))) (hash "1pvg7na8xlx252lic3xvzdd6x46qjnyr3rndgi3rf35db966mgx8")))

