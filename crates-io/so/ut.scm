(define-module (crates-io so ut) #:use-module (crates-io))

(define-public crate-southbound-0.0.1 (crate (name "southbound") (vers "0.0.1") (hash "0vjsvdlc4agmq47pqf13srh23jd052d5c7011b40xh7zyp743vzd")))

(define-public crate-southbound-cli-0.0.1 (crate (name "southbound-cli") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3") (default-features #t) (kind 0)))) (hash "15lyjgjgf8j2y40c983hrcqwz3mggz8gkz3vpi2blm0x4nscrar6")))

