(define-module (crates-io so a_) #:use-module (crates-io))

(define-public crate-soa_derive-0.1 (crate (name "soa_derive") (vers "0.1.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.10") (default-features #t) (kind 0)))) (hash "1g51d52bxilpvzlamv8jc69hahd1pxr88cn1x5mpb0dvn6d64vka")))

(define-public crate-soa_derive-0.1 (crate (name "soa_derive") (vers "0.1.1") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.10") (default-features #t) (kind 0)))) (hash "038dmgpdaa7gxck1zh2x2w1546rl880amysba375w2h54yf8d3g8")))

(define-public crate-soa_derive-0.2 (crate (name "soa_derive") (vers "0.2.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.10") (default-features #t) (kind 0)))) (hash "0dxarsqjyxvrrd4wadx4zkv2zdy36cc8fjdnvz2wn2gbahzd6rsr")))

(define-public crate-soa_derive-0.3 (crate (name "soa_derive") (vers "0.3.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.10") (default-features #t) (kind 0)))) (hash "1iqfdh3xx0a7ajr1qcnpihkpwic86vpxik1apv7dv3x5zaa909pr")))

(define-public crate-soa_derive-0.3 (crate (name "soa_derive") (vers "0.3.1") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.10") (default-features #t) (kind 0)))) (hash "0vbwyfxfbvcgglx191q0nxg65g3n435r4cx5a2vz8qhck7dxpq70")))

(define-public crate-soa_derive-0.4 (crate (name "soa_derive") (vers "0.4.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "case") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.10") (default-features #t) (kind 0)))) (hash "1x3bqp5b9ffbxixi2y4pyhhrff016l3ccm6mvj02wz5zhi9qwk7d")))

(define-public crate-soa_derive-0.4 (crate (name "soa_derive") (vers "0.4.1") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "case") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.10") (default-features #t) (kind 0)))) (hash "133l3x78aq0pcc00s13acsi4dx471kz4xr9y0ybqdpf79axk86g9")))

(define-public crate-soa_derive-0.5 (crate (name "soa_derive") (vers "0.5.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "soa_derive_internal") (req "^0.5") (default-features #t) (kind 0)))) (hash "0l2vpbx9l2ra5mqs62vijaj5g2xdhb6cpihnhwcn2krz341dyf2j")))

(define-public crate-soa_derive-0.6 (crate (name "soa_derive") (vers "0.6.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "soa_derive_internal") (req "^0.5") (default-features #t) (kind 0)))) (hash "0wcwryyynrifnpxn0wx6kcsb1dclhn6ivfwdzimrcdkx7bskqn3j")))

(define-public crate-soa_derive-0.6 (crate (name "soa_derive") (vers "0.6.1") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "soa_derive_internal") (req "^0.5") (default-features #t) (kind 0)))) (hash "040yl4bq1x4dzmsjyq3j5w65f81dnsakad45kfvfj3pmxf7s1204")))

(define-public crate-soa_derive-0.6 (crate (name "soa_derive") (vers "0.6.2") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "soa_derive_internal") (req "^0.5") (default-features #t) (kind 0)))) (hash "1dnmv0qhdglkap7bcvr5pfy7k4v01i7cksr7vzmk6n29ddk1alvn")))

(define-public crate-soa_derive-0.7 (crate (name "soa_derive") (vers "0.7.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "soa_derive_internal") (req "^0.6") (default-features #t) (kind 0)))) (hash "0l87xpp5lpiknn9wq9shvfff1xl5ldzibvbzhbd3lf0amypkh3cr")))

(define-public crate-soa_derive-0.8 (crate (name "soa_derive") (vers "0.8.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "soa_derive_internal") (req "^0.8") (default-features #t) (kind 0)))) (hash "1xlcjhbchf8mjhwfjpnv444anrwxs7d035kfj5qnp0z9v8b0n8lz")))

(define-public crate-soa_derive-0.8 (crate (name "soa_derive") (vers "0.8.1") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "soa_derive_internal") (req "^0.9") (default-features #t) (kind 0)))) (hash "0gmzai3kpr8bh6bv1m4c3w4440vhd7xqgs4wh0smc5ishxlsgapb")))

(define-public crate-soa_derive-0.10 (crate (name "soa_derive") (vers "0.10.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "soa_derive_internal") (req "^0.10") (default-features #t) (kind 0)))) (hash "1xshqh1bzyby9plzfkidz98a6mpf2a43fx9w0jsapci918zjcc4m")))

(define-public crate-soa_derive-0.11 (crate (name "soa_derive") (vers "0.11.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "soa_derive_internal") (req "^0.11") (default-features #t) (kind 0)))) (hash "1s0cipsk7wls1pc9g68wg0w624xr4im7wg69q0rc64hicdrx6vc0")))

(define-public crate-soa_derive-0.12 (crate (name "soa_derive") (vers "0.12.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "soa_derive_internal") (req "^0.12") (default-features #t) (kind 0)))) (hash "08lkwgxyjr14sadcxs60fgi1l62dd2gv245ahq1rw6naaj1qjyrj")))

(define-public crate-soa_derive-0.13 (crate (name "soa_derive") (vers "0.13.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "permutation") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "soa_derive_internal") (req "^0.13") (default-features #t) (kind 0)))) (hash "0mxs57jf220rmwq7aa4n65ydbqrkwv6arpklhsm6nzaybfg1aqpy") (rust-version "1.63")))

(define-public crate-soa_derive_internal-0.5 (crate (name "soa_derive_internal") (vers "0.5.0") (deps (list (crate-dep (name "case") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.10") (default-features #t) (kind 0)))) (hash "1kixnln97pkdphig85cpniijpl7ivxpx545ixsyi3qa8hbswyq3z")))

(define-public crate-soa_derive_internal-0.5 (crate (name "soa_derive_internal") (vers "0.5.1") (deps (list (crate-dep (name "quote") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.13") (default-features #t) (kind 0)))) (hash "1dqfvksq8npqsbap52cccvbyg0n05ri1kv14lavaqhnys8b4lqql")))

(define-public crate-soa_derive_internal-0.5 (crate (name "soa_derive_internal") (vers "0.5.2") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14") (default-features #t) (kind 0)))) (hash "1vnjma3dkksjbygvx9jhpmqv2j83plrkphs36rvl7v8yqhw0ssa6")))

(define-public crate-soa_derive_internal-0.6 (crate (name "soa_derive_internal") (vers "0.6.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14") (default-features #t) (kind 0)))) (hash "1v0dv1x3ns7qijh63yxnqma4hrfqgji39qz8wjfs8bgbmcxp6973")))

(define-public crate-soa_derive_internal-0.6 (crate (name "soa_derive_internal") (vers "0.6.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14") (default-features #t) (kind 0)))) (hash "1h1f9hq45hani7wmgahqvnxcc32b9jqhabl0r7f6h26w0wmwkqmm")))

(define-public crate-soa_derive_internal-0.6 (crate (name "soa_derive_internal") (vers "0.6.2") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1y8jlp3pk7lkdanrlgzvfznv53pm12cy3clwmb3yr4gzb0g6gabb")))

(define-public crate-soa_derive_internal-0.6 (crate (name "soa_derive_internal") (vers "0.6.3") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "04yw75c43lm48rlgc3wmgjfyznmirn0xv7jrbj5nna3hk3hk3yqc")))

(define-public crate-soa_derive_internal-0.8 (crate (name "soa_derive_internal") (vers "0.8.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "002b41q6n6ifbbkpdkl62ciyr74i42g07pcg21bh169mx2m4xlbq")))

(define-public crate-soa_derive_internal-0.9 (crate (name "soa_derive_internal") (vers "0.9.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0pnkm8c047s56inhj91mbahfbqysk8g3cg04yc4c90q83lkrspyd")))

(define-public crate-soa_derive_internal-0.9 (crate (name "soa_derive_internal") (vers "0.9.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1s8vcd0p240cvyayg6g6wnr8yxhw5wrvn842g7667hkck7jph5ij")))

(define-public crate-soa_derive_internal-0.9 (crate (name "soa_derive_internal") (vers "0.9.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1mrvb0yz4a1jd114hjw8s1xsjzvpjvmx8b6a28qggrkpjnm0kvwy") (yanked #t)))

(define-public crate-soa_derive_internal-0.10 (crate (name "soa_derive_internal") (vers "0.10.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "03x7mrdhqzakhy5802ylazyf3yyjwag9sv0znd5iamhnw129mp02")))

(define-public crate-soa_derive_internal-0.10 (crate (name "soa_derive_internal") (vers "0.10.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1damgwwamd0y1h52rckg8zmjnvlpsm4s2bgq8syrb7rcsdmq0ix5")))

(define-public crate-soa_derive_internal-0.11 (crate (name "soa_derive_internal") (vers "0.11.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "01qfljh232gcy3j7rh6rl964jmsl8bs2706nghk6wfsbhhc1liay")))

(define-public crate-soa_derive_internal-0.11 (crate (name "soa_derive_internal") (vers "0.11.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1zr786ywisay9z1g8x3g13xmnkvds8cwsh79g66np3zr6lq9q15q") (yanked #t)))

(define-public crate-soa_derive_internal-0.12 (crate (name "soa_derive_internal") (vers "0.12.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1a9man7qqyxnqbj9055pvr91d9ajd0x4dyjlcwyp6mjiscysmjvv")))

(define-public crate-soa_derive_internal-0.13 (crate (name "soa_derive_internal") (vers "0.13.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("derive" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1ffvaa2d49w59dipsniw2kfmgwzarxdjfyjlskbjxabisadfyr0g") (rust-version "1.63")))

(define-public crate-soa_derive_internal-0.13 (crate (name "soa_derive_internal") (vers "0.13.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("derive" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1p2jhkx893mv4af50qg33g8pshlwg3sr9xz4yb85ancq7g2gcpdd") (rust-version "1.63")))

(define-public crate-soa_derive_internal-0.13 (crate (name "soa_derive_internal") (vers "0.13.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("derive" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0y1nlc9fka3f8j5z0lrv4w5nq53smn7asnsmf557014f2my9pwm5") (rust-version "1.63")))

