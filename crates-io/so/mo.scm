(define-module (crates-io so mo) #:use-module (crates-io))

(define-public crate-somo-0.1 (crate (name "somo") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "inquire") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "procfs") (req "^0.15.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.18") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)) (crate-dep (name "termimad") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "0k9r7aygqb1v20kpslwclcb5b605nqs79abp6kfzs0qavzfvxj9g")))

(define-public crate-somo-0.1 (crate (name "somo") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "inquire") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "procfs") (req "^0.15.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.18") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)) (crate-dep (name "termimad") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1zwv9xnzcijmzjf4hb1bgivn1gya8xh1l9py5slid6yv9as6b9y2")))

(define-public crate-somo-0.1 (crate (name "somo") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "inquire") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "procfs") (req "^0.15.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.18") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)) (crate-dep (name "termimad") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "064w96i6x2qc36a3rinm0v8i1lpgik4az7mm6wj6xwrx2h3wlcj4")))

(define-public crate-somo-0.2 (crate (name "somo") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "inquire") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "procfs") (req "^0.15.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.18") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)) (crate-dep (name "termimad") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0jmjgb6w9ahncjav9mbp1hc38abbgql8wfncmwdayky46a8bwnpb")))

(define-public crate-somok-1 (crate (name "somok") (vers "1.0.0") (hash "069ndx49vgil1bkglgr3rkjdbwfnbf47f0bj31b5b7in1did2nr5")))

(define-public crate-somok-1 (crate (name "somok") (vers "1.1.0") (hash "1s1474bdk9wxla9wvlpaay0qgff66bi6p4p3b7npyl4xqvxg6ysb")))

(define-public crate-somok-1 (crate (name "somok") (vers "1.1.1") (hash "1mnfqb64ayh31bx8l5bz7rsv1ki6gm9kfnpdg63iryks526r2dng")))

(define-public crate-somok-1 (crate (name "somok") (vers "1.1.2") (hash "1ylkilwd81dgxc57yi890lj6vx0dhg69sxhj8hx7hllpwcys07fd")))

(define-public crate-somok-1 (crate (name "somok") (vers "1.1.3") (hash "10mq7q73nggrvp4jk5sdpqlb28d6ra3z22h31148wnhlvl05a8f3")))

(define-public crate-somok-1 (crate (name "somok") (vers "1.1.4") (hash "07pbrsawi2p4nq0nn7jmvjbi8523xg4cgb0dd0c2fchl4rbxnjc6")))

(define-public crate-somok-1 (crate (name "somok") (vers "1.2.0") (hash "11zr2wqj3g72hd85s63zbjzci5nnik32gqlhk45gpizi2ywlr4rj")))

(define-public crate-somok-1 (crate (name "somok") (vers "1.3.0") (hash "0zgxcds6w2w9w7zqwssjdyg151i811qslsfznx22brx2lsk937li")))

(define-public crate-somok-1 (crate (name "somok") (vers "1.4.0") (hash "086shcn1b97rj7kpzf34zab4rmr8jwyifdr0sjljswzy35r99lg6")))

(define-public crate-somok-1 (crate (name "somok") (vers "1.5.0") (hash "1zzial9kkgk2nxlypns3qqdzmbd5gs56x7x8gcwbz50qz4pjcqf7")))

