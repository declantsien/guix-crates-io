(define-module (crates-io so n_) #:use-module (crates-io))

(define-public crate-son_of_grid_engine-0.0.1 (crate (name "son_of_grid_engine") (vers "0.0.1") (deps (list (crate-dep (name "num_cpus") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "0iwhn1m4fg1s3sidk6rxzwdrmih83a2n65vflvr5bhbc06dwwba0")))

(define-public crate-son_of_grid_engine-0.1 (crate (name "son_of_grid_engine") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.7") (default-features #t) (kind 0)))) (hash "00wc05s56bq6p7awlmh8svfdljw5zkrxfinz6b5agh339fdk4s3f")))

(define-public crate-son_of_grid_engine-0.2 (crate (name "son_of_grid_engine") (vers "0.2.0") (deps (list (crate-dep (name "file") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "threadpool") (req "^1.7") (default-features #t) (kind 0)))) (hash "0p8fs36m4j60n5gk0hz12jr6s7062w9jxnjh8852cb0xlhwymdly")))

