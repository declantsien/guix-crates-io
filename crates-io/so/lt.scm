(define-module (crates-io so lt) #:use-module (crates-io))

(define-public crate-soltrack-1 (crate (name "soltrack") (vers "1.0.0") (deps (list (crate-dep (name "bs58") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "solana-client") (req "^1.8.5") (default-features #t) (kind 0)) (crate-dep (name "solana-sdk") (req "^1.8.5") (default-features #t) (kind 0)))) (hash "1npy9x1239x9dzlw9zw148za6zd6l1ik7l370bnsgk6f5n4qq0hm")))

(define-public crate-solts-0.1 (crate (name "solts") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "07nmqmklnisajdsgxwv73wwvvyxfkbhj7zgql30zfi490rfcybzs")))

(define-public crate-solts-0.1 (crate (name "solts") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "1g6x9jj61jkvm8rqaznbippbsby4d3p10fv0pcpyq84apj3j2fjn")))

