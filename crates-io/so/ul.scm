(define-module (crates-io so ul) #:use-module (crates-io))

(define-public crate-soul-0.0.0 (crate (name "soul") (vers "0.0.0") (hash "00djychaqkisr5rran9wsnhg3hjds65afygx6wqk3baa4iilgwgc")))

(define-public crate-soulbound-0.0.0 (crate (name "soulbound") (vers "0.0.0") (hash "0xqmwz3kqnniwzyyjgllfjqcvyl4yj215pvpk3rd2awli4d8wwj2")))

(define-public crate-soulboundsigner-0.1 (crate (name "soulboundsigner") (vers "0.1.0") (deps (list (crate-dep (name "anchor-lang") (req ">=0.22, <=0.24") (default-features #t) (kind 0)) (crate-dep (name "anchor-spl") (req ">=0.22, <=0.24") (default-features #t) (kind 0)) (crate-dep (name "vipers") (req "^2.0") (default-features #t) (kind 0)))) (hash "0fipyk9ccmnsizv6q38qh9zz1jbk4hzfcgb5gl9a3gyilvmp0fzj") (features (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-soulog-1 (crate (name "soulog") (vers "1.0.0") (hash "1206rnnm8041n2ya6l38pmanisrmsqh0sa3v8l2bavxyi0s1hx46") (yanked #t)))

(define-public crate-soulog-1 (crate (name "soulog") (vers "1.0.1") (hash "0a9f29ra0ijwlpzzhbhklfrkhp2jhbdy9q88hljilmc68s57p44w") (yanked #t)))

(define-public crate-soulog-1 (crate (name "soulog") (vers "1.0.2") (hash "106pzzyf24gjyi73gg8bagxh45cmnlvfls40xn077zd0150ik9in") (yanked #t)))

(define-public crate-soulog-1 (crate (name "soulog") (vers "1.0.3") (hash "10mzg0lmp5p2glfdzyyki833g83lhybjgr2k4i2gh4gd7lm1ib9g")))

(define-public crate-soulog-1 (crate (name "soulog") (vers "1.0.4") (hash "0v0w7y4rvwzw45v9dyc9pl52drx5fpi4whmwqqh9510rxj83m8yx")))

(define-public crate-soulog-1 (crate (name "soulog") (vers "1.1.0") (hash "0k9qib2gn6y5xm6hc0rcdcx8wldg94gy7iip0xlr0mdizdmwv34z")))

(define-public crate-soulog-1 (crate (name "soulog") (vers "1.2.0") (hash "0bkzbvpzhgxjmlgivx2q2krcz6qwn388hwjllimmc4427xi559nw")))

(define-public crate-soulog-1 (crate (name "soulog") (vers "1.2.1") (hash "1grv976lzasw30244xckaylymcqglqc1qhm8yyb3hjfz0r9p2rya")))

(define-public crate-soulseek-0.1 (crate (name "soulseek") (vers "0.1.0") (hash "03yrhn7fysyaj41y288xv4fqv02rx96n3zza4g5l5xi8c54p10xs")))

(define-public crate-soulxtech-0.1 (crate (name "soulxtech") (vers "0.1.0") (hash "07kp4c02ym4pbb08k1fysf2sb9d7cklrjda54lpa3km4r7iz1kdw") (yanked #t)))

