(define-module (crates-io so ne) #:use-module (crates-io))

(define-public crate-sonet-rs-1 (crate (name "sonet-rs") (vers "1.0.0-beta") (deps (list (crate-dep (name "futures") (req "^0.3.19") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.13.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1mnqi9dpfkncpa706m5d3jqr1z1jrdjgvscv88ihvlfsgf23cmmb")))

(define-public crate-sonet-rs-1 (crate (name "sonet-rs") (vers "1.0.0-RC") (deps (list (crate-dep (name "futures") (req "^0.3.19") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.13.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "10c4ldcilqr2ic90jfnyqqiwy8d6irbqxwr4ig5r9appr8p6lxl4")))

(define-public crate-sonet-rs-1 (crate (name "sonet-rs") (vers "1.0.0") (deps (list (crate-dep (name "futures") (req "^0.3.19") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.13.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0r9ks6z2prrp7zcf54b0npndvdvmaikklni13b54d1xf6j8js7gc")))

