(define-module (crates-io so rb) #:use-module (crates-io))

(define-public crate-sorbet-0.0.0 (crate (name "sorbet") (vers "0.0.0") (hash "1if4w1h1hvri34yp4xackz4qhpi2dkvp16d4hklq9ngwpk0n9bhf")))

(define-public crate-sorbet-color-0.0.1 (crate (name "sorbet-color") (vers "0.0.1") (deps (list (crate-dep (name "once_cell") (req "^1.9") (default-features #t) (kind 2)))) (hash "0i592m2dhavssbzb3llf9v0ivajgvks9r3a7aqnlf0ddkc0bbzzi")))

(define-public crate-sorbet-color-0.0.2 (crate (name "sorbet-color") (vers "0.0.2") (deps (list (crate-dep (name "once_cell") (req "^1.9") (default-features #t) (kind 2)))) (hash "0zhmihq2bl80m69mz8rvgqwh59bwsgv5ck29vjs276v3gx9a3g3i")))

(define-public crate-sorbet-color-0.1 (crate (name "sorbet-color") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.9") (default-features #t) (kind 2)) (crate-dep (name "strum") (req "^0.24") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.12") (optional #t) (default-features #t) (kind 0)))) (hash "05xqpg0rdjqmjpqhb089mfp2m7vn7hly37w8a6ck87z6zcg18h2j")))

(define-public crate-sorbet-color-0.1 (crate (name "sorbet-color") (vers "0.1.1") (deps (list (crate-dep (name "once_cell") (req "^1.9") (default-features #t) (kind 2)) (crate-dep (name "strum") (req "^0.24") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.12") (optional #t) (default-features #t) (kind 0)))) (hash "1q2178z9mrn9bf3zfakkcxxpw5m91ylp3mdij8a47y1cg37wsg4w")))

(define-public crate-sorbet-color-0.2 (crate (name "sorbet-color") (vers "0.2.0") (deps (list (crate-dep (name "once_cell") (req "^1.9") (default-features #t) (kind 2)) (crate-dep (name "strum") (req "^0.24") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.12") (optional #t) (default-features #t) (kind 0)))) (hash "0fdf1prf1cgihjhvxfivw0axc9ydlzmhysdnghr5js3bh6ncilzj")))

(define-public crate-sorbet-color-0.3 (crate (name "sorbet-color") (vers "0.3.0") (deps (list (crate-dep (name "once_cell") (req "^1.9") (default-features #t) (kind 2)) (crate-dep (name "strum") (req "^0.24") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.12") (optional #t) (default-features #t) (kind 0)))) (hash "18460iwa5mapy6rjr6b9pdbzcm4lxnp2lpw2503ak7wkyzg6xqqr")))

(define-public crate-sorbet-color-0.3 (crate (name "sorbet-color") (vers "0.3.1") (deps (list (crate-dep (name "once_cell") (req "^1.10") (default-features #t) (kind 2)) (crate-dep (name "strum") (req "^0.24") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.12") (optional #t) (default-features #t) (kind 0)))) (hash "1yb39h7yacz7amdc7870z1wgvvvsavca69mg1nxyngdclg5jr9c4")))

(define-public crate-sorbus-0.5 (crate (name "sorbus") (vers "0.5.0") (hash "0dlcbdqn9qf8kmc3n7308fql0avwxf7ll9snaz4pzb0a4mk7hjg1")))

(define-public crate-sorbus-0.7 (crate (name "sorbus") (vers "0.7.0") (hash "1mj43l47vdn16yvz1frffhqqr4za6fmnn2i7mbh108jm4x177k44")))

(define-public crate-sorbus-0.8 (crate (name "sorbus") (vers "0.8.0") (hash "0hhv7fj136vi514c30qiy2r8gbr3hshpm3dqfdwibjsxgxvzcnkw")))

(define-public crate-sorbus-0.8 (crate (name "sorbus") (vers "0.8.1") (deps (list (crate-dep (name "identity-hash") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "17hyrisvgpczm1ffmzlsvmpysnig9ldv6ah5vmgf1k7dr99gf5dy")))

(define-public crate-sorbus-0.9 (crate (name "sorbus") (vers "0.9.0") (deps (list (crate-dep (name "identity-hash") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "13iz8a9lfi9dvfpbxplb7vgiv235brhafgbqdb93pbl17w042hp3")))

(define-public crate-sorbus-0.10 (crate (name "sorbus") (vers "0.10.0") (deps (list (crate-dep (name "identity-hash") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0w09hc0si0b4q7ncdhwxhd5msnq8bqrjims5njv6ab1l2l5sddy4")))

(define-public crate-sorbus-0.11 (crate (name "sorbus") (vers "0.11.0") (deps (list (crate-dep (name "identity-hash") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "05hd0zq79ynmxv7k3jpr29apmrqd4gxmynwbj65wqbrgvcsr9xqy")))

(define-public crate-sorbus-0.12 (crate (name "sorbus") (vers "0.12.0") (deps (list (crate-dep (name "identity-hash") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1y79vr3cqsiim6siqfq8ij8dz3w2xcdwlz5dpkzjwigaph12k8nl")))

(define-public crate-sorbus-0.12 (crate (name "sorbus") (vers "0.12.1") (deps (list (crate-dep (name "identity-hash") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1lhrzk71clp709l62n680gb0g5y78b7bliyw5imf1w76rhbywbs9")))

