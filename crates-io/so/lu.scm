(define-module (crates-io so lu) #:use-module (crates-io))

(define-public crate-solute-0.1 (crate (name "solute") (vers "0.1.0") (deps (list (crate-dep (name "draw") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "0v5w6izpiwm9ba9qrk0gb79fbwk19fgwf9lk6yyyk2c8sba5jpvp")))

(define-public crate-solution-0.1 (crate (name "solution") (vers "0.1.0") (hash "01xxb446g0mrxwwqfpkmgh00x56v1lhbijarkqin40jp7k5qyd4g")))

