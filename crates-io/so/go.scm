(define-module (crates-io so go) #:use-module (crates-io))

(define-public crate-sogouda-0.1 (crate (name "sogouda") (vers "0.1.0-alpha") (deps (list (crate-dep (name "web-view") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0xm539fdi3ysdh1ir8q1vh4i9n7sz2l565f2skwa05mrgsnzdxk2")))

(define-public crate-sogouda-0.1 (crate (name "sogouda") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "web-view") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0n727q2ip6nb3bgbgp1dmdvm4idp6ywi7gqfzpn41qrz3ll1xwm3")))

(define-public crate-sogouda-0.1 (crate (name "sogouda") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "web-view") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1xas22zkh9sja1gb42hcyslhzp2hc0560h9rsyrffbdagrhjv03m")))

(define-public crate-sogouda-0.1 (crate (name "sogouda") (vers "0.1.0-alpha.3") (deps (list (crate-dep (name "web-view") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1rk5b2qxc84ncdwiahdbv25bnd6bng9ghfk1ay71hdpadhwm674r")))

(define-public crate-sogouda_core-0.1 (crate (name "sogouda_core") (vers "0.1.1") (deps (list (crate-dep (name "web-view") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1z401lajp61i01bvjxvsw8v51xd2ay2av5w9jgmqxq6mn5hxl5k0")))

