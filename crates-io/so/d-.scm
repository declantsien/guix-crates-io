(define-module (crates-io so d-) #:use-module (crates-io))

(define-public crate-sod-actix-web-0.2 (crate (name "sod-actix-web") (vers "0.2.1") (deps (list (crate-dep (name "actix") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "actix-web") (req "^4.3.1") (default-features #t) (kind 0)) (crate-dep (name "actix-web-actors") (req "^4.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.175") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0.175") (default-features #t) (kind 2)) (crate-dep (name "sod") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "0zmnnpirqqaw1a84wb7vdr9yq1qjwk5sj9jpggl2fh6kjh64qmlr")))

(define-public crate-sod-actix-web-0.2 (crate (name "sod-actix-web") (vers "0.2.2") (deps (list (crate-dep (name "actix") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "actix-web") (req "^4.3.1") (default-features #t) (kind 0)) (crate-dep (name "actix-web-actors") (req "^4.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.175") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0.175") (default-features #t) (kind 2)) (crate-dep (name "sod") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1msi7ag779wk0g3k5g1q31jj5zgbg8wr2yb8r8bfwpiy5afh4rr9")))

(define-public crate-sod-actix-web-0.3 (crate (name "sod-actix-web") (vers "0.3.1") (deps (list (crate-dep (name "actix") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "actix-web") (req "^4.3.1") (default-features #t) (kind 0)) (crate-dep (name "actix-web-actors") (req "^4.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.175") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0.175") (default-features #t) (kind 2)) (crate-dep (name "sod") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "06qjk7i2s0d1sil5kllbi6z76zivjmx1kfxjb0lb1zimink5vdny")))

(define-public crate-sod-bus-0.2 (crate (name "sod-bus") (vers "0.2.1") (deps (list (crate-dep (name "bus") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "sod") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "12q1m8s4sz6zlqcxl8lw5y9yyxp8lxd9vq459s2ixzflhm8ds07z")))

(define-public crate-sod-bus-0.2 (crate (name "sod-bus") (vers "0.2.2") (deps (list (crate-dep (name "bus") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "sod") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1risizk8s2l1kvs4sdbjgg1drd1xgqa8d7wb1cr51kgjcgy8sa57")))

(define-public crate-sod-bus-0.2 (crate (name "sod-bus") (vers "0.2.3") (deps (list (crate-dep (name "bus") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "sod") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "00h0wjr3hrlsfphj6xci2si3ccv2vsc62r8zq81bap1kcqkg9vfi")))

(define-public crate-sod-bus-0.2 (crate (name "sod-bus") (vers "0.2.4") (deps (list (crate-dep (name "bus") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "sod") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1w0znlz4qdd43pnpm0nqs44mb7pljj1qc6fvnq4vimv3sdh2j6a9")))

(define-public crate-sod-bus-0.3 (crate (name "sod-bus") (vers "0.3.1") (deps (list (crate-dep (name "bus") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "sod") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "16szggk8kinrf49fa2fm6i6j1n3j0q93dndzc3gmh065hnmi1a5q")))

(define-public crate-sod-crossbeam-0.2 (crate (name "sod-crossbeam") (vers "0.2.1") (deps (list (crate-dep (name "crossbeam") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "sod") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0w57fmihmf1lqgsjq0hwfvb1d62w18hnr9jfar3fm3nnc1i3n4ym")))

(define-public crate-sod-crossbeam-0.2 (crate (name "sod-crossbeam") (vers "0.2.2") (deps (list (crate-dep (name "crossbeam") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "sod") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0a8q53w934y1bdfa5kzg57xsp14wwjkfpqhnia3zhlvhqif6jzic")))

(define-public crate-sod-crossbeam-0.2 (crate (name "sod-crossbeam") (vers "0.2.3") (deps (list (crate-dep (name "crossbeam") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "sod") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1yz7v9ys6sdi1hr5dyv97lrbznmh0fqmpgfrzg8w6jvpmzan9ix9")))

(define-public crate-sod-crossbeam-0.3 (crate (name "sod-crossbeam") (vers "0.3.1") (deps (list (crate-dep (name "crossbeam") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "sod") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0rajd0a5g9ns0y3nl5plb034acb1fyk7nl1gx3dm5z26g02pv5zl")))

(define-public crate-sod-log-0.2 (crate (name "sod-log") (vers "0.2.1") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "sod") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1wl8hbix5fckxhxr2b4d5r5z7l6rhjnc2myb8qjv6z752rghv5m2")))

(define-public crate-sod-log-0.2 (crate (name "sod-log") (vers "0.2.2") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "sod") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1kghy8zbkj3dpc0a6scyhcic0xmr2bszz71w6wl3wn4xg1yjq8id")))

(define-public crate-sod-log-0.2 (crate (name "sod-log") (vers "0.2.3") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "sod") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "11vs7bki3a8279ywa28grjipcmbfv05bk7i4sw7jznm5i4hgqvbm")))

(define-public crate-sod-log-0.3 (crate (name "sod-log") (vers "0.3.1") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "sod") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1fvvs0pnmx6j5m4rsb3dszqfdhcvs01q94dfxdiifzxkhk08rzy4")))

(define-public crate-sod-mpsc-0.1 (crate (name "sod-mpsc") (vers "0.1.0") (deps (list (crate-dep (name "sod") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "155w61xgv0y5lfdzxij0l9112h2ypqnrzfgyaqkhk07skws855l8") (yanked #t)))

(define-public crate-sod-mpsc-0.1 (crate (name "sod-mpsc") (vers "0.1.1") (deps (list (crate-dep (name "sod") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "027lzh6vz27ci0ydnhala17hza2r4fv8prfgl50gf4zb5xlx7l5z")))

(define-public crate-sod-mpsc-0.1 (crate (name "sod-mpsc") (vers "0.1.2") (deps (list (crate-dep (name "sod") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0hxm88a7vyk04hjmjnk2kjy147yxy2i0k3zvsybdqv6rmm4rzvwn")))

(define-public crate-sod-mpsc-0.2 (crate (name "sod-mpsc") (vers "0.2.1") (deps (list (crate-dep (name "sod") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0fzvy5fghwljx65vhr8qkii2839m3mjxdxz8v69vflxk3q2zqiz0")))

(define-public crate-sod-mpsc-0.2 (crate (name "sod-mpsc") (vers "0.2.2") (deps (list (crate-dep (name "sod") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1fzxy2i213h19asy09xn4w1dkndf36929kn68xbb0fv075q7jcpi")))

(define-public crate-sod-mpsc-0.2 (crate (name "sod-mpsc") (vers "0.2.3") (deps (list (crate-dep (name "sod") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0hv4as1j68am7yrrf07c2283v4rc1yw4xh1c65jm1hyr9psi86bx")))

(define-public crate-sod-mpsc-0.3 (crate (name "sod-mpsc") (vers "0.3.1") (deps (list (crate-dep (name "sod") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "09ii6py8v514ikr6cwk5njyl797qz39kdhhlk5lzg72sk2yfdrp4")))

(define-public crate-sod-tungstenite-0.2 (crate (name "sod-tungstenite") (vers "0.2.1") (deps (list (crate-dep (name "sod") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "tungstenite") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 2)))) (hash "1j7jzwvilgsq0rcv0vnqwlpczjpc3a5hkpdbnljwcwxk8hci53ic")))

(define-public crate-sod-tungstenite-0.2 (crate (name "sod-tungstenite") (vers "0.2.2") (deps (list (crate-dep (name "sod") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "tungstenite") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 2)))) (hash "0ak8rcdghl3z5bbw0bih7x1mpkxkl5rz6q0zn7kh0c3lcdk7hgzq")))

(define-public crate-sod-tungstenite-0.3 (crate (name "sod-tungstenite") (vers "0.3.1") (deps (list (crate-dep (name "sod") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "tungstenite") (req "^0.20.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 2)))) (hash "0wpln3139pw52mg1xqz0zqdvrgpkwqw36i0y8qb72yqas5k5pg5c")))

(define-public crate-sod-tungstenite-0.3 (crate (name "sod-tungstenite") (vers "0.3.2") (deps (list (crate-dep (name "sod") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "tungstenite") (req "^0.20.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 2)))) (hash "0gpg8syvvpv873l5bpxarrnh3b0anic272qfn8g5xj0gdsbxi6ng")))

(define-public crate-sod-tungstenite-0.3 (crate (name "sod-tungstenite") (vers "0.3.3") (deps (list (crate-dep (name "sod") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "tungstenite") (req "^0.20.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 2)))) (hash "0bzyh2sv79fvancf8r5nqkkyyzbmwfv9wnlfgvad60pkpvi40fky")))

