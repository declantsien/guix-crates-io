(define-module (crates-io so cr) #:use-module (crates-io))

(define-public crate-socrates-0.0.1 (crate (name "socrates") (vers "0.0.1") (deps (list (crate-dep (name "socrates-core") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0kr4vnl1l16gxbvipcs3d6qmdamwgai84167qqr3hrv9lxv49qv0")))

(define-public crate-socrates-core-0.0.1 (crate (name "socrates-core") (vers "0.0.1") (hash "0lxac3b7ds168ha11bfxgfjj0vcfqcli9w9798lxr6y2f815rrmn")))

(define-public crate-socrates-launcher-0.0.1 (crate (name "socrates-launcher") (vers "0.0.1") (deps (list (crate-dep (name "socrates") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "030ppwk530n3y4m13kbdn4ys6lgkby3hdjlg804plm7x47lr2rm9")))

(define-public crate-socrates_rs-1 (crate (name "socrates_rs") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0gblhsv66m4ijx8byh4chwf82jfdp85nnx4qa7y92in83dfp8m6g")))

(define-public crate-socratic-0.0.1 (crate (name "socratic") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 2)) (crate-dep (name "ciborium") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ciborium-io") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.2.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "test-log") (req "^0.2.10") (features (quote ("trace"))) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.11") (features (quote ("env-filter" "fmt"))) (kind 2)))) (hash "10f01wh984krrhsjmzl6r9j0xrxmcgn8v5jyyrwl38g73hpdpsyb") (features (quote (("default" "cbor") ("cbor" "ciborium" "ciborium-io"))))))

