(define-module (crates-io so em) #:use-module (crates-io))

(define-public crate-soem-0.1 (crate (name "soem") (vers "0.1.0") (deps (list (crate-dep (name "SOEM-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "boolinator") (req "^2.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1pnf6k0x2sk0vkfg1h6gwz3lxigzgw6y0vzhi22442m23cqw30gh")))

(define-public crate-soem-0.2 (crate (name "soem") (vers "0.2.0") (deps (list (crate-dep (name "SOEM-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "boolinator") (req "^2.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1q3g18wlmhmj0mmjymi9g45nl0w2yirjha93w4g8sb1y9d84zqhz")))

(define-public crate-soem-0.3 (crate (name "soem") (vers "0.3.0") (deps (list (crate-dep (name "SOEM-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "boolinator") (req "^2.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1f72rclg0rkpxgvdyllpni86k6w1gfbsph42dmslsijbjbvk508q")))

(define-public crate-SOEM-sys-0.1 (crate (name "SOEM-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.42.2") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1zqlfkwy9rfwn3qwfwrzzd2qb7jx60qdhdhdqay0hzldgd4cbfgq")))

(define-public crate-SOEM-sys-0.1 (crate (name "SOEM-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.42.2") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "05hb6qvhrx7rgqc9vmm4rxnyasnm71jjj2yvqdmm7g5nyaqbqqkl")))

(define-public crate-SOEM-sys-0.1 (crate (name "SOEM-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.42.2") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1wcn2zwg7v8ybda11nwc8bylf235mny5sscb63807kifb1c1gkwg")))

(define-public crate-SOEM-sys-0.2 (crate (name "SOEM-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.53.2") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0alyjnmq4hwwf2165g36hijinf6jz1cbnq1vjv2bvjjqzxbp2mbb")))

