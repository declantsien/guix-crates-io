(define-module (crates-io as tm) #:use-module (crates-io))

(define-public crate-astmaker-0.1 (crate (name "astmaker") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "0xfps8zl7rhhsjilkr4pfmszw3yr2jmbvivxm2j9mafcrh9c3mw4")))

(define-public crate-astmaker-0.1 (crate (name "astmaker") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "0x4q6avzkfzd00nh5qj7s4xrlf6gj91gasidbsy589wyq1i5ijp3")))

(define-public crate-astmaker-0.2 (crate (name "astmaker") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "0v0dj87p3m30zvvy5d1pp03xmkv23a4lz16373bcgvxkq1asmj2j")))

