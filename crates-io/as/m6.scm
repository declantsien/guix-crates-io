(define-module (crates-io as m6) #:use-module (crates-io))

(define-public crate-asm6502-0.1 (crate (name "asm6502") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^2.0") (default-features #t) (kind 0)))) (hash "0ly1yyiydzrp6c1c2hl7k5hbiqkg8xm86s3bv8py6386wr16cw06")))

(define-public crate-asm6502-0.1 (crate (name "asm6502") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^2.0") (default-features #t) (kind 0)))) (hash "02m81nf53rbqqdw1k4ws183fdw6309y9sw6qgah3iszdna5x0xn2")))

(define-public crate-asm6502-0.1 (crate (name "asm6502") (vers "0.1.2") (deps (list (crate-dep (name "nom") (req "^2.0") (default-features #t) (kind 0)))) (hash "0pj9pwxvba046mlkqlsc60bsjs18ryvxrkw8w5n0n9ml4r4yzfzk")))

