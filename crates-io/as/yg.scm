(define-module (crates-io as yg) #:use-module (crates-io))

(define-public crate-asygnal-0.0.1 (crate (name "asygnal") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2.66") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3.8") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1b4c3n2plxk96sik96ra0yah8yksrw2h6lg31jcmq2lwqb91fpqi") (features (quote (("_docs"))))))

(define-public crate-asygnal-0.0.2 (crate (name "asygnal") (vers "0.0.2") (deps (list (crate-dep (name "futures") (req "^0.3.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.66") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3.8") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1701jhlqhvx2kpryfvmfrrhr006r11rhzmhl7dxsx2kgllka2dpn") (features (quote (("stream" "futures") ("_docs"))))))

