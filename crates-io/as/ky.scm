(define-module (crates-io as ky) #:use-module (crates-io))

(define-public crate-asky-0.1 (crate (name "asky") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.26.0") (default-features #t) (kind 0)))) (hash "1f4z5v5p3cg1dqbzc8aypjlbfkxgp48nm7cpfgl18ina7haww4mr")))

(define-public crate-asky-0.1 (crate (name "asky") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.26.0") (default-features #t) (kind 0)))) (hash "0y98g6135sm8fn2f4kb707fppk6wp83a2bagc1vj14kngd49mb6z")))

