(define-module (crates-io as ca) #:use-module (crates-io))

(define-public crate-ascas-0.1 (crate (name "ascas") (vers "0.1.0") (deps (list (crate-dep (name "libm") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "1dvb9w74dic85jxh6qkm5mkhqwzb85q047wpb46f24w5hxlk1h8h") (yanked #t)))

(define-public crate-ascas-0.1 (crate (name "ascas") (vers "0.1.5") (deps (list (crate-dep (name "libm") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "08r4iq4hd4q4dbs6s2d75ss5bv4hqzc4m3lwn547kfwi2xjnp8hf") (yanked #t)))

