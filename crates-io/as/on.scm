(define-module (crates-io as on) #:use-module (crates-io))

(define-public crate-ason-0.1 (crate (name "ason") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.35") (default-features #t) (kind 0)) (crate-dep (name "hexfloat2") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "09cd1j7x7jrfy2nzcxrhdnwp197yc4dvnzkv9pmm7cdlik60v8qi")))

(define-public crate-ason-0.1 (crate (name "ason") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.35") (default-features #t) (kind 0)) (crate-dep (name "hexfloat2") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0lfqv7cf13zyyx2dh1hly7fahn5p72a1zhbrqvxbmhfjghidabay")))

(define-public crate-ason-0.1 (crate (name "ason") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.35") (default-features #t) (kind 0)) (crate-dep (name "hexfloat2") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "06k34zicrw5j3b4zpllpwf6fh2hc28b2qi7i6zifzxrhp5cz5djs")))

(define-public crate-ason-0.1 (crate (name "ason") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.35") (default-features #t) (kind 0)) (crate-dep (name "hexfloat2") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "08rrad80laxlsir8aqi0lkf1v9mc5rd74m4kx2iz5lp4kzs7al32")))

(define-public crate-ason-0.1 (crate (name "ason") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4.35") (default-features #t) (kind 0)) (crate-dep (name "hexfloat2") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "066hinzl9j5wr16m9b7nvd6bykcx94skxxh2mr5njbfgqxj5qmvw")))

(define-public crate-ason-0.1 (crate (name "ason") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4.35") (default-features #t) (kind 0)) (crate-dep (name "hexfloat2") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0w4kymdnqiiimd60bx0f10j04jp7ry2addi9kgsss8lr2xdqfjan")))

(define-public crate-ason-0.1 (crate (name "ason") (vers "0.1.6") (deps (list (crate-dep (name "chrono") (req "^0.4.35") (default-features #t) (kind 0)) (crate-dep (name "hexfloat2") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1h5973cx84y5x3rjy96nbik7i3m3jgvf6drdgx43sf12ir5gdq1i")))

(define-public crate-ason-0.2 (crate (name "ason") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.35") (default-features #t) (kind 0)) (crate-dep (name "hexfloat2") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "07xly4paa6rg11ybpf4aav9m0ma4g4yfssqj7bl0aacwzva8f0s8")))

