(define-module (crates-io as t2) #:use-module (crates-io))

(define-public crate-ast2str-1 (crate (name "ast2str") (vers "1.1.0") (deps (list (crate-dep (name "ast2str-derive") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "ast2str-lib") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "0lh9gh0ggal51hawgi5k32sn3cp84vjriql864qjqmrhpqd5syzg")))

(define-public crate-ast2str-1 (crate (name "ast2str") (vers "1.1.1") (deps (list (crate-dep (name "ast2str-derive") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "ast2str-lib") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "0myxqb0simn9b5zr2kqq5ss198ch67dafzxk0y472sjs3v7r1d7i")))

(define-public crate-ast2str-1 (crate (name "ast2str") (vers "1.2.1") (deps (list (crate-dep (name "ast2str-derive") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "ast2str-lib") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "04d5bzj1r6z93wizcjn2p3h4081laa2hnz9p0yblx49cc2zi15ih")))

(define-public crate-ast2str-1 (crate (name "ast2str") (vers "1.3.0") (deps (list (crate-dep (name "ast2str-derive") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "ast2str-lib") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "1f1zv259h1137wc15f8y1p05cysr4g440wh0b80h6zbzxcsk9mxy") (features (quote (("impl_hashbrown" "ast2str-lib/impl_hashbrown") ("allocator_api" "ast2str-lib/allocator_api"))))))

(define-public crate-ast2str-1 (crate (name "ast2str") (vers "1.4.0") (deps (list (crate-dep (name "ast2str-derive") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "ast2str-lib") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "04y0pqblfqk1997v1xzsjnw5q05cfmf9gwxiffayqpq4xzzhzbv2") (features (quote (("impl_hashbrown" "ast2str-lib/impl_hashbrown") ("allocator_api" "ast2str-lib/allocator_api"))))))

(define-public crate-ast2str-1 (crate (name "ast2str") (vers "1.4.1") (deps (list (crate-dep (name "ast2str-derive") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "ast2str-lib") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "0q5pbil730jhlm50bw9nmcvp8pnbz25i9p4p71gx5xkqwdvqfs71") (features (quote (("impl_hashbrown" "ast2str-lib/impl_hashbrown") ("allocator_api" "ast2str-lib/allocator_api"))))))

(define-public crate-ast2str-derive-0.2 (crate (name "ast2str-derive") (vers "0.2.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.64") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1snpl030md1x6rh6gg23hafc2nsq9vjc3k89ypz88fx73pgyanmb")))

(define-public crate-ast2str-derive-0.3 (crate (name "ast2str-derive") (vers "0.3.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.64") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1miprhkqskaj6zinvl1x090cn1s0drsffnkd8ladas4b9snnflmx")))

(define-public crate-ast2str-derive-0.4 (crate (name "ast2str-derive") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.64") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0g6vgvks9mavlcmk2zd0ln1g9d44f22pac0nc5pwa6idb106sip8")))

(define-public crate-ast2str-derive-0.4 (crate (name "ast2str-derive") (vers "0.4.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.64") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0h4rggwgxy7yc4x1zyw0wawm6d2cv54k5276d1aqj7r08myhpnhw")))

(define-public crate-ast2str-lib-1 (crate (name "ast2str-lib") (vers "1.1.0") (deps (list (crate-dep (name "pretty_assertions") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "0clqb6mjg5lmyqsmbchk399q786prdm0j52a03lf9jgmyyrg8xy2")))

(define-public crate-ast2str-lib-1 (crate (name "ast2str-lib") (vers "1.2.0") (deps (list (crate-dep (name "hashbrown") (req "^0.12.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "0apqcg0wcxjncvj9hvyndd3r0fv8larg78dyy4py8jmlaksgcfpi") (features (quote (("impl_hashbrown" "hashbrown") ("allocator_api" "hashbrown/nightly"))))))

(define-public crate-ast2str-lib-1 (crate (name "ast2str-lib") (vers "1.2.1") (deps (list (crate-dep (name "hashbrown") (req "^0.12.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "1dhq8pvnm1qhi7c4gw96gczmb7q3f4pl6nip1sh3ajvjl8l55ncf") (features (quote (("impl_hashbrown" "hashbrown") ("allocator_api" "hashbrown/nightly"))))))

(define-public crate-ast2str-lib-1 (crate (name "ast2str-lib") (vers "1.3.0") (deps (list (crate-dep (name "hashbrown") (req "^0.12.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "1i3apakqrkjxdd24182av8avj72axiw8l5n24fwifqnmvznv6xs4") (features (quote (("impl_hashbrown" "hashbrown") ("allocator_api" "hashbrown/nightly"))))))

