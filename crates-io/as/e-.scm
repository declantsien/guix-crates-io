(define-module (crates-io as e-) #:use-module (crates-io))

(define-public crate-ase-swatch-0.1 (crate (name "ase-swatch") (vers "0.1.0") (deps (list (crate-dep (name "console_error_panic_hook") (req "^0.1.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structure") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.63") (features (quote ("serde-serialize"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.13") (default-features #t) (kind 2)))) (hash "15jf4iwgjfk6ay68la3k8ipsvkwhwvj17h1rnwzmjpaw8bhcwlfv") (features (quote (("default" "console_error_panic_hook"))))))

