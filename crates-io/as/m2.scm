(define-module (crates-io as m2) #:use-module (crates-io))

(define-public crate-asm2x6xtool-0.1 (crate (name "asm2x6xtool") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "rusb") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "12jnp64w2lwx0p6km2hbk8gmmsvifvkiyw9iiivj4z4ggs1bv5vp")))

