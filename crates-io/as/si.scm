(define-module (crates-io as si) #:use-module (crates-io))

(define-public crate-assign-1 (crate (name "assign") (vers "1.0.0") (hash "1080z3hc2brprd9jcm0z46ij1dwr7k1g7jrhkjbb0vprpg7ddpla")))

(define-public crate-assign-1 (crate (name "assign") (vers "1.0.1") (hash "114jym8yj98l9fw8qhvslzgvpgjq9kcx7bz88f90ayvnggrjx03l")))

(define-public crate-assign-1 (crate (name "assign") (vers "1.1.0") (hash "0rqx3cj0r3cpfpxqaqsxbrg36gin1mgarjhlxxq5xv1swdznixaa")))

(define-public crate-assign-1 (crate (name "assign") (vers "1.1.1") (hash "00h0r04iyyhhnjr8fwkmw3fpvpd41bn9x1dz8s9j5kdyg3nkw2az")))

(define-public crate-assign-resources-0.1 (crate (name "assign-resources") (vers "0.1.0") (hash "1lbrs3541rb2zyz6yv68pf388i9dz2ag64pz18x62yk0dr63xq6r")))

(define-public crate-assign-resources-0.2 (crate (name "assign-resources") (vers "0.2.0") (hash "0yf1hygbklfbp3cjc3ndk1wc2x83ifmfvlbvcfh1p0rn84819si7")))

(define-public crate-assign-resources-0.3 (crate (name "assign-resources") (vers "0.3.0") (hash "1g8w0lwyksxaqm57ingm87i0hmywrb9wqahr12sdn6zkyfpw9dv3")))

(define-public crate-assign-resources-0.4 (crate (name "assign-resources") (vers "0.4.0") (hash "0wz1m6plkzdw1nn1y1fpivq9hdr01qhyym51i8xgd6p9zx0vv7dc")))

(define-public crate-assign-resources-0.4 (crate (name "assign-resources") (vers "0.4.1") (hash "11bznwwn540p8zcb02wqmfnxyiv47fys3dl4s1qw3x94jq8wa3vc")))

(define-public crate-assign1-0.1 (crate (name "assign1") (vers "0.1.0") (hash "0pcgrfmlnzcp8zcrxsldzsd4mv194mv624k1wx8snx9313sd2yn4")))

(define-public crate-assign1-0.1 (crate (name "assign1") (vers "0.1.1") (hash "0w7l6dn58fjl8rmj2n6g2z4iasdllq7r38i2vsfi21c4sk2liw4r")))

(define-public crate-Assignment-0.0.2 (crate (name "Assignment") (vers "0.0.2") (hash "0d8vrg60s8n8fjqzax12qyd9w7k6si75vjxqwj1rh8k0gk30vmcl")))

(define-public crate-assimp-0.0.1 (crate (name "assimp") (vers "0.0.1") (deps (list (crate-dep (name "assimp-sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "cgmath") (req "*") (default-features #t) (kind 2)) (crate-dep (name "glium") (req "*") (default-features #t) (kind 2)) (crate-dep (name "glutin") (req "*") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 2)))) (hash "0qaagjmchhznr4fy7713fzrnkgj31bnwjjyr03075c0ighww4wln")))

(define-public crate-assimp-0.0.3 (crate (name "assimp") (vers "0.0.3") (deps (list (crate-dep (name "assimp-sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "cgmath") (req "*") (default-features #t) (kind 0)) (crate-dep (name "glium") (req "*") (default-features #t) (kind 2)) (crate-dep (name "glutin") (req "*") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1m5vf7rp9cj01rbf1rvn3fmwnhd71xcd2zwq4fq0xh9zi24agnh0")))

(define-public crate-assimp-0.0.4 (crate (name "assimp") (vers "0.0.4") (deps (list (crate-dep (name "assimp-sys") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "cgmath") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "glium") (req "^0.3.7") (default-features #t) (kind 2)) (crate-dep (name "glutin") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "1wwqp48fn1a0v7r3kqkvmklmrnswmcsnn934251sshbs5h1gpncj")))

(define-public crate-assimp-0.0.5 (crate (name "assimp") (vers "0.0.5") (deps (list (crate-dep (name "assimp-sys") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "cgmath") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "glium") (req "^0.3.7") (default-features #t) (kind 2)) (crate-dep (name "glutin") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "0dnwimv04rm2q70ga92w3nagydah907sxs79ygdkz7grq7jmzgk2")))

(define-public crate-assimp-0.0.6 (crate (name "assimp") (vers "0.0.6") (deps (list (crate-dep (name "assimp-sys") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "cgmath") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "glium") (req "^0.3.7") (default-features #t) (kind 2)) (crate-dep (name "glutin") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "05mpnzfx2728yqlwh9fzxpxvl5i26mw513fzgqgzfjmbxwxja3nf")))

(define-public crate-assimp-0.0.7 (crate (name "assimp") (vers "0.0.7") (deps (list (crate-dep (name "assimp-sys") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "cgmath") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "glium") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "156vnnkc0sllfaly5ard0ayyx9jfch805gh28z7fmzd8rh0crlb7")))

(define-public crate-assimp-0.0.8 (crate (name "assimp") (vers "0.0.8") (deps (list (crate-dep (name "assimp-sys") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "cgmath") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "glium") (req "^0.11.0") (default-features #t) (kind 2)))) (hash "04sirl96sbx3cajxqz3jxl4dji0481hqc7p0g7i2rkcryva5m7si")))

(define-public crate-assimp-0.1 (crate (name "assimp") (vers "0.1.0") (deps (list (crate-dep (name "assimp-sys") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "cgmath") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cgmath") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "glium") (req "^0.12.3") (default-features #t) (kind 2)))) (hash "0992pkgp74zx4328vj8g535kdqvb0cpi38b8a9xvhpn2vwljzpiv")))

(define-public crate-assimp-0.2 (crate (name "assimp") (vers "0.2.0") (deps (list (crate-dep (name "assimp-sys") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "cgmath") (req "^0.12.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cgmath") (req "^0.12.0") (default-features #t) (kind 2)) (crate-dep (name "glium") (req "^0.15.0") (default-features #t) (kind 2)))) (hash "03jz3d4xzkgib2jc0bhd8ws039wnhcmyxnn02vywgxi9sjal21z8")))

(define-public crate-assimp-0.2 (crate (name "assimp") (vers "0.2.1") (deps (list (crate-dep (name "assimp-sys") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "cgmath") (req "^0.12.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cgmath") (req "^0.12.0") (default-features #t) (kind 2)) (crate-dep (name "glium") (req "^0.15.0") (default-features #t) (kind 2)))) (hash "10kyzxk3ax2wd8vypvb0vjx6br77n14pqrn0ly0l2mg8zi68spr7")))

(define-public crate-assimp-0.3 (crate (name "assimp") (vers "0.3.0") (deps (list (crate-dep (name "assimp-sys") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "cgmath") (req "^0.12.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cgmath") (req "^0.12.0") (default-features #t) (kind 2)) (crate-dep (name "glium") (req "^0.15.0") (default-features #t) (kind 2)))) (hash "0n7sdyibpwfg3xwmdpkkg0882lim87w3ayjqa2d8pz31ma4y0hpp")))

(define-public crate-assimp-0.3 (crate (name "assimp") (vers "0.3.1") (deps (list (crate-dep (name "assimp-sys") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "cgmath") (req "^0.15.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cgmath") (req "^0.15.0") (default-features #t) (kind 2)) (crate-dep (name "glium") (req "^0.18.0") (default-features #t) (kind 2)))) (hash "0zyqg7m2jsdwbai6lf2qqbi968nigmy90f0rd4wjjm0yq84k804a")))

(define-public crate-assimp-rs-0.0.1 (crate (name "assimp-rs") (vers "0.0.1") (hash "14kqwxm4v8k8wgm3cib4pgksq69m9605mnr5i9iwpvs5ad2vybvl") (yanked #t)))

(define-public crate-assimp-sys-0.0.1 (crate (name "assimp-sys") (vers "0.0.1") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "cgmath") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1x6bfxq69k1d8i857aqii33lw8g802xjxqp9p7xml5sd69wva083")))

(define-public crate-assimp-sys-0.0.2 (crate (name "assimp-sys") (vers "0.0.2") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0y1zm057cxxqnwc7191drhjsm8pf94mv3k3k7qs0f04g8ck3xjrv")))

(define-public crate-assimp-sys-0.0.3 (crate (name "assimp-sys") (vers "0.0.3") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0xdp9rgnz93zyfx79hh695ihil2aa3q2h1jxrllrjd734q9yj8lg")))

(define-public crate-assimp-sys-0.0.4 (crate (name "assimp-sys") (vers "0.0.4") (deps (list (crate-dep (name "bitflags") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "0swlnsn8lzc0wvq0fhlw10ivw5ylk8kbd7x6pv8lp8rnkhmxfzbn")))

(define-public crate-assimp-sys-0.0.5 (crate (name "assimp-sys") (vers "0.0.5") (deps (list (crate-dep (name "bitflags") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "09l5qarc7nf10p7jppxjpvanc2ribszzbbfy07x6fm7y4sqvmwi5")))

(define-public crate-assimp-sys-0.0.6 (crate (name "assimp-sys") (vers "0.0.6") (deps (list (crate-dep (name "bitflags") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "14wrdr6zjbl2v9xr6bv35x3zv6mw1j55pqw1ap916aqn133hpd0n") (yanked #t)))

(define-public crate-assimp-sys-0.0.7 (crate (name "assimp-sys") (vers "0.0.7") (deps (list (crate-dep (name "bitflags") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "04b4016bbg1iqfi5h3x22wfk3ymqa4vp2n4r0m7pc5a21pxs7fcn")))

(define-public crate-assimp-sys-0.1 (crate (name "assimp-sys") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1b8sr61j6rlnmpbx4hd0zlhyjpvifshvjrdqyq0pm28hjd7grj0d")))

(define-public crate-assimp-sys-0.2 (crate (name "assimp-sys") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "11aipx9840w3rz5ghlpassqjwxsnfjhx43kqwawysiaq0j6hfn3i")))

(define-public crate-assimp-sys-0.2 (crate (name "assimp-sys") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "07m50b4q0rgcdh2w7h69j5zdl3b6igxgfgvxlzcjx45dkwqdvzmf")))

(define-public crate-assimp-sys-0.2 (crate (name "assimp-sys") (vers "0.2.2") (deps (list (crate-dep (name "bitflags") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1qr0hbklycx74w89y8nig1xgrkixvc821vy172j0cywm07vfiq6q")))

(define-public crate-assimp-sys-0.3 (crate (name "assimp-sys") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1.25") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.9") (default-features #t) (kind 1)))) (hash "0c0ks1j4g4ri3c9k15m88rq4mbyyvddn9djv4i5k7wws2dz0icq4")))

(define-public crate-assimp-sys-0.3 (crate (name "assimp-sys") (vers "0.3.1") (deps (list (crate-dep (name "bitflags") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1.25") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.9") (default-features #t) (kind 1)))) (hash "0x8qs5bwyj44khya46dn5m4g52kchkip8w1gzyn7if5b73fszl1d")))

(define-public crate-assist-sys-0.1 (crate (name "assist-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "rebound-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1208wcp8x08nmajcwbqjsw2z73d8dqm0327ikwnp73isi2glhqqz") (links "assist")))

(define-public crate-assist-sys-0.2 (crate (name "assist-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "rebound-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0hnr44vjq63z8lmvwciplcpyhw58fyx2k8l2angdqxvi1kfnsf7l") (links "assist")))

(define-public crate-assistant_common-0.1 (crate (name "assistant_common") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-appender") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (features (quote ("env-filter"))) (default-features #t) (kind 0)))) (hash "1fr633vv8g7b14ycy6rp63algmfdwlqv0gkdwbp4a2gi1z4yfc6s")))

(define-public crate-assistant_daemon-0.1 (crate (name "assistant_daemon") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "assistant_common") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "axum") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "cron") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^6.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.24") (features (quote ("json" "stream"))) (default-features #t) (kind 0)) (crate-dep (name "russh") (req "^0.42.0") (default-features #t) (kind 0)) (crate-dep (name "russh-keys") (req "^0.42.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("winuser"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "01sdn63g6rgqwwvp8jxandxix9dknlrsmi6r0jyb72rn461vz4mm")))

(define-public crate-assistant_db-0.1 (crate (name "assistant_db") (vers "0.1.0") (hash "0hy7b759f1lfb04af59y10gmh630sjayhmfw8ryb9r9mb5gay088")))

