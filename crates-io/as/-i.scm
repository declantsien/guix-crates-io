(define-module (crates-io as #{-i}#) #:use-module (crates-io))

(define-public crate-as-is-0.0.1 (crate (name "as-is") (vers "0.0.1-alpha") (hash "1bnihbd24pb46nk6b7aghvxgz210x24mhyxd32iarai9bgmiznnw") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-as-is-0.0.1 (crate (name "as-is") (vers "0.0.1-alpha.2") (hash "00csw9z3f3silqv3ngh9px28z3n5khsgbysq4h3bnn4qd445k8vn") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-as-is-0.0.1 (crate (name "as-is") (vers "0.0.1-alpha.3") (hash "0xm3vxkzj1hjjh2zbmyylnxxgqkzw4r7m3bqsxxjnqvzcx78ffwj") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-as-is-0.0.1 (crate (name "as-is") (vers "0.0.1-alpha.4") (hash "1yr0wjy6qhvz6azng64jml91z7a5wfj18r48ai7xc8vj7m10vn19") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-as-is-0.0.2 (crate (name "as-is") (vers "0.0.2-alpha") (hash "0h9wa7q06z4mhzsh2jpdy8q8zyk8c6kbnrgjz9iarncnfr1r4479") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-as-is-0.0.3 (crate (name "as-is") (vers "0.0.3-alpha") (hash "0gbkqh1z7mknav3v1gn9vyarfkjpqrlm3gs1ibv6w39dq8i27zkq") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-as-is-0.0.4 (crate (name "as-is") (vers "0.0.4-alpha") (hash "1rgcn1l09fq8xdfh2ljiiw4yvmq0hgs4c6g85xslzmq5fhxbv10i") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (yanked #t)))

(define-public crate-as-is-0.0.5 (crate (name "as-is") (vers "0.0.5-alpha") (hash "07z5np8s6ck86y5pl7v0k7dng58y0gk7ckdrn7kf607msb3n9znz") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (yanked #t)))

(define-public crate-as-is-0.0.5 (crate (name "as-is") (vers "0.0.5-alpha.1") (hash "08ay5q5ygalq5552py0c734i9bglqyk84gl2mgiiv40jnxkvk3yd") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (yanked #t)))

(define-public crate-as-is-0.0.5 (crate (name "as-is") (vers "0.0.5-alpha.2") (hash "0a9mk87wk8nsmlx9yvh457psaqr6rh809xfvzwqnxpk1pckapxzk") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (yanked #t)))

(define-public crate-as-is-0.0.6 (crate (name "as-is") (vers "0.0.6") (hash "0vd2gmgs08pjm23vlxd4g63ff84fxl6cpmclb5cqrp9j9j8kcqyw") (features (quote (("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-as-is-0.0.7 (crate (name "as-is") (vers "0.0.7") (hash "0rm9va65695c5m4318qmkdw4vahx9m55j7ggsn90nfy512gqgzcq") (features (quote (("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-as-is-0.0.8 (crate (name "as-is") (vers "0.0.8") (hash "1hgsc585s84aah23al574zpsz96dsyvmi1rspvh79ncw74g4vr92") (features (quote (("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-as-is-0.0.9 (crate (name "as-is") (vers "0.0.9") (hash "1a1jp5qigbz2vp1l7blazkmnf096srr6ygl932awrbljbnfc0f1z") (features (quote (("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-as-is-0.0.10 (crate (name "as-is") (vers "0.0.10") (hash "1krly27ls59dfgaxcl220ijv1sx6szbinr2kpxz0p9npxpzsyxar") (features (quote (("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-as-is-0.0.11 (crate (name "as-is") (vers "0.0.11") (hash "0d2r55110529dc00668iyxsdxv6p8y0lmyig1i27wbiq5jkrq6bw") (features (quote (("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-as-is-0.0.12 (crate (name "as-is") (vers "0.0.12") (hash "062dqn24wvc808bkr3yvansn8q1r3022pi07pcsjxhdhnk0d7gyb") (features (quote (("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-as-is-0.0.13 (crate (name "as-is") (vers "0.0.13") (hash "0h0v3ifl5sv6w85sw53a8fidj9iwc1rnlk7y56m8ypkf9x413a72") (features (quote (("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-as-is-0.0.14 (crate (name "as-is") (vers "0.0.14") (hash "1fcm3wkx1zda85li7jcj4in4kmaypa58n27fga3a3ihbzlh70rk3") (features (quote (("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-as-is-0.0.15 (crate (name "as-is") (vers "0.0.15") (hash "0bslj5qa91x14jy33qz60zjjffzfhd1miwdl51inkjp4rmajki3b") (features (quote (("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-as-is-0.0.16 (crate (name "as-is") (vers "0.0.16") (hash "0z8mnc4xymwd7xg07vpapnf3fz2b019rmdclrb12968gfyzwshxm") (features (quote (("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-as-is-0.0.17 (crate (name "as-is") (vers "0.0.17") (hash "11wqja0r1bya3c7d2n71v3gxzdzr30mxq0qk4jxb82lbfhmmvvdl") (features (quote (("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-as-is-0.0.18 (crate (name "as-is") (vers "0.0.18") (hash "0s0xfld19z30zhpl2x0f75x588rqzalnxd7cg8lajj2rrnj95lp2") (features (quote (("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-as-is-0.0.19 (crate (name "as-is") (vers "0.0.19") (hash "0qvxs3jvidjm9rpgcmhgq2c1r8n3psqpd9ihnzmi590g41k1szyr") (features (quote (("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-as-is-0.0.20 (crate (name "as-is") (vers "0.0.20") (hash "0vmjcvmmqd33h0wg0k9550mfbda8c5bc0x1rkdp84c25b4mryyrd") (features (quote (("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-as-is-0.0.21 (crate (name "as-is") (vers "0.0.21") (hash "08zlnzw7cwgfq8kwi65776i75mbvwyakrzl26g4f97wryzd6q2ya") (features (quote (("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-as-is-0.0.22 (crate (name "as-is") (vers "0.0.22") (hash "15mih4zqhc51cyggx07m9l8nmc0an2aw03r1pzsxdz3nr1c1s14k") (features (quote (("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-as-is-0.0.23 (crate (name "as-is") (vers "0.0.23") (hash "0gfkv7zbl2h3xlsw3abcx5485clxlrkhzqvjk3ih9x058vzp7vxx") (features (quote (("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-as-is-0.0.24 (crate (name "as-is") (vers "0.0.24") (hash "10lcsiy8ni4bv3lsf3vggf78q9gmyjvg8nyd0s8ccm01iswrcw6x") (features (quote (("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-as-is-0.0.25 (crate (name "as-is") (vers "0.0.25") (deps (list (crate-dep (name "ref-ops") (req "^0.2") (default-features #t) (kind 0)))) (hash "0cfm2jqmkwr4svwn4vvcflywvs3y8f3yllxambd5h73pvhhfimzz") (features (quote (("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-as-is-0.0.26 (crate (name "as-is") (vers "0.0.26") (deps (list (crate-dep (name "ref-ops") (req "^0.2") (default-features #t) (kind 0)))) (hash "0m6542rzj1h1s7r756lf28qj6bk0z9xid960kvg087yj6bdhdrvx") (features (quote (("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-as-is-0.0.27 (crate (name "as-is") (vers "0.0.27") (deps (list (crate-dep (name "ref-ops") (req "^0.2") (default-features #t) (kind 0)))) (hash "052vrbhzpricdhjwzr8663ppkn7wlfqxn48dlyvbygc94isf30x9") (features (quote (("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-as-is-0.0.28 (crate (name "as-is") (vers "0.0.28") (deps (list (crate-dep (name "ref-ops") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "siphasher") (req "^1") (default-features #t) (kind 2)))) (hash "0mmpk9ldq0y304apwb3lx0xs7p07iayrwkylpd74g9zisl007hq5") (features (quote (("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-as-is-0.0.29 (crate (name "as-is") (vers "0.0.29") (deps (list (crate-dep (name "char-buf") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "ref-ops") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "siphasher") (req "^1") (default-features #t) (kind 2)))) (hash "1d0na43kgnnnskwpx13flbbn8g1haimdpkgk3s2qbypsx3xqd382") (features (quote (("default" "alloc") ("alloc"))))))

