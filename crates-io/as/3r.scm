(define-module (crates-io as #{3r}#) #:use-module (crates-io))

(define-public crate-as3razen-0.1 (crate (name "as3razen") (vers "0.1.0") (deps (list (crate-dep (name "as3_parser") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^2.4.1") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "file_paths") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "smodel") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "1nfcnxksbxqnwisavj1f6m0n1wrmi3z3yvv9hhamrlp04rd262ih")))

