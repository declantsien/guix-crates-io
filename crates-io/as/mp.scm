(define-module (crates-io as mp) #:use-module (crates-io))

(define-public crate-asmpeach-0.1 (crate (name "asmpeach") (vers "0.1.45") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.67") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "1jnkjyn96735x3g5rbd8ykdrl0jwk13daigzk35w3h8m434029k1")))

(define-public crate-asmpeach-0.1 (crate (name "asmpeach") (vers "0.1.46") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.73") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "14h77mpljm7ccsdx3lla6aw20m0jq1y5f9yh8ix34nh1iyz3zrq2")))

(define-public crate-asmpeach-0.1 (crate (name "asmpeach") (vers "0.1.47") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.77") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "0q62hqsbf6yg3agcmxsqfqf95mi5h0ii353dczc4chh1d0gp6j77")))

