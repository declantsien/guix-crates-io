(define-module (crates-io as su) #:use-module (crates-io))

(define-public crate-assume-0.1 (crate (name "assume") (vers "0.1.0") (hash "0p7pxywv88yzngsrzyazjpcw0cm11c9zfk4jcl8k0y9b1r7admsc")))

(define-public crate-assume-0.2 (crate (name "assume") (vers "0.2.0") (hash "15d7abqn706vgxaj5pxfhmlxmmpdbxp2i3ldnfbxaqn0bagzq93f")))

(define-public crate-assume-0.2 (crate (name "assume") (vers "0.2.1") (hash "140j11hlcaqd8qwi2bdg2klr7ii06j1sa9aplikcmbx0297yy8vf")))

(define-public crate-assume-0.3 (crate (name "assume") (vers "0.3.0") (hash "11n95bj0bgzn3n93mrw243qbi0zanxdqrqlh30zh29wrnb604anl")))

(define-public crate-assume-0.4 (crate (name "assume") (vers "0.4.0") (hash "124slg194by4vy9a2dkj9hcksgp4ja1k8kd4wgchc7s5xxjxg8y8")))

(define-public crate-assume-0.5 (crate (name "assume") (vers "0.5.0") (hash "0g8c6j1gnvjqwyjm6d1hf5w5hv3h3i0bc3kh8vq4xw802jhrqvsx")))

(define-public crate-assume-rolers-0.1 (crate (name "assume-rolers") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "aws-config") (req "^0.51") (default-features #t) (kind 0)) (crate-dep (name "aws-types") (req "^0.51") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "rusoto_config") (req "^0.48") (default-features #t) (kind 0)) (crate-dep (name "rusoto_core") (req "^0.48") (default-features #t) (kind 0)) (crate-dep (name "rusoto_sts") (req "^0.48") (default-features #t) (kind 0)) (crate-dep (name "skim") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (features (quote ("env-filter"))) (default-features #t) (kind 0)))) (hash "01762dr13rrpb0g3zzp35gpdv6z80spij8967ly5w00i96pmr3mc")))

(define-public crate-assume-rolers-0.2 (crate (name "assume-rolers") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "aws-config") (req "^0.51") (default-features #t) (kind 0)) (crate-dep (name "aws-types") (req "^0.51") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive" "string"))) (default-features #t) (kind 0)) (crate-dep (name "clap_complete") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "rusoto_config") (req "^0.48") (default-features #t) (kind 0)) (crate-dep (name "rusoto_core") (req "^0.48") (default-features #t) (kind 0)) (crate-dep (name "rusoto_sts") (req "^0.48") (default-features #t) (kind 0)) (crate-dep (name "skim") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (features (quote ("env-filter"))) (default-features #t) (kind 0)))) (hash "1x15lcmai3yxcln1nqkn2br43zr0nqz8v7rpabq8ggrq48ig4nqg")))

(define-public crate-assume-rolers-0.2 (crate (name "assume-rolers") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "aws-config") (req "^0.51") (default-features #t) (kind 0)) (crate-dep (name "aws-types") (req "^0.51") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive" "string"))) (default-features #t) (kind 0)) (crate-dep (name "clap_complete") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "rusoto_config") (req "^0.48") (default-features #t) (kind 0)) (crate-dep (name "rusoto_core") (req "^0.48") (default-features #t) (kind 0)) (crate-dep (name "rusoto_sts") (req "^0.48") (default-features #t) (kind 0)) (crate-dep (name "skim") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (features (quote ("env-filter"))) (default-features #t) (kind 0)))) (hash "008sbbrl32jjl1ggrb21fr1nnn0cgh72r7650b4knq2a30s5v5dd")))

(define-public crate-assume-rolers-0.3 (crate (name "assume-rolers") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "assume-rolers-schema") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "aws-config") (req "^0.51") (default-features #t) (kind 0)) (crate-dep (name "aws-types") (req "^0.51") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive" "string"))) (default-features #t) (kind 0)) (crate-dep (name "clap_complete") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "rusoto_config") (req "^0.48") (default-features #t) (kind 0)) (crate-dep (name "rusoto_core") (req "^0.48") (default-features #t) (kind 0)) (crate-dep (name "rusoto_sts") (req "^0.48") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "skim") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (features (quote ("env-filter"))) (default-features #t) (kind 0)) (crate-dep (name "wasi-common") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "wasmtime") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "wasmtime-wasi") (req "^3.0") (default-features #t) (kind 0)))) (hash "02in11fc92c36m2ld96q6ykb16j52fgdqimzg57wwr2v0i4c6y9h")))

(define-public crate-assume-rolers-schema-0.3 (crate (name "assume-rolers-schema") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0jy651lyhhn124ywzn1vvxa7v859x2v7bsnncwp7wk26rcwmrzhb")))

(define-public crate-assure-1 (crate (name "assure") (vers "1.0.0") (hash "10qjzlr04m15cjh9n35wsd20n2py7npbknc6bjjsm9pjqb5jci91")))

(define-public crate-assure-1 (crate (name "assure") (vers "1.1.0") (hash "0aa7i8kq6r45gf1g1c4g4paa95l03m3xm91hifpzv7kfkycfw3km")))

(define-public crate-assure-1 (crate (name "assure") (vers "1.2.0") (hash "1h6lq1ph910x5z8iv3azn37zaqq53s22a7slp8vgywbcc8a2k7vz")))

(define-public crate-assure-1 (crate (name "assure") (vers "1.3.0") (hash "1b3r48mj1nsi5mslskncaszbn404qbs41z11kjh3y87kj62qz8an")))

(define-public crate-assure-1 (crate (name "assure") (vers "1.4.0") (hash "01nd19y7bffm7j5y4grlwyz0kbhwnlxkp62bbi96ihl88lyr0c1f")))

(define-public crate-assure-1 (crate (name "assure") (vers "1.5.0") (hash "0ag2bl3s08hqa5zv2h2ghcf5h5a8d6yjbhgwbwpbpajjgy41wk7s")))

(define-public crate-assure-1 (crate (name "assure") (vers "1.6.0") (hash "1p98w6wjf63w4lsj59w3dllqniqf8s9azdk7mc12g65wb6bma2qz")))

(define-public crate-assure-1 (crate (name "assure") (vers "1.7.0") (hash "1rxzmcw7k1cvlcbvnkwc5fy728i3sz4sbb2bp51rwxrgvy0pzc2h")))

(define-public crate-assure-1 (crate (name "assure") (vers "1.8.0") (hash "1pvqnsbdb6a5kj784sbszmdsv9fhm4xycvah34lygj1h43j0k2bv")))

(define-public crate-assure-1 (crate (name "assure") (vers "1.9.0") (hash "08wwn7q041i8j4laiabbma5z43rhf8l5dsn626xf6zw87wzrmiz3")))

(define-public crate-assure-1 (crate (name "assure") (vers "1.10.0") (hash "1yrh91q7sy92md2svhgqy2s3qpk1d52cl4nz59mh27igsgpc18qn")))

(define-public crate-assure-1 (crate (name "assure") (vers "1.11.0") (hash "0crj749nbhvjjhwv6rgl83rf7ci2rz7mmp4ikbypk1y7dbw626vd")))

(define-public crate-assure-2 (crate (name "assure") (vers "2.0.0") (hash "0qy5i5g8y23p72x7bimr55lgddy22kanl8i638va2amx5vx49rm8")))

(define-public crate-assure-2 (crate (name "assure") (vers "2.1.0") (hash "09z4idnnw0087bm7kp46c39hns22qf4hz23n1m71g14bs2iiyclg")))

