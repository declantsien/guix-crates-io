(define-module (crates-io as mk) #:use-module (crates-io))

(define-public crate-asmkit-0.0.0 (crate (name "asmkit") (vers "0.0.0") (deps (list (crate-dep (name "asmkit-core") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "asmkit-x86_64") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "05r16cyhhzzrs299pb22y1i4h6fshd2zfgfqaaqy73v82zxl71s5")))

(define-public crate-asmkit-core-0.0.0 (crate (name "asmkit-core") (vers "0.0.0") (hash "09ay7bkbc66xh7cz1r2m0p5ad53najsfjh6bqkr48jgfimr0608n")))

(define-public crate-asmkit-x86_64-0.0.0 (crate (name "asmkit-x86_64") (vers "0.0.0") (deps (list (crate-dep (name "asmkit-core") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "00m1f77qfy00hg38qzrkmlfnwckqwgbs6wr8n7s8bbnzykicbx9v")))

