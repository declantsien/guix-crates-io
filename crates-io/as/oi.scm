(define-module (crates-io as oi) #:use-module (crates-io))

(define-public crate-asoiaf-api-0.1 (crate (name "asoiaf-api") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "0g1qfqv4apavlj77ld62b3s8afcpxg7mf33228xlw1cik93bxpnb")))

(define-public crate-asoiaf-api-0.1 (crate (name "asoiaf-api") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.24") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "0i1r96l2mg9l6r499pg1qxxhmkxhwhnn8zig26xbaa9975kaw08p")))

