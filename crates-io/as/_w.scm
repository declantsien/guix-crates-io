(define-module (crates-io as _w) #:use-module (crates-io))

(define-public crate-as_warp_reply-0.1 (crate (name "as_warp_reply") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3") (default-features #t) (kind 0)))) (hash "1yg0iwkjgw9nzvsh4n3cmap4xspf9ddfdvyxmfm0fdy0dbcihz7p")))

(define-public crate-as_with_bytes-0.1 (crate (name "as_with_bytes") (vers "0.1.0") (hash "0dmksw0cgvkcfnb6vnlvbff7sg7fzav01624p06sd1qiznwccgvw") (yanked #t)))

(define-public crate-as_with_bytes-0.1 (crate (name "as_with_bytes") (vers "0.1.1") (hash "0zl0hqdd1pgqxlpmqxn45f5i7vbb96n11q2b0asb5s6zik0idw01") (yanked #t)))

(define-public crate-as_with_bytes-0.1 (crate (name "as_with_bytes") (vers "0.1.2") (hash "15zgw803f6aaqr7a6vpz03r84gbp51x56l46riqm4w7q4d5p9wk7") (yanked #t)))

