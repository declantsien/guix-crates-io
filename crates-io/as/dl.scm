(define-module (crates-io as dl) #:use-module (crates-io))

(define-public crate-asdl-1 (crate (name "asdl") (vers "1.0.0") (deps (list (crate-dep (name "insta") (req "^0.8.1") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^5") (features (quote ("std"))) (kind 0)))) (hash "15bmgnl2h7b7ilvhdkcwnldixl58cbr1xcc5xn8jzja7mqqm58j3")))

(define-public crate-asdl-1 (crate (name "asdl") (vers "1.0.1") (deps (list (crate-dep (name "difference") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "insta") (req "^0.8.1") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^5.0.1") (features (quote ("std"))) (kind 0)))) (hash "0mmagv9dv1z34gjym8d6bgpw5s6nzhjs8fxnri9ab0whf509akww")))

