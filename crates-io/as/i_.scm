(define-module (crates-io as i_) #:use-module (crates-io))

(define-public crate-asi_opengl-0.1 (crate (name "asi_opengl") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0amm3nyr8rfd64vwy122yjc2yn51wkq0w2c1k5v6qa6s410wqa9g")))

(define-public crate-asi_opengl-0.2 (crate (name "asi_opengl") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0zf000b38p5y1bjmn07c517gnxn7rp0iy5hvlv5hwqa5d21gjhcv")))

(define-public crate-asi_opengl-0.3 (crate (name "asi_opengl") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0pn97r1j1p00zsypqrv0vdqb7rgvh9kynppdz44hm23qwfjd6i0d")))

(define-public crate-asi_opengl-0.3 (crate (name "asi_opengl") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1mb0svpsq99hip2x85s7k3dfn21wgxp8rvxvq3k457j52hfznnd1")))

(define-public crate-asi_opengl-0.3 (crate (name "asi_opengl") (vers "0.3.2") (deps (list (crate-dep (name "dl_api") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "175iwlv3v2is5ddmzl3v2sfs48c3y3pzccadjvczi2vqlkpin0a7")))

(define-public crate-asi_opengl-0.3 (crate (name "asi_opengl") (vers "0.3.3") (deps (list (crate-dep (name "dl_api") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0cjyzw20h5367gm4v269zfqghyzbgl6wq8x3hwhafplq56z4b5ms")))

(define-public crate-asi_opengl-0.3 (crate (name "asi_opengl") (vers "0.3.4") (deps (list (crate-dep (name "dl_api") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1r4gmnaxqknrbiwyri39w6rd9qq8hxlszcgqfvmwv5vm966c9crr")))

(define-public crate-asi_opengl-0.4 (crate (name "asi_opengl") (vers "0.4.0") (deps (list (crate-dep (name "dl_api") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "038nwlpn6aqh1nqapdhb1sb2lggs8fa987xpss4gnpcv2002831k")))

(define-public crate-asi_opengl-0.5 (crate (name "asi_opengl") (vers "0.5.0") (deps (list (crate-dep (name "dl_api") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "09i0mbskcmy1ds06ds9a3abyb530dip4yhdh2vqczb7fpa613g6r")))

(define-public crate-asi_opengl-0.5 (crate (name "asi_opengl") (vers "0.5.1") (deps (list (crate-dep (name "dl_api") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0wsbbw3kpyiwmqald8cd4aw4r1920bslpd9nhn2a70z1cf9rwywz")))

(define-public crate-asi_opengl-0.6 (crate (name "asi_opengl") (vers "0.6.0") (deps (list (crate-dep (name "dl_api") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "19ww27yp1y9cvx6v3n75vyfqly4dfhr8zqjad81b43mbxpjc9s2m")))

(define-public crate-asi_opengl-0.6 (crate (name "asi_opengl") (vers "0.6.1") (deps (list (crate-dep (name "dl_api") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0i36j0njd8s6blvp6pnac6pzfn749mpp2gmpxwwa87f7n7gsa8rp")))

(define-public crate-asi_opengl-0.6 (crate (name "asi_opengl") (vers "0.6.2") (deps (list (crate-dep (name "dl_api") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1d018plvx2j392xml3yrjid344zrfldj910jzbzkbsfagix2g0ns")))

(define-public crate-asi_vulkan-0.1 (crate (name "asi_vulkan") (vers "0.1.0") (deps (list (crate-dep (name "afi") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "ami") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "^0.3.50") (default-features #t) (kind 1)))) (hash "0szzdwskkjqjki19sc1wsqp8fmqyaqhdvn7b60vv8y2y9nzkziyd") (features (quote (("validation") ("checks"))))))

(define-public crate-asi_vulkan-0.1 (crate (name "asi_vulkan") (vers "0.1.1") (deps (list (crate-dep (name "ami") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "^0.3.50") (default-features #t) (kind 1)))) (hash "13n4szxg6clda738z670zkxqwifi0vp3v97vqd76bqr6x7xxkxif") (features (quote (("validation") ("checks"))))))

(define-public crate-asi_vulkan-0.2 (crate (name "asi_vulkan") (vers "0.2.0") (deps (list (crate-dep (name "ami") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "^0.3.50") (default-features #t) (kind 1)))) (hash "0906xhvhp88nyy3bs5b9axqjys07ryldia3ha57qn7nxbdyw03y9") (features (quote (("validation") ("checks"))))))

(define-public crate-asi_vulkan-0.2 (crate (name "asi_vulkan") (vers "0.2.1") (deps (list (crate-dep (name "ami") (req "^0.5") (default-features #t) (kind 0)))) (hash "1gsy97j6rv3aywq3zl01qxl2yfcd704wzqk2fjamfkbk9v4k9h22") (features (quote (("validation") ("checks"))))))

(define-public crate-asi_vulkan-0.3 (crate (name "asi_vulkan") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1vk43x6xch0g5h8rxy6798d1hf0jxd1kc7jr4i6qxwak3h3g4fia") (features (quote (("default") ("checks"))))))

(define-public crate-asi_vulkan-0.4 (crate (name "asi_vulkan") (vers "0.4.0") (deps (list (crate-dep (name "ami") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0xj38p50j901azbpzbnbhfbzp7rc21c40dc8x1js3p3lhkkriizy") (features (quote (("default") ("checks"))))))

(define-public crate-asi_vulkan-0.5 (crate (name "asi_vulkan") (vers "0.5.0") (deps (list (crate-dep (name "ami") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1l04gc7xnsi4s9wi79if629nf41lfslcf3nyhy270jiavar71jvh") (features (quote (("default") ("checks"))))))

(define-public crate-asi_vulkan-0.6 (crate (name "asi_vulkan") (vers "0.6.0") (deps (list (crate-dep (name "ami") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "dl_api") (req "^0.1") (default-features #t) (kind 0)))) (hash "075yyp8wdw1prcapirnzpc60yckz8s96d1s6plahfxdpndd7ppzm") (features (quote (("default") ("checks"))))))

(define-public crate-asi_vulkan-0.7 (crate (name "asi_vulkan") (vers "0.7.0") (deps (list (crate-dep (name "ami") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "dl_api") (req "^0.1") (default-features #t) (kind 0)))) (hash "0snqgq4qdzws9fmxij662d1rn4n8hw2ga62cgmcg8ijfznw60lvq") (features (quote (("default") ("checks"))))))

(define-public crate-asi_vulkan-0.7 (crate (name "asi_vulkan") (vers "0.7.1") (deps (list (crate-dep (name "ami") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "dl_api") (req "^0.1") (default-features #t) (kind 0)))) (hash "1gmv1xvza9rm4mwdy5bbbq1r493w0y3pr0w0ynal1knakskmvn25") (features (quote (("default") ("checks"))))))

(define-public crate-asi_vulkan-0.7 (crate (name "asi_vulkan") (vers "0.7.2") (deps (list (crate-dep (name "ami") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "dl_api") (req "^0.1") (default-features #t) (kind 0)))) (hash "0iixks5naff0zmw8w8xvmi657i93kcfa5qnrcl4962vyq4hhzabw") (features (quote (("default") ("checks"))))))

(define-public crate-asi_vulkan-0.8 (crate (name "asi_vulkan") (vers "0.8.0") (deps (list (crate-dep (name "dl_api") (req "^0.2") (default-features #t) (kind 0)))) (hash "1l41mqhzscxsra9l0rh23mf3vk62kb4ayyyz53lcxsbjlj6cck6x") (features (quote (("default") ("checks"))))))

(define-public crate-asi_vulkan-0.9 (crate (name "asi_vulkan") (vers "0.9.0") (deps (list (crate-dep (name "awi") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "dl_api") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "euler") (req "^0.4") (default-features #t) (kind 0)))) (hash "0g05jc99q4aw52fz91ch4a4nn14jh56pjszgh2dc26s5w7hp2bxg") (features (quote (("default") ("checks"))))))

