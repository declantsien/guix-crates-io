(define-module (crates-io as hm) #:use-module (crates-io))

(define-public crate-ashmem-0.1 (crate (name "ashmem") (vers "0.1.0") (deps (list (crate-dep (name "ioctl-sys") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "15nzswpj2zy06ckwxpl7g8z4j1nc47h6fi6l9lqvcmgkkngm6fax") (yanked #t)))

(define-public crate-ashmem-0.1 (crate (name "ashmem") (vers "0.1.1") (deps (list (crate-dep (name "ioctl-sys") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1dknicg708w4mgz2vi0kyqazmkj941m7c44vhnqzzip1365sagdl")))

(define-public crate-ashmem-0.1 (crate (name "ashmem") (vers "0.1.2") (deps (list (crate-dep (name "ioctl-sys") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1b05c63zpb4p04q9dfrlfhggdkl9wm26d2vnlbd2jyhqj9vgfixr")))

