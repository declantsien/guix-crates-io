(define-module (crates-io as _b) #:use-module (crates-io))

(define-public crate-as_base-0.1 (crate (name "as_base") (vers "0.1.0") (deps (list (crate-dep (name "as_base_proc_macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0m77wsrkvsx8ci0xax0x12b2k5xp4m066lkz01kmabnd6hzwianv")))

(define-public crate-as_base-0.1 (crate (name "as_base") (vers "0.1.1") (deps (list (crate-dep (name "as_base_proc_macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0kh8yfm2znmdd7565h7wvlwxrhnypal0q0wm8ijpibnic6s2rh17")))

(define-public crate-as_base-0.1 (crate (name "as_base") (vers "0.1.2") (deps (list (crate-dep (name "as_base_proc_macro") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1l0cpyg1vnjh3jfqwrh5vamcg6sdv9fk00486k92qkcik2a8lybs")))

(define-public crate-as_base-0.1 (crate (name "as_base") (vers "0.1.3") (deps (list (crate-dep (name "as_base_proc_macro") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0n1fcsxfcjjahlwcdjblwf2rf4fcprbimm7992wi017ifhls68xw")))

(define-public crate-as_base_proc_macro-0.1 (crate (name "as_base_proc_macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-crate") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.28") (default-features #t) (kind 0)))) (hash "13zv1f668m16ffw459y2a5vgkh5g71gwnlirwjv82226cxr1k927")))

(define-public crate-as_base_proc_macro-0.1 (crate (name "as_base_proc_macro") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro-crate") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.28") (default-features #t) (kind 0)))) (hash "1xl3bmn2qajs2kvyq8bi1hfji4d4hf9n6gxnxj39cwdn29dfbdya")))

(define-public crate-as_bool-0.1 (crate (name "as_bool") (vers "0.1.0") (hash "0m0s6sanyym527lq7m59csjrjx9xmi288c1sbky2j768p7f8xdig")))

(define-public crate-as_bool-0.1 (crate (name "as_bool") (vers "0.1.1") (hash "1w99bjfzviy8dvwsfyjwgccdxgr54ccwfi1pz3csicrii577gwbp")))

(define-public crate-as_bool-0.1 (crate (name "as_bool") (vers "0.1.2") (hash "0p6xic5bjfypnzwfr0xbmixi2ggwz6avkfxcvh2p0wyx7pmhayc6")))

(define-public crate-as_bool-0.1 (crate (name "as_bool") (vers "0.1.3") (hash "1kg8g9b8725s6lbr8w97y8d5zdv4j969wkznpz96hs0ryhcfx1hl")))

