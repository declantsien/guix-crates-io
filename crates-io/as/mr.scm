(define-module (crates-io as mr) #:use-module (crates-io))

(define-public crate-asmr-0.0.1 (crate (name "asmr") (vers "0.0.1") (deps (list (crate-dep (name "nom") (req "^7.0") (default-features #t) (kind 0)) (crate-dep (name "nom-fields") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1szk2iw3k9pw0as32za1b8279m11n1fbhg6fqir6nmwxcz4b6cnf")))

