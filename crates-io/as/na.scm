(define-module (crates-io as na) #:use-module (crates-io))

(define-public crate-asnames-0.1 (crate (name "asnames") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "oneio") (req "^0.10.1") (features (quote ("remote"))) (kind 0)))) (hash "0w6s9nxf2avl7inl8kf3839pl1nkpdfwpk67wfigmpz5s1lbykn8")))

