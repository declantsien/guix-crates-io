(define-module (crates-io as -w) #:use-module (crates-io))

(define-public crate-as-witx-0.1 (crate (name "as-witx") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.1") (features (quote ("wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "witx") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0v1r2zglm6gnllvjiq3kja4fgw6mwr3b578vfs7fq65iwz4ij8ys")))

(define-public crate-as-witx-0.1 (crate (name "as-witx") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.1") (features (quote ("wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "witx") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0jhf1jiwrklc514r3c9spdxzyk64jzs13x2dws1g716xz8ghlf6k")))

