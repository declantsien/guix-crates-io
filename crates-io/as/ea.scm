(define-module (crates-io as ea) #:use-module (crates-io))

(define-public crate-asearch-0.1 (crate (name "asearch") (vers "0.1.0") (hash "07ljk1n0qd19z231hbr9f5h2w5blsrs2dz5q3xi8r48bw5zm9lc4")))

(define-public crate-asearch-0.1 (crate (name "asearch") (vers "0.1.1") (hash "0sp59h9kf2b0x72xiivbvi2lsdzfhi5j1rimxlgvhaw9rd48vsmb")))

(define-public crate-asearch-0.1 (crate (name "asearch") (vers "0.1.2") (hash "139mqh91a9zdaa7i6ilsrfgvz0w20x7r31s08ymbl113jqakqy1m")))

