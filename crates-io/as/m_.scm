(define-module (crates-io as m_) #:use-module (crates-io))

(define-public crate-asm_block-0.1 (crate (name "asm_block") (vers "0.1.0") (hash "1w8sjg4bg8ymfrqig9ccvrrhvgip0jglamgppq372cjcd5zfi0r3")))

(define-public crate-asm_block-0.1 (crate (name "asm_block") (vers "0.1.1") (hash "0kyb8b6aqcy7ssc126mcp2zzf3dg6a84cd4z5x4y7i1w0v7l9mqp")))

(define-public crate-asm_block-0.1 (crate (name "asm_block") (vers "0.1.2") (hash "1d3skiffa94xmhmi09karzrbm35677py3bxfk959ha3ar1dkyd2k")))

(define-public crate-asm_block-0.1 (crate (name "asm_block") (vers "0.1.3") (hash "1aqi5777yx3blrq6qirpn9m60a82srpw3z8r3wrhzvqmry80jv26")))

(define-public crate-asm_riscv-0.1 (crate (name "asm_riscv") (vers "0.1.0") (hash "0g1195d9fzgh23hacha7qzhss9r6zf2l8rvcqmsnxxyqrqp05b2s")))

(define-public crate-asm_riscv-0.2 (crate (name "asm_riscv") (vers "0.2.0") (hash "1gh1xc9fr9mz3klcjlrz9pb9b9nbpz5wzysc8h2sffgjcpx8iy19")))

(define-public crate-asm_unroll-0.1 (crate (name "asm_unroll") (vers "0.1.0") (hash "0d1pa0kl54hw5x68wi2c2fxmi912dcfad1jlax6wa64myqiq26x8")))

(define-public crate-asm_unroll-0.1 (crate (name "asm_unroll") (vers "0.1.1") (hash "16ygmzlg7v1qwpxkk6hqbfvvww4qf0qjyrw4pfqly9h1vc8hd2y4")))

