(define-module (crates-io as tc) #:use-module (crates-io))

(define-public crate-astc-decode-0.1 (crate (name "astc-decode") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.23") (default-features #t) (kind 2)))) (hash "1p8sc4lc74b34l4v4lycvsq5h7ris2fs8xkw7wliramjxcr8r8zm")))

(define-public crate-astc-decode-0.2 (crate (name "astc-decode") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.23") (default-features #t) (kind 2)))) (hash "04mi1gwkiysqkggxp8isgq94wbm15ssnvvlayxpbdm6z08a47vfa")))

(define-public crate-astc-decode-0.3 (crate (name "astc-decode") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.23") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.3") (default-features #t) (kind 2)))) (hash "051jx5ls0xigpr6bzbv1ql6d95bd59ng15kdlihniymb43xi3sgy")))

(define-public crate-astc-decode-0.3 (crate (name "astc-decode") (vers "0.3.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.23") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.3") (default-features #t) (kind 2)))) (hash "0srp1vvdjlx7kr3ws7q910jv13nafx5yr4jn8kxbf81f43k5hq1d")))

(define-public crate-astcenc-rs-0.1 (crate (name "astcenc-rs") (vers "0.1.0") (deps (list (crate-dep (name "astcenc-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "half") (req "^1.6") (default-features #t) (kind 0)))) (hash "0gc2b9cqxxn2a85yh3902rrgrw8f8sndycay96d55s97n5xdhglz")))

(define-public crate-astcenc-sys-0.1 (crate (name "astcenc-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.55") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "07axaln1ajm2rhjyab1dmjhjj90qbhzsvwjpg7vdahhy6q5xp13c")))

(define-public crate-astcenc-sys-0.1 (crate (name "astcenc-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.55") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1fhs8m4799ljmssddr7hd4an69v4rrn9qmlvgmknp7bkzrxrkirl")))

(define-public crate-astcenc-sys-0.1 (crate (name "astcenc-sys") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.55") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "012gbm8w7crvqwq9qs38a2p098qw71hiasyil8xw252qy1r6ig1n")))

(define-public crate-astcenc-sys-0.1 (crate (name "astcenc-sys") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.55") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1l2zbb7cgv59589a9l5lks3aaf7k5j60b1h2s75qmdc461yaz1cm")))

(define-public crate-astcenc-sys-0.1 (crate (name "astcenc-sys") (vers "0.1.5") (deps (list (crate-dep (name "bindgen") (req "^0.55") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "07z0lj59qlplg9sngfh0ph3kl0aa8xkvshbbbl21xvhglasky6pd")))

