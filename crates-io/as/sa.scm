(define-module (crates-io as sa) #:use-module (crates-io))

(define-public crate-assassin-0.0.0 (crate (name "assassin") (vers "0.0.0") (hash "0p46yhhsmnphhbvwbaacilhkkjzp5m5vfzfc2i2h0i92lxr9r5ib")))

(define-public crate-assay-0.1 (crate (name "assay") (vers "0.1.0") (deps (list (crate-dep (name "assay-proc-macro") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rusty-fork") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16.0") (features (quote ("rt-multi-thread"))) (default-features #t) (kind 0)))) (hash "19ln9ic41glh83ilrzwj5px3rv38pq91k62wmv6fizawrnm85393")))

(define-public crate-assay-0.1 (crate (name "assay") (vers "0.1.1") (deps (list (crate-dep (name "assay-proc-macro") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rusty-fork") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16.0") (features (quote ("rt-multi-thread"))) (default-features #t) (kind 0)))) (hash "0f0rw4vdnnbfdh10cvrqjl8xv44hdxr3vlm431zspznabiw00r4b")))

(define-public crate-assay-proc-macro-0.1 (crate (name "assay-proc-macro") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0pcnniy2zspcnwpyzgqz92nxc4izj48nambjncjf4px7qqvxcpqb")))

(define-public crate-assay-proc-macro-0.1 (crate (name "assay-proc-macro") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "07l8a02px058hh6hg4vpzz3g8r4wwmal205170divkrcawdi552x")))

(define-public crate-assayer-0.2 (crate (name "assayer") (vers "0.2.0") (hash "02zfrcqv15pcy7jhkls4dr76kwyvg9172bz0r2nz23s8bk2wgdgi")))

(define-public crate-assayer-0.2 (crate (name "assayer") (vers "0.2.1") (hash "1k7xjrwniv354kz0ds7fvdc2dpic9psk08n64738s3va1sp8hvdz")))

(define-public crate-assayer-0.2 (crate (name "assayer") (vers "0.2.2") (hash "16wr39x0i6j0g4s4648pv6fdi1hnvb3fafxn3zvyjpy0diga71vy")))

