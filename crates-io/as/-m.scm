(define-module (crates-io as -m) #:use-module (crates-io))

(define-public crate-as-method-0.1 (crate (name "as-method") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "116h9g1jidbl6r41a79jmsnrr47223gzg196q1mrp074iacjb9dm")))

