(define-module (crates-io as -r) #:use-module (crates-io))

(define-public crate-as-raw-xcb-connection-0.1 (crate (name "as-raw-xcb-connection") (vers "0.1.0") (hash "0is9k1gsx2a7809qcvrxgv7r4d6z076axc13js30anl43l90w1s5")))

(define-public crate-as-raw-xcb-connection-1 (crate (name "as-raw-xcb-connection") (vers "1.0.0") (hash "1am99fbsp5f5vnbvr0qnjma36q49c9zvdbn0czwwvian18mk2prd") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-as-raw-xcb-connection-1 (crate (name "as-raw-xcb-connection") (vers "1.0.1") (hash "0sqgpz2ymv5yx76r5j2npjq2x5qvvqnw0vrs35cyv30p3pfp2m8p") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-as-result-0.1 (crate (name "as-result") (vers "0.1.0") (hash "01v3kvi8s81scgwg92rk8j7scy2c2d4zvqvm80pgh5m6srgvycsl")))

(define-public crate-as-result-0.2 (crate (name "as-result") (vers "0.2.0") (hash "0rpk7licrkfq5isw54j6ypdnjfvgglic9hzf3pzivylyb4264wq0")))

(define-public crate-as-result-0.2 (crate (name "as-result") (vers "0.2.1") (hash "1dx8avnj0hnckb6x39nwhcqpqzy8qi81dr2scp6i0530q71wl0ip")))

