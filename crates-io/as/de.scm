(define-module (crates-io as de) #:use-module (crates-io))

(define-public crate-asdev-0.1 (crate (name "asdev") (vers "0.1.1") (deps (list (crate-dep (name "dialoguer") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1hsi31d7dmvi3rm9f42mh61khvy12z4ypik0xrk6hsg0b6xf8rqy")))

(define-public crate-asdev-0.1 (crate (name "asdev") (vers "0.1.2") (deps (list (crate-dep (name "dialoguer") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1i1gglvsz2czd33p3sr22hspr38qs3l7ldw33wy6zx2mv74vxji2")))

(define-public crate-asdev-0.1 (crate (name "asdev") (vers "0.1.3") (deps (list (crate-dep (name "dialoguer") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "05d00cm5vq98sk6d2q24ssccxg8y4hjjymlm62wnfrfribi6zvnf")))

(define-public crate-asdev-0.1 (crate (name "asdev") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "02l57m9m686nlry09rkhhs31q7ylfnxhss1awb0r77hf8imw5r2i")))

(define-public crate-asdev-0.1 (crate (name "asdev") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0kwmv6fwrl04ywk0my3aa3a971z2d9bd8skkicidpsv2py8hs3z7")))

