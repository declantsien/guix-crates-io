(define-module (crates-io as -b) #:use-module (crates-io))

(define-public crate-as-byte-sequence-0.1 (crate (name "as-byte-sequence") (vers "0.1.0") (hash "1iaj72ifbfn1xs8l7lcm522la94sprvm4zgwga9w3585sr4q5lxf")))

(define-public crate-as-bytes-0.1 (crate (name "as-bytes") (vers "0.1.0") (hash "1d1vl54z5v42g4gafl43h2daw4a4v2ia16az4csvj9wq1xqr808g")))

(define-public crate-as-bytes-0.2 (crate (name "as-bytes") (vers "0.2.0") (hash "1rrmzm3bpmxshn9wy1x51z9nvz1a0kvha64l6p7fb6lj7s9jnxz8")))

