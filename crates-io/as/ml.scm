(define-module (crates-io as ml) #:use-module (crates-io))

(define-public crate-asml-iomod-registry-common-0.1 (crate (name "asml-iomod-registry-common") (vers "0.1.0") (deps (list (crate-dep (name "asml_iomod_dynamodb") (req "^0.1.5") (default-features #t) (kind 0) (package "assemblylift-iomod-dynamodb-guest")) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1034rx8xgfv7swc6px95rx107c0f1ff2y50r3fn70f1516j6dlwl")))

(define-public crate-asml-iomod-registry-common-0.1 (crate (name "asml-iomod-registry-common") (vers "0.1.1") (deps (list (crate-dep (name "asml_iomod_dynamodb") (req "^0.1.5") (default-features #t) (kind 0) (package "assemblylift-iomod-dynamodb-guest")) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "19ya3j5pkd85yjkiagzcv9imvymlh4nhawb359p0rf1wvqrvcmp6")))

(define-public crate-asml-iomod-registry-common-0.1 (crate (name "asml-iomod-registry-common") (vers "0.1.2") (deps (list (crate-dep (name "asml_iomod_dynamodb") (req "^0.1.5") (default-features #t) (kind 0) (package "assemblylift-iomod-dynamodb-guest")) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1mx3jlai0fc4wp0j6n5spzz1z0bx1bbpkk1f9rlc0v74f7mm7c4z")))

(define-public crate-asml-iomod-registry-common-0.1 (crate (name "asml-iomod-registry-common") (vers "0.1.3") (deps (list (crate-dep (name "asml_iomod_dynamodb") (req "^0.1.5") (default-features #t) (kind 0) (package "assemblylift-iomod-dynamodb-guest")) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0i4jb7bv5kwsdpnjyl37yc8hm67qx9zkc0zcqwybf22b0ryi7yhb")))

(define-public crate-asml-iomod-registry-common-0.1 (crate (name "asml-iomod-registry-common") (vers "0.1.4") (deps (list (crate-dep (name "asml_iomod_dynamodb") (req "^0.1") (default-features #t) (kind 0) (package "assemblylift-iomod-dynamodb-guest")) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "05b3k17zhvhd10y0058lkb5dram6wfak2bkh4gykby9bdwhbfz2z")))

