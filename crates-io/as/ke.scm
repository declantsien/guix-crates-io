(define-module (crates-io as ke) #:use-module (crates-io))

(define-public crate-asker-0.1 (crate (name "asker") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0pd5syhwzi2jlnmr0b5bwqk3aj0a96mpwydzhwck7xyjayl9hxkp")))

(define-public crate-asker-0.2 (crate (name "asker") (vers "0.2.0") (deps (list (crate-dep (name "crossterm") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "safe_print") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "13rwhw2p8xiax8bs35kbc1d0gqk4bpw7sphdq5i4k5dynn7gm6z6")))

(define-public crate-asker-0.3 (crate (name "asker") (vers "0.3.0") (deps (list (crate-dep (name "crossterm") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "safe_print") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "18qqa1d999w6qiimhdfj23ncsb7rq0gs418biy6ahlp40ii6namd")))

