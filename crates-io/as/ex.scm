(define-module (crates-io as ex) #:use-module (crates-io))

(define-public crate-asex-0.1 (crate (name "asex") (vers "0.1.0") (deps (list (crate-dep (name "pin-project-lite") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-test") (req "^0.4") (default-features #t) (kind 2)))) (hash "13pjjhkby7k8fxbfrhzv9j6m33mn9jg9wnk4jzi8fk474zi39x5l")))

(define-public crate-asex-0.2 (crate (name "asex") (vers "0.2.0") (deps (list (crate-dep (name "pin-project-lite") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-test") (req "^0.4") (default-features #t) (kind 2)))) (hash "0nxqh9ssnj81h6z954v481cfi858nwlnq4dywfggcd8qqd0w8mfi")))

(define-public crate-asex-0.3 (crate (name "asex") (vers "0.3.0") (deps (list (crate-dep (name "pin-project-lite") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-test") (req "^0.4") (default-features #t) (kind 2)))) (hash "1yqx5lj4p7248miyw3mjxcsjrf6jcl9gbqliv937m57i1qgvkf6d")))

(define-public crate-asexp-0.1 (crate (name "asexp") (vers "0.1.0") (hash "0y34qjqmxqxpp0rcs36v7ldy09sshhjaap58zbw94xvcwsr2gq5f")))

(define-public crate-asexp-0.1 (crate (name "asexp") (vers "0.1.1") (hash "0vqnfhi1n6k76yqh0rll8v8ayzqlf005xpw19nhs6gjbvmjk0g17")))

(define-public crate-asexp-0.1 (crate (name "asexp") (vers "0.1.2") (hash "1d7ih0hncy49lf4m32v57lwn9h1r6pvwzm4jj9hgd5hksn1k7k6j")))

(define-public crate-asexp-0.2 (crate (name "asexp") (vers "0.2.0") (deps (list (crate-dep (name "grabinput") (req "^0.1") (default-features #t) (kind 2)))) (hash "0civdwb3ki931c9mg5qacc6axb6rd7mazj40bfwxqrnkdhmzv6gp")))

(define-public crate-asexp-0.3 (crate (name "asexp") (vers "0.3.0") (deps (list (crate-dep (name "grabinput") (req "^0.1") (default-features #t) (kind 2)))) (hash "1hkhg2k5ijncfwzxa56kvyz224cyqdwi1wkavf643mdzj52198m8")))

(define-public crate-asexp-0.3 (crate (name "asexp") (vers "0.3.1") (deps (list (crate-dep (name "grabinput") (req "^0.1") (default-features #t) (kind 2)))) (hash "06w31j5dfmrnvk16fqr934wwyqaxddxf8kk1lic13n2gwyhsyckp")))

(define-public crate-asexp-0.3 (crate (name "asexp") (vers "0.3.2") (deps (list (crate-dep (name "grabinput") (req "^0.1") (default-features #t) (kind 2)))) (hash "0li6h191ppfyrsv6iwppbaxsmcbpc3sb2b8wgwq4g2bmrrhqfdjy")))

