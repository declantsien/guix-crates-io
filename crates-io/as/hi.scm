(define-module (crates-io as hi) #:use-module (crates-io))

(define-public crate-ashiba-0.1 (crate (name "ashiba") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "10m49amp9i6m7n4iirb9f3n45sgf4lh0a2bc8f0rm6dww1g3rv3k")))

