(define-module (crates-io as no) #:use-module (crates-io))

(define-public crate-asnom-0.1 (crate (name "asnom") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "1adkwznxxv4xmwx28vdmynwl4kkzcvmhxfpqzqcmwv6w6n6gy99h")))

