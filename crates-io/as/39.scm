(define-module (crates-io as #{39}#) #:use-module (crates-io))

(define-public crate-as3935-0.1 (crate (name "as3935") (vers "0.1.0-pre.1") (deps (list (crate-dep (name "chrono") (req "^0.4.7") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)) (crate-dep (name "simple-signal") (req "^1.1.1") (default-features #t) (kind 2)) (crate-dep (name "simple_logger") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "01zcplafqd1pa0h3a0pzmrqcc96mm9l7ca22bg9ry7mm2xwxlyfd")))

(define-public crate-as3935-bbn-0.1 (crate (name "as3935-bbn") (vers "0.1.0-pre.2") (deps (list (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.7") (default-features #t) (kind 2)) (crate-dep (name "simple-signal") (req "^1.1.1") (default-features #t) (kind 2)) (crate-dep (name "simple_logger") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "0k1dv7p8z6ppravkzq05qixwhy92hs3hi426ryj78smlmj2r73gm")))

(define-public crate-as3956-0.0.0 (crate (name "as3956") (vers "0.0.0") (hash "0d63417z1r3wglg2bd6h657g5rrzkg85jz9gl5i6mggwsxj3yl5q")))

