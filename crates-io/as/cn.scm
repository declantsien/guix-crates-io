(define-module (crates-io as cn) #:use-module (crates-io))

(define-public crate-ascn-rs-0.1 (crate (name "ascn-rs") (vers "0.1.0") (deps (list (crate-dep (name "indicatif") (req "^0.17.3") (default-features #t) (kind 0)) (crate-dep (name "pgn-reader") (req "^0.23.0") (default-features #t) (kind 0)) (crate-dep (name "shakmaty") (req "^0.24.0") (default-features #t) (kind 0)))) (hash "0a7i2nxh6hhm205mwg8l50dkxr9dn8v1g9qq6lxpdq1spaknm3h3")))

(define-public crate-ascn-rs-0.2 (crate (name "ascn-rs") (vers "0.2.0") (deps (list (crate-dep (name "chess") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.3") (default-features #t) (kind 0)) (crate-dep (name "pgn-rs") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1kkpb2pcl4563qwimh33x7b90imxyynhxprfllc1pwqxq32wy8h5")))

(define-public crate-ascn-rs-0.3 (crate (name "ascn-rs") (vers "0.3.0") (deps (list (crate-dep (name "chess") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.3") (default-features #t) (kind 0)) (crate-dep (name "pgn-rs") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "0x1fbp0k6ks863i9w1rrxb6sms678kk2bq8spn6jh2q3yh4ginaa")))

