(define-module (crates-io as #{50}#) #:use-module (crates-io))

(define-public crate-as5048a-0.1 (crate (name "as5048a") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0bmf07hvmf1a5qm8s8nss0r2p0lidb48x7hrrvshqz7k92d6siqg")))

(define-public crate-as5048a-0.2 (crate (name "as5048a") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0f3h8pwyvmnpnkw6pyqca6szjm6rs3pxicvy9frdai4w7sd5sm90")))

(define-public crate-as5048a-0.2 (crate (name "as5048a") (vers "0.2.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "0zqncl1bipfx504yznpmz0aspv6m7gh407fzmrrqigipsrajz1m3")))

