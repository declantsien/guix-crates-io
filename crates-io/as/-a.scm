(define-module (crates-io as -a) #:use-module (crates-io))

(define-public crate-as-any-0.1 (crate (name "as-any") (vers "0.1.0") (hash "0y6qvmf6z2gzay8lshzmplia23rghsnqmdc55j4sykq4lsxij577") (features (quote (("alloc"))))))

(define-public crate-as-any-0.2 (crate (name "as-any") (vers "0.2.0") (hash "1r7sjg284vs6msm2l8jq3g6shlf3zk8l23mx1m3nr5v3v0244mhb") (features (quote (("alloc"))))))

(define-public crate-as-any-0.2 (crate (name "as-any") (vers "0.2.1") (hash "1jx418fg66z2m4gvgymp9lgqifkgaxi7qqnrrkkmirkpcqscp308") (features (quote (("alloc"))))))

(define-public crate-as-any-0.3 (crate (name "as-any") (vers "0.3.0") (hash "19rz5hgvhb257j7pj5abldpdlaz2xwzhr6pjy3k6fnczrkp1jd4c")))

(define-public crate-as-any-0.3 (crate (name "as-any") (vers "0.3.1") (hash "0fn9hj7m4xk8n8yrad4i3hm9b268cg2rhqiarcyci8cr9sj312jv")))

