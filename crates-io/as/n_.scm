(define-module (crates-io as n_) #:use-module (crates-io))

(define-public crate-asn_codecs_derive-0.1 (crate (name "asn_codecs_derive") (vers "0.1.0") (deps (list (crate-dep (name "asn-codecs") (req "=0.1.0") (default-features #t) (kind 0)) (crate-dep (name "bitvec") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "01k6qxpvw9qdgwbrz1qh0wid1wxsjgfzx16p022kdwv2ld0ask32")))

