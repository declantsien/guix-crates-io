(define-module (crates-io as ai) #:use-module (crates-io))

(define-public crate-asai-0.1 (crate (name "asai") (vers "0.1.0") (hash "0qq8j90svicf5pq7vvf8fi3any44bsbah1bq8vca57gz1q3lbkbn") (yanked #t)))

(define-public crate-asai-0.1 (crate (name "asai") (vers "0.1.1") (deps (list (crate-dep (name "asai-macro") (req "^0.1") (default-features #t) (kind 0)))) (hash "1kwn1ib85c9cp48c0jp2a7kd5s86ggls2nqal77xvil4r88cbgxb") (yanked #t)))

(define-public crate-asai-0.1 (crate (name "asai") (vers "0.1.2") (deps (list (crate-dep (name "asai-macro") (req "^0.1") (default-features #t) (kind 0)))) (hash "0mshvrbbkav01776hbvia10mg8pajf4z584rg8f1hh8h28cjms7j")))

(define-public crate-asai-macro-0.1 (crate (name "asai-macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.27") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "09m7lz5scdcjh9bjdwngcwnb12gw67ccq0ms54k57rmrxps65gyw")))

