(define-module (crates-io as _a) #:use-module (crates-io))

(define-public crate-as_any_min-1 (crate (name "as_any_min") (vers "1.0.0") (hash "1l5s22ixsrk6d8hqbik4w51nr8bmpk71vqs8ljww49cl5jk4gxf1")))

(define-public crate-as_any_min-1 (crate (name "as_any_min") (vers "1.0.1") (hash "1wc5m1p6sj8acqsf7011b65ifkn2lhfry55av005lg06rm88xfd7")))

(define-public crate-as_any_min-1 (crate (name "as_any_min") (vers "1.0.2") (hash "057j6f0rdz1yhzlp7lgbq95rwasbscwsvlzjcg6c159r42kiykvf")))

