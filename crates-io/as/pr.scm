(define-module (crates-io as pr) #:use-module (crates-io))

(define-public crate-asprim-0.1 (crate (name "asprim") (vers "0.1.0") (hash "144imsrc5w436rfi3mfy7rb296xyzklglxiy7jg2lfzaih4r1gf1")))

(define-public crate-asprim-0.1 (crate (name "asprim") (vers "0.1.1") (hash "16ky0wdkh3bkpyjmgmvsksf1gawflsylqyn03xmk34ycl5fzcvir")))

(define-public crate-asprim-0.2 (crate (name "asprim") (vers "0.2.0") (hash "10sgj6qg3wbz0xmv96inm844sm3vl1yyaz116a67h41axlrhkwpw")))

