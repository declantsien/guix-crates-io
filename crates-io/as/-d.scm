(define-module (crates-io as -d) #:use-module (crates-io))

(define-public crate-as-dyn-trait-0.1 (crate (name "as-dyn-trait") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.16") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "11zc5jakjbdhmzdr6fsrvpb98ddnjclw28jsb2svvmw2ydqb0rls")))

(define-public crate-as-dyn-trait-0.2 (crate (name "as-dyn-trait") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.16") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "04g7nbmllg06z8062jzs8whyiznhs5cs0w2fq6cc4m9hbdrfc41w")))

