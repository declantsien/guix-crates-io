(define-module (crates-io as -f) #:use-module (crates-io))

(define-public crate-as-ffi-bindings-0.1 (crate (name "as-ffi-bindings") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "wasmer") (req "^2.0") (default-features #t) (kind 0)))) (hash "1mdww4sawxa373484lrigx48iybbv198w8w601bir8vywxg5hcxv")))

(define-public crate-as-ffi-bindings-0.1 (crate (name "as-ffi-bindings") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "wasmer") (req "^2.0") (default-features #t) (kind 0)))) (hash "1j1q4vb0629y0fsm0jly7pg1bq1x1ipmhyghydjybaww1b41l2zj")))

(define-public crate-as-ffi-bindings-0.2 (crate (name "as-ffi-bindings") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "wasmer") (req "^2.0") (default-features #t) (kind 0)))) (hash "0a3cwgw2vs373ij1f240sr7zp3rkm1sn04fx2k5syj7axg5fa44k")))

(define-public crate-as-ffi-bindings-0.2 (crate (name "as-ffi-bindings") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "wasmer") (req "^2.0") (default-features #t) (kind 0)))) (hash "06219jpx2f09hcrfg6j3rlq2klx34n5df3jl0b5j3hbyxll03nqm") (rust-version "1.56.1")))

(define-public crate-as-ffi-bindings-0.2 (crate (name "as-ffi-bindings") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "wasmer") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "0ydv4ppx8fgqw6pmfbvbycic45a7b72hjn92l04xgdzi89f80xsj") (rust-version "1.56.1")))

(define-public crate-as-ffi-bindings-0.2 (crate (name "as-ffi-bindings") (vers "0.2.3") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "wasmer") (req "^2.2.0-rc1") (default-features #t) (kind 0)))) (hash "1rl3ml517rdy3212w880d9a4sf4jlbgf980ngl22xqkrm21m2d4d") (rust-version "1.56.1")))

(define-public crate-as-ffi-bindings-0.2 (crate (name "as-ffi-bindings") (vers "0.2.3-anyptr") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "wasmer") (req "^2.2.1") (default-features #t) (kind 0)))) (hash "0gyw9jhbdq9qzwd5dzx169xmlfzzy4jmr2ydi4j0qg22mwr88fvz") (rust-version "1.56.1")))

(define-public crate-as-ffi-bindings-0.2 (crate (name "as-ffi-bindings") (vers "0.2.4") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "wasmer") (req "^2.2.1") (default-features #t) (kind 0)))) (hash "0im5idmjxpvz81jph53xy2zq1ynvhm89f7yw4f5cx43l12m8bylg") (rust-version "1.56.1")))

(define-public crate-as-ffi-bindings-0.2 (crate (name "as-ffi-bindings") (vers "0.2.5") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "wasmer") (req "^2.3.0") (default-features #t) (kind 0)))) (hash "04jz0sl983y67wffvr8ga4ndc8vls5bi249h0bkhh5rrh0jzxiw2") (rust-version "1.56.1")))

(define-public crate-as-ffi-bindings-0.2 (crate (name "as-ffi-bindings") (vers "0.2.6") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "wasmer") (req "^2.3.0") (default-features #t) (kind 0)))) (hash "183hwaly2fgj34064i9axniwc4mlndd09mrc509s64hvy016qwv3") (features (quote (("no_thread")))) (rust-version "1.56.1")))

(define-public crate-as-ffi-bindings-0.2 (crate (name "as-ffi-bindings") (vers "0.2.7") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "wasmer") (req "^2.3.0") (default-features #t) (kind 0)))) (hash "0jdhfywr748ap9w2mj8zx4v6ddbcssg5mw1cwm3j3yp0cg9ys212") (features (quote (("no_thread")))) (rust-version "1.56.1")))

