(define-module (crates-io as _t) #:use-module (crates-io))

(define-public crate-as_tuple-0.1 (crate (name "as_tuple") (vers "0.1.0") (deps (list (crate-dep (name "as_tuple_derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "0865kg3nhzf36xh4ijx2jvm0ykdlsry8n33sbx4584r0219viasv")))

(define-public crate-as_tuple_derive-0.1 (crate (name "as_tuple_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1l2yrnh6w02s3z4df2n5pwvyj2l11hazqcb5r22r6qsa4g33vhwr")))

