(define-module (crates-io as db) #:use-module (crates-io))

(define-public crate-asdb-taxa-0.1 (crate (name "asdb-taxa") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.97") (default-features #t) (kind 0)))) (hash "0xrglvial5ja4kapx984qvli4achmcml4v3shb90kcs30hqpzgvd")))

