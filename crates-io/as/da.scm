(define-module (crates-io as da) #:use-module (crates-io))

(define-public crate-asdalvlkajbldkjba-0.1 (crate (name "asdalvlkajbldkjba") (vers "0.1.0") (hash "02d401rqgridb27yy6m81dp4mrkz3hw2zd9slpsspdcygrzr913z")))

(define-public crate-asdalvlkajbldkjba-0.2 (crate (name "asdalvlkajbldkjba") (vers "0.2.0") (hash "0dvvl668bwsjp2pbxwsfyhhpf9d8xgahqssm85nqnqcxfw03r6kg")))

(define-public crate-asdalvlkajbldkjba-0.1 (crate (name "asdalvlkajbldkjba") (vers "0.1.1") (deps (list (crate-dep (name "wee_alloc") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0i27dz08pjh8k52plw90m7zgnd95zlza7bvbj6rmlgflajsbr9mc")))

(define-public crate-asdalvlkajbldkjba-0.1 (crate (name "asdalvlkajbldkjba") (vers "0.1.2") (deps (list (crate-dep (name "wee_alloc") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0fbirvr6g4hqy04zcra6psw2hbbgggxzrg20vnpdyxnjn92c7szl")))

