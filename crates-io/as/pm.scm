(define-module (crates-io as pm) #:use-module (crates-io))

(define-public crate-aspmatch-0.1 (crate (name "aspmatch") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "~2.33") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0iqx9v3xc3dk78b9pn44c384s5cl0q82b7y91pqfmx68vhm920lm")))

