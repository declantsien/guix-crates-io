(define-module (crates-io as mu) #:use-module (crates-io))

(define-public crate-asmu-0.0.0 (crate (name "asmu") (vers "0.0.0") (hash "028f9dx8kv6935myw4lnp5vdms43wwb5ggg3waj2x8xsayj55hsl")))

(define-public crate-asmuth_bloom_secret_sharing-0.2 (crate (name "asmuth_bloom_secret_sharing") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.25.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1b09ssd45q7qgq2xrpr9npxa0m00c2143nzbyqlvj00s03aq0kki")))

(define-public crate-asmuth_bloom_secret_sharing-0.2 (crate (name "asmuth_bloom_secret_sharing") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2.25.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0x0vaxhbh12c4wrv3mlx8qz99zljyg0z49149w45l0cqaqbaxyb8")))

(define-public crate-asmuth_bloom_secret_sharing-0.2 (crate (name "asmuth_bloom_secret_sharing") (vers "0.2.2") (deps (list (crate-dep (name "built") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "clap") (req "^2.25.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "01m1n4vs8q4m0kybgwayi1yd0gcyhfac1wy350imbyif9cy33iv5")))

