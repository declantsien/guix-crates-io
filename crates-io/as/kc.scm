(define-module (crates-io as kc) #:use-module (crates-io))

(define-public crate-askcommand-0.1 (crate (name "askcommand") (vers "0.1.0") (deps (list (crate-dep (name "async-openai") (req "^0.16.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "12zi3vcp3kfvqyqfl704zwwinhnfams953sc3a43185hqsms1x3i")))

