(define-module (crates-io as he) #:use-module (crates-io))

(define-public crate-asherpa-0.0.1 (crate (name "asherpa") (vers "0.0.1") (deps (list (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.23") (features (quote ("rustls-tls"))) (kind 0)) (crate-dep (name "tempfile") (req "^3.9.0") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("rt" "macros"))) (default-features #t) (kind 2)))) (hash "14rbsq3hnhys6x2g1s2vhlja5r9ljbxiihf93waij4dhdk8af6bm")))

