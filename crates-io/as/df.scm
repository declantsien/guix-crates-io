(define-module (crates-io as df) #:use-module (crates-io))

(define-public crate-asdf-0.1 (crate (name "asdf") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.9.13") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9.13") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.9.10") (default-features #t) (kind 0)))) (hash "1wdbhzhna37v7b8d4phkj4jvj7z6lwbyljq5qwqrq4sr9hqm8g15")))

(define-public crate-asdf-pixel-sort-0.0.1 (crate (name "asdf-pixel-sort") (vers "0.0.1") (deps (list (crate-dep (name "image") (req "^0.23") (kind 0)))) (hash "15j7q8ndag86z6fr7sr22pnvmxpgn34kwm8wm1yzk8pp4c037k1x")))

(define-public crate-asdf-pixel-sort-0.0.2 (crate (name "asdf-pixel-sort") (vers "0.0.2") (deps (list (crate-dep (name "image") (req "^0.23") (kind 0)) (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 2)))) (hash "1015mirlsm6jpl18sp8z59gjja0x9g14shkxwy56rm4vf6s100bx")))

(define-public crate-asdf-pixel-sort-0.0.3 (crate (name "asdf-pixel-sort") (vers "0.0.3") (deps (list (crate-dep (name "image") (req "^0.23") (kind 0)) (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 2)))) (hash "0c0igih9r5cpk52nsrcngwmpjf6nfpx6fvaqs4dv1f38hwsvzdss")))

(define-public crate-asdf-pixel-sort-0.0.4 (crate (name "asdf-pixel-sort") (vers "0.0.4") (deps (list (crate-dep (name "image") (req "^0.23") (kind 0)) (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 2)))) (hash "1zxw72va7nm1wy43qrihql4zybs6bkdnlvw6ly0yp58scdcia86z")))

(define-public crate-asdf-pixel-sort-0.0.5 (crate (name "asdf-pixel-sort") (vers "0.0.5") (deps (list (crate-dep (name "image") (req "^0.23") (kind 0)) (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "1xcn4n68dahb83gkcyqkkdzwbasws9mk094iimnzlscim5dmfxgc")))

(define-public crate-asdf-pixel-sort-0.0.6 (crate (name "asdf-pixel-sort") (vers "0.0.6") (deps (list (crate-dep (name "image") (req "^0.23.14") (kind 0)) (crate-dep (name "image") (req "^0.23.14") (features (quote ("bmp"))) (kind 2)) (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "1kxi9nf2iqqly9nd158jfp21r9hzqdyfvphg7zcm6kr5q53p0gn7")))

(define-public crate-asdf-pixel-sort-0.0.7 (crate (name "asdf-pixel-sort") (vers "0.0.7") (deps (list (crate-dep (name "image") (req "^0.23.14") (kind 0)) (crate-dep (name "image") (req "^0.23.14") (features (quote ("bmp"))) (kind 2)) (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "14y360ccgiq8gzfqax0vv8mim8bhq79ynymqr6iqsn9mfyx6bsb8")))

(define-public crate-asdf-pixel-sort-0.1 (crate (name "asdf-pixel-sort") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.23.14") (kind 0)) (crate-dep (name "image") (req "^0.23.14") (features (quote ("bmp"))) (kind 2)) (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "1swa1imf32b1xnqff22y0jmr9rjl15s7hp2nxirp2abzq3v7n1mc")))

(define-public crate-asdf-pixel-sort-0.1 (crate (name "asdf-pixel-sort") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.23.14") (kind 0)) (crate-dep (name "image") (req "^0.23.14") (features (quote ("bmp"))) (kind 2)) (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "0sam14dlv3dy5jb32q4z4kfbg1f617r43ff80bnps4q2qa9id4gs")))

(define-public crate-asdf-pixel-sort-0.2 (crate (name "asdf-pixel-sort") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.23.14") (kind 0)) (crate-dep (name "image") (req "^0.23.14") (features (quote ("bmp"))) (kind 2)) (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "1rdpvbs4dmybzazgy9mq3xjwq2scqfd22s34chrwawwxhf18yamy")))

(define-public crate-asdf-qwer-0.1 (crate (name "asdf-qwer") (vers "0.1.0") (hash "1phyg389iis4kkmcp1bcv9mk8qhf2hlcjz3fvp4rjbfnqg5jl9d9")))

(define-public crate-asdfg-0.1 (crate (name "asdfg") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "open") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "1l7420pvpks45hdmkw5nk6sj4v1q66g2nq48mi0mri17524jw0k5")))

(define-public crate-asdfg-0.2 (crate (name "asdfg") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0") (default-features #t) (kind 0)) (crate-dep (name "open") (req "^5.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "1fqh4cqgsdx1m02crqgm1jg1rwwdnr3npl54v8pvnlk09zlya50z")))

(define-public crate-asdfler-0.4 (crate (name "asdfler") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2") (features (quote ("cargo" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.157") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.19") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^4.4.0") (default-features #t) (kind 0)))) (hash "1fcjlgrbgpsffmvr4660n5jkq87r31akg98hm98czakgbi4m29xq")))

