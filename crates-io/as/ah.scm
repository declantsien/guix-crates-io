(define-module (crates-io as ah) #:use-module (crates-io))

(define-public crate-asahi-bless-0.1 (crate (name "asahi-bless") (vers "0.1.0") (deps (list (crate-dep (name "apple-nvram") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "gpt") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1") (default-features #t) (kind 0)))) (hash "0v9bswzblmjhhg4s6jb9fsvcnlqy43av0p8k3z59h2a3b3yb6yp6")))

(define-public crate-asahi-bless-0.2 (crate (name "asahi-bless") (vers "0.2.0") (deps (list (crate-dep (name "apple-nvram") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "gpt") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1") (default-features #t) (kind 0)))) (hash "0dabbbnd39hma2psbn8vyzdc8jpjm6znxw4cf23pwd5fcyfq0qzp")))

(define-public crate-asahi-bless-0.2 (crate (name "asahi-bless") (vers "0.2.1") (deps (list (crate-dep (name "apple-nvram") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "gpt") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1") (default-features #t) (kind 0)))) (hash "0x153750d3pn6pwvwpk348n66xv4vxd5q4z0wqvw4qv1yl1wzl5h")))

(define-public crate-asahi-bless-0.3 (crate (name "asahi-bless") (vers "0.3.0") (deps (list (crate-dep (name "apple-nvram") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "gpt") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1") (default-features #t) (kind 0)))) (hash "1x5g6pb5i3zgrlxvdzzrv58sp647cp5z8vdp3gp7czdlbyy1rhpx")))

(define-public crate-asahi-btsync-0.1 (crate (name "asahi-btsync") (vers "0.1.0") (deps (list (crate-dep (name "apple-nvram") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "rust-ini") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "1zkq2sl2w2bgpr3zjiqdp0x0mnf2mgxzrk723dc932d0dwjyi8dj")))

(define-public crate-asahi-btsync-0.2 (crate (name "asahi-btsync") (vers "0.2.0") (deps (list (crate-dep (name "apple-nvram") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "rust-ini") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "1a3hd2adg81k8cf3l4bpn99fg7bw6lcj5c3snjs89s8m9h4gyy5y")))

(define-public crate-asahi-nvram-0.1 (crate (name "asahi-nvram") (vers "0.1.0") (deps (list (crate-dep (name "apple-nvram") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("cargo"))) (default-features #t) (kind 0)))) (hash "131dfrkvmwim806i53292aij226kqvp395260bqxi55by04iid3a")))

(define-public crate-asahi-nvram-0.2 (crate (name "asahi-nvram") (vers "0.2.0") (deps (list (crate-dep (name "apple-nvram") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("cargo"))) (default-features #t) (kind 0)))) (hash "0b0pig15vw05zl44hpdwlzk84d7c871a28bqvl501gl718ljfkg9")))

(define-public crate-asahi-nvram-0.2 (crate (name "asahi-nvram") (vers "0.2.1") (deps (list (crate-dep (name "apple-nvram") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("cargo"))) (default-features #t) (kind 0)))) (hash "04qp73w97792flzkv9b3qb549iwa9mam837b0l8zh04lcnbcgci6")))

(define-public crate-asahi-portable-0.1 (crate (name "asahi-portable") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.2.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)) (crate-dep (name "smurf") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "1yvbxsl3cyr2prsry7z73h1c94ffihyx81gm6jrrn4r14gs5ry55") (yanked #t)))

(define-public crate-asahi-wifisync-0.1 (crate (name "asahi-wifisync") (vers "0.1.0") (deps (list (crate-dep (name "apple-nvram") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "rust-ini") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "0qw0plapda7w1mw9df7hyqhllr705sl6ylqpyj5jcyjachqk7y0w")))

(define-public crate-asahi-wifisync-0.2 (crate (name "asahi-wifisync") (vers "0.2.0") (deps (list (crate-dep (name "apple-nvram") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "rust-ini") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "1hb68im62jkzi1v8lip8ripiaz0c2n04k4mkdfrcbakb1s3hhzm8")))

