(define-module (crates-io r5 #{28}#) #:use-module (crates-io))

(define-public crate-r528-pac-0.0.1 (crate (name "r528-pac") (vers "0.0.1") (deps (list (crate-dep (name "bare-metal") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0namw0v1mppvlwvhfp9z3g09pkhly4n9mv44nr8ilvs2ak20w8sh") (features (quote (("rt"))))))

(define-public crate-r528-pac-0.0.2 (crate (name "r528-pac") (vers "0.0.2") (deps (list (crate-dep (name "bare-metal") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1gni5qjd2m0s55523b8iqaal5grd7mxk4gwhbz1h1iz61bm3pjij") (features (quote (("rt"))))))

(define-public crate-r528-pac-0.0.3 (crate (name "r528-pac") (vers "0.0.3") (deps (list (crate-dep (name "bare-metal") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0is486zdac5g8v03zwhfvwar59n7by1h4ylsz832wxsj4igy1niw") (features (quote (("rt"))))))

