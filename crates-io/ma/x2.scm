(define-module (crates-io ma x2) #:use-module (crates-io))

(define-public crate-max2034x-0.1 (crate (name "max2034x") (vers "0.1.0") (deps (list (crate-dep (name "device-driver") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.5") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0b9alad9rxls434fxmb0v0d196819sgyr2sywwsq4ajqijm4kqdg") (yanked #t)))

(define-public crate-max2034x-0.1 (crate (name "max2034x") (vers "0.1.1") (deps (list (crate-dep (name "device-driver") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.5") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (kind 0)))) (hash "002ngrbdx0p2wb7gyh1s6b2fk7pn3flgsqb5asmwb7z6j8bxqsv3")))

(define-public crate-max2034x-0.1 (crate (name "max2034x") (vers "0.1.3") (deps (list (crate-dep (name "device-driver") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (kind 0)))) (hash "1b2h734g14kn49p797w6fyrjvf75wlya2phzvwxsdxcqrr26i6wm")))

