(define-module (crates-io ma nm) #:use-module (crates-io))

(define-public crate-manman-0.1 (crate (name "manman") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "15i314spw0p31fd5acbr631mmil0zjyxng54f7iwz349vr0q22id")))

(define-public crate-manman-0.1 (crate (name "manman") (vers "0.1.1") (hash "1d02a7iwjf6acvmpf2d2cy281js5wcvm1i5d5mn634jy5yljh127")))

(define-public crate-manman-0.1 (crate (name "manman") (vers "0.1.2") (hash "1zkc2gjwspsfkpp0rcgddhfx0nmv88mi7x0nsim8lks8nkqkjnw9")))

