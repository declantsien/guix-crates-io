(define-module (crates-io ma ph) #:use-module (crates-io))

(define-public crate-maph-0.1 (crate (name "maph") (vers "0.1.0") (deps (list (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "05074dswsx1psxm4819hh6ax6z4kvf94irxlrp57mjy6g0cx9f1h")))

(define-public crate-maph-0.1 (crate (name "maph") (vers "0.1.1") (deps (list (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "01vaz7gny9v68mv6ln06iz9lhpw3c1p13rsb6sb8n04hzpz3v9jy")))

(define-public crate-maph-0.1 (crate (name "maph") (vers "0.1.2") (deps (list (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "04ia077fap0sisl5sfb9bvjjq6gpc0qifc89qlyzxmb1vydw02za")))

(define-public crate-maph-0.1 (crate (name "maph") (vers "0.1.3") (deps (list (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0gznn1aj68vzwmpzw82wipp9mxg0gnw92c1qsdia4a64yxi5ndbq")))

(define-public crate-maph-0.2 (crate (name "maph") (vers "0.2.0") (deps (list (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1iqmw1ggg15gr7b2ckjlx1842xkfjwyh44khw7dzac29a9x66kg7")))

(define-public crate-maph-0.2 (crate (name "maph") (vers "0.2.1") (deps (list (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0c4q69f9s4147wasj7j3hl7vrsaml4fpx8wl7hg9f47djvk9xjgk")))

(define-public crate-maph-0.2 (crate (name "maph") (vers "0.2.2") (deps (list (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0db214bwav2623myzhviifh52jlx5lg74z32yapngic4sy92d239")))

(define-public crate-maph-0.2 (crate (name "maph") (vers "0.2.3") (deps (list (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0v6bypwx5v349j1v7z1yckhjg0q1yfilrdm6dln5f1sz2z26a40j")))

(define-public crate-maph-0.2 (crate (name "maph") (vers "0.2.4") (deps (list (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0a9nbn36dgkrk9mvnlhmyqw44d0pd93kriv7lq6mgq13i3424166")))

(define-public crate-maph-0.2 (crate (name "maph") (vers "0.2.5") (deps (list (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0lxd4pbvnjz9x25jz2pgasmsvd3xwzzlcr9haxwq9xxa2h82aziy")))

(define-public crate-maph-0.2 (crate (name "maph") (vers "0.2.6") (deps (list (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0kgapfslnax4v17f7m9bli8cl02ybb94jyq16hggv9qrkg67g7y7")))

(define-public crate-maph-0.6 (crate (name "maph") (vers "0.6.0") (deps (list (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1im6sw662f53lw2by2pi10xbc21vlriql5m04jn7zpiikcx9g0gj")))

(define-public crate-maph-0.6 (crate (name "maph") (vers "0.6.1") (deps (list (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0lgk34yiyrq7lp0xjdka0fnbf0h8r3ncb992vi0s1hlphvj2d826")))

(define-public crate-maph-0.6 (crate (name "maph") (vers "0.6.2") (deps (list (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1vx87jq0vxy6l9r3a499g8ivhgxxrar4k6wryx0l160986yhlby2")))

(define-public crate-maph-0.6 (crate (name "maph") (vers "0.6.3") (deps (list (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0i5ds1bg8xqb9avwiva2biz1h8qpbwg6yxmlnrhj3kglykgr5wni")))

(define-public crate-maph-0.6 (crate (name "maph") (vers "0.6.35") (deps (list (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "15z86s9a2figc5ql6c74q78gfr30qbgw43x2glq3zm8x7clgb5pc")))

(define-public crate-maph-0.6 (crate (name "maph") (vers "0.6.36") (deps (list (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "18ppq1gadcmg5j3m1a62m6knszg0r7b4r58zp85m2a3qdvqw7xl5")))

(define-public crate-maph-0.6 (crate (name "maph") (vers "0.6.4") (deps (list (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0725k22xcmd0zrsn88x82q7avhky7b7xx8f71n407qj6bj7n51jk")))

(define-public crate-maph-0.7 (crate (name "maph") (vers "0.7.0") (deps (list (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "04vssm0garyjfbzsch5f70m35nfhdiqicsqif9z5hcyipvafkxlp")))

(define-public crate-maph-0.7 (crate (name "maph") (vers "0.7.1") (deps (list (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "11wrm9jb78jc6rsc2l94srz0vbhyj3svmy18fn4k7gcd9zbswm0h")))

(define-public crate-maph-0.7 (crate (name "maph") (vers "0.7.2") (deps (list (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "1z6c5svql5lx5np6qsxh4iyg4zrgk374nq7qpyc8bz9l3gqcr7ds")))

(define-public crate-maph-0.7 (crate (name "maph") (vers "0.7.25") (deps (list (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "0x2afdj6i50gjzdbbbzjx0vm4qfyw8rgy548qamb7hvrdzain9lf") (yanked #t)))

(define-public crate-maph-0.7 (crate (name "maph") (vers "0.7.3") (deps (list (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "0v3bh7zih3asi623whzzsv1cs5px91f2lv7x7g9f2ak4yx6vghqv")))

(define-public crate-maph-0.7 (crate (name "maph") (vers "0.7.4") (deps (list (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "1fzbqkimbcg706aqlxx69dwyipmnpdmpzpf1lbr8x2nhrapcipdl")))

(define-public crate-maph-0.7 (crate (name "maph") (vers "0.7.5") (deps (list (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "1nz9mw29xa79nn50s4d1fwmcpw9l79zzra0mqcz2is07nwv65jvm")))

(define-public crate-maph-0.7 (crate (name "maph") (vers "0.7.6") (deps (list (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "0ajyjkpz526k7mq50yb5rrmr14972f58lslazf1gykgnasiy7zmw")))

(define-public crate-maph-0.7 (crate (name "maph") (vers "0.7.7") (deps (list (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "1bz8sy0cf0s4338rn75dbz83njk8nb6mbbsnspy83h9pdfjpddjc")))

(define-public crate-maph-0.7 (crate (name "maph") (vers "0.7.8") (deps (list (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "1q755z16x1dv7avmdnlmfz866m5qzx74zspz1kfi8r47k85xy6sa")))

(define-public crate-maph-0.7 (crate (name "maph") (vers "0.7.9") (deps (list (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "1sp4micbl1wysi7wk3bis3bgx35v32pggvvbcb6asdj6af9d39hh")))

(define-public crate-maph-0.7 (crate (name "maph") (vers "0.7.10") (deps (list (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "0q0mdiijd50l68s4a36cwkgh9rn3ja16mczx6dsfbfw5smv5zjwi")))

(define-public crate-maph-0.7 (crate (name "maph") (vers "0.7.11") (deps (list (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "1dif5pc1b80yfnymrpi4zn91wqjbxn55cywk03m7wlf0wsizrv4z")))

(define-public crate-maph-0.8 (crate (name "maph") (vers "0.8.0") (deps (list (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "07br54dbgg48mq20bdv06v95dl147jrjygmckxwnm2f98hijzkpi")))

(define-public crate-maph-0.8 (crate (name "maph") (vers "0.8.1") (deps (list (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "0h17qzyrn5grawipi4mzdikpw8pr2sbgl2cnwkimq76jz1plmvwn")))

(define-public crate-maphash-0.0.0 (crate (name "maphash") (vers "0.0.0") (hash "0vakyk4cfbgpwfhhc05nx8iwycghmjh2mcfsd3hsfy7rdl5b1arg") (yanked #t)))

