(define-module (crates-io ma tp) #:use-module (crates-io))

(define-public crate-matplotrust-0.1 (crate (name "matplotrust") (vers "0.1.0") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "0r5qkpy599kp4wyp4iwkiqlgp7gak2fqrnq2lishfq50w3q3r2dx")))

(define-public crate-matplotrust-0.1 (crate (name "matplotrust") (vers "0.1.1") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "0qzip9d12ka73izjfx2f62xxr71gxab8i80rphy0hhk67maah0py")))

(define-public crate-matplotrust-0.1 (crate (name "matplotrust") (vers "0.1.2") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "0s8s3lvhlhx3g9zck3j8ylm454aayfb8pmmmadiblb2505w4r467")))

(define-public crate-matplotrust-0.1 (crate (name "matplotrust") (vers "0.1.3") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "1xkzngf8rmrb1zq2yz22amlk0s01l6fk3jy88a9zmw3shhkz2821")))

(define-public crate-matplotrust-0.1 (crate (name "matplotrust") (vers "0.1.4") (deps (list (crate-dep (name "probability") (req "^0.15.12") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "1k6ksfcrw8x445jf0y6fp4jvzyqgi02gm4xsdv50zszx7sf2vd1a")))

(define-public crate-matplotrust-0.1 (crate (name "matplotrust") (vers "0.1.5") (deps (list (crate-dep (name "base64") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "probability") (req "^0.15.12") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "0qjvqih89s9i6znnrq48jsl25akd7bnzabb4pmkzi71lcdmzd7zn")))

(define-public crate-matplotrust-0.1 (crate (name "matplotrust") (vers "0.1.6") (deps (list (crate-dep (name "base64") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "probability") (req "^0.15.12") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "1rsq60n99f53ap0nznvs9hlvc7w4wkgsq3bs5m3h2ji2x8v0cs2c")))

(define-public crate-matplotrust-0.1 (crate (name "matplotrust") (vers "0.1.7") (deps (list (crate-dep (name "base64") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "probability") (req "^0.15.12") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "05dhsns5r87gvb1rbajbkjrr42k4zwxv4sb1y9fri66zayhv5ngx")))

