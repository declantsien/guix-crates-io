(define-module (crates-io ma v-) #:use-module (crates-io))

(define-public crate-mav-sdk-0.1 (crate (name "mav-sdk") (vers "0.1.0") (deps (list (crate-dep (name "prost") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.8") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 2)) (crate-dep (name "tonic") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.5") (features (quote ("rustfmt"))) (default-features #t) (kind 1)))) (hash "1v9si7b5s659hmimf4wsd8vd5sq58gpa8bw1s399f9b0n0z73iz6")))

