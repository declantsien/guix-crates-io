(define-module (crates-io ma cv) #:use-module (crates-io))

(define-public crate-macvtap-0.1 (crate (name "macvtap") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.52") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.69") (default-features #t) (kind 0)))) (hash "0ch99fpkvh913iin13q38gzpwp7xv5mhvnk2wf6g293sfyh613a9")))

(define-public crate-macvtap-0.1 (crate (name "macvtap") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0.52") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.69") (default-features #t) (kind 0)))) (hash "0jy69466kbkkbgd2vrv8rk789az1chnxivrfk9xyplj5dr9hbxfk")))

(define-public crate-macvtap-0.1 (crate (name "macvtap") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0.52") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.69") (default-features #t) (kind 0)))) (hash "0zaxvqym1xrw8fs0sbz972ix6wq8qkygqgq44rzxl4bakidzpr2j")))

