(define-module (crates-io ma dv) #:use-module (crates-io))

(define-public crate-madvise-0.1 (crate (name "madvise") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.29") (default-features #t) (kind 0)))) (hash "11hv2qn3hsvy8d8nmqsxxvmwfh2jnf6429zir7738ascqg1pa7jf")))

(define-public crate-madvr_parse-0.1 (crate (name "madvr_parse") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "1js78y6ldv52l097b0gdai5qh8ak7bm52kk65g2wp0354sx27ay8")))

(define-public crate-madvr_parse-0.1 (crate (name "madvr_parse") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0qrlz78a0dn25wawck22p5dj77p5nlnikadarfq01cqxaw3vrcn2")))

(define-public crate-madvr_parse-0.1 (crate (name "madvr_parse") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.45") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0g59f50745ijn0jfh43p74jxgps04w14s6w28vd20377x83442cd") (rust-version "1.51.0")))

(define-public crate-madvr_parse-1 (crate (name "madvr_parse") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0qnap718dphi3lwqw6vvm8dg1wc1lwnj05ixccxghzpcvdsp04dv") (rust-version "1.56.0")))

(define-public crate-madvr_parse-1 (crate (name "madvr_parse") (vers "1.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "06kk93y5f8s6i5sly4m9rmcwh8zqx7n0zmf59idvz478v5ad3y3b") (rust-version "1.56.0")))

(define-public crate-madvr_parse-1 (crate (name "madvr_parse") (vers "1.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.77") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.5.0") (default-features #t) (kind 0)))) (hash "1rifqjvmbj96mvbws21kv9xafy8j39h78v12wgi4h9sw1azigmmz") (rust-version "1.70.0")))

