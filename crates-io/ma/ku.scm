(define-module (crates-io ma ku) #:use-module (crates-io))

(define-public crate-makudaun-0.0.0 (crate (name "makudaun") (vers "0.0.0") (deps (list (crate-dep (name "clap") (req "^4.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^1.0.0-alpha.7") (default-features #t) (kind 0)))) (hash "1dvma3qjvd33w6sqy583h7n893cc64kjj78wcsph4gik7afdgns3")))

(define-public crate-makudaun-0.0.1 (crate (name "makudaun") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^4.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^1.0.0-alpha.7") (default-features #t) (kind 0)))) (hash "1riiqk3k64lxpcja2lw20wm7rfidpagxfrpcmqyrnk3rvwax3fzk")))

(define-public crate-makudaun-0.0.2 (crate (name "makudaun") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^4.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^1.0.0-alpha.7") (default-features #t) (kind 0)))) (hash "137wg6i7nyng1xjn31im22afxh7rjjn74zbj0c7s42ppg48hqvzn")))

