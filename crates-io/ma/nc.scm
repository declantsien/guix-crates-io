(define-module (crates-io ma nc) #:use-module (crates-io))

(define-public crate-mancala-0.1 (crate (name "mancala") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "03wfckbr4qki068v7i4777hrkdb5iqii7gnwi3br6m22k111vgjz")))

(define-public crate-mancala_board-0.1 (crate (name "mancala_board") (vers "0.1.0") (hash "1fcn5b17czpbqvqqgj7z0fa675bv458zlh1nbp44n19bifqnakzg")))

(define-public crate-mancala_board-0.1 (crate (name "mancala_board") (vers "0.1.1") (hash "13wh527ii5yvc5w3kkm4a20dwgjldjlhhn0ghm57d9wg9hy6al82")))

(define-public crate-mancala_board-0.1 (crate (name "mancala_board") (vers "0.1.2") (hash "0362nmrv1m9jl3avm8w1apkc8sz4xhq74vgl37j4qynmwalx47gx")))

(define-public crate-mancala_board-0.1 (crate (name "mancala_board") (vers "0.1.3") (hash "1nvvm0xi0b2pw39k963wwbijagbl33z3mnw37yiiwpa8vlpgbh7x")))

(define-public crate-mancala_board-0.1 (crate (name "mancala_board") (vers "0.1.4") (hash "1gqjp5vg2j7x780zyj1lr92nzqkq4jmyvgivs99l20a251vc2b5b")))

(define-public crate-mancala_board-0.1 (crate (name "mancala_board") (vers "0.1.5") (hash "1n6v08jdnakgis15vp99b5xp4ky94jczpg1igavp82jk4j6xzp0d")))

(define-public crate-mancala_board-0.1 (crate (name "mancala_board") (vers "0.1.6") (hash "0aikaf3592ms0jwn8hr9b8ml2g64cngp6kajx3dy2wrxpj1zshxi")))

(define-public crate-mancala_board-0.1 (crate (name "mancala_board") (vers "0.1.7") (hash "053ahkr6d887h3a5sxmw5v8f6fr4c5lcs9yn7mnj4s8vr6mbcfal")))

(define-public crate-mancala_board-0.1 (crate (name "mancala_board") (vers "0.1.8") (hash "0mdsj60j2h6p21i2qrqphzbyaca46d0is1n7divvfpnwwza1ifsf")))

(define-public crate-mancala_board-0.1 (crate (name "mancala_board") (vers "0.1.9") (hash "0aicgk7wvgnpdkg0l08gj7p7ml5kin3di6bbcyzfzbvcs94qq7jl")))

(define-public crate-mancala_board-0.1 (crate (name "mancala_board") (vers "0.1.10") (hash "19ljmcxjp8y3dsn3p19h9lq27gsshcv0axhlyvl37ns932k9y8yz")))

(define-public crate-mancala_board-0.1 (crate (name "mancala_board") (vers "0.1.11") (hash "0h42hw8s48z59hjyy0wfccfmips8gxrz1xq2z83n4bg0cl8kikqr")))

(define-public crate-mancala_board-0.1 (crate (name "mancala_board") (vers "0.1.12") (hash "1dm387gsmr204vi579i6arx9i8ka2plm1p8ic2mrwjfswsv6zbs7")))

(define-public crate-mancala_board-0.1 (crate (name "mancala_board") (vers "0.1.13") (hash "1rk70d19pwgcn83bws8yrxvlwpygfkychh47h9kl1as5rpgisxk3")))

(define-public crate-mancala_board-0.1 (crate (name "mancala_board") (vers "0.1.14") (hash "00hadb6xnj4m8ghi1xpm4klyjikdynprsay5j04fqzcv10rs43mi")))

(define-public crate-mancala_board-0.1 (crate (name "mancala_board") (vers "0.1.15") (hash "0fxjl0w5nh7dnq4kqzg0di3zalfgyi51qhy7rwwishb7zj6jbm0r")))

(define-public crate-mancala_board-0.1 (crate (name "mancala_board") (vers "0.1.16") (hash "0mafl10c22fdyrmix4d11gj8vzl93ddpdnxvics45ykbr531vfar")))

(define-public crate-mancala_board-0.2 (crate (name "mancala_board") (vers "0.2.0") (hash "1abv1r6bm0vwzprmdn9nc0n4wgr3rbdxlw4ghpbfb0xdqngm46l6")))

(define-public crate-mancer-0.1 (crate (name "mancer") (vers "0.1.0") (hash "1qiaz3pjw2dn4i0z5i7lr3srh50k0x3zzdxgb51qkg8v57scfhq9")))

(define-public crate-manchester-code-0.1 (crate (name "manchester-code") (vers "0.1.0") (deps (list (crate-dep (name "defmt") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.5") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0v815451lfamgnzgaskx3ff1pbz1mxq1f6a6k7mgljcfcp4cc7vn")))

(define-public crate-manchester-code-0.2 (crate (name "manchester-code") (vers "0.2.0") (deps (list (crate-dep (name "defmt") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.5") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1zjls1ci18y3q4czigqr5s8nb0w82mrhv8da9mn58jhhfzwgd73i")))

(define-public crate-manchu-converter-0.1 (crate (name "manchu-converter") (vers "0.1.0") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "18x6h92xrwv8lbk4hm41jg7zg0dar3mh4mkpi2rcsk7qpg7cy47r")))

(define-public crate-manchu-converter-0.1 (crate (name "manchu-converter") (vers "0.1.1") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "1lx7rkxv85yljqxjqsi8hrl5g72vnng1ycdfa387p79v7w5vfrad")))

(define-public crate-manchu-converter-0.2 (crate (name "manchu-converter") (vers "0.2.0") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "1l4c61ga7siphmn8nwkymb8mn7crzjcy9vyhw589rf916dvkm9qv")))

(define-public crate-manchu-converter-0.2 (crate (name "manchu-converter") (vers "0.2.1") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "1yda6rrcps8bxkagz31ac1s8a60b680q56ybap802ipkd4z1qz2h")))

(define-public crate-manchu-converter-0.2 (crate (name "manchu-converter") (vers "0.2.2") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "0dv179swarmgbdqr3vibv9l6rmnfmzqnd4vnd155z6bfjxvsm30m")))

(define-public crate-manchu-converter-0.2 (crate (name "manchu-converter") (vers "0.2.3") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "1jg3hzp5pfc5ryiarp4hxmlwhyw63v3hfaw1nbjawqg1c6pbrllj")))

(define-public crate-manchu-converter-0.3 (crate (name "manchu-converter") (vers "0.3.0") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "1rnzrq22fp0rwnhyz79w58ma2fixrhgr50fal6k776zwlw88glhn")))

