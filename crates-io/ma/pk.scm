(define-module (crates-io ma pk) #:use-module (crates-io))

(define-public crate-mapkitjs-token-gen-0.1 (crate (name "mapkitjs-token-gen") (vers "0.1.0") (deps (list (crate-dep (name "jsonwebtoken") (req "^7.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "1xxnd618bdx3qwnj03qj5lkdp7i0zylcaz3dli6dpcvahcab4lfa")))

(define-public crate-mapkitjs-token-gen-0.2 (crate (name "mapkitjs-token-gen") (vers "0.2.0") (deps (list (crate-dep (name "jsonwebtoken") (req "^7.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "0n8b2zgyznnnyh2b4wv1j9ccwy4jnjlmml1y1ywyrr9qrii0hwha")))

