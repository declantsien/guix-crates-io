(define-module (crates-io ma ne) #:use-module (crates-io))

(define-public crate-mane-0.1 (crate (name "mane") (vers "0.1.0-SNAPSHOT") (hash "0c55lxz33f074xb8w2xwhw4v7grydk92kic8rbi9i2j045b8z634")))

(define-public crate-manenko-ds-0.0.1 (crate (name "manenko-ds") (vers "0.0.1") (hash "0f80hdl7qai067gpwnm18j4ac1n4pg8g1s4jrmmmy5b15hls1lbs")))

(define-public crate-manenko-is-0.0.1 (crate (name "manenko-is") (vers "0.0.1") (hash "0x6zhsxb99n4azlvkdazprgy60wd5qirk6rizy3hdmljrijgg8dl")))

(define-public crate-manenko-ss-0.0.1 (crate (name "manenko-ss") (vers "0.0.1") (hash "0b9gnc5wjlzjvppnxs2zfjls6yandambwp4p4s4l0i4bc6r8qnfk")))

(define-public crate-manenko-ws-0.0.1 (crate (name "manenko-ws") (vers "0.0.1") (hash "0phb5xxbdyqwdpyd8687ppc90vrd867x1axgqd8fyza1d2r34xyc")))

(define-public crate-manes_minigrep-0.1 (crate (name "manes_minigrep") (vers "0.1.0") (hash "0pzsm2wm6vbrsg3n3vkxqdamp8q9dh3sz2gy19wixcrrqk8c9lhl")))

