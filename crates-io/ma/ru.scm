(define-module (crates-io ma ru) #:use-module (crates-io))

(define-public crate-maru-0.0.1 (crate (name "maru") (vers "0.0.1") (hash "1x77yr9pfdwzf8xqz4g7viklj07z5fg8wsgxz605gpni51rik7vw")))

(define-public crate-maruc-0.1 (crate (name "maruc") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "druid") (req "^0.7.0") (features (quote ("im"))) (default-features #t) (kind 0)) (crate-dep (name "matrix-sdk") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "03fvz4rldf0bi04pg66c7k83zqcp3lvidsyzgqzkdj97w2cbsn4i")))

(define-public crate-marui-0.1 (crate (name "marui") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1m3399kq5cb4wx1j5x9p9g8hbvpi3ljbnwj0yq4xx2ah4x26pzkw")))

(define-public crate-marui-0.1 (crate (name "marui") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1cqj24897hdm4nghig1xnn9mb1411zcjwg0z4q81nhdg7jvvjhrg")))

(define-public crate-marui-0.2 (crate (name "marui") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0ldgrnwcwmzvnabakskvzfqh12936z9rhfgzx2yl4wxwdyf0h9ji")))

(define-public crate-maruw-0.1 (crate (name "maruw") (vers "0.1.0") (hash "06fp62yq395s3rmqxvyv9c1k4kp7ay939cmpbyh67ixvqzb2kx4w")))

