(define-module (crates-io ma zt) #:use-module (crates-io))

(define-public crate-mazter-0.4 (crate (name "mazter") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.49") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "cli-log") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "crokey") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.23.1") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^1.2") (default-features #t) (kind 0)))) (hash "0q27nyj7rrg68qd03vj9p9c1gald9c1kq8npsm3xi9gia4m7klyd") (rust-version "1.56")))

(define-public crate-mazter-0.4 (crate (name "mazter") (vers "0.4.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.49") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "cli-log") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "crokey") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.23.1") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^1.2") (default-features #t) (kind 0)))) (hash "0aqh889yrbdrldwd16camxd7lk6p1gm8gr88ky2dmwrdkdmwp93m") (rust-version "1.56")))

(define-public crate-mazter-1 (crate (name "mazter") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.49") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "cli-log") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "crokey") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.10") (default-features #t) (kind 0)) (crate-dep (name "termimad") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^1.2") (default-features #t) (kind 0)))) (hash "1c1xi6s0klla07dwmada4kcjqwi4dw3l7wpadfx0yw38zajf0b8f") (rust-version "1.56")))

(define-public crate-mazth-0.0.0 (crate (name "mazth") (vers "0.0.0") (hash "1fy8zzdsb4ma5npyp6pzy6svf157bghdzlgnja55ds5nndc7znhw")))

(define-public crate-mazth-0.0.1 (crate (name "mazth") (vers "0.0.1") (hash "19mljxycmwk34qbp93yz48m99b0z10fahijpi1bkjan05kqzkasl")))

(define-public crate-mazth-0.1 (crate (name "mazth") (vers "0.1.0") (hash "1iw6cq2cb00p326nqp1zma94qlpprbpahaysxzx4sckdn6bpl7il")))

(define-public crate-mazth-0.1 (crate (name "mazth") (vers "0.1.2") (hash "19ndbyhqzxcmkn7whqqx3kxn4rbrq1mx9jgwcrbv9wkqh99rv8j0")))

(define-public crate-mazth-0.2 (crate (name "mazth") (vers "0.2.0") (hash "189i4nlbqr7ii2lfpd60ffh16mcakykcljypgs9rfqaf9b0xx5dj")))

(define-public crate-mazth-0.2 (crate (name "mazth") (vers "0.2.1") (hash "1c349fkkf0w1gvnb62glf9jkwi0lfasav15vzlr9qd8fi97y02k8")))

(define-public crate-mazth-0.3 (crate (name "mazth") (vers "0.3.0") (deps (list (crate-dep (name "ndarray") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1kja456i0j90j7nqp5ba4jvqm0h9h3js3qzimq8rqr97x52jyq74")))

(define-public crate-mazth-0.3 (crate (name "mazth") (vers "0.3.1") (deps (list (crate-dep (name "ndarray") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0d92zyy3s5ljqcscj7fhl5qd5y74w3w55cl8m7y0ay58ssdmn8wf")))

(define-public crate-mazth-0.4 (crate (name "mazth") (vers "0.4.0") (deps (list (crate-dep (name "ndarray") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1mwfnip06yrcb5xz3q9zrvhx2bvpzmzdhgplir0ws5m4nvhy80m2")))

(define-public crate-mazth-0.4 (crate (name "mazth") (vers "0.4.1") (deps (list (crate-dep (name "ndarray") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0101z5pwhnj442k8cbajwww3731idgb2z45sa65417aiax29rhq0")))

(define-public crate-mazth-0.4 (crate (name "mazth") (vers "0.4.2") (deps (list (crate-dep (name "ndarray") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1hjhkmh7g2s977qj13mp2aiv56g4d0kwwr3s89ylw6jq3yrny9bk")))

(define-public crate-mazth-0.5 (crate (name "mazth") (vers "0.5.0") (deps (list (crate-dep (name "ndarray") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0lkhyhc62xix833m5qrpvq7hn2pmk5capl29vy8anp2gfi1dpgyd")))

(define-public crate-mazth-0.6 (crate (name "mazth") (vers "0.6.0") (deps (list (crate-dep (name "ndarray") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0xmzrk9xks44qi2pwm0mzsfgvcgik9v6blq7bckg7bp2l82fya9y")))

