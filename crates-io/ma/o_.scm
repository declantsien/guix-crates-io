(define-module (crates-io ma o_) #:use-module (crates-io))

(define-public crate-mao_core-0.1 (crate (name "mao_core") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "164i3k6ci33ni51bcmhy30cl11c8g32r03zl1rq8k79v5dfgi382")))

