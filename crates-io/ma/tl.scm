(define-module (crates-io ma tl) #:use-module (crates-io))

(define-public crate-matlab-0.1 (crate (name "matlab") (vers "0.1.0") (hash "1fz2bhhh9jhzvi89mvzn4dc6kajk3f1hnyzn11hj2mc3az8d6vc0")))

(define-public crate-matlab-mat-0.1 (crate (name "matlab-mat") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "enum-primitive-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "ndarr") (req "^0.15") (optional #t) (default-features #t) (kind 0) (package "ndarray")) (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "16jg4hsh7gxpn60az2817743k00sa9syn6v5dilzlzf234zysccl") (features (quote (("ndarray" "ndarr" "num-complex"))))))

(define-public crate-matlab-sys-0.1 (crate (name "matlab-sys") (vers "0.1.0") (hash "1h64lpllgh5g3q0lhlh76wgw6dgx9788njp3vlrh7anfzr7fg5ih") (features (quote (("separate-complex") ("link-lib") ("interleaved-complex") ("default" "interleaved-complex" "link-lib"))))))

(define-public crate-matlab-sys-0.1 (crate (name "matlab-sys") (vers "0.1.1") (hash "160xaymfa76m2wpp559bgp2hkw6lny301arbyznjhmkb935q1xpi") (features (quote (("separate-complex") ("link-lib") ("interleaved-complex") ("default" "interleaved-complex" "link-lib"))))))

(define-public crate-matlab-sys-0.2 (crate (name "matlab-sys") (vers "0.2.0") (hash "14c7rd20952p01kfqlh3hajzl7cmg5qmngvwqy27m2qzilyzqls3") (features (quote (("separate-complex") ("link-lib") ("interleaved-complex") ("default" "interleaved-complex" "link-lib")))) (yanked #t)))

(define-public crate-matlab-sys-0.2 (crate (name "matlab-sys") (vers "0.2.1") (hash "0wjlx5yibdw051dk813sqrcsslqzwc3kxvg9hddnpylxpcwfx0j7") (features (quote (("separate-complex") ("link-lib") ("interleaved-complex") ("default" "interleaved-complex" "link-lib"))))))

(define-public crate-matlab-sys-0.3 (crate (name "matlab-sys") (vers "0.3.0") (hash "08h2ggwz08kjy2pxckggvwjplrplmlgbkqxiqli60dh6hzfrlj2j") (features (quote (("separate-complex") ("interleaved-complex") ("default" "interleaved-complex")))) (links "matlab")))

(define-public crate-matlab-sys-0.3 (crate (name "matlab-sys") (vers "0.3.1") (hash "1h77v0wdyc9wizhf1mrqkgp04qw20jzhw60srqzbp2svadnhm76z") (features (quote (("separate-complex") ("interleaved-complex") ("default" "interleaved-complex")))) (links "matlab")))

(define-public crate-matlabblas-src-0.1 (crate (name "matlabblas-src") (vers "0.1.0") (hash "016gqjg4x9iqhfxnk1vxi6lg58srqfayb0lfk7cni6lm03c6hwk2") (links "blas")))

