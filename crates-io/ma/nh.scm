(define-module (crates-io ma nh) #:use-module (crates-io))

(define-public crate-manhattan-tree-0.1 (crate (name "manhattan-tree") (vers "0.1.0") (deps (list (crate-dep (name "bonzai") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 2)))) (hash "1mbilb76lkgfbx5gfwqs50n0cbygwcfz4czdacdv50qczh7pi1pg")))

(define-public crate-manhattan-tree-0.1 (crate (name "manhattan-tree") (vers "0.1.1") (deps (list (crate-dep (name "bonzai") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 2)) (crate-dep (name "smallqueue") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "through") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0kn6z7fnmm6969zkash2pzm413yknqcd995jyfa51cdzl2k0y3xc")))

