(define-module (crates-io ma x1) #:use-module (crates-io))

(define-public crate-max112x-0.1 (crate (name "max112x") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "1pa7wxrbpl9p2s7bng7dppv4j2r4p7wsqzqqfdvavy8r269h0xgi")))

(define-public crate-max112x-0.2 (crate (name "max112x") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "0y6ds8azh15fpqhy7q0dn00z05jxczbi6gvv4qvhsdw5rz3n6i4h")))

(define-public crate-max112x-0.3 (crate (name "max112x") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "0w84apyzmfapdfd2l3iqac3syxk2dgqw6aa7y885am9i95n8nxq1")))

(define-public crate-max112x-0.4 (crate (name "max112x") (vers "0.4.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qhb7vdngpf87qhpmngmryydymzlrq4d5i4gvldf5zcsk6f7hvpr")))

(define-public crate-max112x-0.5 (crate (name "max112x") (vers "0.5.0") (deps (list (crate-dep (name "bitflags") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "02km0g3z3x8hg5d05x3y8a5x4k7kl3l5a4px1alqy9ksqc99y02c")))

(define-public crate-max112x-0.6 (crate (name "max112x") (vers "0.6.0") (deps (list (crate-dep (name "bitflags") (req "^2.4") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.10") (default-features #t) (kind 2)))) (hash "0x6vgjbak8f9ninmky6kp8rpp1l14ici2drasjhrcmc8yc93afhm")))

(define-public crate-max112x-0.7 (crate (name "max112x") (vers "0.7.0") (deps (list (crate-dep (name "bitflags") (req "^2.4") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.10") (default-features #t) (kind 2)))) (hash "0vkxdgz4wbzbzcdm8311c8wz874zn6xwbb9i3cr61jsx73vrajg9")))

(define-public crate-max11300-0.1 (crate (name "max11300") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "heapless") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "seq-macro") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1k8adlhjp5fzb15h1nqagxv5bzvng25ry545k04hcwjghcjsm6x2")))

(define-public crate-max11300-0.1 (crate (name "max11300") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "heapless") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "seq-macro") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0gh0jak8m6qrl9x80pww9p8xl1zw9gh26213py5m7wvx87rf437h")))

(define-public crate-max11300-0.2 (crate (name "max11300") (vers "0.2.0") (deps (list (crate-dep (name "critical-section") (req "^1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "critical-section") (req "^1.1") (features (quote ("std"))) (default-features #t) (kind 2)) (crate-dep (name "embassy-sync") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-async") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.10") (features (quote ("eh1" "embedded-hal-async"))) (default-features #t) (kind 2)) (crate-dep (name "heapless") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "seq-macro") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1nn7bjq5yk6z8r7g1582q536b74qwizib66w8kklh6j3q0ixv3zn") (features (quote (("default" "cs") ("cs" "critical-section"))))))

(define-public crate-max116xx-10bit-0.1 (crate (name "max116xx-10bit") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.6") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0azjj2m6fxjkn70cc77v997fm018vh2kna43fxjybr7ls2ckwkis")))

(define-public crate-max116xx-10bit-0.1 (crate (name "max116xx-10bit") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.6") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0fxddgxdsd8grhd8kbdw2982gbvrnlqjal142rvcicbhr8d8gx8n")))

(define-public crate-max116xx-10bit-0.2 (crate (name "max116xx-10bit") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.6") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "04cc1girny4l8l5qvqmrlnr22d1ksp9ijd3zxlyhx86ds4cn9d83")))

(define-public crate-max116xx-10bit-0.2 (crate (name "max116xx-10bit") (vers "0.2.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.6") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1mv0jpw04yrm51kg0fvqjxiyv2jrmjkvm4asc4j60isaljr1a2m3")))

(define-public crate-max145784-driver-0.1 (crate (name "max145784-driver") (vers "0.1.0") (deps (list (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-async") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "modular-bitfield") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "1c0p46rdg1rdan0hllay4r0mzmbc4q3vjxsivnjyaqi6rynj1g60") (features (quote (("default")))) (yanked #t) (v 2) (features2 (quote (("defmt-03" "embedded-hal-async/defmt-03" "dep:defmt"))))))

(define-public crate-max14578e-driver-0.1 (crate (name "max14578e-driver") (vers "0.1.0") (deps (list (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-async") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "modular-bitfield") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "1sqy5pyc3wbmp2jrxzgk7s3191nnyj7mc3dypv9jn3scp6ja5fj0") (features (quote (("default")))) (v 2) (features2 (quote (("defmt-03" "embedded-hal-async/defmt-03" "dep:defmt"))))))

(define-public crate-max17048-0.1 (crate (name "max17048") (vers "0.1.0") (deps (list (crate-dep (name "cortex-m") (req "^0.5.7") (default-features #t) (kind 2)) (crate-dep (name "cortex-m-rt") (req "^0.6.5") (default-features #t) (kind 2)) (crate-dep (name "cortex-m-semihosting") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "panic-semihosting") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "stm32l4xx-hal") (req "^0.3.2") (features (quote ("rt" "stm32l4x2"))) (default-features #t) (kind 2)))) (hash "197k462i0jsgl1d7vqpzc06sxqs3kpg0xvqwjdd0f4h9qgj25icq")))

(define-public crate-max170xx-0.1 (crate (name "max170xx") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)))) (hash "0m34q2pl7s0rv6yn9kk58ivb89n6nc820q5zd3xpsaxh0rrp2pgr")))

(define-public crate-max170xx-0.1 (crate (name "max170xx") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)))) (hash "10azgzr6rff3ikfi4sb7sz8kw4lmpg0wp0hhybbg81pwg599l970")))

(define-public crate-max170xx-1 (crate (name "max170xx") (vers "1.0.0") (deps (list (crate-dep (name "embedded-hal") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.10.0") (features (quote ("eh1"))) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.4") (default-features #t) (kind 2)))) (hash "0rlkm0yniwlq01hilyn5p0vyjqrwnbh585kah2rb4nkqygf1aqz3")))

(define-public crate-max1720x-0.1 (crate (name "max1720x") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "1awy2x6kxgsaysqdf8m7784lp7qy4s2sh4a8mydmnxdmqlhyf49j")))

(define-public crate-max17320-0.1 (crate (name "max17320") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "1j7d4w6amhr1gbz0pfcivxwg9b8f8ck3fh6h260gr33a5k3v3a80")))

