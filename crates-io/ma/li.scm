(define-module (crates-io ma li) #:use-module (crates-io))

(define-public crate-malice-0.1 (crate (name "malice") (vers "0.1.0") (deps (list (crate-dep (name "alice-open-data") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "alice-sys") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2.1") (default-features #t) (kind 0)) (crate-dep (name "root-io") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0dkd851kw03h4d035k89l74lpfackckki30mzhqzmdh34aj6dg62") (features (quote (("default") ("cpp" "alice-sys"))))))

(define-public crate-malice-0.1 (crate (name "malice") (vers "0.1.1") (deps (list (crate-dep (name "alice-open-data") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "alice-sys") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2.1") (default-features #t) (kind 0)) (crate-dep (name "root-io") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1mlshcxyamc508j4nazgr060r0rmrhb5abgra1jsiylqxxj05wln") (features (quote (("default") ("cpp" "alice-sys"))))))

(define-public crate-malice-0.1 (crate (name "malice") (vers "0.1.2") (deps (list (crate-dep (name "alice-open-data") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "alice-sys") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2.1") (default-features #t) (kind 0)) (crate-dep (name "root-io") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "09anyb4isk1ybkhhhv8fd5glj3lmdnlxnxfvv7vc4ydcxwjpxrxd") (features (quote (("default") ("cpp" "alice-sys"))))))

(define-public crate-malice-0.2 (crate (name "malice") (vers "0.2.0") (deps (list (crate-dep (name "alice-open-data") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "alice-sys") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.0.1") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "bitflags") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "= 0.3.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)) (crate-dep (name "root-io") (req "^0.2") (default-features #t) (kind 0)))) (hash "112knsp875sh7dj5gc1r8w40hcz0lf2alc9shv33gz2w7wf94pa5") (features (quote (("default") ("cpp" "alice-sys"))))))

(define-public crate-malice-0.2 (crate (name "malice") (vers "0.2.1") (deps (list (crate-dep (name "alice-open-data") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "alice-sys") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.0.1") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "bitflags") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "= 0.3.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)) (crate-dep (name "root-io") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "01v1vagd4qkyvxsr3gn7w1faj2ivqksvcdpszy7lq0k67cg4h2mh") (features (quote (("default") ("cpp" "alice-sys"))))))

(define-public crate-malice-0.3 (crate (name "malice") (vers "0.3.0") (deps (list (crate-dep (name "alice-open-data") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "alice-sys") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)) (crate-dep (name "root-io") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("macros"))) (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.60") (default-features #t) (kind 0)))) (hash "1x2c7jpx4pdmga8c2yv2azhn2vidw6vrndlb0r3l6lgy94hb7ml9") (features (quote (("default") ("cpp" "alice-sys"))))))

(define-public crate-malicious-0.1 (crate (name "malicious") (vers "0.1.0") (hash "0vn59335bw5kc1nsshrf5yk0dmxzxh62c07jvayy2vczhi2li86h")))

(define-public crate-maligned-0.0.0 (crate (name "maligned") (vers "0.0.0") (hash "0w97ds6pj400a82pss7v2fnqdsssa2fgsbr0bgd02zq4h3wg512x")))

(define-public crate-maligned-0.1 (crate (name "maligned") (vers "0.1.0") (hash "01cafaw9qpxpgian0acz2jswhqkdsa3g2ryivsifmdr6jcyj3d0i") (features (quote (("default" "alloc" "align-128k") ("alloc") ("align-8k" "align-4k") ("align-64k" "align-32k") ("align-4k" "align-2k") ("align-32k" "align-16k") ("align-2k" "align-1k") ("align-1k") ("align-16k" "align-8k") ("align-128k" "align-64k"))))))

(define-public crate-maligned-0.2 (crate (name "maligned") (vers "0.2.0") (hash "1dj6jc1m16nf5sj020rn1l1yhpg3aqb2w6lriirjwj20hkfcj4w4") (features (quote (("default" "alloc") ("clippy") ("alloc") ("align-8k" "align-4k") ("align-64k" "align-32k") ("align-4k" "align-2k") ("align-32k" "align-16k") ("align-2k" "align-1k") ("align-1k") ("align-16k" "align-8k") ("align-128k" "align-64k"))))))

(define-public crate-maligned-0.2 (crate (name "maligned") (vers "0.2.1") (hash "0r1lx0ll7sk8gzx3kn560g9fyg9f4frji2p4jgr7g3r8x35w723y") (features (quote (("default" "alloc") ("clippy") ("alloc") ("align-8k" "align-4k") ("align-64k" "align-32k") ("align-4k" "align-2k") ("align-32k" "align-16k") ("align-2k" "align-1k") ("align-1k") ("align-16k" "align-8k") ("align-128k" "align-64k"))))))

(define-public crate-malignius-0.0.1 (crate (name "malignius") (vers "0.0.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.73") (default-features #t) (kind 0)) (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 2)) (crate-dep (name "rusqlite") (req "^0.29.0") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "02ic7q4m57g5x5cg02hz4yndw91xangghgcxkf7bpx4v3l11jlzc")))

(define-public crate-maligog-0.1 (crate (name "maligog") (vers "0.1.0") (hash "1ja8s867bsrad28m09fq6r00qs22hajqx72kjqhkjmwqcddg1zrw") (yanked #t)))

(define-public crate-malik_sum-0.1 (crate (name "malik_sum") (vers "0.1.0") (hash "0x5qr38l85wp19yvs6fzpgl2qv0jrw4a393njw4kzbxgmymgpqpv")))

(define-public crate-malik_sum-0.1 (crate (name "malik_sum") (vers "0.1.1") (hash "1wdhqqyz0ysp1n3w4hpfyhpv78b2gs1pmxdz4cfcck5ifw1k68x9")))

(define-public crate-malik_sum-0.1 (crate (name "malik_sum") (vers "0.1.2") (hash "0315ayrc88rzif5izfrd5iyqzymxwwp6bw714pz39mbv9kavm0vn") (yanked #t)))

(define-public crate-malik_sum-0.1 (crate (name "malik_sum") (vers "0.1.3") (hash "0qlaps3qvd134msmaj998c3kwwda3f51vqv5n3731cxlaid1jj08")))

(define-public crate-malik_sum-0.1 (crate (name "malik_sum") (vers "0.1.4") (hash "04gsqnc153w47wkb7p43lb4kbkbx7ywx4pf704s58b8rmwxjz77r")))

(define-public crate-malik_sum-0.1 (crate (name "malik_sum") (vers "0.1.5") (hash "07f6x7s7jlgij7sl8a3dgb1ffid2kdff76b88ly51ax2003hqpff")))

(define-public crate-malik_sum-0.1 (crate (name "malik_sum") (vers "0.1.6") (hash "12b7hphl3lhylin49zx1hgbmy0qb70b892gmqi24f8qz58ddxccw")))

(define-public crate-malik_sum-0.1 (crate (name "malik_sum") (vers "0.1.7") (hash "0jsiy4wjmbmdjhh22lwdg523m7l5bd698zyqb51xbcwayzix0zdl")))

(define-public crate-maliput-0.1 (crate (name "maliput") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "cxx") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "maliput-sdk") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "maliput-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "14n8q64dmll457fh3srnl8abdmhwyqdqy1n4zbf2wfjdp1ldr2pn") (yanked #t)))

(define-public crate-maliput-0.1 (crate (name "maliput") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "cxx") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "maliput-sdk") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "maliput-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "12vb502692zxlnpmgpvb8323n31v22f18y4a0zsp1ha75s2wk4gb")))

(define-public crate-maliput-0.1 (crate (name "maliput") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "cxx") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "maliput-sdk") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "maliput-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "193cbj0x7cx108kdd0mivq1adnc5mi10paghcx1kkxy6v6s7vjqf")))

(define-public crate-maliput-sdk-0.1 (crate (name "maliput-sdk") (vers "0.1.0") (hash "0mlfkvk946bismn36bx1bg7l0kd89k1rm654nkvwnikgkbbg618i") (yanked #t) (links "maliput-sdk")))

(define-public crate-maliput-sdk-0.1 (crate (name "maliput-sdk") (vers "0.1.1") (hash "1dnn43nqdnszs7zmqadk1vah1cp89kxzq78p3yfnh76h2rgwb6dx") (yanked #t) (links "maliput-sdk")))

(define-public crate-maliput-sdk-0.1 (crate (name "maliput-sdk") (vers "0.1.2") (hash "19qlypdwi8yrprvqwkhj8hmwwfn1nq7ivb3141s2sc2jzw0zn84a") (links "maliput-sdk")))

(define-public crate-maliput-sdk-0.1 (crate (name "maliput-sdk") (vers "0.1.3") (hash "1vq3czra8w9zs1hsx40si8hgdfd6g9n3qdagjpddzl1nzn1hhjp1") (links "maliput-sdk")))

(define-public crate-maliput-sys-0.1 (crate (name "maliput-sys") (vers "0.1.0") (deps (list (crate-dep (name "cxx") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0.78") (default-features #t) (kind 1)) (crate-dep (name "maliput-sdk") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0wny603bxh7wwxddj2c5gfbj87n4a00p0p1qln19lh8nwcp8ac2h")))

(define-public crate-maliput-sys-0.1 (crate (name "maliput-sys") (vers "0.1.1") (deps (list (crate-dep (name "cxx") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0.78") (default-features #t) (kind 1)) (crate-dep (name "maliput-sdk") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1v63jvn9aj041bsqckk5rd9m9071if25fryr12qm4d20v1qxx2x2")))

