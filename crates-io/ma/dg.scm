(define-module (crates-io ma dg) #:use-module (crates-io))

(define-public crate-madgwick-0.1 (crate (name "madgwick") (vers "0.1.0") (deps (list (crate-dep (name "m") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "mat") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0m726n9vxfpc02xzs82lavbal4ayx084mrbpbchjld9p49vir0kz")))

(define-public crate-madgwick-0.1 (crate (name "madgwick") (vers "0.1.1") (deps (list (crate-dep (name "m") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "mat") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1g5ifkrdn0jij6n289g2hj2m70d8ym6v33jp7gzm6c1gfj00lxjj")))

