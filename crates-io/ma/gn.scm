(define-module (crates-io ma gn) #:use-module (crates-io))

(define-public crate-magna-0.1 (crate (name "magna") (vers "0.1.3") (hash "1yms2s2i2wb6anx6yk64dgwyr8xcz5pkpq06hvph146grbj2xqx6") (yanked #t)))

(define-public crate-magna-0.1 (crate (name "magna") (vers "0.1.4") (hash "1dn05ccxj5hb2szn6m0jmh60ym7hhds77163yhvl8iwn6qw70yaz") (yanked #t)))

(define-public crate-magna-0.1 (crate (name "magna") (vers "0.1.77") (hash "09h4hk4jdl1rakzqwzbi2v9v357zf6icgxdp7a0g47f3l4qjfbsz") (yanked #t)))

(define-public crate-magna-0.77 (crate (name "magna") (vers "0.77.0") (hash "0214bbh9qv704q5dgvf5s39impavwsa29li1w9cgyxzcgnavhc2l") (yanked #t)))

(define-public crate-magna-7 (crate (name "magna") (vers "7.7.18") (hash "0v7kdjy55zxraqpy2k5688c84s3r489dlj4vzw75wx9b06hwdmlv") (yanked #t)))

(define-public crate-magna-2018 (crate (name "magna") (vers "2018.7.7") (hash "007agpj6q3kxrglc063v15w4mn4ad7j6qzz71f8h1jk0sap9bij7") (yanked #t)))

(define-public crate-magna-2019 (crate (name "magna") (vers "2019.12.13") (hash "10785c7axc9lwsk9092f3z4gfgh7k0szywrc5i9hayg83qji88k3") (yanked #t)))

(define-public crate-magna-9999 (crate (name "magna") (vers "9999.999.99") (hash "1kcb2k4sj3nfhv7h37b2psh133jv6k6wlxj0ycs3ymzgxfws651c") (yanked #t)))

(define-public crate-magna-9 (crate (name "magna") (vers "9.9.9") (hash "1scd6ilqp25x8p074945wxghq633m5nbqccr5ywdrw7id9qi9fn9") (yanked #t)))

(define-public crate-magna-99999 (crate (name "magna") (vers "99999.99999.99999") (hash "1cxywmjy4ycaj13d5q0g1dw8xkq3mvilb84r0j8gpywzhmmakx3q") (yanked #t)))

(define-public crate-magna-9999999 (crate (name "magna") (vers "9999999.9999999.9999999") (hash "13lrb3cwlz4p5zsmyy16gqps95xklaad4vhnk5vngxhb4p7dxcqg") (yanked #t)))

(define-public crate-magna-999999999 (crate (name "magna") (vers "999999999.999999999.999999999") (hash "1j7h3yqyy3r700w3zvy0ys92jxyxkpsk0jv57hmrjhcg6iyqdn5m")))

(define-public crate-magne-flame-0.1 (crate (name "magne-flame") (vers "0.1.0") (deps (list (crate-dep (name "actix") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.64") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winuser" "winbase" "ioapiset" "processthreadsapi" "processenv" "errhandlingapi" "synchapi" "handleapi" "namedpipeapi" "fileapi" "memoryapi" "winerror" "jobapi2"))) (default-features #t) (kind 0)))) (hash "1v44qzhsqdfjvrm1x5lkm4k7qyhbyp11qxnc0947j9s4bj5xcsgk")))

(define-public crate-magne-flame-0.1 (crate (name "magne-flame") (vers "0.1.1") (deps (list (crate-dep (name "actix") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.64") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winuser" "winbase" "ioapiset" "processthreadsapi" "processenv" "errhandlingapi" "synchapi" "handleapi" "namedpipeapi" "fileapi" "memoryapi" "winerror" "jobapi2"))) (default-features #t) (kind 0)))) (hash "1iri6kbkjwjxphiwfbbx5l21g5fn4j25idxml0i7vmnhkb4daqyr")))

(define-public crate-magnesia-0.1 (crate (name "magnesia") (vers "0.1.0") (hash "0q4h793zhz4gmvzmxkd0ka7iwvd5gk1xrs1hkpdaxzn5izlrpgjh")))

(define-public crate-magnesia-0.1 (crate (name "magnesia") (vers "0.1.1") (hash "0jzzbw3dmpwddqslj49gxn9n1sagi3mc3r7ccvyh9bbp097jf892")))

(define-public crate-magnesia-0.1 (crate (name "magnesia") (vers "0.1.2") (hash "0h28968i1xh5s3acsfb8d15067m4hxgarb1sgn87q73fbiacs8vf")))

(define-public crate-magnesia-0.2 (crate (name "magnesia") (vers "0.2.0") (hash "1w6rg2y8dkcp2q8y7lrdk9nmqlnz9lmj54rbcjn6j54jhwihz3p3")))

(define-public crate-magnesia-0.2 (crate (name "magnesia") (vers "0.2.1") (hash "103p5f3y1bc3w6qdbj8ds1mjhjv7p3cj5x3ccp4lp2c2p1q4smhs")))

(define-public crate-magnesia-0.2 (crate (name "magnesia") (vers "0.2.2") (hash "0q1nlcvmscsxqfmpi0qpvrqf6s1hmikjf46dnwszyrj5sq5vmsmm")))

(define-public crate-magnesia-0.3 (crate (name "magnesia") (vers "0.3.0") (hash "1k0ab7sdzbd7lf4c1d4r2ir35i56jyl7qrnl6sxbc8j70sd4y8px")))

(define-public crate-magnesia-0.3 (crate (name "magnesia") (vers "0.3.1") (hash "145xwywl7zgrn5l9hqy0x31i0r7zfwl6r7nwcrqmn5lnci4r7im5")))

(define-public crate-magnesia-0.3 (crate (name "magnesia") (vers "0.3.2") (hash "0m4lh64ld2yv96zlk9zy6yxfd4yi8y1sh3yal7yxqkaarbzcd80a")))

(define-public crate-magnesia-0.3 (crate (name "magnesia") (vers "0.3.3") (hash "1r1yhbqp1svj98afklf261pyminlkmgnal3kq8fcaxvhdc83dyzb")))

(define-public crate-magnesia-0.4 (crate (name "magnesia") (vers "0.4.0") (hash "1gq6psxny2zfa15rbqgp4gp6l945nasjk798sial4babb6ri6gj6")))

(define-public crate-magnesia-0.4 (crate (name "magnesia") (vers "0.4.1") (hash "1l1laqb1jvyj1bakvpcmmziba03i7w95sizjrhyg7lhhq8ag1ysy")))

(define-public crate-magnesia-0.5 (crate (name "magnesia") (vers "0.5.0") (hash "00nhvs0b940gibiflq40l3bavn0kqsy45055x37s0adyx79va1a2")))

(define-public crate-magnesia-0.6 (crate (name "magnesia") (vers "0.6.0") (hash "0x26z7h0my0rm0j726601f7pn3ggnvywc2bawkknmmcphbrqywh5")))

(define-public crate-magnesia-0.6 (crate (name "magnesia") (vers "0.6.1") (hash "0irm2kbkf2c5vnm8dychhkpnb965m8y310fl6ikw48rncb1rv7lg")))

(define-public crate-magnesia-0.7 (crate (name "magnesia") (vers "0.7.0") (hash "144lw7gasvsqnjncsihiz6bd4v8r57z6v0i0wask33dpdjrcmv9x")))

(define-public crate-magnesia-0.7 (crate (name "magnesia") (vers "0.7.1") (hash "08bysa7ik4p62j10yib418z72xa3yzw94bw79wbx454ifczfqb6i")))

(define-public crate-magnesia-0.7 (crate (name "magnesia") (vers "0.7.2") (hash "132w41gwxabfbqrx4jm9pfngc5n802i0ckf88lgacy48ll5d10fx")))

(define-public crate-magnesia-0.7 (crate (name "magnesia") (vers "0.7.3") (hash "0n7hz8cyycvrsl32112dkbs1c3c134k6bix1k1wnpkkqv05ldkdr")))

(define-public crate-magnesia-0.7 (crate (name "magnesia") (vers "0.7.4") (hash "1x8j77pdn6psbvls7xk6vm90mkyvw9dwrqmi73cd5i5q55g4b004")))

(define-public crate-magnesia-0.7 (crate (name "magnesia") (vers "0.7.5") (hash "0ci7jqpfzcjghd3zd0hgw0wcx0zxzsl2hn7jwapm7f0jsp43n0iy")))

(define-public crate-magnesia-0.7 (crate (name "magnesia") (vers "0.7.6") (hash "10nsmw31yc8i4i6v8r1whmnjkj2x3mfddpgsk8c7203xk69jk64x")))

(define-public crate-magnesia-0.7 (crate (name "magnesia") (vers "0.7.7") (hash "0mzby2da3ri8vh0vyzydkhr7nr53ycrjymvw13xgii6i6kdvm9g6")))

(define-public crate-magnesia-0.7 (crate (name "magnesia") (vers "0.7.8") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0w47f1xabnilv0hwxcrbvcn0pnai4843ic6i3wmac240g8fnsypn")))

(define-public crate-magnesia-0.7 (crate (name "magnesia") (vers "0.7.9") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "01m08ysnr5xi49nly3i9hcjmv3p0vplwkv8s8sdqz5qz2aim6mb3")))

(define-public crate-magnesia-0.8 (crate (name "magnesia") (vers "0.8.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "194j71z71y6jan9a8qq163xkifsw8mzyyy48c83jax1g7jnnb9kh")))

(define-public crate-magnesia-0.8 (crate (name "magnesia") (vers "0.8.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1dzkzvimyq4mhz5plrd1xjjpswikvg258yjs7n7q5y7jg86m83pa")))

(define-public crate-magnesia-0.8 (crate (name "magnesia") (vers "0.8.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "14y0vd3yisw52wmcj71h0d97kphy5gmi98q0783qyp8w42iqdm6v")))

(define-public crate-magnesia-0.9 (crate (name "magnesia") (vers "0.9.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1s0pnl3dxdsrdm6grhfkjr9k9hxz680aqpqkhw03b8j1y43mqd5f")))

(define-public crate-magnesia-0.9 (crate (name "magnesia") (vers "0.9.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1r6hlav269407602m9s2hw08raqdlmaalls0y891k36m7nr8nxsh")))

(define-public crate-magnesia-0.9 (crate (name "magnesia") (vers "0.9.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1n49mbg1lawh9y0dcnly3mnc0ig1nlay2g3ama4ll22v7wnqnwfd")))

(define-public crate-magnesia-0.9 (crate (name "magnesia") (vers "0.9.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1i965xy5k3g1ib7fcyr6z8iz1davcyh1dkhsdql6hxi5rwcdnq05")))

(define-public crate-magnesia-0.10 (crate (name "magnesia") (vers "0.10.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1pwgb20ay2sjjlklp59hm7hp4akwkg98p6s6ij7k4rygmbcsjxxn")))

(define-public crate-magnesia-0.11 (crate (name "magnesia") (vers "0.11.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1zwdfyd97rxigrds3jd83x4qscq5pm5jbssab1l7kpms33d1cy9q")))

(define-public crate-magnesia-0.11 (crate (name "magnesia") (vers "0.11.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0x5c54m4xrfwl2v7wdnm3g22nk85km1xhjrif4d36ndb579ygh24")))

(define-public crate-magnesium-0.0.0 (crate (name "magnesium") (vers "0.0.0") (deps (list (crate-dep (name "bytemuck") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "getrandom") (req "^0.1") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "getrandom") (req "^0.1") (features (quote ("stdweb"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "linefeed") (req "^0.6") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "stdweb") (req "^0.4") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "textwrap") (req "^0.11") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "textwrap") (req "^0.11") (features (quote ("term_size"))) (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)))) (hash "03q8zd0nbjr41pxcqq961ad2hw8h0b6glq6czir0qg7fr3z2pd28") (yanked #t)))

(define-public crate-magnesium-0.0.1 (crate (name "magnesium") (vers "0.0.1") (hash "19q0x8qh6zdjnzm7nv6y9p2yzy0qr58cvlldxzh2vj9s2jf9jgg9")))

(define-public crate-magnesium-0.0.2 (crate (name "magnesium") (vers "0.0.2") (hash "156r82cw6jnssghrxjssj6h4sm51ph61i1y2dac0rzkis63dvhwh")))

(define-public crate-magnesium-0.0.3 (crate (name "magnesium") (vers "0.0.3") (hash "0sj70kxpfsnqm3ampps6pwqdlms28z6sypjjdb6ykwlch43gw85h")))

(define-public crate-magnesium-0.0.4 (crate (name "magnesium") (vers "0.0.4") (hash "1f3knrhzgxiii28j03s3vww2vn99z4lmc8spjnrzf924j83xfvgy")))

(define-public crate-magnesium-1 (crate (name "magnesium") (vers "1.0.0-alpha.0") (hash "0yhn5qzcczgpp7dpf82il4v9q8cv8yrxaw02b7lkmc6idqd4qnqy") (features (quote (("alloc"))))))

(define-public crate-magnesium-1 (crate (name "magnesium") (vers "1.0.0") (hash "02hmf1hjrqr3qg0ixiqrcak8znm4sdjazlbfh816ax4izdrqq1ca") (features (quote (("alloc"))))))

(define-public crate-magnesium-1 (crate (name "magnesium") (vers "1.1.0") (hash "0ibdx0gx46a2wfjx7sb0vbfgx1saq02sq0yg3x3y4gd4xdvzmg8c") (features (quote (("alloc"))))))

(define-public crate-magnesium-1 (crate (name "magnesium") (vers "1.2.0") (hash "1dglj13j7vpc55q7ykdhg4ph1m5aii6fifax1qcl0x6hf8alcbxc") (features (quote (("alloc"))))))

(define-public crate-magnesium-1 (crate (name "magnesium") (vers "1.2.1") (hash "168zzh93vyda5fch9715vw3i5hx12nw2p0xzvspsig2r7iwgmm7n") (features (quote (("alloc"))))))

(define-public crate-magnesium-1 (crate (name "magnesium") (vers "1.3.0") (hash "0f29ykm7di9y32gqd4b8i93ln1d0zmw9fm2kaah0cha2vhz8v93w") (features (quote (("alloc"))))))

(define-public crate-magnesium-engine-0.0.1 (crate (name "magnesium-engine") (vers "0.0.1") (hash "0v61ryvpdv09pnv5z5r8mq9a3g8dzh489wif9db1b835k4spfzl7")))

(define-public crate-magnesium-engine-0.0.2 (crate (name "magnesium-engine") (vers "0.0.2") (hash "1i9hzmq07n09r2cn2x4c7vmxgmbkffn38bq63b4bgni2i7ak7kgv")))

(define-public crate-magnet-0.1 (crate (name "magnet") (vers "0.1.0") (hash "1155rmmxkjj3v9rd51r5g0azlq6xak1adpjw324wdqal0srdx4r3") (yanked #t)))

(define-public crate-magnet-uri-0.1 (crate (name "magnet-uri") (vers "0.1.0") (deps (list (crate-dep (name "serde_urlencoded") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "1azcvxbp8f9nva90i72hj2km6ql09ivjv35v63rdsi3s1ljmz5xc")))

(define-public crate-magnet-uri-0.2 (crate (name "magnet-uri") (vers "0.2.0") (deps (list (crate-dep (name "serde_urlencoded") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "0i48b5m4ylx28vysxj1qxhiicndh3pnqacqwp0k4bnxq9djqmvbd")))

(define-public crate-magnet-url-1 (crate (name "magnet-url") (vers "1.0.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "18lzaaq2ml05x9j0kc7xl32h8rvymr59p5vd4lha5718zhzcmm36") (yanked #t)))

(define-public crate-magnet-url-1 (crate (name "magnet-url") (vers "1.0.1") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0vmab539gdyggi40n7mnp0kcgav4mpfvyz2ciazvq7a05fiib3yn") (yanked #t)))

(define-public crate-magnet-url-1 (crate (name "magnet-url") (vers "1.0.2") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0miiyqa7kn49g4lk11m4p38f24zcjiy2r6rr6wkdvd487xb6l653") (yanked #t)))

(define-public crate-magnet-url-1 (crate (name "magnet-url") (vers "1.0.3") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1rqz8vcy1rlh3cv0w0k0dwpc9hshkxzrw4lz13cf8k0il2w4dfjv")))

(define-public crate-magnet-url-1 (crate (name "magnet-url") (vers "1.0.4") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1vxpb8yj5xbmwd9h7c32xmbbkwxy8xn2zzj46bh223y65inzxszh")))

(define-public crate-magnet-url-1 (crate (name "magnet-url") (vers "1.0.5") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1jgw27lsdxzwrpgpz39yp0l9fq6nrsskbjbmlgvca4p76gnf2fcm")))

(define-public crate-magnet-url-1 (crate (name "magnet-url") (vers "1.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0w7b13hakspwig4zsxj1nm0agaacvylv8h2wah8il1zbqqxhasdc")))

(define-public crate-magnet-url-1 (crate (name "magnet-url") (vers "1.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "127h3vp6k3qniwykkfwbmbf81ymmffk97xfwqp7c53sqa53j7qiq")))

(define-public crate-magnet-url-1 (crate (name "magnet-url") (vers "1.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1x988phsrrnl8fkan9mjh0lsg7qqrcc5qiwskw66kr4sxbj1nffk")))

(define-public crate-magnet-url-1 (crate (name "magnet-url") (vers "1.2.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1whbrch7cwx1f24vw0q8divp4rkc71vzlin8yf0pq2vf4wp3p0lg")))

(define-public crate-magnet-url-1 (crate (name "magnet-url") (vers "1.2.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0h8x78d1n2ak41wa75rv33a574da9ac7asn8k3l2h4p7d56wm4p2")))

(define-public crate-magnet-url-2 (crate (name "magnet-url") (vers "2.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0x3xfdpag2p51i9rx8gcimigrj22aqp7hq0crh0cm2p80i04qfxc")))

(define-public crate-magnet_app-0.0.1 (crate (name "magnet_app") (vers "0.0.1") (deps (list (crate-dep (name "diesel") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "magnet_core") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "magnet_more") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "r2d2-diesel") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)))) (hash "089d4dwkhjhg1i4h7csf2nqaqjdvf76g7f2xl5ajmhjb6hlqh1lg")))

(define-public crate-magnet_core-0.0.1 (crate (name "magnet_core") (vers "0.0.1") (deps (list (crate-dep (name "hyper") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "typemap") (req "^0.3") (default-features #t) (kind 0)))) (hash "0r0jzghvwpggcaf3qscyry16azq0x9cvk7wzm72b89vf5wk631cy")))

(define-public crate-magnet_derive-0.1 (crate (name "magnet_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.13.1") (default-features #t) (kind 0)))) (hash "1bbw6an61r07lpxsgkw8iqfgcm8r8m0lbvs73jz0cqyq9ifbmr06")))

(define-public crate-magnet_derive-0.1 (crate (name "magnet_derive") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.13.1") (default-features #t) (kind 0)))) (hash "0ghlwah8smvfbgfarmqda6zidw1l8ixqsjng9x73ca2y04ar5g5l")))

(define-public crate-magnet_derive-0.1 (crate (name "magnet_derive") (vers "0.1.2") (deps (list (crate-dep (name "quote") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.13.1") (default-features #t) (kind 0)))) (hash "0dpwr0c9yg1l86hcmxpda2zsxck5svk3p8d5mgwbxmimlr9c7sbi")))

(define-public crate-magnet_derive-0.1 (crate (name "magnet_derive") (vers "0.1.3") (deps (list (crate-dep (name "quote") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.13.1") (default-features #t) (kind 0)))) (hash "04vmw4yrq321n5gj98dhy4i6mqvc1ms51b147ag99hif95hgcxnp")))

(define-public crate-magnet_derive-0.1 (crate (name "magnet_derive") (vers "0.1.4") (deps (list (crate-dep (name "quote") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.13.1") (default-features #t) (kind 0)))) (hash "04k8xi9r275p1f4qpl7varawq0a8mw8y16xfpjps8g4qfd6gym33")))

(define-public crate-magnet_derive-0.2 (crate (name "magnet_derive") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.13.1") (default-features #t) (kind 0)))) (hash "0m1p7nnsqh7wchncfxdvwxmjd7rx0xn69wix7v4bfv78l28wc6j1")))

(define-public crate-magnet_derive-0.2 (crate (name "magnet_derive") (vers "0.2.1") (deps (list (crate-dep (name "quote") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.13.11") (default-features #t) (kind 0)))) (hash "0s579707rz8jzx3623ycddb06bwl0fbjv7rhikgnm08lkdxdsr4m")))

(define-public crate-magnet_derive-0.3 (crate (name "magnet_derive") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14.4") (default-features #t) (kind 0)))) (hash "012jr1rw1cxbmi974zsi1q337nj1gk0xjflhf25k2ipf1l1bmnzr")))

(define-public crate-magnet_derive-0.3 (crate (name "magnet_derive") (vers "0.3.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14.4") (default-features #t) (kind 0)))) (hash "1mvjmkbhyldcms7yv6kmq01b2kqwj5rlwpk3322fwmb3np8r3rqz")))

(define-public crate-magnet_derive-0.3 (crate (name "magnet_derive") (vers "0.3.2") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14.5") (default-features #t) (kind 0)))) (hash "06zi5b0n25bjnhnjisg6yklla8rssf8zn2kcgkrx3n3s6fpw5kay")))

(define-public crate-magnet_derive-0.3 (crate (name "magnet_derive") (vers "0.3.3") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14.7") (default-features #t) (kind 0)))) (hash "0d17mjyj1jqxsy1fyk1bwgfgclgb97k8si6nkqqn8n30idv8nhbd")))

(define-public crate-magnet_derive-0.4 (crate (name "magnet_derive") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14.7") (default-features #t) (kind 0)))) (hash "1ap40n7h01ywmx3wzxxrmb1f2gn6x9had96qv9sg7vmzpi77vhcs")))

(define-public crate-magnet_derive-0.5 (crate (name "magnet_derive") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14.9") (default-features #t) (kind 0)))) (hash "1sr2la0gvkghjg9b85zi1shf556vbnrhz3aznj6p10n7js8k77wv")))

(define-public crate-magnet_derive-0.6 (crate (name "magnet_derive") (vers "0.6.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14.9") (default-features #t) (kind 0)))) (hash "01s7r82g68cqzmvxwak559jn48c1672c04rv80ng9gzqsbw0if65")))

(define-public crate-magnet_derive-0.7 (crate (name "magnet_derive") (vers "0.7.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14.9") (default-features #t) (kind 0)))) (hash "0gn503f4x4nkqkgw9hp4hvh7q4cj4kmmjswcxflsa2yrjp27ai9r")))

(define-public crate-magnet_derive-0.8 (crate (name "magnet_derive") (vers "0.8.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14.9") (default-features #t) (kind 0)))) (hash "1zf883066sxg8aw9dw34knz9cbgcq94jbcgcrm5xvalr8gy4b9wc")))

(define-public crate-magnet_more-0.0.1 (crate (name "magnet_more") (vers "0.0.1") (deps (list (crate-dep (name "liquid") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "magnet_core") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)))) (hash "1b7ij7y68829j5dzg39qf07r242m6h968c67dm958911a9nxcr3h")))

(define-public crate-magnet_rs-0.1 (crate (name "magnet_rs") (vers "0.1.0") (hash "1j3mfxarn3dj7chc03zi5vf63bfri7ifv6v1xm8zn2ckzd5f5s87")))

(define-public crate-magnet_rs-0.2 (crate (name "magnet_rs") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.16") (features (quote ("rayon"))) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1ahm26307gp62amnr1k6zw4wza89qskgkb13jca4ccbm0lfklgvr")))

(define-public crate-magnet_rs-0.2 (crate (name "magnet_rs") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.16") (features (quote ("rayon"))) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1qa0a1xkc5mfhhwww1kwwg2qi2q9qmfbvgi92i4hp2ry3rzawmm0")))

(define-public crate-magnet_rs-0.2 (crate (name "magnet_rs") (vers "0.2.3") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "lodestone_core") (req "^0.2") (default-features #t) (kind 0)))) (hash "0vxdmxwqyrprylchzf61s3f96g81jbbzj02d2s689q539bgxy8q4")))

(define-public crate-magnet_schema-0.1 (crate (name "magnet_schema") (vers "0.1.0") (deps (list (crate-dep (name "bson") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "magnet_derive") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "1slzamh7ihrnbx029r5s0lpr3pw63f61scq8l71dgwi3bns4ffhy")))

(define-public crate-magnet_schema-0.1 (crate (name "magnet_schema") (vers "0.1.1") (deps (list (crate-dep (name "bson") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "magnet_derive") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.6") (optional #t) (default-features #t) (kind 0)))) (hash "1jxn0nflsqk439i0k9cb4r58n82h1j80vpjqxxwnwhhggvnhmvdh")))

(define-public crate-magnet_schema-0.1 (crate (name "magnet_schema") (vers "0.1.2") (deps (list (crate-dep (name "bson") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "magnet_derive") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.6") (optional #t) (default-features #t) (kind 0)))) (hash "19kxijsql1576jxcpkddfzakn7h0nrxl1lldnarkh949r9cdn3cp")))

(define-public crate-magnet_schema-0.1 (crate (name "magnet_schema") (vers "0.1.3") (deps (list (crate-dep (name "bson") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "magnet_derive") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.6") (optional #t) (default-features #t) (kind 0)))) (hash "0n07rbdjgcp5yhjjji1xiyp46rl7vm2mi57w1cmx4kvqdr311dax")))

(define-public crate-magnet_schema-0.1 (crate (name "magnet_schema") (vers "0.1.4") (deps (list (crate-dep (name "bson") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "magnet_derive") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.6") (optional #t) (default-features #t) (kind 0)))) (hash "1ih5jcwlr982z4c80jdn8glmg93xiqc7npb4a50gh7hbi1046zm4")))

(define-public crate-magnet_schema-0.2 (crate (name "magnet_schema") (vers "0.2.0") (deps (list (crate-dep (name "bson") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "magnet_derive") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.6") (optional #t) (default-features #t) (kind 0)))) (hash "0ibk76710n79xmara4b5h8y2z8vny855hk5cam1dw8577v42mzif")))

(define-public crate-magnet_schema-0.2 (crate (name "magnet_schema") (vers "0.2.1") (deps (list (crate-dep (name "bson") (req "^0.12.2") (default-features #t) (kind 0)) (crate-dep (name "magnet_derive") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.6") (optional #t) (default-features #t) (kind 0)))) (hash "1wzr8gw399csgr5zy861z6r7gvi0yvbbzi8g82qg1ycz88lvrf0i")))

(define-public crate-magnet_schema-0.3 (crate (name "magnet_schema") (vers "0.3.0") (deps (list (crate-dep (name "bson") (req "^0.12.2") (default-features #t) (kind 0)) (crate-dep (name "magnet_derive") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.6") (optional #t) (default-features #t) (kind 0)))) (hash "0l4p31h7a11l4v0nv9vjkq17sjyd2a5g8iqd1zg77i4qkhflb5n7")))

(define-public crate-magnet_schema-0.3 (crate (name "magnet_schema") (vers "0.3.1") (deps (list (crate-dep (name "bson") (req "^0.12.2") (default-features #t) (kind 0)) (crate-dep (name "magnet_derive") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.6") (optional #t) (default-features #t) (kind 0)))) (hash "01vwcqdcp2qmzmx18qs2q0bgzfdvvap0awrsygsk9xn1ijninq4r")))

(define-public crate-magnet_schema-0.3 (crate (name "magnet_schema") (vers "0.3.2") (deps (list (crate-dep (name "bson") (req "^0.12.2") (default-features #t) (kind 0)) (crate-dep (name "magnet_derive") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.6") (optional #t) (default-features #t) (kind 0)))) (hash "0izyfzsybqv4g74wdrj9qg9g7qlmaajhrq1rc1q0zbknjmq3bwkm")))

(define-public crate-magnet_schema-0.3 (crate (name "magnet_schema") (vers "0.3.3") (deps (list (crate-dep (name "bson") (req "^0.12.3") (default-features #t) (kind 0)) (crate-dep (name "magnet_derive") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.6") (optional #t) (default-features #t) (kind 0)))) (hash "130a47ifxsp03y0s4czk3kmml3ydklsfzp5smznvxp83in635v3b")))

(define-public crate-magnet_schema-0.4 (crate (name "magnet_schema") (vers "0.4.0") (deps (list (crate-dep (name "bson") (req "^0.13.0") (features (quote ("u2i"))) (default-features #t) (kind 0)) (crate-dep (name "magnet_derive") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.6") (optional #t) (default-features #t) (kind 0)))) (hash "1nwg77fs417cb4cfnnmh7frd2lmcly4w50ya6qzkxzyxxmv10dv6")))

(define-public crate-magnet_schema-0.5 (crate (name "magnet_schema") (vers "0.5.0") (deps (list (crate-dep (name "bson") (req "^0.13.0") (features (quote ("u2i"))) (default-features #t) (kind 0)) (crate-dep (name "magnet_derive") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.6") (optional #t) (default-features #t) (kind 0)))) (hash "1dvy83bp66gafsnzifkc619wcpvcwk66phzdwj41dy2il4b4kvp9")))

(define-public crate-magnet_schema-0.6 (crate (name "magnet_schema") (vers "0.6.0") (deps (list (crate-dep (name "bson") (req "^0.13.0") (features (quote ("u2i"))) (default-features #t) (kind 0)) (crate-dep (name "magnet_derive") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.6") (optional #t) (default-features #t) (kind 0)))) (hash "0msg5cd9qz6kjwq6g1q3rapxzsbq3wry4vhgpwpcs888h1jhdpcl")))

(define-public crate-magnet_schema-0.7 (crate (name "magnet_schema") (vers "0.7.0") (deps (list (crate-dep (name "bson") (req "^0.13.0") (features (quote ("u2i"))) (default-features #t) (kind 0)) (crate-dep (name "magnet_derive") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.7.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.7.1") (features (quote ("v4" "serde"))) (optional #t) (default-features #t) (kind 0)))) (hash "0b7arb592yisl9krhd2s8fd559zm2b0jdma34z35cvr4vmq5ka8q")))

(define-public crate-magnet_schema-0.8 (crate (name "magnet_schema") (vers "0.8.0") (deps (list (crate-dep (name "bson") (req "^0.13.0") (features (quote ("u2i"))) (default-features #t) (kind 0)) (crate-dep (name "magnet_derive") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.7.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.7.1") (features (quote ("v4" "serde"))) (optional #t) (default-features #t) (kind 0)))) (hash "1a9f7awdb0vc6yk1qsbz63bwbl8hwgb2x0rw4cay1iy0ipyvzbw6")))

(define-public crate-magnetar-0.1 (crate (name "magnetar") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "axum") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "magnetar_core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.24") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tower") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tower-http") (req "^0.3") (features (quote ("cors" "trace"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (features (quote ("env-filter"))) (default-features #t) (kind 0)))) (hash "1kfnnbn8pwciqrn1rbdjxs1azr0jrxsqnfzmjsy41vzb6za6hdlz")))

(define-public crate-magnetar_core-0.1 (crate (name "magnetar_core") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0f17i257i1w9z109yd9vj15ai4v3q62hkv2qq8y7ddalwg7i8smb")))

(define-public crate-magnetease-0.1 (crate (name "magnetease") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "macros"))) (default-features #t) (kind 0)))) (hash "14p155p24y0gasv4p3fd3aj9wb0dnz95h5sgq482h341r6ln2him")))

(define-public crate-magnetease-0.1 (crate (name "magnetease") (vers "0.1.1") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "rustls-tls"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "macros"))) (default-features #t) (kind 0)))) (hash "18zgz2njrr1671301haz24cms972j88ssdgkki7fhjz81jdaaks6")))

(define-public crate-magnetease-0.1 (crate (name "magnetease") (vers "0.1.2") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12") (features (quote ("json" "rustls-tls"))) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "macros"))) (default-features #t) (kind 0)))) (hash "1s20ghaz7n4x7pnp5v10bb0bz3da9adj9nng5iy2g01ryf8z8z20")))

(define-public crate-magnetfinder-0.1 (crate (name "magnetfinder") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "comfy-table") (req "^4.1.1") (default-features #t) (kind 0)) (crate-dep (name "config") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^3.0.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "12l5jh5w8ky0yalh6jk8wi6vx8jrdm12m373lq18ylrdm2glmg8q")))

(define-public crate-magnetfinder-0.2 (crate (name "magnetfinder") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "comfy-table") (req "^4.1.1") (default-features #t) (kind 0)) (crate-dep (name "config") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^3.0.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "09sv3yfmzh9413fbcyi1kg979gsyab2njh79s1dvjmmf98l00c1g")))

(define-public crate-magnetfinder-0.3 (crate (name "magnetfinder") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "comfy-table") (req "^4.1.1") (default-features #t) (kind 0)) (crate-dep (name "config") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^3.0.2") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.2.0") (features (quote ("json" "socks-proxy"))) (default-features #t) (kind 0)))) (hash "0xzc7sjl29n6iqgnfszvwjy3qbhsan11ims8ldb0wqzgshir1fmj")))

(define-public crate-magnetfinder-0.4 (crate (name "magnetfinder") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "comfy-table") (req "^4.1.1") (default-features #t) (kind 0)) (crate-dep (name "config") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^3.0.2") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.2.0") (features (quote ("json" "socks-proxy"))) (default-features #t) (kind 0)))) (hash "0ajwqm6sp5cck7i52lfdli3si24shbzzfcgyq98rw19ykrfrqb27")))

(define-public crate-magnetic-1 (crate (name "magnetic") (vers "1.0.0") (hash "1wl33nkbkr21a8hqzql1jw8qnqcr533y7rzad97pajz6fp0z90g3")))

(define-public crate-magnetic-1 (crate (name "magnetic") (vers "1.0.1") (hash "1s5wihrfhjk2jv3s2ra9sms7fd8lgn34grxxnm68wr2yz6vv5ivl")))

(define-public crate-magnetic-1 (crate (name "magnetic") (vers "1.0.2") (hash "1qp2832fllfqxidxrwp8g1hxfzlh184fddqnf82h7n6nsj39kikp")))

(define-public crate-magnetic-2 (crate (name "magnetic") (vers "2.0.0") (hash "1cy0fda7vkby9xfv573aqmmdrizksdfgi4h8nhv9qv50f16n2rc7") (features (quote (("unstable"))))))

(define-public crate-magnetic-2 (crate (name "magnetic") (vers "2.0.1") (hash "1y5fl099jyhmm962zpigga6fhvn7i7rhv1h0fvn5y7m5fmfzlkgl") (features (quote (("unstable"))))))

(define-public crate-magnetic-2 (crate (name "magnetic") (vers "2.1.0") (deps (list (crate-dep (name "crossbeam-utils") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1") (default-features #t) (kind 2)))) (hash "0x0k2bh75rwikk9p3nnwzvhj9c5hr7lyc6hkx6rxaij3w9angjh4") (features (quote (("unstable"))))))

(define-public crate-magnetic-2 (crate (name "magnetic") (vers "2.2.0") (deps (list (crate-dep (name "crossbeam-utils") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1") (default-features #t) (kind 2)))) (hash "0ng47hbsgd8pw489mhhybwv9bjlvlffai7lc9vmvz4kcz3bfgysl") (features (quote (("unstable"))))))

(define-public crate-magnetic-2 (crate (name "magnetic") (vers "2.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-utils") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1") (default-features #t) (kind 2)))) (hash "0qsnw6r0jqmdzk1aqqxcgk911kf2z86y0qm6yykzd6zyvqdlqhj9") (yanked #t)))

(define-public crate-magnetic-2 (crate (name "magnetic") (vers "2.4.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-utils") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1") (default-features #t) (kind 2)))) (hash "0d9mrbsr2b1pxbmd9fhmm1gm9kyn1xjnfvga1hvipa6qkyss77nn")))

(define-public crate-magnetic-2 (crate (name "magnetic") (vers "2.4.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-utils") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1") (default-features #t) (kind 2)))) (hash "01zahkp1mrf7b80r00nz76d4a1y0ylmc26dmcshq597npnqpix7x")))

(define-public crate-magnetic-monopole-0.1 (crate (name "magnetic-monopole") (vers "0.1.0") (deps (list (crate-dep (name "mathrs") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "1n7h4bja1pcia4pa802bi8vignj3asaql8m2pnb52pc0vy8v2155") (yanked #t)))

(define-public crate-magnetic-monopole-0.1 (crate (name "magnetic-monopole") (vers "0.1.1") (deps (list (crate-dep (name "mathrs") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "15n0fgbyydjbbv07w3vqw0xdw1ndpbf93j3mc68jr05xi24fv5j7")))

(define-public crate-magnetic-monopole-0.2 (crate (name "magnetic-monopole") (vers "0.2.0") (deps (list (crate-dep (name "mathrs") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "0yp5hcjna9w2bx2rx4n68fq4kr7m9vrxch5bzh3fbhab0ziv6y0d")))

(define-public crate-magnetite-0.0.1 (crate (name "magnetite") (vers "0.0.1") (hash "1i4flngh71902ils4m930h0kpvh7lsdiaqdv19272jn5mkxg9ybl")))

(define-public crate-magneto-0.0.0 (crate (name "magneto") (vers "0.0.0") (hash "0fnsvnig59f2ic7x4nzgmgck649vlh410phiji9mxh2g1n2v7z3c")))

(define-public crate-magnetron-0.1 (crate (name "magnetron") (vers "0.1.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1x9wkr2663hgg2a5mgcp4vgky38zh8kdq6pjwgb9z91c890q557p") (rust-version "1.61")))

(define-public crate-magnetron-0.1 (crate (name "magnetron") (vers "0.1.1") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0z0s8763bxazrr7pvv5w45n1q9nhidw188id3pfwhh2csfzjq599") (rust-version "1.61")))

(define-public crate-magnetron-0.2 (crate (name "magnetron") (vers "0.2.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "10pi02ycrbf2sml223sljxdbglswb3yglkzp8lh9d4rha01kvifk") (rust-version "1.61")))

(define-public crate-magnetron-0.3 (crate (name "magnetron") (vers "0.3.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.117") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0ijaw3apc6bac5g1cvsni1alb6jg31w7i3cnpfm9zwsgmqpjmyjb") (rust-version "1.61")))

(define-public crate-magnetron-0.4 (crate (name "magnetron") (vers "0.4.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.117") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1amgbz8cnr17vngpfbji1h4hdcsw6qar1clyyaja8pgidisr5nv6") (rust-version "1.61")))

(define-public crate-magnetron-0.5 (crate (name "magnetron") (vers "0.5.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.117") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1nr5sf3fvrvzrzkw4v245yip1mf97q8h2vbsbhdmdqx7s8k0v90h") (rust-version "1.66")))

(define-public crate-magnify-0.1 (crate (name "magnify") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)))) (hash "17fi3pzi657sm744az87qvgsa8s40p3m26h4masddp3wvpb6c7in")))

(define-public crate-magnitude-0.1 (crate (name "magnitude") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "1m2n0x6k89lv75mk4iz7vyfs7plkbclcjxlxr36df620am288dfg")))

(define-public crate-magnitude-0.1 (crate (name "magnitude") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "0gj2yd9fd9szi3nmiix0jqmpl8pxdaimh7bx4cmymcmj1b8f1yqz")))

(define-public crate-magnitude-0.2 (crate (name "magnitude") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "0984afikwc5zpxzdr59xm6m019bfmkjnq4n9l3wg0h0ap0dbx204")))

(define-public crate-magnitude-0.3 (crate (name "magnitude") (vers "0.3.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "0yn9alyw3irwgkrar8l1kazcl6sqwjni74hy4rb1n9cmaqjhix6j")))

(define-public crate-magnitude-0.3 (crate (name "magnitude") (vers "0.3.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "1k117qcgmci1fw4mabvif1wg8qhwdk1rzpzy60jl94nmkpipj5gd")))

(define-public crate-magnitude-0.3 (crate (name "magnitude") (vers "0.3.2") (deps (list (crate-dep (name "num-traits") (req ">=0.2.12, <0.3.0") (default-features #t) (kind 0)))) (hash "1jyjpd76i3xdx74r99ymliha8c3krxwh0fbvprsx6xmj1gjlrdiv")))

(define-public crate-magnolia-0.1 (crate (name "magnolia") (vers "0.1.0") (hash "0clcccq6shii1m7671cq8vhd4s1jl5f4i3slkvxj05qa5qw1mk02") (yanked #t)))

(define-public crate-magnum-0.1 (crate (name "magnum") (vers "0.1.0") (deps (list (crate-dep (name "audiopus") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "bitreader") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "caf") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "kira") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ogg") (req "^0.8.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.13.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "01nczvaw6zdgdcbc5ggxx86l1d722nz17d253m4jq6lh05p78q7x") (features (quote (("with_rodio" "rodio") ("with_ogg" "ogg") ("with_kira" "kira") ("with_caf" "caf") ("default" "with_caf" "with_ogg") ("all" "with_caf" "with_ogg" "with_rodio" "with_kira"))))))

(define-public crate-magnum-0.1 (crate (name "magnum") (vers "0.1.1") (deps (list (crate-dep (name "audiopus") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "bitreader") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "caf") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "kira") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ogg") (req "^0.8.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.13.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "000dk0xydsn6qd0s79p3xpax6544jr9j9041y40p6li5zqirl5hi") (features (quote (("with_rodio" "rodio") ("with_ogg" "ogg") ("with_kira" "kira") ("with_caf" "caf") ("default" "with_caf" "with_ogg") ("all" "with_caf" "with_ogg" "with_rodio" "with_kira"))))))

(define-public crate-magnum-1 (crate (name "magnum") (vers "1.0.0") (deps (list (crate-dep (name "audiopus") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "bitreader") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "caf") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "kira") (req "^0.5.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ogg") (req "^0.8.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.14.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "0smjw6kfm3j1gk9qfv39jsqv7r13c9pp0h71jshlfcad7qax553d") (features (quote (("with_rodio" "rodio") ("with_ogg" "ogg") ("with_kira" "kira") ("with_caf" "caf") ("default" "with_caf" "with_ogg") ("all" "with_caf" "with_ogg" "with_rodio" "with_kira"))))))

(define-public crate-magnum-opus-0.3 (crate (name "magnum-opus") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "opusic-sys") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0rf8jqcg25ny4ycp846srzyc081v0mgz8qxfw72nhv4f278bb0l1")))

(define-public crate-magnum-opus-0.3 (crate (name "magnum-opus") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "opusic-sys") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1v9s1dmrlpn67ndnzb7wzy83zsa8fchqnjxvzwyn1g3h0wwk9k6y")))

(define-public crate-magnum-opus-0.3 (crate (name "magnum-opus") (vers "0.3.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "opusic-sys") (req "^0.3") (default-features #t) (kind 0)))) (hash "1hpcld3qapa5bpc80j6jq4pm9l3sy5min7ic55mlgbhkfcmasjgz")))

(define-public crate-magnus-0.1 (crate (name "magnus") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "magnus-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1px8xq4l7s58bj76cvnsli8nv6jry93zizc2by267hlq6g4dq98q") (features (quote (("embed")))) (links "ruby")))

(define-public crate-magnus-0.2 (crate (name "magnus") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "magnus-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1jbg39y50prl6hifqrvxab24h501f67kx255ki7xxyrhyypxafhx") (features (quote (("embed")))) (links "ruby")))

(define-public crate-magnus-0.2 (crate (name "magnus") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "magnus-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0b52kgnbsscy87jzarwngcfqsc52mib3dclqgqm3q8ysxxrhcwpi") (features (quote (("embed")))) (links "ruby")))

(define-public crate-magnus-0.3 (crate (name "magnus") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "magnus-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rb-sys") (req "^0.8.1") (optional #t) (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "rb-sys") (req "^0.8.1") (features (quote ("link-ruby"))) (optional #t) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1qgf1m14hijvj6iipr6gzb2fnhiiih796a49ja9a6habwjvyianv") (features (quote (("rb-sys-interop" "rb-sys") ("embed"))))))

(define-public crate-magnus-0.3 (crate (name "magnus") (vers "0.3.1") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "magnus-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rb-sys") (req "^0.8.1") (optional #t) (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "rb-sys") (req "^0.8.1") (features (quote ("link-ruby"))) (optional #t) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0jiljv6f0y7iwxj0gdlcaacrd71sjrgrmj6rxf9k70q04rif6wp7") (features (quote (("rb-sys-interop" "rb-sys") ("embed"))))))

(define-public crate-magnus-0.3 (crate (name "magnus") (vers "0.3.2") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "magnus-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rb-sys") (req "^0.8.1") (optional #t) (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "rb-sys") (req "^0.8.1") (features (quote ("link-ruby"))) (optional #t) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1lv95d9m6mxqzvsf97c5nr9ws2wkzd95x2yy0kw495ifi8riaglq") (features (quote (("rb-sys-interop" "rb-sys") ("embed"))))))

(define-public crate-magnus-0.4 (crate (name "magnus") (vers "0.4.0") (deps (list (crate-dep (name "magnus-macros") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rb-sys") (req "^0.9.44") (features (quote ("bindgen-rbimpls" "bindgen-deprecated-types"))) (kind 0)) (crate-dep (name "rb-sys-env") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "1z3ps0idc89wlijyk81bhdj0nqd9dzimypq9kmnfjkgcyzpmada5") (features (quote (("ruby-static" "rb-sys/ruby-static") ("rb-sys-interop") ("embed" "rb-sys/link-ruby"))))))

(define-public crate-magnus-0.4 (crate (name "magnus") (vers "0.4.1") (deps (list (crate-dep (name "magnus-macros") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rb-sys") (req "^0.9.44") (features (quote ("bindgen-rbimpls" "bindgen-deprecated-types"))) (kind 0)) (crate-dep (name "rb-sys-env") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "0dpvb44w7x6sfsn6p0fma5ipmnh9aspywfi7036xydn394g15785") (features (quote (("ruby-static" "rb-sys/ruby-static") ("rb-sys-interop") ("embed" "rb-sys/link-ruby"))))))

(define-public crate-magnus-0.4 (crate (name "magnus") (vers "0.4.2") (deps (list (crate-dep (name "magnus-macros") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rb-sys") (req "^0.9.44") (features (quote ("bindgen-rbimpls" "bindgen-deprecated-types"))) (kind 0)) (crate-dep (name "rb-sys-env") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "00ajmkxyvl890z9gpxlg7ys9028da66mqky3yf0hgalsnfcbgxx5") (features (quote (("ruby-static" "rb-sys/ruby-static") ("rb-sys-interop") ("embed" "rb-sys/link-ruby"))))))

(define-public crate-magnus-0.4 (crate (name "magnus") (vers "0.4.3") (deps (list (crate-dep (name "magnus-macros") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rb-sys") (req "^0.9.44") (features (quote ("bindgen-rbimpls" "bindgen-deprecated-types"))) (kind 0)) (crate-dep (name "rb-sys-env") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "1b1llrmf8sx03vsngjmw2z5g83sqh2pmfchkgynb1qm4bpsh9nf7") (features (quote (("ruby-static" "rb-sys/ruby-static") ("rb-sys-interop") ("embed" "rb-sys/link-ruby"))))))

(define-public crate-magnus-0.4 (crate (name "magnus") (vers "0.4.4") (deps (list (crate-dep (name "magnus-macros") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rb-sys") (req "^0.9.44") (features (quote ("bindgen-rbimpls" "bindgen-deprecated-types"))) (kind 0)) (crate-dep (name "rb-sys-env") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "0v3xqn3m033h8czidk90slkwkmalvqvch96mpzfrz96ssw66d1zw") (features (quote (("ruby-static" "rb-sys/ruby-static") ("rb-sys-interop") ("embed" "rb-sys/link-ruby"))))))

(define-public crate-magnus-0.5 (crate (name "magnus") (vers "0.5.0") (deps (list (crate-dep (name "bytes") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "magnus-macros") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rb-sys") (req "^0.9.56") (features (quote ("bindgen-rbimpls" "bindgen-deprecated-types"))) (kind 0)) (crate-dep (name "rb-sys-env") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "1yfd3hn6mpr94syc7zk8zj5las1v5zlydl17s83gcxr4jjd42dxg") (features (quote (("ruby-static" "rb-sys/ruby-static") ("rb-sys-interop") ("embed" "rb-sys/link-ruby") ("bytes-crate" "bytes"))))))

(define-public crate-magnus-0.5 (crate (name "magnus") (vers "0.5.1") (deps (list (crate-dep (name "bytes") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "magnus-macros") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rb-sys") (req "^0.9.56") (features (quote ("bindgen-rbimpls" "bindgen-deprecated-types"))) (kind 0)) (crate-dep (name "rb-sys-env") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "1g4mimv6cy9fi9ngi1dah3y7k6z7yv5k96rrl5b970ny87bn67xc") (features (quote (("ruby-static" "rb-sys/ruby-static") ("rb-sys-interop") ("embed" "rb-sys/link-ruby") ("bytes-crate" "bytes"))))))

(define-public crate-magnus-0.5 (crate (name "magnus") (vers "0.5.2") (deps (list (crate-dep (name "bytes") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "magnus-macros") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rb-sys") (req "^0.9.56") (features (quote ("bindgen-rbimpls" "bindgen-deprecated-types"))) (kind 0)) (crate-dep (name "rb-sys-env") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "198qb4xj5vb86zm4az373bpl8pjvas90br5kjq4v8c0id18s7zgk") (features (quote (("ruby-static" "rb-sys/ruby-static") ("rb-sys-interop") ("embed" "rb-sys/link-ruby") ("bytes-crate" "bytes"))))))

(define-public crate-magnus-0.5 (crate (name "magnus") (vers "0.5.3") (deps (list (crate-dep (name "bytes") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "magnus-macros") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rb-sys") (req "^0.9.56") (features (quote ("bindgen-rbimpls" "bindgen-deprecated-types"))) (kind 0)) (crate-dep (name "rb-sys-env") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "10db6lzxwflz1ss1s4kwhmxg2xnahvs62aanxx9yfli57i319p68") (features (quote (("ruby-static" "rb-sys/ruby-static") ("rb-sys-interop") ("embed" "rb-sys/link-ruby") ("bytes-crate" "bytes"))))))

(define-public crate-magnus-0.5 (crate (name "magnus") (vers "0.5.4") (deps (list (crate-dep (name "bytes") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "magnus-macros") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rb-sys") (req "^0.9.56") (features (quote ("bindgen-rbimpls" "bindgen-deprecated-types"))) (kind 0)) (crate-dep (name "rb-sys-env") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "01bm7j2p6jvr78hhgwfkmljk4hl7cf4j7a3v1g1wg7jc530c6q5m") (features (quote (("ruby-static" "rb-sys/ruby-static") ("rb-sys-interop") ("embed" "rb-sys/link-ruby") ("bytes-crate" "bytes"))))))

(define-public crate-magnus-0.6 (crate (name "magnus") (vers "0.6.0") (deps (list (crate-dep (name "bytes") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "magnus-macros") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "rb-sys") (req "^0.9.77") (features (quote ("bindgen-rbimpls" "bindgen-deprecated-types"))) (kind 0)) (crate-dep (name "rb-sys-env") (req "^0.1.1") (default-features #t) (kind 1)) (crate-dep (name "seq-macro") (req "^0.3") (default-features #t) (kind 0)))) (hash "0hrpnc0jda4r5s6zz79zkw0f1l4v6jsqwv8bn7k8hvi3zrdmisb8") (features (quote (("ruby-static" "rb-sys/ruby-static") ("rb-sys") ("friendly-api") ("embed" "rb-sys/link-ruby") ("deprecated-send-sync-value") ("default" "friendly-api")))) (v 2) (features2 (quote (("bytes" "dep:bytes"))))))

(define-public crate-magnus-0.5 (crate (name "magnus") (vers "0.5.5") (deps (list (crate-dep (name "bytes") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "magnus-macros") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rb-sys") (req "^0.9.81") (features (quote ("bindgen-rbimpls" "bindgen-deprecated-types" "stable-api"))) (kind 0)) (crate-dep (name "rb-sys") (req "^0.9") (features (quote ("stable-api-compiled-fallback"))) (kind 2)) (crate-dep (name "rb-sys-env") (req "^0.1.2") (default-features #t) (kind 1)))) (hash "1knbqg4v9cd6z9ckpb8h6j6vvyl1jdcdaijqxy0x0cy41dkkfyjv") (features (quote (("ruby-static" "rb-sys/ruby-static") ("rb-sys-interop") ("embed" "rb-sys/link-ruby") ("bytes-crate" "bytes"))))))

(define-public crate-magnus-0.6 (crate (name "magnus") (vers "0.6.1") (deps (list (crate-dep (name "bytes") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "magnus-macros") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "rb-sys") (req "^0.9.81") (features (quote ("bindgen-rbimpls" "bindgen-deprecated-types" "stable-api"))) (kind 0)) (crate-dep (name "rb-sys") (req "^0.9") (features (quote ("stable-api-compiled-fallback"))) (kind 2)) (crate-dep (name "rb-sys-env") (req "^0.1.2") (default-features #t) (kind 1)) (crate-dep (name "seq-macro") (req "^0.3") (default-features #t) (kind 0)))) (hash "15a6nrqg2pbn38l0qfmfnqcqnlw3rnacn2wim1q85kpq8mx8j5h5") (features (quote (("ruby-static" "rb-sys/ruby-static") ("rb-sys") ("friendly-api") ("embed" "rb-sys/link-ruby") ("deprecated-send-sync-value") ("default" "friendly-api")))) (v 2) (features2 (quote (("bytes" "dep:bytes"))))))

(define-public crate-magnus-0.6 (crate (name "magnus") (vers "0.6.2") (deps (list (crate-dep (name "bytes") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "magnus-macros") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "rb-sys") (req "^0.9.81") (features (quote ("bindgen-rbimpls" "bindgen-deprecated-types" "stable-api"))) (kind 0)) (crate-dep (name "rb-sys") (req "^0.9") (features (quote ("stable-api-compiled-fallback"))) (kind 2)) (crate-dep (name "rb-sys-env") (req "^0.1.2") (default-features #t) (kind 1)) (crate-dep (name "seq-macro") (req "^0.3") (default-features #t) (kind 0)))) (hash "04rs3gkbbn29njpb63div19fljr8pw74c8n6x4l88vk7jr3m8y27") (features (quote (("ruby-static" "rb-sys/ruby-static") ("rb-sys") ("friendly-api") ("embed" "rb-sys/link-ruby") ("deprecated-send-sync-value") ("default" "friendly-api")))) (v 2) (features2 (quote (("bytes" "dep:bytes"))))))

(define-public crate-magnus-0.6 (crate (name "magnus") (vers "0.6.3") (deps (list (crate-dep (name "bytes") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "magnus-macros") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "rb-sys") (req "^0.9.81") (features (quote ("bindgen-rbimpls" "bindgen-deprecated-types" "stable-api"))) (kind 0)) (crate-dep (name "rb-sys") (req "^0.9") (features (quote ("stable-api-compiled-fallback"))) (kind 2)) (crate-dep (name "rb-sys-env") (req "^0.1.2") (default-features #t) (kind 1)) (crate-dep (name "seq-macro") (req "^0.3") (default-features #t) (kind 0)))) (hash "17baxmqlzz34a0jck3wby32wq7v0fxarg9h9rpin2ixnn0gs7iqg") (features (quote (("ruby-static" "rb-sys/ruby-static") ("rb-sys") ("friendly-api") ("embed" "rb-sys/link-ruby") ("deprecated-send-sync-value") ("default" "friendly-api")))) (v 2) (features2 (quote (("bytes" "dep:bytes"))))))

(define-public crate-magnus-0.6 (crate (name "magnus") (vers "0.6.4") (deps (list (crate-dep (name "bytes") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "magnus-macros") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "rb-sys") (req "^0.9.81") (features (quote ("bindgen-rbimpls" "bindgen-deprecated-types" "stable-api"))) (kind 0)) (crate-dep (name "rb-sys") (req "^0.9") (features (quote ("stable-api-compiled-fallback"))) (kind 2)) (crate-dep (name "rb-sys-env") (req "^0.1.2") (default-features #t) (kind 1)) (crate-dep (name "seq-macro") (req "^0.3") (default-features #t) (kind 0)))) (hash "0ya431mavvjyk4d1l75cg291kxqclb4q57i4k3h6phx81bs7wndi") (features (quote (("ruby-static" "rb-sys/ruby-static") ("rb-sys") ("friendly-api") ("embed" "rb-sys/link-ruby") ("deprecated-send-sync-value") ("default" "friendly-api")))) (v 2) (features2 (quote (("bytes" "dep:bytes"))))))

(define-public crate-magnus-macros-0.1 (crate (name "magnus-macros") (vers "0.1.0") (deps (list (crate-dep (name "darling") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0j4vilxz7xsw4p037h92c9prk0i2i1i1mmrihddy7xrfpv58z5i7")))

(define-public crate-magnus-macros-0.2 (crate (name "magnus-macros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "01cfy7wwiiz2r7h3c6ahs5nnl3hz3b48zd2wwyj6f3yb11lvmj5c")))

(define-public crate-magnus-macros-0.3 (crate (name "magnus-macros") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1bkq4d6rjkwwrbihxwyj1rxv2kiawnizcbjjjw61h1gazqxv4v10")))

(define-public crate-magnus-macros-0.4 (crate (name "magnus-macros") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1snzzfrk4vncf8yzs4aqsaglay753q58dqap27zk49qvi74p3al5")))

(define-public crate-magnus-macros-0.4 (crate (name "magnus-macros") (vers "0.4.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ywvfhni3rrncz9blswaxadady6gqrz757apm88w0hjlskqpmhbc")))

(define-public crate-magnus-macros-0.6 (crate (name "magnus-macros") (vers "0.6.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1qvigw6ndq73fn6xiffaa52qfvidlhl5k7w18zv6a1cnw8hchs2r")))

