(define-module (crates-io ma sk) #:use-module (crates-io))

(define-public crate-mask-0.1 (crate (name "mask") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.5") (kind 0)))) (hash "0yfy75w4fkfxh5m3dx6knsdjkyhsb3b6jdcdjhgxrxf9v0i01kiq")))

(define-public crate-mask-0.2 (crate (name "mask") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.5") (kind 0)))) (hash "1w4qb03z3nysklljdmy4ywvs4hw6bydh13pvlh5h4m0x5nmciv89")))

(define-public crate-mask-0.2 (crate (name "mask") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.5") (kind 0)))) (hash "17cirqvqpvl5w5nhrwsnysd59wmhia758a7n01hngibr9zffgjxn")))

(define-public crate-mask-0.3 (crate (name "mask") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.5") (kind 0)))) (hash "16kp476jjkk70gv73c47w5fcdfw5y7i85mwq67agpihllxq03max")))

(define-public crate-mask-0.3 (crate (name "mask") (vers "0.3.1") (deps (list (crate-dep (name "assert_cmd") (req "^0.11.1") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^0.11.3") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.5") (kind 0)))) (hash "1r6788yh6b1dk33rxhg3xpyiw60wfdhsmn6i38cj16a1c9a00d35")))

(define-public crate-mask-0.4 (crate (name "mask") (vers "0.4.0") (deps (list (crate-dep (name "assert_cmd") (req "^0.11.1") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^0.11.3") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1.0.1") (default-features #t) (kind 2)) (crate-dep (name "pulldown-cmark") (req "^0.5") (kind 0)))) (hash "1r99nml3km4gjd8rsd43d4i8ffw0pvn7mv7rnsdri55kzs6kh09l")))

(define-public crate-mask-0.5 (crate (name "mask") (vers "0.5.0") (deps (list (crate-dep (name "assert_cmd") (req "^0.11.1") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^0.11.3") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1.0.1") (default-features #t) (kind 2)) (crate-dep (name "pulldown-cmark") (req "^0.5") (kind 0)))) (hash "17ha3mmf8mk7sqyc3s0yv563pnhv8kz2gwfi3bchfkmb8x6kgqry")))

(define-public crate-mask-0.5 (crate (name "mask") (vers "0.5.1") (deps (list (crate-dep (name "assert_cmd") (req "^0.11.1") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^0.11.3") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1.0.1") (default-features #t) (kind 2)) (crate-dep (name "pulldown-cmark") (req "^0.5") (kind 0)))) (hash "1bzgk207r83fs8k2583b5k8c4mcnrzhs7yy4k16w36107hpyzvw8")))

(define-public crate-mask-0.5 (crate (name "mask") (vers "0.5.2") (deps (list (crate-dep (name "assert_cmd") (req "^0.11.1") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^0.11.3") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1.0.1") (default-features #t) (kind 2)) (crate-dep (name "pulldown-cmark") (req "^0.5") (kind 0)))) (hash "15k11p984s21y013in8mhzandj9rqfp23618db5r5dy0basxxf8d")))

(define-public crate-mask-0.6 (crate (name "mask") (vers "0.6.0") (deps (list (crate-dep (name "assert_cmd") (req "^0.11.1") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^0.11.3") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1.0.1") (default-features #t) (kind 2)) (crate-dep (name "pulldown-cmark") (req "^0.5") (kind 0)))) (hash "0lvpg0waqayx3c6x83l9184xfb3nj3ricgqz9997yq0460a29b8k")))

(define-public crate-mask-0.7 (crate (name "mask") (vers "0.7.0") (deps (list (crate-dep (name "assert_cmd") (req "^0.11.1") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^0.11.3") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1.0.1") (default-features #t) (kind 2)) (crate-dep (name "pulldown-cmark") (req "^0.5") (kind 0)))) (hash "1mwcn90sfpb0xn9h4qfx3f9jm59nhy6jd28xbjfq2i3r959vsjvp")))

(define-public crate-mask-0.7 (crate (name "mask") (vers "0.7.1") (deps (list (crate-dep (name "assert_cmd") (req "^0.11.1") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^0.11.3") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1.0.1") (default-features #t) (kind 2)) (crate-dep (name "pulldown-cmark") (req "^0.5") (kind 0)))) (hash "0swzk506nxkfja0g85w1z3rnf8cpfigamk1rx9w12rzzs66xiidx")))

(define-public crate-mask-0.8 (crate (name "mask") (vers "0.8.0") (deps (list (crate-dep (name "assert_cmd") (req "^0.11.1") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^0.11.3") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1.0.1") (default-features #t) (kind 2)) (crate-dep (name "pulldown-cmark") (req "^0.5") (kind 0)))) (hash "1wrv649ibnpxzf6iw58304pfn4zbcgw0f3z5d1x23v5g29dknrnd")))

(define-public crate-mask-0.9 (crate (name "mask") (vers "0.9.0") (deps (list (crate-dep (name "assert_cmd") (req "^0.11.1") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^0.11.3") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1.0.1") (default-features #t) (kind 2)) (crate-dep (name "pulldown-cmark") (req "^0.5") (kind 0)))) (hash "1rbzsqai6b23vplvfsxz4p12v35lvsrkmrz0qawdf38878chivw9")))

(define-public crate-mask-0.10 (crate (name "mask") (vers "0.10.0") (deps (list (crate-dep (name "assert_cmd") (req "^0.11.1") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^0.11.3") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1.0.1") (default-features #t) (kind 2)) (crate-dep (name "pulldown-cmark") (req "^0.5") (kind 0)))) (hash "10vc7ill5z9asdxlwvfvvm556639vm1l651j92qmpa3a4z3rdyj9")))

(define-public crate-mask-0.11 (crate (name "mask") (vers "0.11.0") (deps (list (crate-dep (name "assert_cmd") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2.33") (features (quote ("wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "mask-parser") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0fp3yhrss7amy45nqih8flfwmkys2mfvv8cv6wz8wdc0j23c98g9")))

(define-public crate-mask-0.11 (crate (name "mask") (vers "0.11.1") (deps (list (crate-dep (name "assert_cmd") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2.33") (features (quote ("wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "mask-parser") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0b787f2skqg2ndn4f0mmiaiqzh8l4816mpgli47wvv8d87qi5p1n")))

(define-public crate-mask-0.11 (crate (name "mask") (vers "0.11.2") (deps (list (crate-dep (name "assert_cmd") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2.33") (features (quote ("wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "mask-parser") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "14jwbpzxprjwv44r3cbaxvf2vi79aar5bg9rsdnqpqckmy8wpxln")))

(define-public crate-mask-0.11 (crate (name "mask") (vers "0.11.3") (deps (list (crate-dep (name "assert_cmd") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2.33") (features (quote ("wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "mask-parser") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1vykrajvpk2bfrlvkkhvz21rspjin4jv96857fqxjn13klm0cxmm")))

(define-public crate-mask-0.11 (crate (name "mask") (vers "0.11.4") (deps (list (crate-dep (name "assert_cmd") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2.33") (features (quote ("wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "mask-parser") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "11j0w1p4wgclqsn5xlmg5zfksq7rav8vjnix72zmdigdgy16b19a")))

(define-public crate-mask-parser-0.1 (crate (name "mask-parser") (vers "0.1.0") (deps (list (crate-dep (name "pulldown-cmark") (req "^0.5") (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1d8fha4y3yv98f24hxf474pk5q3rwazirqz6fmcnkqd8pkcwgjdn")))

(define-public crate-mask-parser-0.2 (crate (name "mask-parser") (vers "0.2.0") (deps (list (crate-dep (name "pulldown-cmark") (req "^0.5") (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0mlzg07wn0k72nl9m51gra7n0rlg6f2pp9ll2bd3f7wi0sjiyqap")))

(define-public crate-mask-parser-0.2 (crate (name "mask-parser") (vers "0.2.1") (deps (list (crate-dep (name "pulldown-cmark") (req "^0.5") (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1nlryma1yl8hvb851a7yifkdy3gl2g9ar59nnzzxfkqniwq6bd62")))

(define-public crate-mask-text-0.1 (crate (name "mask-text") (vers "0.1.0") (deps (list (crate-dep (name "insta") (req "^1.17.2") (features (quote ("filters"))) (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.15.0") (default-features #t) (kind 2)))) (hash "13w5r4c79bkmyqzhfgvk241wpr34lfki99s8q1rvxyar39njplm2")))

(define-public crate-mask-text-0.1 (crate (name "mask-text") (vers "0.1.1") (deps (list (crate-dep (name "insta") (req "^1.17.2") (features (quote ("filters"))) (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.15.0") (default-features #t) (kind 2)))) (hash "07zm08082hgy3h0fggqg2422ymglx3d9pdbqm27allc1hbdfk7v2")))

(define-public crate-mask-text-0.1 (crate (name "mask-text") (vers "0.1.2") (deps (list (crate-dep (name "insta") (req "^1.17.2") (features (quote ("filters"))) (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.15.0") (default-features #t) (kind 2)))) (hash "1ghpmlcri2bfch3k6v4wql5bhvg9h0jrx1hmszskk1k105jrgw7x")))

(define-public crate-masked-0.1 (crate (name "masked") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "1xvjaz5l0i4fsnrx2537n7qsgpg105rw5dyhkwm4xa24hgr9z8fk")))

(define-public crate-masked-0.1 (crate (name "masked") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0fd0yllxdd385rvqzi1rrkcjmzv16hb2vd0pnpwmidrgvmbp57ry")))

(define-public crate-masked-0.1 (crate (name "masked") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "1iwhl1ai6d8qpw84lwbdjj2nwryv8awdp75wnafmf6bvvw42f5rr")))

(define-public crate-masked-0.1 (crate (name "masked") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "1d55zmxni6anrscmly0hakp19mzav59d14024xfpl1vy565ndp42")))

(define-public crate-masked-0.1 (crate (name "masked") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0xy4kh81azm3lqpiv4s4alfznpnlmgj6w9ar1xkna388zpk498an")))

(define-public crate-maskedvbyte-rs-0.1 (crate (name "maskedvbyte-rs") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.4") (default-features #t) (kind 1)))) (hash "0zjf1g0vn1rvp4d7q2154cwcbk4cbra1dcvkzkbir2bzmllqym15")))

(define-public crate-maskedvbyte-rs-0.1 (crate (name "maskedvbyte-rs") (vers "0.1.1") (deps (list (crate-dep (name "maskedvbyte-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "12si6fd3jgb9ga195ncrin2h5cmafjs7dzsy1j6liva2s20f3x2r")))

(define-public crate-maskedvbyte-sys-0.1 (crate (name "maskedvbyte-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.39.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 1)))) (hash "1lxv2q50sfwax9wxzrslfdaji32h6mszq755w9g3an4mhsmwr5g2")))

(define-public crate-masker-0.0.1 (crate (name "masker") (vers "0.0.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0xjvm0cs4kgngjh2dccmj3fc7dfffhm2gwpslqhnxyasnr3p43q4")))

(define-public crate-masker-0.0.2 (crate (name "masker") (vers "0.0.2") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1hihq0wvjdhg564i99b7vyhgpp1laz3n9fdy56s4i74cxzpkr6my")))

(define-public crate-masker-0.0.3 (crate (name "masker") (vers "0.0.3") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0b3cg9ndl5z0h17i2a4yaaqzbymb1n75fy9mcancisc1fk8rli2c")))

(define-public crate-masker-0.0.4 (crate (name "masker") (vers "0.0.4") (deps (list (crate-dep (name "bytes") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)))) (hash "07w8h7kym616ip6h4sawcgq53dqgabi78acjj8gjyla47k5k1pjf") (features (quote (("streams" "bytes" "futures"))))))

(define-public crate-maskerad_memory_allocators-2 (crate (name "maskerad_memory_allocators") (vers "2.0.0") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)))) (hash "0yf7ysxg4c1ci6f5z09zk5j6km9i7vmlwhncg7rf9l27d305f4ab")))

(define-public crate-maskerad_memory_allocators-2 (crate (name "maskerad_memory_allocators") (vers "2.0.1") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)))) (hash "06iqy91lka3q6c3rczb051fq81b4lrw1cx01yqbj5lym9rhxcd1h")))

(define-public crate-maskerad_memory_allocators-3 (crate (name "maskerad_memory_allocators") (vers "3.0.0") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)))) (hash "139h5jrmmddkjq01fjmmdh5p3d73rddmh9pdb76zp4qwrz4bgg68")))

(define-public crate-maskerad_memory_allocators-3 (crate (name "maskerad_memory_allocators") (vers "3.0.1") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)))) (hash "0sydkvjphxx224kahf1xgvmb9jri5m6r57wrlk4aa5yi56qw83vg")))

(define-public crate-maskerad_memory_allocators-3 (crate (name "maskerad_memory_allocators") (vers "3.1.0") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)))) (hash "1swrl3lghxdzk8sd46myjkqd55k3zmrqzbvhqmawa2y00i3s6sv8")))

(define-public crate-maskerad_memory_allocators-3 (crate (name "maskerad_memory_allocators") (vers "3.1.1") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)))) (hash "0x7n0nvsnyrk3zp008r6gf1is32jm5dbndkw3svqm2vvd4k9f7rv")))

(define-public crate-maskerad_memory_allocators-3 (crate (name "maskerad_memory_allocators") (vers "3.1.2") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)))) (hash "1sgazbp92bawiqxq6392dkm6m30mqc4bxw28rgf6cfqapxs8kbxr")))

(define-public crate-maskerad_memory_allocators-4 (crate (name "maskerad_memory_allocators") (vers "4.0.0") (hash "06pjx227kmx6hxsjdlfgma6n4n4kmwgyvhdl74j8hrp8z155zsrj")))

(define-public crate-maskerad_memory_allocators-4 (crate (name "maskerad_memory_allocators") (vers "4.0.1") (hash "125scvqmz7gffns7vp7nc9q8y72rcmcqb63n9wdd44i5dk2f551r")))

(define-public crate-maskerad_memory_allocators-4 (crate (name "maskerad_memory_allocators") (vers "4.0.2") (hash "1r77rb38lwcqxksv5ksv27528awjf0vfw118cqib7z6ma0xkyy1p")))

(define-public crate-maskerad_memory_allocators-5 (crate (name "maskerad_memory_allocators") (vers "5.0.0") (deps (list (crate-dep (name "serde") (req "~1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0cw6i98jkjh2qn9idfpsyk68gh8hd1qqjm3d6wni5119z60kb7mz")))

(define-public crate-maskerad_memory_allocators-5 (crate (name "maskerad_memory_allocators") (vers "5.0.1") (deps (list (crate-dep (name "serde") (req "~1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1vl8fffdva5kxiv247zp47x6z4ksjdcwfpln6mivyjl94f2r48m7")))

(define-public crate-maskerad_memory_allocators-5 (crate (name "maskerad_memory_allocators") (vers "5.1.0") (deps (list (crate-dep (name "log") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1409y7a0as7bj2cjqa34ql5lby1irra9j4g44qy9gjqbx221362n")))

(define-public crate-maskerad_memory_allocators-5 (crate (name "maskerad_memory_allocators") (vers "5.2.0") (deps (list (crate-dep (name "log") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0y9hc8sjza75zghlzzkh28ai56iq7avxbdph7wawc7xhv6rbycj0")))

(define-public crate-maskerad_object_pool-0.1 (crate (name "maskerad_object_pool") (vers "0.1.0") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.38") (default-features #t) (kind 2)))) (hash "1a0il5m7jg87k36fklffy42r5612y3mr6jf3vlvjg05d9fpnls8c")))

(define-public crate-maskerad_object_pool-0.1 (crate (name "maskerad_object_pool") (vers "0.1.1") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.38") (default-features #t) (kind 2)))) (hash "1chzwx4rvg46wx8mfnc6hpy1svvfb0hicj7nzczkca2mys6q2ak6")))

(define-public crate-maskerad_object_pool-0.1 (crate (name "maskerad_object_pool") (vers "0.1.2") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.38") (default-features #t) (kind 2)))) (hash "0rws9wbh9dj9xv211bryxi9d8dv2c45yj9rm55smfczlwkg3clq0")))

(define-public crate-maskerad_object_pool-0.2 (crate (name "maskerad_object_pool") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "~1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1rdiigbmb7xbid33f7m4vd2fvd6dfcslhk9y6q56lkpcy3kwp17m")))

(define-public crate-maskerad_object_pool-0.2 (crate (name "maskerad_object_pool") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "~1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0gn97i99crg3vavdg3k5rxjnr9qc6plg1iy4kxmjv838x9b3hrfz")))

(define-public crate-maskerad_object_pool-0.2 (crate (name "maskerad_object_pool") (vers "0.2.2") (deps (list (crate-dep (name "serde") (req "~1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1m2wrjm140ghc1dlq3shzlgxylmplvwyjxqfcvmgbxzq8hx0r7yw")))

(define-public crate-maskerad_object_pool-0.3 (crate (name "maskerad_object_pool") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0v5i8fvkwwfkf190xiva2jjb92q134caq0cb4inc12lb4zr9lyxy")))

(define-public crate-maskerad_stack_allocator-0.1 (crate (name "maskerad_stack_allocator") (vers "0.1.0") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.38") (default-features #t) (kind 2)))) (hash "03icyiw2sf1a74jhylyi071wgbmmnc8kkf9p466phvvfsiqs0sh7")))

(define-public crate-maskerad_stack_allocator-0.1 (crate (name "maskerad_stack_allocator") (vers "0.1.1") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.38") (default-features #t) (kind 2)))) (hash "0j09qs202rh46hwnxq1vwwi4mrqj84aikd1fb8j7h0059bvbzk9d")))

(define-public crate-maskerad_stack_allocator-0.1 (crate (name "maskerad_stack_allocator") (vers "0.1.2") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.38") (default-features #t) (kind 2)))) (hash "0z60mj0sxm31lsab3g3wy56hy2nwq8j3cz34zhi4bq9q8d1cd07v")))

(define-public crate-maskerad_stack_allocator-1 (crate (name "maskerad_stack_allocator") (vers "1.0.0") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.38") (default-features #t) (kind 2)))) (hash "0lh1c3dsgah8j7c4njzljln5j3h59d0i9jciirh4r2fkdjk2dvqs")))

(define-public crate-maskerad_stack_allocator-1 (crate (name "maskerad_stack_allocator") (vers "1.0.1") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.38") (default-features #t) (kind 2)))) (hash "1q2iqz4pjrdm5k85swr009d2wm5ly21146pia15aijzjicyj42fr")))

(define-public crate-maskerad_stack_allocator-1 (crate (name "maskerad_stack_allocator") (vers "1.1.0") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.38") (default-features #t) (kind 2)))) (hash "0rp6xh3j2s7k6dc52sb5rxl8ymc9zx0wrnva3h9vqk542a7jg8j9")))

(define-public crate-maskerad_stack_allocator-1 (crate (name "maskerad_stack_allocator") (vers "1.1.1") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.38") (default-features #t) (kind 2)))) (hash "19dg67rbxah7q10b05qa68d7mdndrmpv1lfmp4chkf8vcafq9dpz")))

(define-public crate-maskerad_stack_allocator-1 (crate (name "maskerad_stack_allocator") (vers "1.1.2") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.38") (default-features #t) (kind 2)))) (hash "114na0xfkhl4al66i38g219ihg55bgysdk1lhmc4bdgpj2f0hbaf")))

