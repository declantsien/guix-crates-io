(define-module (crates-io ma lo) #:use-module (crates-io))

(define-public crate-malogany-0.1 (crate (name "malogany") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1.2") (default-features #t) (kind 0)))) (hash "1z03smikmplw36k5lvyk88wflaaa6fi32wirdl7ykydhll17pvd3")))

(define-public crate-malogany-0.2 (crate (name "malogany") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "scopeguard") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1.2") (default-features #t) (kind 0)))) (hash "0prnr9np6dy056jl6xg7klj16ills0ivahnw204iavm5i1yfq1z5")))

(define-public crate-malory-0.1 (crate (name "malory") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("sync" "signal" "rt-core" "rt-threaded" "net" "stream" "time" "macros"))) (default-features #t) (kind 0)))) (hash "0qhvg3w2hlvz49104mlk40lh3xih81p61h63r1jw7jdwfx1c0gsz")))

(define-public crate-malory-0.2 (crate (name "malory") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("signal" "macros" "sync" "time" "rt-multi-thread"))) (default-features #t) (kind 0)))) (hash "1gd0accwm02hk0s51ik5s9qjaalmvdhy6yqrx3hgb72zlrjyfn21")))

(define-public crate-malory-0.2 (crate (name "malory") (vers "0.2.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("signal" "macros" "sync" "time" "rt-multi-thread"))) (default-features #t) (kind 0)))) (hash "0c35622mvzpdp9890941qwbgarc7fpb1bmhbsgj41xynajx7n10g")))

