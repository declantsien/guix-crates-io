(define-module (crates-io ma cd) #:use-module (crates-io))

(define-public crate-macd-rs-0.1 (crate (name "macd-rs") (vers "0.1.0") (deps (list (crate-dep (name "ema-rs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ta-common") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1fz6rpnv94862ysjbb040k7q42x514l1l94a5bbr1kvh3c0qg6a1")))

