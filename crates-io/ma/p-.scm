(define-module (crates-io ma p-) #:use-module (crates-io))

(define-public crate-map-engine-0.1 (crate (name "map-engine") (vers "0.1.0") (deps (list (crate-dep (name "gdal") (req "^0.11") (features (quote ("ndarray"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ziqc7xr8zk25byyfm6j9mbv9i44mms9jc5ijh1hgp43ghx5dwd8")))

(define-public crate-map-engine-server-0.1 (crate (name "map-engine-server") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1.6.5") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "map-engine") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)) (crate-dep (name "tide") (req "^0.16") (features (quote ("h1-server" "logger"))) (kind 0)) (crate-dep (name "tide-testing") (req "^0.1") (default-features #t) (kind 2)))) (hash "1rf53bzaj03r3mz9gw1f7yivi3s0n6d1gk9bjxdjsslvj2b2chwb")))

(define-public crate-map-gen-2d-0.1 (crate (name "map-gen-2d") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0raxf0drlz29g1pzwxhvxd5s3fd01hbw98810fbxk5syz176ayyb")))

(define-public crate-map-gen-2d-0.1 (crate (name "map-gen-2d") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0snfd080w18wn1wpam7f1ljvyfgzb37x9kryrxicb7mpk2jfpdl1")))

(define-public crate-map-gen-2d-0.1 (crate (name "map-gen-2d") (vers "0.1.15") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0159l2n0plwakj3ka6m1z3x3xmx63a2ha1x45s7l9axbzbczwi75")))

(define-public crate-map-macro-0.1 (crate (name "map-macro") (vers "0.1.0") (hash "0gh52pvxyfyinnkmiiw1ilw19x6anf2k5f9bkimqk4y8kbih554m")))

(define-public crate-map-macro-0.2 (crate (name "map-macro") (vers "0.2.0") (hash "0c01k9hhxj76q1nmfkk261l3s9l6h0835p4npma1m6s456rc7pmk")))

(define-public crate-map-macro-0.2 (crate (name "map-macro") (vers "0.2.1") (hash "0b22d50251lg1bcgnmypmi84a64v3l163mi2c8nns8bfzic0hnwx")))

(define-public crate-map-macro-0.2 (crate (name "map-macro") (vers "0.2.2") (hash "1i72mdgc3a6bggmrpglx94rja2azzl58i79lv9qjfcbnqcvfr3ih")))

(define-public crate-map-macro-0.2 (crate (name "map-macro") (vers "0.2.3") (deps (list (crate-dep (name "criterion") (req "^0.3.6") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1ds26mi9qca8js3lslvjg4nq777mkwcwksccgv3a1sjzd9nq34pz")))

(define-public crate-map-macro-0.2 (crate (name "map-macro") (vers "0.2.4") (deps (list (crate-dep (name "criterion") (req "^0.3.6") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "09cxn8lkvziqc0i2ifmwamns7vljn00gh9gvpv0g5hz6p92bzzqd")))

(define-public crate-map-macro-0.2 (crate (name "map-macro") (vers "0.2.5") (deps (list (crate-dep (name "criterion") (req "^0.3.6") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0pzkzawnvn0k0zl0b4da8drjbdbhgid2j863brii3712mjxbxja8")))

(define-public crate-map-macro-0.2 (crate (name "map-macro") (vers "0.2.6") (deps (list (crate-dep (name "criterion") (req "^0.3.6") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1rp3vdqjcizkvpr11q8zfivxfjfrb275amifianqvk2s738znbkw")))

(define-public crate-map-macro-0.3 (crate (name "map-macro") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3.6") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.14") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14") (default-features #t) (kind 2)))) (hash "0fldbch9qwy9z4x4d4v9i1j2mvgbgslaq59i92iyahln4m10m5gv") (features (quote (("std") ("hashbrown") ("default" "std")))) (v 2) (features2 (quote (("__docs" "dep:hashbrown"))))))

(define-public crate-map-of-indexes-0.1 (crate (name "map-of-indexes") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "05ans6fv52xzb2f5k0b27dk7w32zf5ij47pghfx3m76swx1hj4m4")))

(define-public crate-map-of-indexes-0.1 (crate (name "map-of-indexes") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0yfhx6wb0cyx3grskhn3k4kw8mvbcll9mzwgxdvl8sm8lxxcb39h")))

(define-public crate-map-of-indexes-0.1 (crate (name "map-of-indexes") (vers "0.1.2") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "07f056jgl41krq9h37g0zjzxkwxnbf7c7yfq4y8anmshaa2j8ykk")))

(define-public crate-map-of-indexes-0.1 (crate (name "map-of-indexes") (vers "0.1.3") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "052w0i5v03wyjh16v224hmzsc336635dwr308zzkbrk3343cslac")))

(define-public crate-map-of-indexes-0.1 (crate (name "map-of-indexes") (vers "0.1.4") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1q9sc9wnmwwy83581drdxpmc9h4ky1vs05k5zrx8mi1lika258qq")))

(define-public crate-map-ok-0.1 (crate (name "map-ok") (vers "0.1.0") (hash "0bp1q11zkwf4hm2m4xha3dy86cck61iqbdf63k2b6zchfvph1rq4")))

(define-public crate-map-ok-1 (crate (name "map-ok") (vers "1.0.0") (hash "1rwyhhq40w4bwgxmhmb35kgfvby96d8hvwfrydj8brgpbs6ljpc6")))

(define-public crate-map-parser-0.1 (crate (name "map-parser") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.13") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24.3") (default-features #t) (kind 0)))) (hash "027vnjjz7cdsgwi901wr0qhshmrg36vlda2xjikjj1p0274bg4wb") (yanked #t)))

(define-public crate-map-parser-0.1 (crate (name "map-parser") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.13") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24.3") (default-features #t) (kind 0)))) (hash "11fas6dm4dwzlarnn0imasvcapppy5vgz82sgjgwirlckbdw78b2") (yanked #t)))

(define-public crate-map-parser-0.1 (crate (name "map-parser") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.13") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24.3") (default-features #t) (kind 0)))) (hash "157sl832d5m9l41cn93lwpywvl8qgnh4qhzalxggq3rjs7n2p442") (yanked #t)))

(define-public crate-map-parser-0.1 (crate (name "map-parser") (vers "0.1.3") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.13") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24.3") (default-features #t) (kind 0)))) (hash "0hpycf94szf7c6vhah9zssnnnzyjkbivxlfggns0cbiy6rs0gbi9") (yanked #t)))

(define-public crate-map-parser-0.1 (crate (name "map-parser") (vers "0.1.4") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.13") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24.3") (default-features #t) (kind 0)))) (hash "155p16q83ysjc0f7yysn1bdqp82w41vigdx8ih36jwiky5757q9d") (yanked #t)))

(define-public crate-map-parser-0.1 (crate (name "map-parser") (vers "0.1.5") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.13") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24.3") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "13zfyyyczlxyjz2yb117qk5vrg6jsdmgl08f6g5388zkm6cxy89c")))

(define-public crate-map-range-0.1 (crate (name "map-range") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1y9baxnd3kihrjjfsqf4w5synjv11r8adzmnw11ajvvybl3fyh99") (yanked #t)))

(define-public crate-map-range-0.1 (crate (name "map-range") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1y4hxykavjy3bzrfz18z30zd2hfn9nhi3ffbxl1s8kpj15n07r6y")))

(define-public crate-map-range-0.1 (crate (name "map-range") (vers "0.1.2") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "19a5d2k3vnw2yziz9jrmq0lf96ydcjqnvzz51dklkdaigy6mbymz") (features (quote (("std" "num-traits/std") ("default" "std"))))))

(define-public crate-map-self-0.1 (crate (name "map-self") (vers "0.1.0") (hash "0xnrgv0zddzd2w5707ykhrdlj4sjmk7ba8vr8m6flcz2zk5s2r0f")))

(define-public crate-map-to-const-0.1 (crate (name "map-to-const") (vers "0.1.0") (deps (list (crate-dep (name "indoc") (req "^1.0.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "sensible-env-logger") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.9.4") (default-features #t) (kind 2)))) (hash "06j7a014a0hawfn72npiy4zwkr2jpdzwq5akhplajarqnlalxxb5")))

(define-public crate-map-to-const-0.2 (crate (name "map-to-const") (vers "0.2.0") (deps (list (crate-dep (name "indoc") (req "^1.0.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "sensible-env-logger") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.9.4") (default-features #t) (kind 2)))) (hash "15iirgi778yvy322ah9rffax1y1nb1bd925zjx0mlrd78qizqcjv")))

(define-public crate-map-to-javascript-html-0.1 (crate (name "map-to-javascript-html") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1k02fsp01m8cxw14b9hbi5c3mrqgmnadhbq6pxbd2gahscyf9wvh")))

(define-public crate-map-to-javascript-html-0.1 (crate (name "map-to-javascript-html") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "08aj1dnc3b50hy5a4x1p6v30r874298g9qf28ynls1gspr4ggn2x")))

(define-public crate-map-to-javascript-html-1 (crate (name "map-to-javascript-html") (vers "1.0.0") (deps (list (crate-dep (name "regex") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1s2jg7ynzvkjqplxc7rjjaa14pn0z89fgrs8g0cjk2zq35ijjh62")))

(define-public crate-map-to-javascript-html-1 (crate (name "map-to-javascript-html") (vers "1.0.1") (deps (list (crate-dep (name "regex") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1pjkkcpnanjwwas1dbzj31pxc41yxqc0cwgc1nzi2l8ciiiynylr")))

(define-public crate-map-to-javascript-html-1 (crate (name "map-to-javascript-html") (vers "1.1.0") (deps (list (crate-dep (name "regex") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "0k1s7h7kfpg492m4za2yzqdcdcla7m5k7h9hrh5czy6lv8i08lh0")))

(define-public crate-map-to-javascript-html-1 (crate (name "map-to-javascript-html") (vers "1.1.1") (deps (list (crate-dep (name "regex") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1c3y6saz3l9zinaj46mxrh6csc6dj6pbyipcaliakrcnslg14zzr")))

(define-public crate-map-to-javascript-html-1 (crate (name "map-to-javascript-html") (vers "1.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "0yqxcymq97ncwaqw2zbjk7i61ikr8xb6674vbb99xs9zg9ff09gc")))

(define-public crate-map-to-javascript-html-1 (crate (name "map-to-javascript-html") (vers "1.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "04y4cjprp5bs4670cxkris1x1nl5dy9h5xpily43l3vqzxin1zw1")))

(define-public crate-map-to-javascript-html-1 (crate (name "map-to-javascript-html") (vers "1.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1308swhrdaksj9nf3vfisjc9j56i6mbbrk7xxxh41i6bjwm4hl37")))

(define-public crate-map-to-javascript-html-1 (crate (name "map-to-javascript-html") (vers "1.2.2") (deps (list (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "14g3g4lgy9g13sgf9n3hndc37l64n4n36kl0d7cpr2q31w1xyv9n")))

(define-public crate-map-to-javascript-html-1 (crate (name "map-to-javascript-html") (vers "1.2.3") (deps (list (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1xmxzvp0wmwxrwb6vnhziclw4s299skpqm93vsjwxgx4cjd5f3d8")))

(define-public crate-map-to-javascript-html-2 (crate (name "map-to-javascript-html") (vers "2.0.0") (deps (list (crate-dep (name "html-escape") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1snc4z1pawavcg71swxf708pj65qgm5jl07pbs8x4913cyxawfkw") (features (quote (("serde" "serde_json")))) (yanked #t)))

(define-public crate-map-to-javascript-html-2 (crate (name "map-to-javascript-html") (vers "2.0.1") (deps (list (crate-dep (name "html-escape") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1f0hk21v1qcwc8zz1wadjyqq12gbah4xs51wvwrsmnyv1lnbng9d") (features (quote (("serde" "serde_json")))) (yanked #t)))

(define-public crate-map-to-javascript-html-2 (crate (name "map-to-javascript-html") (vers "2.0.2") (deps (list (crate-dep (name "html-escape") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0g9p8scz72hm14xw8bn51i0g5jn5923wax12drmv9d2k08908x4h") (features (quote (("std") ("serde" "serde_json") ("default" "std"))))))

(define-public crate-map-to-javascript-html-2 (crate (name "map-to-javascript-html") (vers "2.0.3") (deps (list (crate-dep (name "html-escape") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0371mmdps02y2nqby49phng5cixgh73bfw1swgkhfjv3xchvmbgs") (features (quote (("std") ("serde" "serde_json") ("default" "std"))))))

(define-public crate-map-to-javascript-html-2 (crate (name "map-to-javascript-html") (vers "2.0.4") (deps (list (crate-dep (name "html-escape") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1rxjbcy5bxakvv9b14h9daslsvnbg4w6v483g89m774hx0nlhlc2") (features (quote (("std") ("serde" "serde_json") ("default" "std"))))))

(define-public crate-map-to-javascript-html-2 (crate (name "map-to-javascript-html") (vers "2.0.5") (deps (list (crate-dep (name "html-escape") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0ah1hbgv21ixhg0rlgd4kymayk3zyr490s49483v9qyfjs566qjl") (features (quote (("std") ("serde" "serde_json") ("default" "std"))))))

(define-public crate-map-trait-0.1 (crate (name "map-trait") (vers "0.1.0") (hash "1xj1mx8g0fxy931j88lrj5sqkms68maj5h7j8rf466g3r8zklmbc")))

(define-public crate-map-trait-0.2 (crate (name "map-trait") (vers "0.2.0") (hash "0z0f0691xgrsj28pxylnlyadffgf5f58fyz7v4hmiq7hakgg420i")))

(define-public crate-map-trait-0.3 (crate (name "map-trait") (vers "0.3.0") (hash "0zvbh2hzskg10gdbsk8h7kli56ph738mxcpz4c83jmd7d97699j2")))

(define-public crate-map-trait-0.3 (crate (name "map-trait") (vers "0.3.1") (hash "0nm0f9wjqii8ni21kw7salhqvq9rqia0lqzys99wc5kvd0nrqxfc")))

(define-public crate-map-trait-0.3 (crate (name "map-trait") (vers "0.3.2") (hash "0jwddqhh9hy86kzfdmx4hrh0vqb4nkdvfm4bf984xh9mrs3hip4s")))

