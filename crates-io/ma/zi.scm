(define-module (crates-io ma zi) #:use-module (crates-io))

(define-public crate-mazinator-0.1 (crate (name "mazinator") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "1fs005yb5yhb7zdxr57ivc5d2b7bdnrjm8knbxgrxiy88dyyjal4")))

(define-public crate-mazinator-0.1 (crate (name "mazinator") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "1h08c5zcxi30s33jdbkzwyazhg547z1bbzjn0wc99z9gz3lnn109") (yanked #t)))

(define-public crate-mazinator-0.1 (crate (name "mazinator") (vers "0.1.2") (deps (list (crate-dep (name "colored") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "1yrni2y6bzs2myp343sn2g9mmvgmg8028q9w5y2wa47637r4mjzv")))

