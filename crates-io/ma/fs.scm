(define-module (crates-io ma fs) #:use-module (crates-io))

(define-public crate-mafs-0.1 (crate (name "mafs") (vers "0.1.0") (deps (list (crate-dep (name "bytemuck") (req "^1.11") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1cxgva0qa4ynbg14ag57rd4zx3qwv5irzk2f33vfzzimwgghbj16") (v 2) (features2 (quote (("bytemuck" "dep:bytemuck"))))))

