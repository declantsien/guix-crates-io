(define-module (crates-io ma nn) #:use-module (crates-io))

(define-public crate-mann_kendall-0.1 (crate (name "mann_kendall") (vers "0.1.0") (deps (list (crate-dep (name "cbindgen") (req "~0.23") (default-features #t) (kind 1)) (crate-dep (name "distrs") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "~0.2") (default-features #t) (kind 0)))) (hash "01abq62w6p54qvc8q6x6rpz01j7ss50c4arvaqwv7vh3gmc6sifd")))

