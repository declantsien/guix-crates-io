(define-module (crates-io ma x-) #:use-module (crates-io))

(define-public crate-max-7219-led-matrix-util-0.1 (crate (name "max-7219-led-matrix-util") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "gpio-cdev") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "max7219") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0gahk6bxppr17hyw5k59pllhllvksfsjgdk4fy5yirw1nqw61k2d")))

(define-public crate-max-7219-led-matrix-util-0.1 (crate (name "max-7219-led-matrix-util") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "gpio-cdev") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "max7219") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1h4frlvnhmgxf0lx50vq5l8hxxgjjbdisvblafni08xpvf488zcv")))

(define-public crate-max-7219-led-matrix-util-0.1 (crate (name "max-7219-led-matrix-util") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "gpio-cdev") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "max7219") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0v9iy7qlcqz54xf6fcqnjf9x38wwaqr80w6ljfk0c0rsa62a3hfb")))

(define-public crate-max-7219-led-matrix-util-0.1 (crate (name "max-7219-led-matrix-util") (vers "0.1.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "gpio-cdev") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "max7219") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0c4b2p7p4ab8qvik56mkpadq9flcasnzbj29vb9nwk3adnivvpkb")))

(define-public crate-max-7219-led-matrix-util-0.1 (crate (name "max-7219-led-matrix-util") (vers "0.1.4") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "gpio-cdev") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "max7219") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0xx7r4hxyaiw8whybqpa4mvprh4win3f50n6cavgwzgwsfnaf09s")))

(define-public crate-max-7219-led-matrix-util-0.2 (crate (name "max-7219-led-matrix-util") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "gpio-cdev") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "max7219") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1xffwd8wdyxr68j7bsmdrkl11gqfxbasd41hyfj9rxznp9gli36j") (features (quote (("std" "gpio-cdev") ("default" "std"))))))

(define-public crate-max-7219-led-matrix-util-0.2 (crate (name "max-7219-led-matrix-util") (vers "0.2.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "gpio-cdev") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "max7219") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1zv0v3hyrlcfb5s66x0ms73zgyqa5jk91i3v7zplx549p3qp94fk") (features (quote (("std" "gpio-cdev") ("default" "std"))))))

(define-public crate-max-7219-led-matrix-util-0.2 (crate (name "max-7219-led-matrix-util") (vers "0.2.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "gpio-cdev") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "max7219") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0n2ykcj5apvlcl6avav5hz87j0nxwbvm18llmz68xaa4d56pqxkd") (features (quote (("std" "gpio-cdev") ("default" "std"))))))

(define-public crate-max-7219-led-matrix-util-0.2 (crate (name "max-7219-led-matrix-util") (vers "0.2.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "gpio-cdev") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "max7219") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0qj6dg21rvclfvpcyanpd4xv416qrdf46k2zyhv94xb8fjkyq0b2") (features (quote (("std" "gpio-cdev") ("default" "std"))))))

(define-public crate-max-7219-led-matrix-util-0.2 (crate (name "max-7219-led-matrix-util") (vers "0.2.4") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "gpio-cdev") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "max7219") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "185l93v7b901iz13p8z26c4sv5bb0piyr4znyza0jjbds829fgf9") (features (quote (("std" "gpio-cdev") ("default" "std"))))))

(define-public crate-max-grrs-0.1 (crate (name "max-grrs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0.11") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1.0.13") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^3.0.3") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.6.0") (default-features #t) (kind 0)))) (hash "01141hm4y1zrx1nnqqid9cda35ym1pk18pnl11hb8ajk0k6nvm2f")))

(define-public crate-max-subarray-sum-0.1 (crate (name "max-subarray-sum") (vers "0.1.0") (hash "07wg1icncx9hnb092gfji8ybcvsmfvgyaffa2wmk5024x8gg495h") (yanked #t)))

(define-public crate-max-subarray-sum-0.1 (crate (name "max-subarray-sum") (vers "0.1.1") (hash "05b372nm6lr72sz36xdrh68gnjlqpnz9kxmy3wkkgw5bvhjhqyv0") (yanked #t)))

(define-public crate-max-subarray-sum-0.1 (crate (name "max-subarray-sum") (vers "0.1.2") (hash "1msidnq6jps6r5896hjq0rr7lqdi8ag302a61j0ksvrvan08i4a0") (yanked #t)))

(define-public crate-max-subarray-sum-0.1 (crate (name "max-subarray-sum") (vers "0.1.3") (hash "1p3x3wljfrbk02azvajbzwmlari7626af8i77q88dzafvfvjbgrs") (yanked #t)))

(define-public crate-max-subarray-sum-0.1 (crate (name "max-subarray-sum") (vers "0.1.4") (hash "03334gm22pvrwmxa8ds6wg5gjr3f1saw9xrwhdnvqcg5n4dxlcxs") (yanked #t)))

(define-public crate-max-subarray-sum-0.1 (crate (name "max-subarray-sum") (vers "0.1.5") (hash "085qphf3b75zz4r5mhjv1lg452jyw8sbjzyf6wyr9xmmbzma9gb3") (yanked #t)))

(define-public crate-max-subarray-sum-0.1 (crate (name "max-subarray-sum") (vers "0.1.6") (hash "1ndgljnhqw2ad451xyma1xnpxmb2n4v3a8xprz2b2kq8gq6byr74") (yanked #t)))

(define-public crate-max-subarray-sum-0.1 (crate (name "max-subarray-sum") (vers "0.1.7") (hash "0fyxshvnk69qbmc5cq7lzsanjfb3637429k5v5bddnhajpq89ryk")))

