(define-module (crates-io ma gm) #:use-module (crates-io))

(define-public crate-magma-0.0.0 (crate (name "magma") (vers "0.0.0") (hash "1qy2fa6k7jbm121amwvd0pm0jx4g6mpvvrb6jsx4j43i92a7labp")))

(define-public crate-magma-0.1 (crate (name "magma") (vers "0.1.0") (deps (list (crate-dep (name "block-cipher-trait") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "byte-tools") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "crypto-tests") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "generic-array") (req "^0.6") (default-features #t) (kind 0)))) (hash "1i22n1k60x440vyql376c5axsc3qxqvj0hwfa2m9lb99jn8l9mfj")))

(define-public crate-magma-0.1 (crate (name "magma") (vers "0.1.1") (deps (list (crate-dep (name "block-cipher-trait") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "byte-tools") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "crypto-tests") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "generic-array") (req "^0.6") (default-features #t) (kind 0)))) (hash "1qr1p93k1kyvisrf3aj02rzv6v0pvsb3v9wwng1mncfrr8alzpfr")))

(define-public crate-magma-0.2 (crate (name "magma") (vers "0.2.0") (deps (list (crate-dep (name "block-cipher-trait") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "byte-tools") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "opaque-debug") (req "^0.1") (default-features #t) (kind 0)))) (hash "19vv6ymdy1sncqawqwwlwqma8c87d8wcfjzf1z6lf0nfvb2lyz6k")))

(define-public crate-magma-0.3 (crate (name "magma") (vers "0.3.0") (deps (list (crate-dep (name "block-cipher-trait") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "block-cipher-trait") (req "^0.6") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "opaque-debug") (req "^0.2") (default-features #t) (kind 0)))) (hash "0v79wbjhs5qfrsp721qvbmdgrip3qmy4chxhss8ybbcyldzgd5mj")))

(define-public crate-magma-0.4 (crate (name "magma") (vers "0.4.0") (deps (list (crate-dep (name "block-cipher") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "block-cipher") (req "^0.7") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "opaque-debug") (req "^0.2") (default-features #t) (kind 0)))) (hash "0zhabs0p5q8w25d1gi13rim5pv2jhgmfb3nc2sz7zkdwk9b3sc62")))

(define-public crate-magma-0.5 (crate (name "magma") (vers "0.5.0") (deps (list (crate-dep (name "block-cipher") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "block-cipher") (req "^0.8") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "opaque-debug") (req "^0.3") (default-features #t) (kind 0)))) (hash "0n2qf8a5n7b4481l7hj4p9qpsn5cm22j7bzdhhx4kb8nk7bv5zbk")))

(define-public crate-magma-0.6 (crate (name "magma") (vers "0.6.0") (deps (list (crate-dep (name "cipher") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "cipher") (req "^0.2") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "opaque-debug") (req "^0.3") (default-features #t) (kind 0)))) (hash "1lg0vwj6znbg6910w3hfx838kq1h0iiww6s1bwq57mp52isbvpqy")))

(define-public crate-magma-0.7 (crate (name "magma") (vers "0.7.0") (deps (list (crate-dep (name "cipher") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "cipher") (req "^0.3") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "opaque-debug") (req "^0.3") (default-features #t) (kind 0)))) (hash "06zrvwrlc2xnqjmzx06haz5fsa4a6yib1cjs1j43v31lr9zjqyak")))

(define-public crate-magma-0.8 (crate (name "magma") (vers "0.8.0") (deps (list (crate-dep (name "cipher") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "cipher") (req "^0.4") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.3") (default-features #t) (kind 2)))) (hash "0wdhbcdas5p3pmv79a8f485qdjp9d7nf4ghywbcq7b331rw07866") (features (quote (("zeroize" "cipher/zeroize")))) (rust-version "1.56")))

(define-public crate-magma-0.8 (crate (name "magma") (vers "0.8.1") (deps (list (crate-dep (name "cipher") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "cipher") (req "^0.4.2") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "1w321p30599kc85bb35ps32ykqc9gzpi5b99dsw793nxrypnj9xb") (features (quote (("zeroize" "cipher/zeroize")))) (rust-version "1.56")))

(define-public crate-magma-0.9 (crate (name "magma") (vers "0.9.0") (deps (list (crate-dep (name "cipher") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "cipher") (req "^0.4.2") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "1d7drri3cdwdzdsgk6n6g372yb2yka7y428sw9z45jszp8jbs2ab") (features (quote (("zeroize" "cipher/zeroize")))) (rust-version "1.56")))

(define-public crate-magma-0.10 (crate (name "magma") (vers "0.10.0-pre.0") (deps (list (crate-dep (name "cipher") (req "=0.5.0-pre.4") (default-features #t) (kind 0)) (crate-dep (name "cipher") (req "=0.5.0-pre.4") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.4") (default-features #t) (kind 2)))) (hash "12m5cxqq8vq28axh44msisilhmxpp2qrfp16k168c7v5pbpr6viv") (features (quote (("zeroize" "cipher/zeroize")))) (rust-version "1.65")))

(define-public crate-magma-says-0.1 (crate (name "magma-says") (vers "0.1.0") (hash "0apm3qixg7qb0dlygsacm1ws5r928cm03fyjmc8pl0lbhw27rld6")))

(define-public crate-magma_api-0.0.0 (crate (name "magma_api") (vers "0.0.0") (hash "07ack9rq6q8zai1nqq406k5nmxxz6fajnm0007cskq3knxg3vy61") (yanked #t)))

(define-public crate-magma_api-0.1 (crate (name "magma_api") (vers "0.1.0-alpha") (deps (list (crate-dep (name "magma_app") (req "^0.1.0-alpha.4") (default-features #t) (kind 0)) (crate-dep (name "magma_audio") (req "^0.1.0-alpha.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "magma_ui") (req "^0.1.0-alpha.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "magma_winit") (req "^0.1.0-alpha.4") (optional #t) (default-features #t) (kind 0)))) (hash "1ahwbpn8n3yav1cmfxc54p2wnvkcd64kn3rngbwa1r1j98jx8cfs") (features (quote (("default" "audio" "winit" "ui")))) (v 2) (features2 (quote (("winit" "dep:magma_winit") ("ui" "dep:magma_ui") ("audio" "dep:magma_audio"))))))

(define-public crate-magma_api-0.1 (crate (name "magma_api") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "magma_app") (req "^0.1.0-alpha.5") (default-features #t) (kind 0)) (crate-dep (name "magma_audio") (req "^0.1.0-alpha.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "magma_ui") (req "^0.1.0-alpha.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "magma_winit") (req "^0.1.0-alpha.5") (optional #t) (default-features #t) (kind 0)))) (hash "13y6811vqldvk26dzijhcpk1a5fvbv7jnhkbk6mlvhk01vzaa629") (features (quote (("default" "audio" "winit" "ui")))) (v 2) (features2 (quote (("winit" "dep:magma_winit") ("ui" "dep:magma_ui") ("audio" "dep:magma_audio"))))))

(define-public crate-magma_app-0.0.0 (crate (name "magma_app") (vers "0.0.0") (deps (list (crate-dep (name "magma_ecs") (req "^0.1.0-alpha.1") (default-features #t) (kind 0)))) (hash "0cqr8l6dmdmmijm2cdgmqszd7wmchi5kkhjcqxfc9q53r6hifj7c") (yanked #t)))

(define-public crate-magma_app-0.1 (crate (name "magma_app") (vers "0.1.0-alpha") (deps (list (crate-dep (name "magma_ecs") (req "^0.1.0-alpha.1") (default-features #t) (kind 0)))) (hash "19wqkn52axgfnxzm9yll4grdf7kc1v7avg8mi4v23dcf3bh42hm5")))

(define-public crate-magma_app-0.1 (crate (name "magma_app") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "magma_ecs") (req "^0.1.0-alpha.2") (default-features #t) (kind 0)))) (hash "0bzmg012kh8wvg485gj3fwl5cq3f3w8s506lwbg6q78vjy6lgqsr")))

(define-public crate-magma_app-0.1 (crate (name "magma_app") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "magma_ecs") (req "^0.1.0-alpha.2") (default-features #t) (kind 0)))) (hash "1rjidawp54r6hda2mxqihb95aby7s19s7a4bir0hz894s3k4pqb2")))

(define-public crate-magma_app-0.1 (crate (name "magma_app") (vers "0.1.0-alpha.3") (deps (list (crate-dep (name "magma_ecs") (req "^0.1.0-alpha.2") (default-features #t) (kind 0)))) (hash "1gi2z0ww6fyrgc9bds07mv1s15blb3v7smp3v5vwinrh6zm983ib")))

(define-public crate-magma_app-0.1 (crate (name "magma_app") (vers "0.1.0-alpha.4") (deps (list (crate-dep (name "magma_ecs") (req "^0.1.0-alpha.2") (default-features #t) (kind 0)))) (hash "03ll8scdcgx11xjxqk3baawr9fwv8pv27msr7bjng0xvxg12lmv4")))

(define-public crate-magma_app-0.1 (crate (name "magma_app") (vers "0.1.0-alpha.5") (deps (list (crate-dep (name "magma_ecs") (req "^0.1.0-alpha.3") (default-features #t) (kind 0)))) (hash "1w8c4j6bbg3940g0iqiwvx5wnmy128bqhlpxkjgwsf2jcmdhp6v2")))

(define-public crate-magma_audio-0.1 (crate (name "magma_audio") (vers "0.1.0-alpha") (deps (list (crate-dep (name "kira") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "magma_app") (req "^0.1.0-alpha.2") (default-features #t) (kind 0)))) (hash "0gy95qp9krih8vhkkd7r4463nfm3p0rpiqki3bm0al5h73f62mg4")))

(define-public crate-magma_audio-0.1 (crate (name "magma_audio") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "kira") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "magma_app") (req "^0.1.0-alpha.4") (default-features #t) (kind 0)))) (hash "0qicjdnmlksbvrlas6m0a8qxr0vn3v03wx3lavb1ghrn3k710iq4")))

(define-public crate-magma_audio-0.1 (crate (name "magma_audio") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "kira") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "magma_app") (req "^0.1.0-alpha.5") (default-features #t) (kind 0)))) (hash "11b9ns71x5i1spgylsfrlfxfiyg07rrhxqwpy4rvcpqp9cx4ndyh")))

(define-public crate-magma_ecs-0.1 (crate (name "magma_ecs") (vers "0.1.0-alpha") (deps (list (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "1rinl2jamh7v8g6m6n9kxc853mpp5j6286gzyr9i70pg36b9s4pn") (yanked #t)))

(define-public crate-magma_ecs-0.1 (crate (name "magma_ecs") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "0msav1dgnx0cy58gm775lsg3mlzajim0jz037882x9zb3qf3d1cr")))

(define-public crate-magma_ecs-0.1 (crate (name "magma_ecs") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "0y2l2amnpiws3iv35fpr1nasxx0fqgrbpxrc0lrlf2j4b3j2nwmf")))

(define-public crate-magma_ecs-0.1 (crate (name "magma_ecs") (vers "0.1.0-alpha.3") (hash "128xasxvyiac8pnc7810y11ldxjmh0ysircj6pmsdq177xlwwd6q")))

(define-public crate-magma_ui-0.1 (crate (name "magma_ui") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "iced") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "iced_winit") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "magma_app") (req "^0.1.0-alpha.4") (default-features #t) (kind 0)) (crate-dep (name "magma_winit") (req "^0.1.0-alpha.4") (default-features #t) (kind 0)))) (hash "1jq3glcrdaw1451jbxfp8i0i4q6vmrzgk52i6pwbf9i5m4anvrjh")))

(define-public crate-magma_ui-0.1 (crate (name "magma_ui") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "iced") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "iced_winit") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "magma_app") (req "^0.1.0-alpha.5") (default-features #t) (kind 0)) (crate-dep (name "magma_winit") (req "^0.1.0-alpha.4") (default-features #t) (kind 0)))) (hash "13avnlpgwy8pxf9ghhqyk77mzzr5gzpxm42zszrnfwrv74za719g")))

(define-public crate-magma_winit-0.1 (crate (name "magma_winit") (vers "0.1.0-alpha") (deps (list (crate-dep (name "magma_app") (req "^0.1.0-alpha.2") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.29.4") (default-features #t) (kind 0)))) (hash "01f5l1lk8yfssxahdww0970hn0jdbdrc6jx9396s904cx0mlq58d")))

(define-public crate-magma_winit-0.1 (crate (name "magma_winit") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "magma_app") (req "^0.1.0-alpha.2") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.29.4") (default-features #t) (kind 0)))) (hash "045bpnvlsxhk95ac1afiadpfp4gri248jbh9cs2v4ilfzkwfbpqg")))

(define-public crate-magma_winit-0.1 (crate (name "magma_winit") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "magma_app") (req "^0.1.0-alpha.2") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.29.4") (default-features #t) (kind 0)))) (hash "00v5rhhdijghjj8x3vsy9ivx3kfvvw506hrmns6j44s4c03h50la")))

(define-public crate-magma_winit-0.1 (crate (name "magma_winit") (vers "0.1.0-alpha.3") (deps (list (crate-dep (name "magma_app") (req "^0.1.0-alpha.2") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.29.4") (default-features #t) (kind 0)))) (hash "1a7gvx3m9i0mpsc32dqwdjg60byv6gwj4bi59rxq4s1gabdyj6ik")))

(define-public crate-magma_winit-0.1 (crate (name "magma_winit") (vers "0.1.0-alpha.4") (deps (list (crate-dep (name "magma_app") (req "^0.1.0-alpha.4") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.29.4") (default-features #t) (kind 0)))) (hash "1gwl7gs214lfwax8snp33x62v6zn2flfhs6ps1h8w19hr0kbmliv")))

(define-public crate-magma_winit-0.1 (crate (name "magma_winit") (vers "0.1.0-alpha.5") (deps (list (crate-dep (name "magma_app") (req "^0.1.0-alpha.5") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.29.4") (default-features #t) (kind 0)))) (hash "0h5lr23iyri2ag5wpi96w4k3cda7rs1wdk8yqpbmy6jg2v2lani9")))

