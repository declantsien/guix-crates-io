(define-module (crates-io ma iz) #:use-module (crates-io))

(define-public crate-maize-0.1 (crate (name "maize") (vers "0.1.0") (deps (list (crate-dep (name "daggy") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "01svp2dqiy76kyrbi9gpfdb2v2biaydiqycpywnqb85864g4bkk4") (yanked #t)))

(define-public crate-maize-0.1 (crate (name "maize") (vers "0.1.1") (deps (list (crate-dep (name "daggy") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1wcv54cm04453i8v2hcbmyxb5iws8g9np32lhjyxw56dddx29mnd")))

