(define-module (crates-io ma gc) #:use-module (crates-io))

(define-public crate-magc-0.1 (crate (name "magc") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "strontium") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.9.0") (default-features #t) (kind 0)))) (hash "061lnmbb1bps0j4g0ra6mf6j623kgdr3759aryamahbd7bphp1j6")))

(define-public crate-magc-0.1 (crate (name "magc") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "strontium") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.9.0") (default-features #t) (kind 0)))) (hash "1picqdizfvcjg9gif3cqnxj8l4ldnp3mgzg1q138yfd8y7gj22jy")))

