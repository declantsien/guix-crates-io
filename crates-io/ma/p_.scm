(define-module (crates-io ma p_) #:use-module (crates-io))

(define-public crate-map_3d-0.1 (crate (name "map_3d") (vers "0.1.0") (hash "1dfb1m68r55vjm6s7qay8kfl8l7ah4rncrfaf7a0mx4aqrd3357h")))

(define-public crate-map_3d-0.1 (crate (name "map_3d") (vers "0.1.1") (hash "1932kz6zqmjr09jzk5z84i8anma002y5ls0xh1b72p6s8r2wsp3r")))

(define-public crate-map_3d-0.1 (crate (name "map_3d") (vers "0.1.2") (hash "1lg6nxpq6iqiwj7cxfbi4rhfy19m7gfyb4slkffaj92brr59rrl0")))

(define-public crate-map_3d-0.1 (crate (name "map_3d") (vers "0.1.3") (hash "0f1mng1hcsfhp9v88wbmgnksi6wl18xh4mz099ywnn3kps61ipwk")))

(define-public crate-map_3d-0.1 (crate (name "map_3d") (vers "0.1.4") (hash "06751xxs2b7hyzdlj11h9fmps3aprpmwya82a0l6s1x34a8q777d")))

(define-public crate-map_3d-0.1 (crate (name "map_3d") (vers "0.1.5") (hash "1i22xl7xwv3hypqnhnsgp0z8fnspwvrzr0g4wnb6rl7v0xhipz5h")))

(define-public crate-map_await-0.1 (crate (name "map_await") (vers "0.1.1") (deps (list (crate-dep (name "aok") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "aok") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "loginit") (req "^0.1.10") (default-features #t) (kind 2)) (crate-dep (name "static_init") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("macros" "rt" "rt-multi-thread" "time" "fs" "io-util"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 2)))) (hash "15pjy3c73d9929ryk82xi183lbxafc5am9x8s908j8ws8jhg3flq")))

(define-public crate-map_await-0.1 (crate (name "map_await") (vers "0.1.2") (deps (list (crate-dep (name "aok") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "aok") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "async-stream") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "kanal") (req "^0.1.0-pre8") (default-features #t) (kind 0)) (crate-dep (name "loginit") (req "^0.1.10") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "static_init") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("rt"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-stream") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 2)))) (hash "1fg7gx7fd476mp1z8bc6k9i3shqikv5wn8zc2cm0hylsqa11zza2")))

(define-public crate-map_await-0.1 (crate (name "map_await") (vers "0.1.3") (deps (list (crate-dep (name "aok") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "aok") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "async-stream") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "kanal") (req "^0.1.0-pre8") (default-features #t) (kind 0)) (crate-dep (name "loginit") (req "^0.1.10") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "static_init") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("rt"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-stream") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 2)))) (hash "166v32lr5pfz1cc9ryfjac9kbvmqfdmb1i4gdh5qlmczwnprcyq6")))

(define-public crate-map_await-0.1 (crate (name "map_await") (vers "0.1.4") (deps (list (crate-dep (name "aok") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "aok") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "async-stream") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "kanal") (req "^0.1.0-pre8") (default-features #t) (kind 0)) (crate-dep (name "loginit") (req "^0.1.10") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "static_init") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("rt"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-stream") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 2)))) (hash "0crazlm1zbqf469gw4fg9kkml7j08kg5y80phvpm6s907gmp38r2")))

(define-public crate-map_await-0.1 (crate (name "map_await") (vers "0.1.5") (deps (list (crate-dep (name "aok") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "aok") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "async-stream") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "kanal") (req "^0.1.0-pre8") (default-features #t) (kind 0)) (crate-dep (name "loginit") (req "^0.1.10") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "static_init") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("rt"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-stream") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 2)))) (hash "0jihaq4apgh85cxzldg3imwvs8zn4j8828n6g5s9jpycjhribm94")))

(define-public crate-map_await-0.1 (crate (name "map_await") (vers "0.1.6") (deps (list (crate-dep (name "aok") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "aok") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "async-stream") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "kanal") (req "^0.1.0-pre8") (default-features #t) (kind 0)) (crate-dep (name "loginit") (req "^0.1.10") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "static_init") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("rt"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-stream") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 2)))) (hash "0014hsygpw7fmjwj9cbksgdfj1jwjis64bwx2qcb3n2lbvgw74d8")))

(define-public crate-map_await-0.1 (crate (name "map_await") (vers "0.1.7") (deps (list (crate-dep (name "aok") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "aok") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "async-stream") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "kanal") (req "^0.1.0-pre8") (default-features #t) (kind 0)) (crate-dep (name "loginit") (req "^0.1.10") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "static_init") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("rt"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-stream") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 2)))) (hash "1v2hi4ih5yjladyxnzv0schjrx0lp89fiiq62fxm2w82k1dck5ds")))

(define-public crate-map_await-0.1 (crate (name "map_await") (vers "0.1.8") (deps (list (crate-dep (name "aok") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "aok") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "async-stream") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "kanal") (req "^0.1.0-pre8") (default-features #t) (kind 0)) (crate-dep (name "loginit") (req "^0.1.10") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "static_init") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("rt"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-stream") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 2)))) (hash "1s2qq6cv3k2iqhmvbn6v9f005209i8p39javh72n4w6s8znng2s7")))

(define-public crate-map_box-0.1 (crate (name "map_box") (vers "0.1.0") (hash "11l6yrcpq7q2z85k3s95w71q3gy8ibx8jic550igfvf43b8icyi1") (yanked #t)))

(define-public crate-map_box-0.1 (crate (name "map_box") (vers "0.1.1") (hash "0f3p74snwdj71ycb8m0i09q040napv4s8mr922b5rqh509mfsssf") (yanked #t)))

(define-public crate-map_box-0.2 (crate (name "map_box") (vers "0.2.0") (hash "1lbp1q6i0bqk7y5pwww24iw95hj3mz0rq8ipsfsf94cwrz0n49yg") (yanked #t)))

(define-public crate-map_box-0.2 (crate (name "map_box") (vers "0.2.1") (hash "0hr8cvwza2bvvacvh7sb1p1cziljlan972wl24m3c9vqv2dzfr6j") (yanked #t)))

(define-public crate-map_box-0.2 (crate (name "map_box") (vers "0.2.2") (hash "1blmybxhchsj2zs51n7q47n5vb56qnasaklfzjh7ifrrzwzlvjbl")))

(define-public crate-map_err-0.1 (crate (name "map_err") (vers "0.1.0") (hash "0svafk5mgh2jci30wk8n4m1brbhk66skclk58v475qyap18p7im3")))

(define-public crate-map_ext-1 (crate (name "map_ext") (vers "1.0.0") (hash "1nzihg2gh0zzc9wr283av86csa6366cdp0db3hcgq3x4rmjljhkj") (features (quote (("unstable"))))))

(define-public crate-map_for-0.1 (crate (name "map_for") (vers "0.1.0") (hash "1vp8p0r4k7ngvgn76q398bpcippmla0fjmr1xlk0n6izfdlrix2j")))

(define-public crate-map_for-0.1 (crate (name "map_for") (vers "0.1.1") (hash "1af5m0cs5rn9hqfjafqv53wrp8c06fwdmbdycx8swbx478ksblks")))

(define-public crate-map_for-0.2 (crate (name "map_for") (vers "0.2.0") (hash "1v02si23b40a52f3jwv0x76zzc3y1dh5gqp3lggh2b66ms3bcmfn")))

(define-public crate-map_for-0.3 (crate (name "map_for") (vers "0.3.0") (hash "1w5svdm49s317p89yvkxagw9jds427v3aqnkya9vl5lr1hfm2z8a")))

(define-public crate-map_generator-0.1 (crate (name "map_generator") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1yalr0jzgw3dw6p92ay4f40xfxhz5zrfw9qsvhdg9rdpn9wlygln")))

(define-public crate-map_generator-0.1 (crate (name "map_generator") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0n3qcqfyar204ya0xd66337l19whniyrf0fy27qp69kd1y2xx8m4")))

(define-public crate-map_generator-0.1 (crate (name "map_generator") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1lcx9k36i9bd5dmkyfwj8ghv1p0ma3sxlz2vg0wcmzx3hc9pq2g3")))

(define-public crate-map_in_place-0.1 (crate (name "map_in_place") (vers "0.1.0") (deps (list (crate-dep (name "scopeguard") (req "^0.2") (default-features #t) (kind 0)))) (hash "09f1ca6aaysxkfvcl16psi2i77vjd0275gym4kvmg8f3i6cl36y5")))

(define-public crate-map_reduce-0.1 (crate (name "map_reduce") (vers "0.1.0") (hash "0q98xrnlf5jrh0rir0zj3pxw1l98pc2gb3jbcr1d8bcqgmlkibl2")))

(define-public crate-map_reduce_omri-0.1 (crate (name "map_reduce_omri") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "19q2yyq4sdl69fml9nix8dsmk0k1cinrfdlcg2gy77n8w8qjj0g5")))

(define-public crate-map_reduce_omri-0.1 (crate (name "map_reduce_omri") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1grfxx50lajl1flrk3x3yg6h51lhqmnrl43d7i78h5n8hy4hjzww")))

(define-public crate-map_reduce_omri-0.1 (crate (name "map_reduce_omri") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1pw9vdqqnzchzz3ak6asn0a7fz4wn2i4cpywwkpa0d4xpk80xlf6")))

(define-public crate-map_retry-0.1 (crate (name "map_retry") (vers "0.1.0") (hash "03jlzl44pmaz854w5gksqig2dnyd0qsf1yba91lpnnafyzihbabh")))

(define-public crate-map_retry-0.1 (crate (name "map_retry") (vers "0.1.1") (hash "0bqv0gin2hadmzv51bw8hc66a72jghqkhcda8fw9ps0aqqp24qkh")))

(define-public crate-map_retry-0.1 (crate (name "map_retry") (vers "0.1.2") (hash "16bynby8mkq6kk2zs1sipdpwlnbqxayigf9m4wby187ywa6dar7f")))

(define-public crate-map_split-0.1 (crate (name "map_split") (vers "0.1.0") (hash "07b61db12dzlyfjnhqhxgk5z06zliwc4m5av6fwcihcfpmnpqpmb")))

(define-public crate-map_split-0.2 (crate (name "map_split") (vers "0.2.0") (hash "08jkr7zhd55d381ri6h1pdq34wd47j3z0v3301yd5mgi4jyhq7v4")))

(define-public crate-map_split-0.2 (crate (name "map_split") (vers "0.2.1") (hash "12wk4cjg88lkbar1dmwlavwrdhb0sfa70z8xj7dsvhklgcm1s972")))

(define-public crate-map_struct-0.1 (crate (name "map_struct") (vers "0.1.0") (hash "1rzrimfds62yni4xlfcrwyfv1936z2nn278fr91gbims8mkm06nk")))

(define-public crate-map_struct-0.2 (crate (name "map_struct") (vers "0.2.0") (hash "0834057cn5wi1i0kyp3wx4sbqs4wzz5zj1qgbqfz2lncczg984nv")))

(define-public crate-map_struct-0.3 (crate (name "map_struct") (vers "0.3.0") (hash "0fwn6kf96xg12vky8qqqkw9k99l36iw3ffqzgn2y11gcmipx46d9")))

(define-public crate-map_tile-0.1 (crate (name "map_tile") (vers "0.1.0") (deps (list (crate-dep (name "geo") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "geo-types") (req "^0.7.9") (default-features #t) (kind 0)) (crate-dep (name "wkt") (req "^0.10.3") (default-features #t) (kind 0)))) (hash "1k73f0zb9m9fllmkcvavkxihjdjxwx4xi6qnlgypycmar5mss93a")))

(define-public crate-map_tile-0.1 (crate (name "map_tile") (vers "0.1.1") (deps (list (crate-dep (name "geo") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "geo-types") (req "^0.7.9") (default-features #t) (kind 0)) (crate-dep (name "wkt") (req "^0.10.3") (default-features #t) (kind 0)))) (hash "0l1bxbfyp15plrrcyi4fbyd124spiar0ir6249lqgdls1ba86cb8")))

(define-public crate-map_to_range-0.1 (crate (name "map_to_range") (vers "0.1.0") (hash "15iclbzvl7db10gysgw4kmyll45ykcdymvcjzsgw0k3b6fxcchzs") (yanked #t)))

(define-public crate-map_to_range-0.1 (crate (name "map_to_range") (vers "0.1.1") (hash "16a29g94r57zdzym234ia6zfxgb5dqchymdgxb61d7631fi0xllc")))

(define-public crate-map_to_range-0.2 (crate (name "map_to_range") (vers "0.2.0") (hash "10gfskmxvhgxz1zgn4midf33sp0d0rj1vq4m4m486q200v5slr17")))

(define-public crate-map_tuple-0.1 (crate (name "map_tuple") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "0s2sh6qf4vjq3fx81im46hs7cqp13gvhz0nxrpxm7igk6hh0w8ib") (features (quote (("tuple64") ("tuple32") ("tuple16") ("tuple128"))))))

(define-public crate-map_tuple-0.1 (crate (name "map_tuple") (vers "0.1.1") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "0hb8mp4abd1nzi5cn1239v1prag8z0djzm9y3h0vn67759c94ijv") (features (quote (("tuple64") ("tuple32") ("tuple16") ("tuple128"))))))

(define-public crate-map_tuple-0.1 (crate (name "map_tuple") (vers "0.1.2") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1mn7i4s8xvv6rpm1q5chpn6pk8pd0kkr1cr73ds04m3yhvin19rk") (features (quote (("tuple64") ("tuple32") ("tuple16") ("tuple128"))))))

(define-public crate-map_tuple-0.1 (crate (name "map_tuple") (vers "0.1.3") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1x6b9d8lr9y9wlggv475k5b48rc469nj6bfdiy6x0skwwi0mxbmh") (features (quote (("tuple64" "tuple32") ("tuple32" "tuple16") ("tuple16") ("tuple128" "tuple64"))))))

(define-public crate-map_vec-0.1 (crate (name "map_vec") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "1cd3cyrmcdg0wb8fhksd7zahwjhzciy6i9nfb6fpnfp6x2awsw6k")))

(define-public crate-map_vec-0.1 (crate (name "map_vec") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "0f6swbk8xw7kgdr3irarq2ppwflv9dqyjyk5h0ymbjzbsgzm128n")))

(define-public crate-map_vec-0.2 (crate (name "map_vec") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1hm7xdvv1sk47drmd8ddw4y0xnfhv6qh4vclxkswymd9bg89w6kf")))

(define-public crate-map_vec-0.2 (crate (name "map_vec") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1ifn3yg4f6k0xbwndacfwv20vc4k4a0j3b44wg9anyq8afw8rxxz")))

(define-public crate-map_vec-0.2 (crate (name "map_vec") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "055piqwznrxck6ig4ds08iij29ihkb2s368mb79vzkip2xlil5g2")))

(define-public crate-map_vec-0.2 (crate (name "map_vec") (vers "0.2.3") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "05d9hs0iip15sph7wjdqqgw2bfd0bwv1c8f2j7z5dwnz2xsm5180")))

(define-public crate-map_vec-0.2 (crate (name "map_vec") (vers "0.2.4") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1xxixmlja537zl9k9p8kanh5q40macvsk3z20flny460fyipmiqd") (features (quote (("default" "serde"))))))

(define-public crate-map_vec-0.2 (crate (name "map_vec") (vers "0.2.5") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1qfpp78ngpgx3whhgp8qpmfihxb95p1p95sv1l8n8qmqhkq804b5") (features (quote (("default" "serde"))))))

(define-public crate-map_vec-0.2 (crate (name "map_vec") (vers "0.2.6") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "128hwbv463nc1rjnr4a9zsilzn83yzfncxa22i16hlxjmzjyggbh") (features (quote (("default" "serde"))))))

(define-public crate-map_vec-0.3 (crate (name "map_vec") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1b9cn2phq4ad6x0mh4gzp0wygyhzc19wp4vacgx2jldx0sdq7sa3") (features (quote (("nightly") ("default" "serde"))))))

(define-public crate-map_vec-0.4 (crate (name "map_vec") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1s56vmlgz80ns5v22h1qhkjcyqm6qyjpznddbpydhn3hxm2jw4qi") (features (quote (("nightly") ("default" "serde"))))))

(define-public crate-map_vec-0.5 (crate (name "map_vec") (vers "0.5.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0pzqxkmsc0c7v2dpx8zkw92h96kvvaz22s0mzhrr2x2lw94wl7hi") (features (quote (("nightly") ("default" "serde"))))))

