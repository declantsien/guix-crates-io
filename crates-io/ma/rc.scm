(define-module (crates-io ma rc) #:use-module (crates-io))

(define-public crate-marc-0.1 (crate (name "marc") (vers "0.1.0") (hash "1yfmp1y79w6cp5dyvangwfv2h4lwds6gn0nki72q02j7yjkczy7g")))

(define-public crate-marc-0.2 (crate (name "marc") (vers "0.2.0") (hash "051lnakcy0bg50yl5ncphn4xd4zsh9m9pp6345715yh521r88iyb")))

(define-public crate-marc-0.2 (crate (name "marc") (vers "0.2.1") (hash "0d220rw9g7iy0938h8rkxbgci85cjxw0hi9z26drplbh3q2gwaly")))

(define-public crate-marc-0.2 (crate (name "marc") (vers "0.2.2") (hash "0y8say22zvdwmvww2a4q2adhvj2c80w84vp36k7mycjxh2343cfl")))

(define-public crate-marc-0.2 (crate (name "marc") (vers "0.2.3") (hash "1mkr9404ssyjan0lfyk1g0a9canh63ki9yk3sfz03jb2aai4z8q6")))

(define-public crate-marc-0.2 (crate (name "marc") (vers "0.2.4") (hash "1hpdvbshrbw25k7m18c19vql0hv7i35lhz549y6xsh6ibslvpac6")))

(define-public crate-marc-0.2 (crate (name "marc") (vers "0.2.5") (hash "0vyiva04qz110i9bjs99wdr66bn8na8d1zpzd1al19s6bs5q1n0l")))

(define-public crate-marc-0.2 (crate (name "marc") (vers "0.2.6") (hash "0lvw68797nhjcgng3s0n8ppq4fim3vmblr2n03y3bj1dqapf7q48") (features (quote (("nightly") ("default"))))))

(define-public crate-marc-0.3 (crate (name "marc") (vers "0.3.0") (hash "1wgkmcb8mkvwmhsdw6iwzig5wz890m6fzwamig6809vyfdi324w4") (features (quote (("nightly") ("default"))))))

(define-public crate-marc-0.4 (crate (name "marc") (vers "0.4.0") (hash "1srhdnhgcb7lipl3iz0imc8jjwh3j3jq0mba1vqv2qhfyvxnqzjx") (features (quote (("nightly") ("default"))))))

(define-public crate-marc-0.5 (crate (name "marc") (vers "0.5.0") (hash "0p1nfp2dawfi0jf94cr54gqvc3dbb3h42pjgqcx1z4804pl6c14c") (features (quote (("nightly") ("default"))))))

(define-public crate-marc-1 (crate (name "marc") (vers "1.0.0") (deps (list (crate-dep (name "error-chain") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1y320xjisp5spdwd8610hxr1ln67aja4q6v9bpl8vxjny5j02znx") (features (quote (("nightly") ("default"))))))

(define-public crate-marc-1 (crate (name "marc") (vers "1.1.0") (deps (list (crate-dep (name "error-chain") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0k8sh8vxzwmaxv1dgq7200j3gmi6iqbybhd8pplrib8kd9kkijn2") (features (quote (("nightly") ("default"))))))

(define-public crate-marc-1 (crate (name "marc") (vers "1.2.0") (deps (list (crate-dep (name "error-chain") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0j5ym3fiamj7bxvqgh40zhipfh64b38aczs40cgsqg8sl7xnd3z1") (features (quote (("nightly") ("default"))))))

(define-public crate-marc-1 (crate (name "marc") (vers "1.2.1") (deps (list (crate-dep (name "error-chain") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1a3y6fakrr9accrwg3m773pfrx2hnaqls98g1x0x6qab2kvvs7vb") (features (quote (("nightly") ("default"))))))

(define-public crate-marc-1 (crate (name "marc") (vers "1.3.0") (deps (list (crate-dep (name "error-chain") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1i6qi4gyw467xgiclw658vmxfxmm6k67pch94arn2qwih5f0127b") (features (quote (("nightly") ("default"))))))

(define-public crate-marc-1 (crate (name "marc") (vers "1.4.0") (deps (list (crate-dep (name "error-chain") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1l2z9wqv9jwnb43pak25qpigalcp6liijknyb085zgrf700zi30r") (features (quote (("nightly") ("default"))))))

(define-public crate-marc-1 (crate (name "marc") (vers "1.5.0") (deps (list (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0k46n2qj68w87aqd73z64ysahcz76bgfnajkyrrxr5jic4im7f1l") (features (quote (("nightly") ("default"))))))

(define-public crate-marc-2 (crate (name "marc") (vers "2.0.0") (hash "0jq57g2cxijv42bm0h08gl3ara3wwndvilqkhi4xhr6gc90y0r7y") (features (quote (("nightly") ("default"))))))

(define-public crate-marc-3 (crate (name "marc") (vers "3.0.0") (hash "0kxmkwpic53mf0yjhzv33rwpfl5b2qvrrr320qg8x8gw5nwrg88g") (features (quote (("nightly") ("default"))))))

(define-public crate-marc-3 (crate (name "marc") (vers "3.1.0") (deps (list (crate-dep (name "xml-rs") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "107pvjvb3gb509rzv3wli40z59dm3ydgagwbd9n8j75ff7cfwkny") (features (quote (("nightly") ("default")))) (v 2) (features2 (quote (("xml" "dep:xml-rs"))))))

(define-public crate-marc-3 (crate (name "marc") (vers "3.1.1") (deps (list (crate-dep (name "xml-rs") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "0mhabhk43p76k542w3ahm4d9x9n3sd550jwp9vqfs833z9hqh5f7") (features (quote (("nightly") ("default")))) (v 2) (features2 (quote (("xml" "dep:xml-rs"))))))

(define-public crate-marc-relators-0.1 (crate (name "marc-relators") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1qnnqsnp1508nj0ylkr6kcmwh9ijjkmpvd1lavd4qizvnf3njg1f") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-marcel-0.1 (crate (name "marcel") (vers "0.1.0") (deps (list (crate-dep (name "iced") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "10ja9jd280szk2x04ncxr8imj1zp6p567mcpa2669qyizh5r7xkp")))

(define-public crate-marcel-0.1 (crate (name "marcel") (vers "0.1.1") (deps (list (crate-dep (name "iced") (req "^0.6.0") (features (quote ("wgpu" "debug" "default_system_font" "tokio"))) (default-features #t) (kind 0)) (crate-dep (name "iced_native") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "ron") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0g4ncrh6yg1sv4i23apb0yq40zhnxrzz6p6v6xzbiffz7q8pgn7k") (features (quote (("dev" "ron"))))))

(define-public crate-marcelo-0.0.1 (crate (name "marcelo") (vers "0.0.1") (hash "1ggc2904h2a5wch2rrrrpwxij5lmfbdlah7z47c7wmgnq5049vaj")))

(define-public crate-MarcFly_LearningRust-0.1 (crate (name "MarcFly_LearningRust") (vers "0.1.0") (hash "1zfc7xi5i52z1vdn1whjfdrd2b728s8sqgdwszbvvf9i351nyd3n")))

(define-public crate-MarcFly_LearningRust-0.1 (crate (name "MarcFly_LearningRust") (vers "0.1.1") (hash "0wgg2kz744d30g19xxnyzd6aa0gqv5962h1pr5kn8637jcd0l5aj")))

(define-public crate-MarcFly_LearningRust-0.1 (crate (name "MarcFly_LearningRust") (vers "0.1.2") (hash "179pcnva7lapm5z7wm7msdkrbjd93abhii3flzy1q5gwrq710l73")))

(define-public crate-MarcFly_LearningRust-0.1 (crate (name "MarcFly_LearningRust") (vers "0.1.3") (hash "0x2v74jdjjfnzy2frf9z6mvkjp7cq55pgr43bvpx9gr3kr1p64az")))

(define-public crate-MarcFly_LearningRust-0.1 (crate (name "MarcFly_LearningRust") (vers "0.1.4") (hash "1csy6w8hix16ncnfj2kr70cir1jy42bgrraqbz0qkkkzk07278yj")))

(define-public crate-MarcFly_LearningRust-0.1 (crate (name "MarcFly_LearningRust") (vers "0.1.5") (hash "12gizq56zj2ch1ln1lacn7kqgkkhg2k1vzj65gg7dwaq4hn1c83h")))

(define-public crate-MarcFly_LearningRust-0.1 (crate (name "MarcFly_LearningRust") (vers "0.1.6") (hash "1gj592jg95i75nrsps4fl713c44sj1d29ksjmq56i2ka5whw3avb")))

(define-public crate-MarcFly_LearningRust-0.1 (crate (name "MarcFly_LearningRust") (vers "0.1.7") (hash "1gi3nbaa91k8smz35fc1hxgpl0p3fgiya6mi4y115hpgkbsbb1hi")))

(define-public crate-march-0.1 (crate (name "march") (vers "0.1.0") (deps (list (crate-dep (name "petgraph") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.162") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 2)))) (hash "0jkj1yx37v3xx4xwij11g794pjvbl4rcvswcwsayc66s98w9y8qj")))

(define-public crate-march_env_logger_init-0.1 (crate (name "march_env_logger_init") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.11.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 0)))) (hash "1nhj92j1sq5ksfbvh6m1h7mghmmz68ddshgrcxhxgxfr8ijkj565")))

(define-public crate-marching-buffer-0.1 (crate (name "marching-buffer") (vers "0.1.0") (hash "09vwqjd4l7w3g2g5nm4wfj05p6r84y06x0y592brxhx4ldv7ij56")))

(define-public crate-marching-cubes-0.1 (crate (name "marching-cubes") (vers "0.1.0") (hash "0mg82fv163yjyjkmg971r6hl80nm7diz5iq8fhnxy8pbmg7j98x7")))

(define-public crate-marching-cubes-0.1 (crate (name "marching-cubes") (vers "0.1.1") (hash "1hk2mr40ky7a5vlanv31snf9sdv4z0719py38nhjvm3rbpl65jah") (features (quote (("glam"))))))

(define-public crate-marching-cubes-0.1 (crate (name "marching-cubes") (vers "0.1.2") (hash "1w187p15zhz64p7kmkfcxbbgwxqpa75csq64l90w0xhnb77d129p") (features (quote (("glam"))))))

(define-public crate-marching-squares-0.1 (crate (name "marching-squares") (vers "0.1.0") (deps (list (crate-dep (name "rayon") (req "^1.3.0") (optional #t) (default-features #t) (kind 0)))) (hash "05brknk371h089lsy8wbg19wc1bs8xvrvcvjp4x1yhxm3z20vmcm") (features (quote (("parallel" "rayon"))))))

(define-public crate-marching-squares-0.1 (crate (name "marching-squares") (vers "0.1.1") (deps (list (crate-dep (name "rayon") (req "^1.3.0") (optional #t) (default-features #t) (kind 0)))) (hash "0f0ccddj516001mfxbas3fzvvvjiav6bkjgsx59zv457mn28liq1") (features (quote (("parallel" "rayon"))))))

(define-public crate-marchive-0.0.0 (crate (name "marchive") (vers "0.0.0") (hash "15hqbd1wj420p5sl48a3bw2p50rmxmqc5la6lkdgyli074kmpg3i")))

(define-public crate-marco-0.1 (crate (name "marco") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "mac_address") (req "^1.1.4") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.151") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "15pq785bacmx1nmvf28c5f7jdcazydl722hxll19rqf5s9rnldw0")))

(define-public crate-marco-cargo-test-gitlab-0.1 (crate (name "marco-cargo-test-gitlab") (vers "0.1.1") (hash "196rgj6m2w9pqjlk2zbrw5rl8rw249mf17hgsxp1d16llscaflxs")))

(define-public crate-marco-crate-example-0.1 (crate (name "marco-crate-example") (vers "0.1.0") (hash "0mnw341g132jlm4whbiqgab2vvqnj8jgl4ypj7xnl2jc9jmi49dd")))

(define-public crate-marco-crate-example-0.1 (crate (name "marco-crate-example") (vers "0.1.1") (hash "18gh6yricwd4rn5km1khcmqv4zfk4zy5v24mw7b57gsgal9bsvmg")))

(define-public crate-marco-crate-example-0.1 (crate (name "marco-crate-example") (vers "0.1.2") (hash "08672pv1ql6swq8mw6m6wpbmf04clnk3layi0hqsvpycrckkr2ac")))

(define-public crate-marco-crate-example-0.1 (crate (name "marco-crate-example") (vers "0.1.3") (hash "0rp3zwlw5vk1drygwarqna73isgbkbjv3qbnpckv1r5254r34rf1")))

(define-public crate-marco-crate-example-0.1 (crate (name "marco-crate-example") (vers "0.1.4") (hash "0lrjl28fknsh1rasg5gab5kfsrv63kdrwqxpivfdmx5x8gcl6jnz")))

(define-public crate-marco-crate-example-0.1 (crate (name "marco-crate-example") (vers "0.1.5") (hash "0hb7697jhssn3h01rnyxmv0ldp5fphxdd89kf59lah0cn26isn85")))

(define-public crate-marco-crate-example-0.1 (crate (name "marco-crate-example") (vers "0.1.6") (hash "1sn98f71a2f0b49ymr24dlq600kwz86wj7b0dphsq82ax9y3p1c8")))

(define-public crate-marco-crate-example-0.1 (crate (name "marco-crate-example") (vers "0.1.7") (hash "06hnd13dn2jz7jnvpa27s1xd48lrmracikjb8375khqriyqgnq4z")))

(define-public crate-marco-crate-example-0.1 (crate (name "marco-crate-example") (vers "0.1.8") (hash "0zyikiwdj2cavv3y47wg7wdapkmkq2mv1zwiiyd6prbxpdh6703s")))

(define-public crate-marco-crate-example-0.1 (crate (name "marco-crate-example") (vers "0.1.9") (hash "0ziqf1q8fwgpla1sb7fxgr8cz3ga38zvp584nyr385pd4dfjcdmy")))

(define-public crate-marco-crate-example-0.1 (crate (name "marco-crate-example") (vers "0.1.10") (hash "012g0792ilfbfr7qqqq7z501dz79szh6wqrywa4hiwc3qnvwx2x5")))

(define-public crate-marco-crate-example-0.1 (crate (name "marco-crate-example") (vers "0.1.11") (hash "1qnrn73z8ix4vxap96p5s7sslfr34qac5blb785ihpflgr6b5wia")))

(define-public crate-marco-crate-example-0.1 (crate (name "marco-crate-example") (vers "0.1.12") (hash "0alp69a7x2cfcqcy99m08vnxzgl5qw0xfga9xam0l34ln8kv735q")))

(define-public crate-marco-crate-example-0.1 (crate (name "marco-crate-example") (vers "0.1.13") (hash "143x3c8cljxv6bvpf82s6ya7pjh7a7qfpnj0qmv5l5mb883xqinb")))

(define-public crate-marco-crate-example-0.1 (crate (name "marco-crate-example") (vers "0.1.14") (hash "1d62dxmf013mjb17l119pifp1d3scp65b8npff0y53jqhiickbv3")))

(define-public crate-marco-gitea-test-0.1 (crate (name "marco-gitea-test") (vers "0.1.0") (hash "18hlqd1r8zlq7m3v43krzghslk1zbji15cky07aqhpalyh0vz0nq")))

(define-public crate-marco-gitea-test-0.1 (crate (name "marco-gitea-test") (vers "0.1.1") (hash "1ypppdi63lr50n2r3axl7ivni60ri8xk985d8y1zpqfyw4jdjjnc")))

(define-public crate-marco-gitea-test-0.1 (crate (name "marco-gitea-test") (vers "0.1.2") (hash "19vsvwj15kxglhv9klgpf3w8ndh4vcmf9xn3zg10daddmw970676")))

(define-public crate-marco-gitea-test-0.1 (crate (name "marco-gitea-test") (vers "0.1.3") (hash "0ch6q5r3i5l5xyqvfsbz81v5iypphkbd7p4cmasaj3099a5s1gzc")))

(define-public crate-marco-gitea-test-0.1 (crate (name "marco-gitea-test") (vers "0.1.4") (hash "0lhxw6dls54miicdldc3qb3cyrgxxy02kkrdyfdzx1i7bhv6d680")))

(define-public crate-marco-test-crate-0.1 (crate (name "marco-test-crate") (vers "0.1.0") (hash "17kwcgp2jcdqkbj32m53ybf9m8wv3f2qpdnll1ml1pybmq8zvpil")))

(define-public crate-marco-test-crate-0.2 (crate (name "marco-test-crate") (vers "0.2.0") (hash "11s23lin3zgk7nxflh2v3zzss6w206pq3h9qv5mnry9g8jiv488m")))

(define-public crate-marco-test-one-0.1 (crate (name "marco-test-one") (vers "0.1.0") (hash "0sr61bvc6lbnl1q03dw3jrjqmzx8j3m0ggkc7qkgwbj6j89sd4a1")))

(define-public crate-marco-test-one-0.1 (crate (name "marco-test-one") (vers "0.1.1") (hash "1vnw1svpynyppqy7zplqqvnrx8ykal12h1pgg7srad6wvvmsyvgs")))

(define-public crate-marco-test-one-0.1 (crate (name "marco-test-one") (vers "0.1.2") (hash "1bjdi2jxhpsfadm4pyj0lcmswzr38vgzwrpnd6z6myjvczg1i6ls")))

(define-public crate-marco-test-one-0.1 (crate (name "marco-test-one") (vers "0.1.3") (hash "14dv8qbmf44bmaj52f4rc3bmnm5kp49j8kl175pika63avlcdxl9")))

(define-public crate-marco-test-one-0.1 (crate (name "marco-test-one") (vers "0.1.4") (hash "0ajck0b7hapbxx0xrvsppqrzbivcy62a03rsczz2zi8mlw87fh3p")))

(define-public crate-marco-test-one-0.1 (crate (name "marco-test-one") (vers "0.1.5") (hash "1vhnjlb3h66y1l3czmvba32pl6mqccm2ana92cxqqy5pdhrg4afg")))

(define-public crate-marco-test-one-0.1 (crate (name "marco-test-one") (vers "0.1.6") (deps (list (crate-dep (name "serde") (req "^1.0.150") (default-features #t) (kind 0)))) (hash "00y459p5arlpy3nnjaisr3p85jnj0nk14n17a5z4zj9zv05rad4j")))

(define-public crate-marco-test-one-0.1 (crate (name "marco-test-one") (vers "0.1.7") (deps (list (crate-dep (name "serde") (req "^1.0.150") (default-features #t) (kind 0)))) (hash "148jiaznmdqnrz939n8ldgmf8ak7fqcc9ixspcyblii94gpprc0b")))

(define-public crate-marco-test-one-0.1 (crate (name "marco-test-one") (vers "0.1.8") (deps (list (crate-dep (name "serde") (req "^1.0.150") (default-features #t) (kind 0)))) (hash "0sck93hha5lw57612bh76b6zira508zj7ffdpbf8r5v7pdbqrfis")))

(define-public crate-marco-test-one-0.1 (crate (name "marco-test-one") (vers "0.1.9") (deps (list (crate-dep (name "serde") (req "^1.0.150") (default-features #t) (kind 0)))) (hash "1475ripakmchb91iwdcgyqvaqcp1sfjcq4y5syl9r2i87wi3nk0f")))

(define-public crate-marco-test-one-0.1 (crate (name "marco-test-one") (vers "0.1.10") (deps (list (crate-dep (name "serde") (req "^1.0.150") (default-features #t) (kind 0)))) (hash "11snz4241w2j4hrqsz3l70yrz3xgk7388mxi5ym28aazjhc29pd0")))

(define-public crate-marco-test-one-0.1 (crate (name "marco-test-one") (vers "0.1.11") (deps (list (crate-dep (name "serde") (req "^1.0.150") (default-features #t) (kind 0)))) (hash "15fmldqkh02ikb5jl84bhhk90h0aqqwpxdzzhjn6lzxdfibn7c8z")))

(define-public crate-marco-test-one-0.1 (crate (name "marco-test-one") (vers "0.1.12") (hash "11f2q648fdaffakmiin4l78fxznpf8b16csgw6yyxjkmwnn8r706")))

(define-public crate-marco-test-one-0.1 (crate (name "marco-test-one") (vers "0.1.13") (hash "1qmc4386k9zkp34x474q7hh6nrm38avgpll7lxyzn9jrn867h764")))

(define-public crate-marco-test-one-0.1 (crate (name "marco-test-one") (vers "0.1.14") (hash "122xkfh5zkfi7hlhfq86gczr22vv4nhvdzx1f461yw7h58pia9jh")))

(define-public crate-marco-test-one-0.1 (crate (name "marco-test-one") (vers "0.1.15") (hash "0v3g06jiq91ym748ix40an26d28by3w3z65vkwcg5srdhjr9660h")))

(define-public crate-marco-test-one-0.1 (crate (name "marco-test-one") (vers "0.1.16") (hash "03glipcgab6yabb30s55bjc7glkdds131hdl771kffgjlf90w2aw")))

(define-public crate-marco-test-one-0.1 (crate (name "marco-test-one") (vers "0.1.17") (hash "0rg294cc9r1bdzakny6smx1bsv6dgsdx22cs1pa483id1iiz5k6n")))

(define-public crate-marco-test-one-0.1 (crate (name "marco-test-one") (vers "0.1.18") (hash "07fld9r5pw0nc3ynj1bvmvibf2rq51dp1lpjlyhg02r955h7ssn0")))

(define-public crate-marco-test-one-0.1 (crate (name "marco-test-one") (vers "0.1.19") (hash "0w63njljd6awymc1rqhjhpd3gmgpfjk41fw9jw2bvhbvhxv8nm9a")))

(define-public crate-marco-test-one-0.1 (crate (name "marco-test-one") (vers "0.1.20") (hash "167pwzfhb87l85hff019w3yr9nqra2lr75dpazydrwzjb6dd1xqa")))

(define-public crate-marco-test-one-0.2 (crate (name "marco-test-one") (vers "0.2.0") (hash "0y61jcb79mfrvdzhhpxz3kdvqv0ysmk52q68q3wbr4dbhyrpwynr")))

(define-public crate-marco-test-one-0.2 (crate (name "marco-test-one") (vers "0.2.1") (hash "03f46bjqzq89jax95r32fapzfbpadw5qr0ccc6dxalv22f4zrgi6")))

(define-public crate-marco-test-one-0.2 (crate (name "marco-test-one") (vers "0.2.2") (hash "1c6rkcnngr6irkf2hsaqjqlgk7w21pxf8n147aznnq8x4ihrmc9y")))

(define-public crate-marco-test-one-0.2 (crate (name "marco-test-one") (vers "0.2.3") (hash "14qi1g7daf5gdg2x9ll0bg1zv92x7baa9zq8izz1lyi5dd8zv1m6")))

(define-public crate-marco-test-one-0.2 (crate (name "marco-test-one") (vers "0.2.4") (hash "019qdpykh6vmvc386s1lyfpbdrdkby356milj84qk4rm50a2vm5c")))

(define-public crate-marco-test-one-0.2 (crate (name "marco-test-one") (vers "0.2.5") (hash "0bdhkqhh5q28p1z02pjjxyl002avs5hhf5pgzy8k9sgfi75dnli8")))

(define-public crate-marco-test-one-0.2 (crate (name "marco-test-one") (vers "0.2.6") (hash "1arv9r6qpijvw6jznchdfgvyj7pvgz9v27njyp1ymb9fkv5hwwhy")))

(define-public crate-marco-test-one-0.3 (crate (name "marco-test-one") (vers "0.3.0") (hash "0aqzm5s6gx2991zvmy8mkp39v5rgpz2pw3i3b4ifc6v4nxrcyb38")))

(define-public crate-marco-test-one-0.3 (crate (name "marco-test-one") (vers "0.3.1") (hash "0qzwraw29pa39h4iixi92vchk7dwbz0nv2rw6ifqmpnybnjkrb2v")))

(define-public crate-marco-test-one-0.3 (crate (name "marco-test-one") (vers "0.3.2") (hash "0f4a344qmb1bvplml6agjrxrnkbv6ikzv2b5ff8az6cw6iqfhcn3")))

(define-public crate-marco-test-one-0.3 (crate (name "marco-test-one") (vers "0.3.3") (hash "0vb4ryyjy8rr3wjz1pm853pc888dyyd80hdhcyazg41zylw3gw68")))

(define-public crate-marco-test-one-0.3 (crate (name "marco-test-one") (vers "0.3.4") (hash "0yvc73nqrcbg9fhk1d13xj21mv8wrg4hzn88y3ky6bmm73r9qlcm")))

(define-public crate-marco-test-one-0.3 (crate (name "marco-test-one") (vers "0.3.5") (hash "0kbjz01q7ympz7m52iy8nyfwjrx5d53znfjdy02f9zd6pkfdi5cb")))

(define-public crate-marco-test-one-0.3 (crate (name "marco-test-one") (vers "0.3.6") (hash "1q8s1pmw6r1cpkcdkc58ricnkajdkvsjixm0zks8whdd1n2q74y9")))

(define-public crate-marco-test-one-0.3 (crate (name "marco-test-one") (vers "0.3.7") (hash "192jxl6mgy6061czcj06ydm81cnvn95ckl9v0bfc4myckay3gy6n")))

(define-public crate-marco-test-one-0.3 (crate (name "marco-test-one") (vers "0.3.8") (hash "1kj3n2ryrppwzdp5731vqxxz3b9qzy2r9hwfxjar80vdghks0y8d")))

(define-public crate-marco-test-one-0.3 (crate (name "marco-test-one") (vers "0.3.9") (hash "1h0g2dqy6pkm9z0r96lcgdpn1gy4q125k33k9f8r3pxiyx2nfcfz")))

(define-public crate-marco-test-one-0.3 (crate (name "marco-test-one") (vers "0.3.10") (hash "0h68z5szhn9yyk15iyxbf2wdscslvhwhy2s0mb9lz1xm6n2mxs3q")))

(define-public crate-marco-test-one-0.3 (crate (name "marco-test-one") (vers "0.3.11") (hash "0hn3fkvdfpfgpqwz17c9isk87c2zs0li81slvlhl6xn8zn12lgcb")))

(define-public crate-marco-test-one-0.3 (crate (name "marco-test-one") (vers "0.3.12") (hash "0sv16r4ibj4pz4s9s1hwscjpsmvrnb9a31wm6d04xfd5aldgznk3")))

(define-public crate-marco-test-three-0.1 (crate (name "marco-test-three") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "lapin") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.6") (default-features #t) (kind 0)))) (hash "102lqlb74bbsyi3d9ivwqylfpc54vwnb17bw7s4c0df0dakwip5j")))

(define-public crate-marco-test-three-0.1 (crate (name "marco-test-three") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "lapin") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.7") (default-features #t) (kind 0)))) (hash "0hkzmq4b2zlpj6r40g9sxqllis515513g9q21wa5m96a2im2zkjj")))

(define-public crate-marco-test-three-0.1 (crate (name "marco-test-three") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "lapin") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.7") (default-features #t) (kind 0)))) (hash "01kjcnjsqq646sic24a6hgkyarhif0m3zl9w47gcpix85cd4f6fz")))

(define-public crate-marco-test-three-0.1 (crate (name "marco-test-three") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "lapin") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.7") (default-features #t) (kind 0)))) (hash "0h8809j5jjpcqdkvn9i3i33jp30cfkcf9n5wypbmk6hig7m3riam")))

(define-public crate-marco-test-three-0.1 (crate (name "marco-test-three") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "lapin") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.8") (default-features #t) (kind 0)))) (hash "02xi74p14v2k303rglpnrg9zl90s90mk5ppa78cdlvs2inssknpp")))

(define-public crate-marco-test-three-0.1 (crate (name "marco-test-three") (vers "0.1.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "lapin") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.8") (default-features #t) (kind 0)))) (hash "0bb3y8x4q0ji3bq6yikly8g1xpnymfhms3x9kbir1cngnwcv00k8")))

(define-public crate-marco-test-three-0.1 (crate (name "marco-test-three") (vers "0.1.6") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "lapin") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.150") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.8") (default-features #t) (kind 0)))) (hash "13l08kb3hlw3wvih3q9x1zdc3z9d59pc5gyay1drjhzpd0lsnyh7")))

(define-public crate-marco-test-three-0.1 (crate (name "marco-test-three") (vers "0.1.7") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "lapin") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.150") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.8") (default-features #t) (kind 0)))) (hash "1ndm97skcclld4w9ifllfkqahqfp6xlabpva7cxwi36dy44dbzy9")))

(define-public crate-marco-test-three-0.1 (crate (name "marco-test-three") (vers "0.1.8") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "lapin") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.150") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.8") (default-features #t) (kind 0)))) (hash "03p98hgjl0xpwbp2ql1f43apciigdlrn30d83q1pa8m5bdwi71gs")))

(define-public crate-marco-test-three-0.1 (crate (name "marco-test-three") (vers "0.1.9") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "lapin") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.155") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.8") (default-features #t) (kind 0)))) (hash "082mzbdag53kmywrlsj782jjh7d919yxy98n76wjl7g01p8dh98h")))

(define-public crate-marco-test-three-0.1 (crate (name "marco-test-three") (vers "0.1.10") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "lapin") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.155") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.8") (default-features #t) (kind 0)))) (hash "1wh0xb3mb6pg6xcy5d4z9k859fsy3pb2cprdblfj70akbxmjg037")))

(define-public crate-marco-test-three-0.1 (crate (name "marco-test-three") (vers "0.1.11") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "lapin") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.155") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.8") (default-features #t) (kind 0)))) (hash "0ya57ixkcbmz6lwivrpvm42blawpagrw3r1kshgv6hnmxpnd96dv")))

(define-public crate-marco-test-three-0.1 (crate (name "marco-test-three") (vers "0.1.12") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "lapin") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.155") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.8") (default-features #t) (kind 0)))) (hash "0jsaq4c654ymk2mqq69v5jji0140bf5mlg2vi2kw563kgrcq9yj9")))

(define-public crate-marco-test-three-0.1 (crate (name "marco-test-three") (vers "0.1.13") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "lapin") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.155") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.8") (default-features #t) (kind 0)))) (hash "0yhw0sb13qdqbpd8qfcix28lds8dan3hhhgyfipg39krx3b3ng95")))

(define-public crate-marco-test-three-0.1 (crate (name "marco-test-three") (vers "0.1.14") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "lapin") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.155") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.9") (default-features #t) (kind 0)))) (hash "0sjqjrmal3isawkr23wl6shyvjnvx85l9mcs6glgb94a0h31qimb")))

(define-public crate-marco-test-three-0.1 (crate (name "marco-test-three") (vers "0.1.15") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "lapin") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.10") (default-features #t) (kind 0)))) (hash "0rjhbd2jshq0dqdajg5n2ydss94b2bsmydqkr28npgwml4z348dk")))

(define-public crate-marco-test-two-0.1 (crate (name "marco-test-two") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "1svr53qip00d4z33a2c40s9h4cwcydd0l3nplqlma12lxd2ln5hh")))

(define-public crate-marco-test-two-0.1 (crate (name "marco-test-two") (vers "0.1.1") (deps (list (crate-dep (name "marco-test-one") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "0lxfmirw3yqh737xcmyg7h58jciad1hcr61ivkqwn2x1acw0clfg")))

(define-public crate-marco-test-two-0.1 (crate (name "marco-test-two") (vers "0.1.2") (deps (list (crate-dep (name "marco-test-one") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "154p0w17k0ivpyhbl3y6l2hiilqjyxxz10h81lhg2rq2j1cli38k")))

(define-public crate-marco-test-two-0.1 (crate (name "marco-test-two") (vers "0.1.3") (deps (list (crate-dep (name "marco-test-one") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "0xpnh0wgf1y3d45q8xiq64wa31ia58bbsjny36fdxydlyqr20v6d")))

(define-public crate-marco-test-two-0.1 (crate (name "marco-test-two") (vers "0.1.4") (deps (list (crate-dep (name "marco-test-one") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "0a1hifxjjpc5rglyr6dacrqc04k631kvvgd191ym9pzm2v66zdri")))

(define-public crate-marco-test-two-0.1 (crate (name "marco-test-two") (vers "0.1.5") (deps (list (crate-dep (name "marco-test-one") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "1grw016kli7z3z0wbamvzh7clgwf32qmzwqz1yb284qb4vfj54zn")))

(define-public crate-marco-test-two-0.1 (crate (name "marco-test-two") (vers "0.1.6") (deps (list (crate-dep (name "marco-test-one") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "0d68avn0mish3c1vaicjc049jm1yys5lss9rwxw4fk8i7gyil1kc")))

(define-public crate-marco-test-two-0.1 (crate (name "marco-test-two") (vers "0.1.7") (deps (list (crate-dep (name "marco-test-one") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "1cipxvb3lmvvnxrnbrs7c9kwnwsn19nrnkkysfc7hccaicds44kg")))

(define-public crate-marco-test-two-0.1 (crate (name "marco-test-two") (vers "0.1.8") (deps (list (crate-dep (name "marco-test-one") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "0vad9ars09y2hmr94lisilypbcw0slnwz4l5x1n44g82gzhwh5g7")))

(define-public crate-marco-test-two-0.1 (crate (name "marco-test-two") (vers "0.1.9") (deps (list (crate-dep (name "marco-test-one") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "036bqaq2900y9yvzr9db5d22rq0kqwrfcq4pprgd22sgwca4i2rb")))

(define-public crate-marco-test-two-0.1 (crate (name "marco-test-two") (vers "0.1.10") (deps (list (crate-dep (name "marco-test-one") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "17qshsf9vz4ngjrfbn2b56qxvlp2mppawrvbrqgxc80cy9anvsz0")))

(define-public crate-marco-test-two-0.1 (crate (name "marco-test-two") (vers "0.1.11") (deps (list (crate-dep (name "marco-test-one") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "19a8986c7fx7vk2b5q6zg11s64dyh2w8y1515pvfml5zdj82kz6s")))

(define-public crate-marco-test-two-0.1 (crate (name "marco-test-two") (vers "0.1.12") (deps (list (crate-dep (name "marco-test-one") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "1s5fgvwz512qifis2nvch960lqcba6yf7lq4xwx17xnscvwjlxwn")))

(define-public crate-marco-test-two-0.1 (crate (name "marco-test-two") (vers "0.1.13") (deps (list (crate-dep (name "marco-test-one") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "151qda1l9fid2m28251fyg7v8nmxm8x6cbz1x8h21y5g99wa32z4")))

(define-public crate-marco-test-two-0.1 (crate (name "marco-test-two") (vers "0.1.14") (deps (list (crate-dep (name "marco-test-one") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "1wh4n4i8f0sjf9dx8vz4gqrkj06cg6kwy2zpg850vvfnx93qcjxv")))

(define-public crate-marco-test-two-0.1 (crate (name "marco-test-two") (vers "0.1.15") (deps (list (crate-dep (name "marco-test-one") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "0lgnsyb7w50057d3px1w7saa05yqrznz2m9hi825npss21ickbcn")))

(define-public crate-marco-test-two-0.1 (crate (name "marco-test-two") (vers "0.1.16") (deps (list (crate-dep (name "marco-test-one") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "0aqxda4mg5kgdmx8ady6km1fjf5i4g0yqkzk4rmj78r0bhfyw203")))

(define-public crate-marco-test-two-0.1 (crate (name "marco-test-two") (vers "0.1.17") (deps (list (crate-dep (name "marco-test-one") (req "^0.1.13") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "0hni1155qqy99yrr2iskwxj8kxj2a7gcsvdd2i4kxps0q5gw3bz2")))

(define-public crate-marco-test-two-0.1 (crate (name "marco-test-two") (vers "0.1.18") (deps (list (crate-dep (name "marco-test-one") (req "^0.1.14") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "1sc9zh4d1smxh260s8izz02943mhnp32fmdgmsi1f68madadsdjl")))

(define-public crate-marco-test-two-0.1 (crate (name "marco-test-two") (vers "0.1.19") (deps (list (crate-dep (name "marco-test-one") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "0d90k57p7ljpyly4x97g61dn8nd0546jnb8zjxks69div4ab1vgy")))

(define-public crate-marco-test-two-0.1 (crate (name "marco-test-two") (vers "0.1.20") (deps (list (crate-dep (name "marco-test-one") (req "^0.1.16") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "1av13b196cx9ycqgwr8jrs222ybrcpmx6h0h7rqmak10r2j63055")))

(define-public crate-marco-test-two-0.1 (crate (name "marco-test-two") (vers "0.1.21") (deps (list (crate-dep (name "marco-test-one") (req "^0.1.17") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "1gyndbq1sp5jl5wd9x8gq113i2wc2apizx0sgshignwkayryja7w")))

(define-public crate-marco-test-two-0.1 (crate (name "marco-test-two") (vers "0.1.22") (deps (list (crate-dep (name "marco-test-one") (req "^0.1.18") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "15k2ym4vg6a7nsr85y6ippjqx0bvrmnmpbc4k3ws2hzn8m14hn75")))

(define-public crate-marco-test-two-0.1 (crate (name "marco-test-two") (vers "0.1.23") (deps (list (crate-dep (name "marco-test-one") (req "^0.1.19") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "08677f236i3mlpai2rmkhlgqyd9w11npbi7yfv5rc0apsj4614nw")))

(define-public crate-marco-test-two-0.1 (crate (name "marco-test-two") (vers "0.1.24") (deps (list (crate-dep (name "marco-test-one") (req "^0.1.19") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "18kdwnfzxjrlch69x6i7l3mhvfnfvkfs046w6ji0d8wjpgvymxk4")))

(define-public crate-marco-test-two-0.2 (crate (name "marco-test-two") (vers "0.2.0") (deps (list (crate-dep (name "marco-test-one") (req "^0.1.20") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "0bp4a4kim17hv28nzzjzaablsr15fx50dciidk2sz9aj875mk8i4")))

(define-public crate-marco-test-two-0.2 (crate (name "marco-test-two") (vers "0.2.1") (deps (list (crate-dep (name "marco-test-one") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "1h4xpw19a7wz318h0pcg2nb736nibd2x5wrz4dgcawkqmksb2d6f")))

(define-public crate-marco-test-two-0.3 (crate (name "marco-test-two") (vers "0.3.0") (deps (list (crate-dep (name "marco-test-one") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "0lmbf283w15228f98ymx1xb5454is74hjgsd6msvkvjjah2ba8b8")))

(define-public crate-marco-test-two-0.4 (crate (name "marco-test-two") (vers "0.4.0") (deps (list (crate-dep (name "marco-test-one") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "0g3lx8r3fwrc0xscv9n5c38k0sqmpb4lga182a69c3wmzx9577kq")))

(define-public crate-marco-test-two-0.4 (crate (name "marco-test-two") (vers "0.4.1") (deps (list (crate-dep (name "marco-test-one") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "06w9cafm9jwz27q6kamjq9967w47brl4cyqzfnsqjx6nnl2n1fyh")))

(define-public crate-marco-test-two-0.4 (crate (name "marco-test-two") (vers "0.4.2") (deps (list (crate-dep (name "marco-test-one") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "04pw350d8lk42jzvs3cxiriblr4xacq9cg5pgzygx6q7mzvcgc28")))

(define-public crate-marco-test-two-0.4 (crate (name "marco-test-two") (vers "0.4.3") (deps (list (crate-dep (name "marco-test-one") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "10kl4hxpsmilhmdj8z3mv6dismr6ydfrlz2f0mgvl8hww8wsjl3q")))

(define-public crate-marco-test-two-0.4 (crate (name "marco-test-two") (vers "0.4.4") (deps (list (crate-dep (name "marco-test-one") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "05ira0wxaj0vpd9gkpjfzchj5jnk8hx43riiw9zp9aqb6gnpqxgm")))

(define-public crate-marco-test-two-0.4 (crate (name "marco-test-two") (vers "0.4.5") (deps (list (crate-dep (name "marco-test-one") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "0h6r86lxv64b035k4xzhca1aq9nakpyk0qa5caqdsyrmi9wm5vbf")))

(define-public crate-marco-test-two-0.4 (crate (name "marco-test-two") (vers "0.4.6") (deps (list (crate-dep (name "marco-test-one") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (default-features #t) (kind 0)))) (hash "087s3gs0kpazjwgb5kawjsm4iprl6q5v71ha4p77rdch417hqrgj")))

(define-public crate-marco-test-two-0.4 (crate (name "marco-test-two") (vers "0.4.7") (deps (list (crate-dep (name "marco-test-one") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28") (default-features #t) (kind 0)))) (hash "1ysvczkw67i51nvfp9xajy1m1yclckc1m6b0pzpxb08800by6i3d")))

(define-public crate-marco-test-two-0.4 (crate (name "marco-test-two") (vers "0.4.8") (deps (list (crate-dep (name "marco-test-one") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28") (default-features #t) (kind 0)))) (hash "06315b19xy09zzfvd618a8lkfpizb283mdvfjcca1cp2f6r299vj")))

(define-public crate-marco-test-two-0.4 (crate (name "marco-test-two") (vers "0.4.9") (deps (list (crate-dep (name "marco-test-one") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (default-features #t) (kind 0)))) (hash "0rgmx1qj8z38ri50jkrjnsi8ihc12kqbkr0i0nb4zlwa3jzd0x3h")))

(define-public crate-marco-test-two-0.4 (crate (name "marco-test-two") (vers "0.4.10") (deps (list (crate-dep (name "marco-test-one") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (default-features #t) (kind 0)))) (hash "0hvfc61rd0151qvc9ra815w51bv74ds4zqfs5wprjqd9pkwcva3j")))

(define-public crate-marco-test-two-0.4 (crate (name "marco-test-two") (vers "0.4.11") (deps (list (crate-dep (name "marco-test-one") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (default-features #t) (kind 0)))) (hash "05rl54zyfbinpsj4cpgc42671b5dmsmvz498x5wzxg2b4nr2j0y9")))

(define-public crate-marco-test-two-0.4 (crate (name "marco-test-two") (vers "0.4.12") (deps (list (crate-dep (name "marco-test-one") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (default-features #t) (kind 0)))) (hash "0ijfv35cvl5psqvc0nns0ws6dyy8a9s28407gqxay8bxm8x1yzk7")))

(define-public crate-marco-test-two-0.4 (crate (name "marco-test-two") (vers "0.4.13") (deps (list (crate-dep (name "marco-test-one") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (default-features #t) (kind 0)))) (hash "0whnkr3y8k33ng5qxl3j9z73n4d7ndg7l2kpn5pph40p74imx86f")))

(define-public crate-marco-test-two-0.4 (crate (name "marco-test-two") (vers "0.4.14") (deps (list (crate-dep (name "marco-test-one") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (default-features #t) (kind 0)))) (hash "011lpx0838v8fm823yzmvq86pnrsjps58jhfblsp5ck3n4xfa57y")))

(define-public crate-marco-test-two-0.4 (crate (name "marco-test-two") (vers "0.4.15") (deps (list (crate-dep (name "marco-test-one") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (default-features #t) (kind 0)))) (hash "038y6gdhs97bzbzg77hslq4a134gqdf1r1nzzpik52iqmwpk0if8")))

(define-public crate-marco-test-two-0.4 (crate (name "marco-test-two") (vers "0.4.16") (deps (list (crate-dep (name "marco-test-one") (req "^0.3.9") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (default-features #t) (kind 0)))) (hash "14m39gjxwh5lg4gplzqjdi6iqbwwiqqf8ay3il7vcaf43567lq22")))

(define-public crate-marco-test-two-0.4 (crate (name "marco-test-two") (vers "0.4.17") (deps (list (crate-dep (name "marco-test-one") (req "^0.3.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (default-features #t) (kind 0)))) (hash "1nfw26qqdsssqgxxmckxhng63mh7dza7i0mig3cdlyk0cryc4dks")))

(define-public crate-marco-test-two-0.4 (crate (name "marco-test-two") (vers "0.4.18") (deps (list (crate-dep (name "marco-test-one") (req "^0.3.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (default-features #t) (kind 0)))) (hash "1y7isnr91zjnyp58yfj09dm8igyw6h0c36aiaxbl7d6kpi6jjp37")))

(define-public crate-marco-test-two-0.4 (crate (name "marco-test-two") (vers "0.4.19") (deps (list (crate-dep (name "marco-test-one") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (default-features #t) (kind 0)))) (hash "16zngbmzcds97w16l6ld49vpcifimzshjqnbfh0374wrc93436x1")))

(define-public crate-marco_paspuel_add_one-0.0.5 (crate (name "marco_paspuel_add_one") (vers "0.0.5") (hash "1zz07bvr4r7s34g2z90rzc51dk19xgr0g947c56v5a2r09gcr7zi")))

(define-public crate-marco_paspuel_add_one-0.0.6 (crate (name "marco_paspuel_add_one") (vers "0.0.6") (hash "1jh7m5z8sam3pyas98lfh4cpfiicjpwzijysnf9wa5sp4qik6xn3")))

(define-public crate-marco_paspuel_add_one-0.0.7 (crate (name "marco_paspuel_add_one") (vers "0.0.7") (hash "0ml5rfz5fi3rqzzlg3lf9vlymkisj0ga2nj2nhrs4kpf2pfkqnsi")))

(define-public crate-marco_paspuel_adder-0.0.5 (crate (name "marco_paspuel_adder") (vers "0.0.5") (hash "1hf01yb18nlv9jn6ljyz929msq71m3rkwydm7108vj4xxdij0nr0")))

(define-public crate-marco_paspuel_adder-0.0.6 (crate (name "marco_paspuel_adder") (vers "0.0.6") (hash "1srzzs8cwc6p8f9qpd4jyb0sf1qvpxi3agy9819x7skhm4jx18b3")))

(define-public crate-marco_paspuel_art-0.1 (crate (name "marco_paspuel_art") (vers "0.1.0") (hash "16jdc6781s8z980py2fghwli14m1midk08ilks86ppxm7cxx410z")))

(define-public crate-marcus-0.1 (crate (name "marcus") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "1nrqcflz62dcz0aizyxlfy94bg6wjmyvnkhma6yhj97pgk63z604")))

(define-public crate-marcus-0.1 (crate (name "marcus") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "15izw7lrz3m0jsmwrwj5hzy85aazmym5srj3bkm6wh4wvc2pd1yl")))

(define-public crate-marcus-0.1 (crate (name "marcus") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "1dhv9a0pzfg3g20qxl0hnfss8pqlp5x3iywhfjsk15jkivh943ml")))

