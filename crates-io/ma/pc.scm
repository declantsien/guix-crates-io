(define-module (crates-io ma pc) #:use-module (crates-io))

(define-public crate-mapcomp-0.1 (crate (name "mapcomp") (vers "0.1.0") (hash "0dflan8b6ihxl0iq9cykgabxnw8rkz4vvjymh1zy6kfhs4h2p2id")))

(define-public crate-mapcomp-0.1 (crate (name "mapcomp") (vers "0.1.1") (hash "1nj5q34dmc2v4kyxkpalmpayj3khhxd3nqjda65z8gs3knrpghck")))

(define-public crate-mapcomp-0.1 (crate (name "mapcomp") (vers "0.1.2") (hash "0r4fgg6zfi4ch64ivg6d069pjszx31ldk35jjanlxxk28p7r4w0k")))

(define-public crate-mapcomp-0.1 (crate (name "mapcomp") (vers "0.1.3") (hash "1f0xqxmhz92l4742n3rykzl105njsrdhjbqzkxrjpj94xb2ipxi2")))

(define-public crate-mapcomp-0.2 (crate (name "mapcomp") (vers "0.2.0") (hash "0cjmiql6jlk90w942gfkrbl6d453dj7b1ashb951gsszsiik77bb")))

(define-public crate-mapcomp-0.2 (crate (name "mapcomp") (vers "0.2.1") (hash "12lmv0s32xndgxrlx3a0hcywgg8z4i80j68105jrsxm66zi913r6")))

(define-public crate-mapcomp-0.2 (crate (name "mapcomp") (vers "0.2.2") (hash "0bdcikcna5drl604h3q7rpk8qnc13g574ka3l2nj44vcxkawp69d")))

