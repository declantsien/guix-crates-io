(define-module (crates-io ma cg) #:use-module (crates-io))

(define-public crate-macgyver-lock-0.2 (crate (name "macgyver-lock") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "memcache") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "1ww76virfnw40dqhi573qais60snii2n4fn137j8hbsgbhif483h")))

