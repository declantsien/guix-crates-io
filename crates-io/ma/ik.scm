(define-module (crates-io ma ik) #:use-module (crates-io))

(define-public crate-maikor-asm-parser-0.1 (crate (name "maikor-asm-parser") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "maikor-language") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "07mqvyv82ijlhcs7zj2zvc6xfscc14419pmgzy4sphdkcflfz3f5")))

(define-public crate-maikor-asm-parser-0.1 (crate (name "maikor-asm-parser") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "maikor-language") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1b0lg3rvf5fsfjkqqi2zk710iwqg91apxyh6fcikb3v0drk8gzm2")))

(define-public crate-maikor-asm-parser-0.1 (crate (name "maikor-asm-parser") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "maikor-language") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "13skcsszpyxjd2mj85shynhvlz3frm16nchb81zx912d2w3fmqk0")))

(define-public crate-maikor-asm-parser-0.1 (crate (name "maikor-asm-parser") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "maikor-platform") (req "^0.1.18") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0n716yfkfl897j76mbnxf4f18r2wv7dl0gz663k9b57j1k0qyln7")))

(define-public crate-maikor-asm-parser-0.1 (crate (name "maikor-asm-parser") (vers "0.1.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "maikor-platform") (req "^0.1.18") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "14p16c7v8bzpjmk6sq7zgxpn60fxk8akh8xqny2yq69yw9mscza9")))

(define-public crate-maikor-asm-parser-0.1 (crate (name "maikor-asm-parser") (vers "0.1.5") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "maikor-platform") (req "^0.1.18") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1svqjb4i7w4xqccbqjpcl1cj1fy6299r0g7jv1xx9a8lq44plkvg")))

(define-public crate-maikor-asm-parser-0.1 (crate (name "maikor-asm-parser") (vers "0.1.6") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "maikor-platform") (req "^0.1.22") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "03psi89zwfsbhfmqq1fdhv4y34bk2vb92pvc510asv7bml27lkrr")))

(define-public crate-maikor-asm-parser-0.1 (crate (name "maikor-asm-parser") (vers "0.1.7") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "maikor-platform") (req "^0.1.23") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0zgcfz6p6862axmn4448n9zrlhvy9x2mcgx14av5a2llh1xi5gzl")))

(define-public crate-maikor-asm-parser-0.1 (crate (name "maikor-asm-parser") (vers "0.1.8") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "maikor-platform") (req "^0.1.25") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1lljxinzr4p28855qss772k98naxzk2yna5hzvjdvhmlz6bi2xxb")))

(define-public crate-maikor-asm-parser-0.1 (crate (name "maikor-asm-parser") (vers "0.1.9") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "maikor-platform") (req "^0.1.25") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0jxx3y5zwf9mr28ifx41699n2yl28k8gwvnr6bm92nhfzianmpwh")))

(define-public crate-maikor-asm-parser-0.1 (crate (name "maikor-asm-parser") (vers "0.1.10") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "maikor-platform") (req "^0.1.25") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1kzjfiarxys336qybrmyq0qkl6pvyn1wrxy1wgnyylq6a8i90kfx")))

(define-public crate-maikor-language-0.1 (crate (name "maikor-language") (vers "0.1.0") (hash "12farrhzdyhvqnqbkq1a0m14d7v5kiivm70avzaaxgva77npx61a")))

(define-public crate-maikor-language-0.1 (crate (name "maikor-language") (vers "0.1.1") (hash "0v1hqbzj6s4fq23zz3k6n50chfqk5akwxa91x89818dy145wvc4h")))

(define-public crate-maikor-language-0.1 (crate (name "maikor-language") (vers "0.1.2") (hash "1hr86xayx6pblyyiidw9g84lq2gwinyg910ig6qkppbybxp9qfm8")))

(define-public crate-maikor-language-0.1 (crate (name "maikor-language") (vers "0.1.3") (hash "1j989phkqwqsqqh5fq09j5yf9dpmmngm2xijq3hm41xzmcas9wkc")))

(define-public crate-maikor-language-0.1 (crate (name "maikor-language") (vers "0.1.4") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "099jqywydyrnfkzy60npsk4scgrp7cpq9af0561w068fjpm9cppj")))

(define-public crate-maikor-language-0.1 (crate (name "maikor-language") (vers "0.1.5") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0qbnpx87hpcqv5xiyrff2amp6p26icpcivyadin3092j6gbf951w")))

(define-public crate-maikor-language-0.1 (crate (name "maikor-language") (vers "0.1.6") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0ryk6rlkj3dszi0wa95kj5ma7jkj58j2zqp7z33a4wij949pv8lp")))

(define-public crate-maikor-language-0.1 (crate (name "maikor-language") (vers "0.1.7") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0i6yms8r85yw54ii7sm3xbw5wigyphsajmy6m7my13nccyfaarij")))

(define-public crate-maikor-language-0.1 (crate (name "maikor-language") (vers "0.1.8") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1iirxzjish7z4rd338d2c95vydpvc2hmrrrk4l1swpsjr5p5hx8v")))

(define-public crate-maikor-language-0.1 (crate (name "maikor-language") (vers "0.1.9") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "106spmhcpnqs2l8y3d0whcj1vhvjncfn3fxxfxl6nqnj1gc0baz8")))

(define-public crate-maikor-language-0.1 (crate (name "maikor-language") (vers "0.1.10") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "00b754pz1d8h0md403i6w63qsx866q8jdvv5jf56mjc4ay4krnmh")))

(define-public crate-maikor-language-0.1 (crate (name "maikor-language") (vers "0.1.11") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "18ghijvlnfk582ykvlv6cxkd71q30bky6qhw6zgn3nfg9zrmcd9n")))

(define-public crate-maikor-language-0.1 (crate (name "maikor-language") (vers "0.1.12") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0rlj0dbc58v49b54a5bwbmyxxn8swpb9v8vzc22dblcg9xd9gmx8")))

(define-public crate-maikor-language-0.1 (crate (name "maikor-language") (vers "0.1.13") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1z0hvsi4y5sj7qhm1lxcc9w23jm66l70p2av8ba3vjiyhn8sfig0")))

(define-public crate-maikor-language-0.1 (crate (name "maikor-language") (vers "0.1.14") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1nafychbdri9mhkl22wk4wyvf305rzl7sxw56dfmrxii7r0i6ab4")))

(define-public crate-maikor-language-0.1 (crate (name "maikor-language") (vers "0.1.15") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "19ay77mn7lqkfzl875jfjv2rab5ynhmxpww04n4xq2wfw27c5c5z")))

(define-public crate-maikor-language-0.1 (crate (name "maikor-language") (vers "0.1.16") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0x73p907d2j7w6d0j0gvffq5zrmvp3j7rx0h9fi9jf2qidg7fr8g")))

(define-public crate-maikor-platform-0.1 (crate (name "maikor-platform") (vers "0.1.17") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1i21s7qj13mxmnas9jv48dndd696zzqd8hapkphgpklzrffz78ml")))

(define-public crate-maikor-platform-0.1 (crate (name "maikor-platform") (vers "0.1.18") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0vcmj72z3jkjay8vbafqjl7j83sz7abw68d773d9lj4m9s72ipz6")))

(define-public crate-maikor-platform-0.1 (crate (name "maikor-platform") (vers "0.1.19") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "01qx3rfd9hqrgs9lq4cafkipbagfi3ax5ry09djgy2krh40d6njb")))

(define-public crate-maikor-platform-0.1 (crate (name "maikor-platform") (vers "0.1.20") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0y7rhca7bsyzrs8kz7xvmbkii64an2sv64iz12jwmh3xn989rbir")))

(define-public crate-maikor-platform-0.1 (crate (name "maikor-platform") (vers "0.1.21") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0lrld1905wpshd9l204q8vdacq2c2zkjm77rdx8yrnw30672srp2")))

(define-public crate-maikor-platform-0.1 (crate (name "maikor-platform") (vers "0.1.22") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0s6pci9p9wkpvgnbzspq9vk5kpissz7fkklm4asia615lhip839r")))

(define-public crate-maikor-platform-0.1 (crate (name "maikor-platform") (vers "0.1.23") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1x3bdf7rnlc2rwpp1l05swvlxn4bnm9p9mkb1ahpx1zk01vi101h")))

(define-public crate-maikor-platform-0.1 (crate (name "maikor-platform") (vers "0.1.24") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0kdbl8w0mrq751g454pl4yhwl342g6kjqhvbgxpggpvna3wd79yc")))

(define-public crate-maikor-platform-0.1 (crate (name "maikor-platform") (vers "0.1.25") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "126hwspk6kdd6lxpczcgsjg8ynxz8xylj7wgixik8m73hc4kx4h8")))

(define-public crate-maikor-platform-0.1 (crate (name "maikor-platform") (vers "0.1.26") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1mkfxg1hfwy776p57zld3kk67c2yd7iqxx2h51c8lpxasw9mpddj")))

(define-public crate-maikor-platform-0.1 (crate (name "maikor-platform") (vers "0.1.27") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "10fjvlsk0a47gxvljis1ffy9pfxrd2g0y3g9iyhjm84wkf7mk7f4")))

(define-public crate-maikor-platform-0.1 (crate (name "maikor-platform") (vers "0.1.28") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1sn2jw4i7z1fmgjg4c0bvbgflld5yzwp4bza7sqblw1bnj6sv8sj")))

(define-public crate-maikor-platform-0.1 (crate (name "maikor-platform") (vers "0.1.29") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1w3is3cjws0ca44xpvzg5abp2rsmym973jaiiwlv2zqxgdxin1j1")))

(define-public crate-maikor-vm-core-0.1 (crate (name "maikor-vm-core") (vers "0.1.0") (hash "0yaq8ra424bai8fyc0lipnq83h529r660923jvj8mzz2n2f88lf6")))

(define-public crate-maikor-vm-core-0.1 (crate (name "maikor-vm-core") (vers "0.1.1") (deps (list (crate-dep (name "maikor-language") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0ppc77wdq57lw4f6wmnn8c1qbvaas4pjnkvykvyybznchhawvq25")))

(define-public crate-maikor-vm-core-0.1 (crate (name "maikor-vm-core") (vers "0.1.2") (deps (list (crate-dep (name "maikor-language") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0psgsxi4na9sbmjbac3jsbzlgqkzl9zq5qhsn3rjq7i7qca38xxh")))

(define-public crate-maikor-vm-core-0.1 (crate (name "maikor-vm-core") (vers "0.1.3") (deps (list (crate-dep (name "maikor-language") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "1xc7054brpbhx632fjz56iiykzxccf7pw2cm694ygxzi0ax5j5gp")))

(define-public crate-maikor-vm-core-0.1 (crate (name "maikor-vm-core") (vers "0.1.4") (deps (list (crate-dep (name "maikor-language") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0yr78pj7q4j8ck1fkqah44lqwdb5jm8nvizhf4fkdmfb1cv69gbc")))

(define-public crate-maikor-vm-core-0.1 (crate (name "maikor-vm-core") (vers "0.1.5") (deps (list (crate-dep (name "maikor-language") (req "^0.1.11") (default-features #t) (kind 0)))) (hash "18r8qs14p3gx74dfasw09hw5y9p1b3kwq02vdspdkxndf4jra6wh")))

(define-public crate-maikor-vm-core-0.1 (crate (name "maikor-vm-core") (vers "0.1.6") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "maikor-platform") (req "^0.1.19") (default-features #t) (kind 0)))) (hash "0ri2fdjxqgc4mz61j08ahiajwbssppsw7ykj7vlsn68mr28csf50")))

(define-public crate-maikor-vm-core-0.1 (crate (name "maikor-vm-core") (vers "0.1.7") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "maikor-platform") (req "^0.1.19") (default-features #t) (kind 0)))) (hash "0rp3yjwl2mplqv3m05jh228gkrls1r56l7lsh2c3q4dkwf3ijxw7")))

(define-public crate-maikor-vm-core-0.1 (crate (name "maikor-vm-core") (vers "0.1.8") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "maikor-platform") (req "^0.1.21") (default-features #t) (kind 0)))) (hash "0nsklnkbabpibyxh40nl205sqjblksla2zhyspwiqw5rzc4j0108")))

(define-public crate-maikor-vm-core-0.1 (crate (name "maikor-vm-core") (vers "0.1.9") (deps (list (crate-dep (name "blip_buf") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "maikor-platform") (req "^0.1.22") (default-features #t) (kind 0)))) (hash "048ki75w55hri0gjrwwsyk9syxadi3cfz82kkgdmwvk3qmqa67ik")))

(define-public crate-maikor-vm-core-0.1 (crate (name "maikor-vm-core") (vers "0.1.10") (deps (list (crate-dep (name "blip_buf") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "maikor-platform") (req "^0.1.25") (default-features #t) (kind 0)))) (hash "0igcqndjgbx3cvrf5jb04p02nwbpqjyw8khs130rg2pylblyyc5j")))

(define-public crate-maikor-vm-core-0.1 (crate (name "maikor-vm-core") (vers "0.1.11") (deps (list (crate-dep (name "blip_buf") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "maikor-platform") (req "^0.1.25") (default-features #t) (kind 0)) (crate-dep (name "maikor-vm-file") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "1s209gnjqnihfqabhsdcxz9447fvs4h7h8byadnh5qrvcwgia0vy")))

(define-public crate-maikor-vm-core-0.1 (crate (name "maikor-vm-core") (vers "0.1.12") (deps (list (crate-dep (name "blip_buf") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "maikor-platform") (req "^0.1.27") (default-features #t) (kind 0)) (crate-dep (name "maikor-vm-file") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0ida1cxmcyzwc7x4p9j0g1n1babyq63qf7d0a82qx03dg3d672ad")))

(define-public crate-maikor-vm-core-0.1 (crate (name "maikor-vm-core") (vers "0.1.13") (deps (list (crate-dep (name "blip_buf") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "maikor-platform") (req "^0.1.27") (default-features #t) (kind 0)) (crate-dep (name "maikor-vm-file") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0czbmbm36m6p23c0drjqinkwv3hbpzdxdlqhg3nisbl3cgiqr97h")))

(define-public crate-maikor-vm-core-0.1 (crate (name "maikor-vm-core") (vers "0.1.14") (deps (list (crate-dep (name "blip_buf") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "maikor-platform") (req "^0.1.27") (default-features #t) (kind 0)) (crate-dep (name "maikor-vm-file") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0vwaian2p4nr45wg2d0snaaffbi15jg7lgxrm3wcnag5dwhd14aj")))

(define-public crate-maikor-vm-core-0.1 (crate (name "maikor-vm-core") (vers "0.1.15") (deps (list (crate-dep (name "blip_buf") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "maikor-platform") (req "^0.1.27") (default-features #t) (kind 0)) (crate-dep (name "maikor-vm-file") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "1r83y3gj20swqnh1kdc8bgz66ibg6lrzkz6af8nkks9ch6prw884")))

(define-public crate-maikor-vm-file-0.1 (crate (name "maikor-vm-file") (vers "0.1.0") (deps (list (crate-dep (name "fastrand") (req "^1.7.0") (default-features #t) (kind 2)) (crate-dep (name "maikor-vm-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1268q5kq3lnwzj41i4blz2nzpdmsfpxshrnw3ng3an57my62sisb")))

(define-public crate-maikor-vm-file-0.1 (crate (name "maikor-vm-file") (vers "0.1.1") (deps (list (crate-dep (name "fastrand") (req "^1.7.0") (default-features #t) (kind 2)) (crate-dep (name "maikor-language") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1ba09wi5gdifvkd9750hk0zxxp8zqhg4dy0hg96qgq8qz8nr76i4")))

(define-public crate-maikor-vm-file-0.1 (crate (name "maikor-vm-file") (vers "0.1.2") (deps (list (crate-dep (name "fastrand") (req "^1.7.0") (default-features #t) (kind 2)) (crate-dep (name "maikor-language") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "08hpp9qyx9gn5d6xvqyrihfmdr3n0mpdnqrv1ns130zbhi5kbg41")))

(define-public crate-maikor-vm-file-0.1 (crate (name "maikor-vm-file") (vers "0.1.3") (deps (list (crate-dep (name "fastrand") (req "^1.7.0") (default-features #t) (kind 2)) (crate-dep (name "maikor-language") (req "^0.1.13") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0cwgv57m3z4a0hl45hfjl50x51pc5pf4vdir938ni3k8wajyy3pi")))

(define-public crate-maikor-vm-file-0.1 (crate (name "maikor-vm-file") (vers "0.1.4") (deps (list (crate-dep (name "fastrand") (req "^1.7.0") (default-features #t) (kind 2)) (crate-dep (name "maikor-language") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1qkcvj59qb2i8x6syja8m5rhfac1yabz1n0mn2sbzzqgz4ws9flh")))

(define-public crate-maikor-vm-file-0.1 (crate (name "maikor-vm-file") (vers "0.1.5") (deps (list (crate-dep (name "fastrand") (req "^1.7.0") (default-features #t) (kind 2)) (crate-dep (name "maikor-platform") (req "^0.1.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0mkq5qikg7awx3ls6cpkz16pjmj94fxpc2538n3bgnrc8b6a5b3y")))

(define-public crate-maikor-vm-file-0.1 (crate (name "maikor-vm-file") (vers "0.1.6") (deps (list (crate-dep (name "maikor-platform") (req "^0.1.25") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "12ccyy3w3msqjckl693wp8iwdn8ny6xhj5gi66cv8ywp9bspdnj3")))

(define-public crate-maikor-vm-file-0.1 (crate (name "maikor-vm-file") (vers "0.1.7") (deps (list (crate-dep (name "maikor-platform") (req "^0.1.25") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0q2v242i1dxhxyilcca553yjh5dvc655fl2ck9pxwkdapb792krq")))

(define-public crate-maikor-vm-file-0.1 (crate (name "maikor-vm-file") (vers "0.1.8") (deps (list (crate-dep (name "maikor-platform") (req "^0.1.25") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "19kmriandrxkq75h8ipn7ssgjs87dwzdiw273d503i293d9q7zxm")))

(define-public crate-maikor-vm-file-0.1 (crate (name "maikor-vm-file") (vers "0.1.9") (deps (list (crate-dep (name "maikor-platform") (req "^0.1.27") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1i736spbc58jsr4c8vykhy1b08bg7cp3bgya57448xphggknwm49")))

(define-public crate-maikor-vm-file-0.1 (crate (name "maikor-vm-file") (vers "0.1.10") (deps (list (crate-dep (name "maikor-platform") (req "^0.1.29") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1i77wdhm35msr2b1ijk2x8vvc1q7a1k713alxrmsi2z2ir48h7s8")))

(define-public crate-maikor-vm-file-0.1 (crate (name "maikor-vm-file") (vers "0.1.11") (deps (list (crate-dep (name "maikor-platform") (req "^0.1.29") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0sr3vi9xiawbj4ic5i0cgqbmiga6q5m7fskjwisf2bh6ib3ik5yc")))

(define-public crate-maikor-vm-interface-0.1 (crate (name "maikor-vm-interface") (vers "0.1.0") (deps (list (crate-dep (name "maikor-vm-core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1dhs68badmz1maykrf68c704n8n4jvzjm7wm49d7c1v7xrll7jqw") (features (quote (("rgba") ("default") ("argb"))))))

(define-public crate-maikor-vm-interface-0.1 (crate (name "maikor-vm-interface") (vers "0.1.1") (deps (list (crate-dep (name "maikor-language") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "maikor-vm-core") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0bk3f05jndj531apjxfpzjbckdj180vf3pcq0rh5l61nvw1xyili") (features (quote (("rgba") ("default") ("argb"))))))

(define-public crate-maikor-vm-interface-0.1 (crate (name "maikor-vm-interface") (vers "0.1.2") (deps (list (crate-dep (name "maikor-language") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "maikor-vm-core") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "09fvi2saapa1z73a2nr15qbnaz9ckf1znvm9f5n0n1d3i644zz2a") (features (quote (("rgba") ("default") ("argb"))))))

(define-public crate-maikor-vm-interface-0.1 (crate (name "maikor-vm-interface") (vers "0.1.3") (deps (list (crate-dep (name "maikor-language") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "maikor-vm-core") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1djsnfvi6gm5h4if1gm0vcpcdw0j6ddi5cgy732dykx9pz5qd6c5") (features (quote (("rgba") ("default") ("argb"))))))

(define-public crate-maikor-vm-interface-0.1 (crate (name "maikor-vm-interface") (vers "0.1.4") (deps (list (crate-dep (name "maikor-language") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "maikor-vm-core") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "17wma0b0ll4j47pjzic4vcg5vnx2l8f91a83dcg90mq1px1d142j") (features (quote (("rgba") ("default") ("argb"))))))

(define-public crate-maikor-vm-interface-0.1 (crate (name "maikor-vm-interface") (vers "0.1.5") (deps (list (crate-dep (name "maikor-platform") (req "^0.1.19") (default-features #t) (kind 0)) (crate-dep (name "maikor-vm-core") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "08chypn5lml7cv60jmizmpcm38n7c6pc1x2l5maqh57f574nkaz3") (features (quote (("rgba") ("default") ("argb"))))))

(define-public crate-maikor-vm-interface-0.1 (crate (name "maikor-vm-interface") (vers "0.1.6") (deps (list (crate-dep (name "cpal") (req "^0.13.5") (default-features #t) (kind 0)) (crate-dep (name "maikor-platform") (req "^0.1.22") (default-features #t) (kind 0)) (crate-dep (name "maikor-vm-core") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "nanorand") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1vsw9vmyg2dx18mi7sl686mzg5x3s6ql05lq3km7h4vvvfszbphs") (features (quote (("rgba") ("default") ("argb"))))))

(define-public crate-maikor-vm-interface-0.1 (crate (name "maikor-vm-interface") (vers "0.1.7") (deps (list (crate-dep (name "cpal") (req "^0.13.5") (default-features #t) (kind 0)) (crate-dep (name "maikor-platform") (req "^0.1.25") (default-features #t) (kind 0)) (crate-dep (name "maikor-vm-core") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "nanorand") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "084hm4vr505n1f42628h45b0lfpmgnrxamyrn4x0980jwcp240s5") (features (quote (("rgba") ("default") ("argb"))))))

(define-public crate-maikor-vm-interface-0.1 (crate (name "maikor-vm-interface") (vers "0.1.8") (deps (list (crate-dep (name "cpal") (req "^0.13.5") (default-features #t) (kind 0)) (crate-dep (name "maikor-platform") (req "^0.1.27") (default-features #t) (kind 0)) (crate-dep (name "maikor-vm-core") (req "^0.1.13") (default-features #t) (kind 0)) (crate-dep (name "nanorand") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0m7p3gr9bdyxp05jvvbmi4arikpr1k6i4q9zllqamh738dyjlyrm") (features (quote (("rgba") ("default") ("argb"))))))

(define-public crate-maikor-vm-interface-0.1 (crate (name "maikor-vm-interface") (vers "0.1.9") (deps (list (crate-dep (name "cpal") (req "^0.13.5") (default-features #t) (kind 0)) (crate-dep (name "maikor-platform") (req "^0.1.27") (default-features #t) (kind 0)) (crate-dep (name "maikor-vm-core") (req "^0.1.14") (default-features #t) (kind 0)) (crate-dep (name "nanorand") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0wx1p8a6c1kig8bfxb7hrlka6vjnj5wraw3qbvzvyjckm19ip118") (features (quote (("rgba") ("default") ("argb"))))))

(define-public crate-maikor-vm-interface-0.1 (crate (name "maikor-vm-interface") (vers "0.1.10") (deps (list (crate-dep (name "cpal") (req "^0.13.5") (default-features #t) (kind 0)) (crate-dep (name "maikor-platform") (req "^0.1.27") (default-features #t) (kind 0)) (crate-dep (name "maikor-vm-core") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "nanorand") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1920bfc6g193g1l47y0g8w1h3l5gvmf1q9s8z3d828w3jz8l7vxi") (features (quote (("rgba") ("default") ("argb"))))))

