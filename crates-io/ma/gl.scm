(define-module (crates-io ma gl) #:use-module (crates-io))

(define-public crate-maglev-0.1 (crate (name "maglev") (vers "0.1.0") (deps (list (crate-dep (name "fasthash") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "primal") (req "^0.2") (default-features #t) (kind 0)))) (hash "1w039hfh80y2ivbfkg91gwwgkn5gla4g18wqpmr6fa18cdbchnfx")))

(define-public crate-maglev-0.1 (crate (name "maglev") (vers "0.1.1") (deps (list (crate-dep (name "primal") (req "^0.2") (default-features #t) (kind 0)))) (hash "1saqyy0abj9slf9xm4f3h684f2i31z2xa2dmjdg1val056bb16ln")))

(define-public crate-maglev-0.1 (crate (name "maglev") (vers "0.1.2") (deps (list (crate-dep (name "primal") (req "^0.2") (default-features #t) (kind 0)))) (hash "053wdjfwiak70hys14rvd71yd2zbvm0jc85zckq0x0ld7nqm0wiz")))

(define-public crate-maglev-0.1 (crate (name "maglev") (vers "0.1.3") (deps (list (crate-dep (name "fasthash") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "primal") (req "^0.2") (default-features #t) (kind 0)))) (hash "1q6h1l1z8rhqkgw3675i75lsf91l464gjq2awsfjmalihdm849f6")))

(define-public crate-maglev-0.2 (crate (name "maglev") (vers "0.2.0") (deps (list (crate-dep (name "fasthash") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "primal") (req "^0.3") (default-features #t) (kind 0)))) (hash "0l731pzchf6q9s2mgpydpbx62l16hy40s379i7z8kxjdgbhqad12")))

(define-public crate-maglev-0.2 (crate (name "maglev") (vers "0.2.1") (deps (list (crate-dep (name "fasthash") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "primal") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 2)))) (hash "0w2kkph9yk4aiipx5lwxk3ypsqnnxnb1axyzazsikl0634ga4vn7")))

(define-public crate-maglev_rs-0.1 (crate (name "maglev_rs") (vers "0.1.0") (deps (list (crate-dep (name "primes") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0jlhnqlg4jr717scx8jsipk6bf22ikksnzm8dvlzdyjjg0k9vm2j")))

(define-public crate-maglio-0.1 (crate (name "maglio") (vers "0.1.1") (hash "1vbsk9hs61vv72jcrfzkscihabsq827dsxf8gfh4anvsaasv1wdn")))

(define-public crate-maglio-0.1 (crate (name "maglio") (vers "0.1.2") (deps (list (crate-dep (name "ordered-float") (req "^2.8.0") (default-features #t) (kind 0)))) (hash "1pqfx0ypzpqdqv7vk6d2kqskissicsfylbjrgwp599maj4dhg9am")))

