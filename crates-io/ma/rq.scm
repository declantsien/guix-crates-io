(define-module (crates-io ma rq) #:use-module (crates-io))

(define-public crate-marquee-0.1 (crate (name "marquee") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "0k51a5apca5lz76p4lrfqhnhw777x8xvflykdn5lwms41csgdyac")))

(define-public crate-marquee-1 (crate (name "marquee") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "1pl1iv7xnq72q1ycc5qa8cqbmgf9hcixq912f2amyf3vgs9k5wqi")))

(define-public crate-marquee-1 (crate (name "marquee") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "1r3fwi6d31ayg35mhbi6kxz0gvirfa1rvmwi18dq7vl6pra5mky9")))

(define-public crate-marquee-1 (crate (name "marquee") (vers "1.1.1") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "1cdyvhx2l0y94k38rqg9rkhng2sbk329gqawqabnpqphc6cnxhzj")))

(define-public crate-marquee-1 (crate (name "marquee") (vers "1.1.2") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "0jp2951g5dj62904npwcs3chl09wi6ms8dadwvpr36syzc7h2cgi")))

