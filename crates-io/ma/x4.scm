(define-module (crates-io ma x4) #:use-module (crates-io))

(define-public crate-max44009-0.1 (crate (name "max44009") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)))) (hash "1icn57sv9s40rd5qdc3mvpj7zbmzrl2slsxmpiw9hh8hjnvdww6m")))

(define-public crate-max44009-0.2 (crate (name "max44009") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)))) (hash "0ljwlv6ralg2cwhmb6h16yv39y8hhl43nr3998s7s8jlmp3rc35x")))

(define-public crate-max485-0.1 (crate (name "max485") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0") (default-features #t) (kind 0)))) (hash "1s48jnvd9drqmm25h0chr0d2wcim913q1z7aa5m7k45vz2fghcav")))

(define-public crate-max485-0.2 (crate (name "max485") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0") (default-features #t) (kind 0)))) (hash "0symv96syszhmbspcwj4mjrfh368z3ddlpvx1bdskkv37daf04i9")))

