(define-module (crates-io ma rv) #:use-module (crates-io))

(define-public crate-marvel-api-rs-0.0.0 (crate (name "marvel-api-rs") (vers "0.0.0") (hash "1s7dgg1bk4zyb0yywvsc216g84qmddyn7bhjy30x14pjbibrg0s4")))

(define-public crate-marvel-snap-0.0.1 (crate (name "marvel-snap") (vers "0.0.1") (hash "0qa1gnaq2facbwkbpzpy40xy0m3a0yrcawi2i0m5sg63w9lw2kvq")))

(define-public crate-marvelsnapdeck-0.1 (crate (name "marvelsnapdeck") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "12d7vjdmx6fq2bhwgsh0ibzwhifki35apznqz1smn9f9aa32pqqd")))

(define-public crate-marvelsnapdeck-0.1 (crate (name "marvelsnapdeck") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1c267g80v6gf687qf9zk9mjpwbb5al58v8zk0w8qj7xqjy6b0myv")))

(define-public crate-marvin-0.1 (crate (name "marvin") (vers "0.1.0") (hash "0qmazznapfy78slb3hf2h0dfk5kwca36xvr2k1cpc50s3arfcmk6")))

(define-public crate-marvin32-0.1 (crate (name "marvin32") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)))) (hash "0c5zgzkxyrvgqrxyc6xkicgp44zk3k58z71sh4v2c247s0033jyw")))

(define-public crate-marvin32-0.1 (crate (name "marvin32") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)))) (hash "0bzj5z6jgzmb8bdm0854km2wl5h57x9a9xxcy5sd87m507h0yr6j")))

