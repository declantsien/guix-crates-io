(define-module (crates-io ma dd) #:use-module (crates-io))

(define-public crate-madder-0.1 (crate (name "madder") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.2.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "config") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "handlebars") (req "^4.3.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "0c3xpmb21aq0wji0ka5yblzvlym1ajg7gl406s8skhi5nsf9lnw0")))

(define-public crate-maddr-0.1 (crate (name "maddr") (vers "0.1.0") (deps (list (crate-dep (name "error-chain") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "mhash") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "varmint") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0k99lfgxl1b7z9vhbs0m499mvy2mzv90a5fq1vzscip96f3b9si7") (yanked #t)))

(define-public crate-maddr-0.2 (crate (name "maddr") (vers "0.2.0") (deps (list (crate-dep (name "error-chain") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "mhash") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "varmint") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0q4y1ss2wp5rygmpcrx8zdrblp0c6yr9b7cz32n631nvsjlpmwbk") (yanked #t)))

(define-public crate-maddr-0.2 (crate (name "maddr") (vers "0.2.1") (deps (list (crate-dep (name "error-chain") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "mhash") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "varmint") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1apzbbrysmig81hb41h77k8j3ajs7yhs6fg7w6kicvgijh08bfz4") (yanked #t)))

(define-public crate-maddr-0.3 (crate (name "maddr") (vers "0.3.0") (deps (list (crate-dep (name "error-chain") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "mhash") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "varmint") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "06zdl7hbgfrnq8bala6z53jcfrib426yfp0zwb3darxqgjnvd1p3") (yanked #t)))

(define-public crate-maddr-0.3 (crate (name "maddr") (vers "0.3.1") (deps (list (crate-dep (name "error-chain") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "mhash") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "varmint") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "03zv01wzgk44hkf7djwsay79pf7zi4x2fvqaszb9v4awgkpcyjgd") (yanked #t)))

