(define-module (crates-io ma lk) #:use-module (crates-io))

(define-public crate-malk-core-0.1 (crate (name "malk-core") (vers "0.1.0") (hash "1y712g26s7yx32653l23kbgr4mn2yg15hjfdjp2x18x8frx2d1c6")))

(define-public crate-malk-core-0.1 (crate (name "malk-core") (vers "0.1.1") (hash "1cn32bxir0pdc51rvc3jlw1ir643gqhj28kya1f1sgw04zglbai8")))

(define-public crate-malk-lexer-0.1 (crate (name "malk-lexer") (vers "0.1.0") (deps (list (crate-dep (name "unicode-brackets") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "unicode-xid") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "16k6xb600ls2ihvlaynvxrw58205z71bxl2z02mmsybjb3qjdg5j")))

(define-public crate-malk-lexer-0.1 (crate (name "malk-lexer") (vers "0.1.1") (deps (list (crate-dep (name "unicode-brackets") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "unicode-xid") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "0l4c67zzxwd9wr8af5dnpy2qrdasq6rdxjvbv68s1wbblk2mpxr5")))

(define-public crate-malkmusl-game-engine-0.1 (crate (name "malkmusl-game-engine") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "glium") (req "^0.32.1") (default-features #t) (kind 0)) (crate-dep (name "glutin") (req "^0.30.10") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)) (crate-dep (name "na") (req "^0.32.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "vulkano") (req "^0.33.0") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.27.5") (default-features #t) (kind 0)))) (hash "1h6krv90xlwwhi8dqd9nrn8jxrwa29c9qsm9bnj0bygixzqaw37w")))

