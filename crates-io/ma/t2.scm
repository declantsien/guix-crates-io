(define-module (crates-io ma t2) #:use-module (crates-io))

(define-public crate-mat2-0.1 (crate (name "mat2") (vers "0.1.0") (deps (list (crate-dep (name "number_traits") (req "^0.1") (default-features #t) (kind 0)))) (hash "103z5ssh27brlag0fm5932k332xiy0xifx3avv7a34ljimgzzc67")))

(define-public crate-mat2-0.1 (crate (name "mat2") (vers "0.1.1") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1h4ffc7lcjv7b4qfkwfnmp2wx89sbrrmkpy72b6cy36ix93vm9k3")))

(define-public crate-mat2-0.1 (crate (name "mat2") (vers "0.1.2") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "11ins3zj2xz68yqy2kvz5m4h9c7j38drq231iaf2pv6j3kyscbmj")))

(define-public crate-mat2-0.1 (crate (name "mat2") (vers "0.1.3") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1nbig5xb1cvf46qimqrs28gk1921farqh0rzdlzg5g0flhk5liv3")))

(define-public crate-mat2-0.1 (crate (name "mat2") (vers "0.1.4") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1dfmxsdin9i3zl5qn2bxbhdp2v12ij3hxj80jprld0f0yj9gnbzp")))

(define-public crate-mat2-0.2 (crate (name "mat2") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "02am0v14wkjashblccaisrj710fcpkcphsyw9dgfpbdf9w0mv17m")))

(define-public crate-mat2-0.2 (crate (name "mat2") (vers "0.2.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "075jq65xmh07npd7k1va14g45syi8bv58n460fxhv811i88hh0pi")))

(define-public crate-mat2image-0.1 (crate (name "mat2image") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24") (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 2)) (crate-dep (name "opencv") (req "^0.64") (features (quote ("rgb"))) (kind 0)) (crate-dep (name "opencv") (req "^0.64") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rgb") (req "^0.8.20") (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "094l9zz307v4gsp36pfndwr65ib2vcy0phdagbfjah6p3h2ccbds") (features (quote (("default" "rayon"))))))

(define-public crate-mat2image-0.1 (crate (name "mat2image") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.24") (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 2)) (crate-dep (name "opencv") (req "^0.66") (features (quote ("rgb"))) (kind 0)) (crate-dep (name "opencv") (req "^0.66") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rgb") (req "^0.8.20") (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0471qrc6j1bv5da7mkf3160q7vwbq172b2lasvmr43501j5f6zy6") (features (quote (("default" "rayon"))))))

(define-public crate-mat2image-0.1 (crate (name "mat2image") (vers "0.1.2") (deps (list (crate-dep (name "image") (req "^0.24") (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 2)) (crate-dep (name "opencv") (req "^0.68") (features (quote ("rgb"))) (kind 0)) (crate-dep (name "opencv") (req "^0.68") (features (quote ("imgcodecs"))) (kind 2)) (crate-dep (name "rayon") (req "^1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rgb") (req "^0.8.20") (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "141ppxwkiwh99n95mcgkg5bnm7vw0x3pvb1pasyfmpc36r6c93x2") (features (quote (("default" "rayon"))))))

(define-public crate-mat2image-0.2 (crate (name "mat2image") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.24") (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 2)) (crate-dep (name "opencv") (req "^0.74") (features (quote ("rgb"))) (kind 0)) (crate-dep (name "opencv") (req "^0.74") (features (quote ("imgcodecs"))) (kind 2)) (crate-dep (name "rayon") (req "^1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rgb") (req "^0.8.20") (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0vgp7pqfa5v59vjim9l9h2szziwlfg7ry4x57pbsk37gfcbnh7vy") (features (quote (("default" "rayon"))))))

