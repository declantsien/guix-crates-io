(define-module (crates-io ma pw) #:use-module (crates-io))

(define-public crate-mapwords-0.1 (crate (name "mapwords") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "04yb7h5w2whcnqsiqs89www3zvv6xqxf5f2qj66fgfdavsa1gjvz")))

(define-public crate-mapwords-0.1 (crate (name "mapwords") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0zdycra213ic66b8a0f56b4skwsvyaq1qc0fxrh6gbvkmmw036l9") (yanked #t)))

(define-public crate-mapwords-0.1 (crate (name "mapwords") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "081p5h5kb6jm728hszd7kjv4fk764hvxa4z46hxqic7hlndrss2s") (yanked #t)))

(define-public crate-mapwords-0.1 (crate (name "mapwords") (vers "0.1.3") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0q2nchrz6y7cd152rn0b1wnap854pxcvzy3b70b4bcjm6ah0p9p3")))

(define-public crate-mapwords-0.1 (crate (name "mapwords") (vers "0.1.4") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "03xqmbmr0y75lpmaygyd0wvfkixcp17j31kgmhjkqx2fki0fw3if")))

(define-public crate-mapwords-0.1 (crate (name "mapwords") (vers "0.1.5") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1krq0p1md0mgs6a5a9dxp5vc3sp1zkl96wxbif6jrwjb1l3hfgxn")))

