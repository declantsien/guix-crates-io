(define-module (crates-io ma pt) #:use-module (crates-io))

(define-public crate-maptiler-cloud-0.1 (crate (name "maptiler-cloud") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "189l6srj61qbhqz1dx90md4c2gfd9nn3rkg2l7wzhviygn649pmv")))

(define-public crate-maptiler-cloud-0.2 (crate (name "maptiler-cloud") (vers "0.2.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1gd068zgf8d13378vdjswjwajnphkgnzrwd1i12ahhpcib20iq6k")))

(define-public crate-maptiler-cloud-0.3 (crate (name "maptiler-cloud") (vers "0.3.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0810jk70zjvlasr7yqjhqsd0i4zppv7gmyzmma0qxadvfy958wvx")))

