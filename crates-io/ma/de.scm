(define-module (crates-io ma de) #:use-module (crates-io))

(define-public crate-made-0.1 (crate (name "made") (vers "0.1.3") (hash "0wrhbkrfkm0nvwcgxjf1lwh2ki6fc8z68l79vi8g042d4jwqfm21")))

(define-public crate-madeleine-0.2 (crate (name "madeleine") (vers "0.2.0") (deps (list (crate-dep (name "assert_fs") (req "^1.0.13") (default-features #t) (kind 2)) (crate-dep (name "commitlog") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "predicates") (req "^3.0.3") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ulid") (req "^1.0.0") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "1v6021nkcb4xgrlarxnfp9dsxr4lpcx3sxacrqwqd6b6r0ks8jvy")))

(define-public crate-madeleine-0.2 (crate (name "madeleine") (vers "0.2.1") (deps (list (crate-dep (name "assert_fs") (req "^1.0.13") (default-features #t) (kind 2)) (crate-dep (name "commitlog") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "predicates") (req "^3.0.3") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ulid") (req "^1.0.0") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "10r9ypsywvcmrzlxry59d1r41ljnfwizhy7fq5z0cm3zvdw6ac0n")))

(define-public crate-madeline-0.0.0 (crate (name "madeline") (vers "0.0.0") (hash "08rwcg30rhz4rs3f4b7cvdbkdl7wczmm8gcdmllkvl44m98fj81v")))

(define-public crate-madepro-0.1 (crate (name "madepro") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "08sm6ssvjgnsqvy3dnyk50mr3j52fy5p1jcas36kkf71gri9n56w")))

