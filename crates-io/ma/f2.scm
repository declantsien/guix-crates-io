(define-module (crates-io ma f2) #:use-module (crates-io))

(define-public crate-maf2bed-0.1 (crate (name "maf2bed") (vers "0.1.0") (hash "130m19j5ml9jlh6vsfkvxk7j4zpalhvvy22g643nl6kl32acasxg")))

(define-public crate-maf2bed-0.2 (crate (name "maf2bed") (vers "0.2.0") (hash "1s4537vabkya548qnc2c6w93w5wk55hv07r1dcnxqzbvda4crk7q")))

(define-public crate-maf2bed-0.3 (crate (name "maf2bed") (vers "0.3.0") (hash "15n50s1vy5m5c1r94jp3112ypgnk4lkk7a0pyql9m9cqsr4z4j4a")))

