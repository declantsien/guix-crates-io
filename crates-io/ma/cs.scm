(define-module (crates-io ma cs) #:use-module (crates-io))

(define-public crate-macs-0.0.0 (crate (name "macs") (vers "0.0.0") (hash "17lb24gyrw968llgbac5l5d536wh5q94zrdzk8b9s3dbm1b2i40k")))

(define-public crate-macsmc-0.1 (crate (name "macsmc") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)))) (hash "1y6hk4nv9pg2fqwhgwsq2m7vx06hn300qs2q3sgfkfan43ba9yir") (yanked #t)))

(define-public crate-macsmc-0.1 (crate (name "macsmc") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)))) (hash "038fh1pr0jicjq4wj79gqy1w3zga3881jbbsf6hw60jnzds0fjr0") (yanked #t)))

(define-public crate-macsmc-0.1 (crate (name "macsmc") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)))) (hash "11wrsipj0mfhx655w15bj936j4lki168ivb5ndxjl86jwz9x7fsw") (yanked #t)))

(define-public crate-macsmc-0.1 (crate (name "macsmc") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)))) (hash "175ig82nfksq5a3213d89zafwl06i6scr3hbh7xp2hjkdiqm2rb0")))

(define-public crate-macsmc-0.1 (crate (name "macsmc") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)))) (hash "1vd2pl889mif6crlf8cfdwk2dw65jrjdrrzxkj2qca6mwda5mflj")))

(define-public crate-macsmc-0.1 (crate (name "macsmc") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)))) (hash "1ax0cf9hgp9cw3p3p9ns1vb6h56zhqrmjcl80bb4716w9p7zvnkg")))

(define-public crate-macsmc-0.1 (crate (name "macsmc") (vers "0.1.6") (deps (list (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)))) (hash "0insq652xm8wyl35sln7qlhfj4hi8qzz1mq6d8gimm5ck3qr2nh6")))

(define-public crate-macsmc-0.1 (crate (name "macsmc") (vers "0.1.7") (deps (list (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)))) (hash "1jylwchdjii6w7rka5j45vzbrq4f22ahwm6bd7ldv3g4iajvrmz1")))

(define-public crate-macstats-0.1 (crate (name "macstats") (vers "0.1.0") (deps (list (crate-dep (name "macsmc") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1a4r09bzksnvx9xp6icvdq79jmh4w33wzy4mifnlc59q4jr8dmh3")))

(define-public crate-macstats-0.1 (crate (name "macstats") (vers "0.1.1") (deps (list (crate-dep (name "macsmc") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "14yl60znfpclmlk6n4w427iqj655njic17gdhgxz05kwcrf3gcgq")))

(define-public crate-macstats-0.1 (crate (name "macstats") (vers "0.1.2") (deps (list (crate-dep (name "macsmc") (req "^0.1") (default-features #t) (kind 0)))) (hash "1wawq9hh6xb64r18q2jph0x3axs2hyy9z67mi46lr76rdz29x1ky")))

(define-public crate-macstats-0.1 (crate (name "macstats") (vers "0.1.3") (deps (list (crate-dep (name "macsmc") (req "^0.1") (default-features #t) (kind 0)))) (hash "0id2dqlvyjcc5cb5sqhws93pgzvwg8hly9hr8lrr3wz8yjj8ggxd")))

