(define-module (crates-io ma ai) #:use-module (crates-io))

(define-public crate-maai-core-0.0.2 (crate (name "maai-core") (vers "0.0.2") (hash "1i8x5ndrrjgynkahwanj2sdr3qppzrs7w15lzkixdf44hp7b3ykg") (yanked #t)))

(define-public crate-maai-core-0.0.3 (crate (name "maai-core") (vers "0.0.3") (hash "186y0jlis090vs4yzb551iidbv0vjpvxqxd3ca4652hdjx6hdrls") (yanked #t)))

(define-public crate-maai-core-0.0.31 (crate (name "maai-core") (vers "0.0.31") (hash "1fci1jmnb5s07acdi6hj5yxmnh25lijsvisyc098jhysx6kw6w1h") (yanked #t)))

