(define-module (crates-io ma ye) #:use-module (crates-io))

(define-public crate-mayer-0.1 (crate (name "mayer") (vers "0.1.0") (hash "1d5njiz1ahlxjkscpbxyd75n5h6pg53y7yz3z3l28jh9abd3xb9b")))

(define-public crate-mayer_multiple-0.0.1 (crate (name "mayer_multiple") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "~0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.39") (default-features #t) (kind 0)))) (hash "0sdvwva6pqaj2in6llsfapxspsvmv6aamvvqyx5xr2pbkavz5kim")))

