(define-module (crates-io ma ts) #:use-module (crates-io))

(define-public crate-matschik-hello-world-0.1 (crate (name "matschik-hello-world") (vers "0.1.0") (hash "09cxkfwqaazh8ld7l6agq5x4q117d4nn4xhzksfzyy9jyr7hyx73")))

(define-public crate-matschik-hello-world-0.2 (crate (name "matschik-hello-world") (vers "0.2.0") (hash "0vx0syjp9ad8yix75lq344rdrf62xc4nwcp29jxk1n9gjdxw0f5s")))

(define-public crate-matschik-hello-world-0.3 (crate (name "matschik-hello-world") (vers "0.3.0") (hash "03v11p67bv7gb969isr83ayh8dsm58rg2x8x7sd1spyvwdb9w8az")))

(define-public crate-matszpk-sokoban-0.1 (crate (name "matszpk-sokoban") (vers "0.1.0") (deps (list (crate-dep (name "int-enum") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)))) (hash "0jns5chiw076x5m0sww9m2i58a5mwa8cbb7cjm4fb8jwzp6208jm")))

(define-public crate-matszpk-sokoban-0.1 (crate (name "matszpk-sokoban") (vers "0.1.1") (deps (list (crate-dep (name "int-enum") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)))) (hash "1bqkfz9dr3mima3zgh3y3h79dplsx1wj84ggy9myff7d74zqdqf7")))

