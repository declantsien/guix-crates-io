(define-module (crates-io ma _p) #:use-module (crates-io))

(define-public crate-ma_proper-0.1 (crate (name "ma_proper") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.28") (default-features #t) (kind 1)))) (hash "07bkk2fcmlc2f567zpl9m029ixmaqhnqx561b2l6sg68izb7jl8s") (features (quote (("volatile_fallback") ("trace") ("default"))))))

(define-public crate-ma_proper-0.1 (crate (name "ma_proper") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0.28") (default-features #t) (kind 1)))) (hash "0s05wl9jgc7gahabb2cpn3xhd28na1lq2v6njl9q9g1jwbsakl8g") (features (quote (("volatile_fallback") ("trace") ("default"))))))

(define-public crate-ma_proper-0.1 (crate (name "ma_proper") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0.30") (default-features #t) (kind 1)))) (hash "0zh0qr5jh5hvmg0byzki67yc2za15as42l4xyprssnim68qqfb6h") (features (quote (("volatile_fallback") ("trace") ("default"))))))

(define-public crate-ma_proper-0.1 (crate (name "ma_proper") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0.30") (default-features #t) (kind 1)))) (hash "1k3wja3srpqzxn9f7sgw3sz1jay84g5bcwji41gv72yfd526a23s") (features (quote (("volatile_fallback") ("trace") ("default"))))))

(define-public crate-ma_proper-0.1 (crate (name "ma_proper") (vers "0.1.4") (deps (list (crate-dep (name "cc") (req "^1.0.30") (default-features #t) (kind 1)))) (hash "1hp4dvrmg1xpafdac7127ah0rn84vb8hdc7j3pqjbqwdy0jxyipr") (features (quote (("volatile_fallback") ("trace") ("default"))))))

(define-public crate-ma_proper-1 (crate (name "ma_proper") (vers "1.0.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "17pp87lqvbna1506jvbi21ckr451j7pw5n3zmsskl92p63vfv36f") (features (quote (("volatile_fallback") ("trace") ("default"))))))

