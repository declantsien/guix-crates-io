(define-module (crates-io ma x7) #:use-module (crates-io))

(define-public crate-max7219-0.1 (crate (name "max7219") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "04xcbdgvijvnrazhn1q68f6v1xdxbgkxvialmlmjhkp9vdzzrym8")))

(define-public crate-max7219-0.1 (crate (name "max7219") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1636bm9snzynxxa9nxmdcjx75k1p1c22fwd48n8r860jxsciz1f1")))

(define-public crate-max7219-0.1 (crate (name "max7219") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1kmia4qdp6rki07i9rlxyc7zymi1r8j5zxa2vq8z5xa0dy4fh8nx")))

(define-public crate-max7219-0.2 (crate (name "max7219") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "0b54pk9x742i4lsj2m2w6g381s79rqd54bqvjcfmvwgdb6m0hs00")))

(define-public crate-max7219-0.2 (crate (name "max7219") (vers "0.2.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "0516058pwfipjsksrnmwlfn971ash1gqsnd656w5s2d07a6mcss2")))

(define-public crate-max7219-0.2 (crate (name "max7219") (vers "0.2.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1d5sci3mnm7sc1nyz7s4cwflfyfksmywzyw3vr41w2iq9mprkcli")))

(define-public crate-max7219-0.3 (crate (name "max7219") (vers "0.3.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "0r333flb3sbzng4pcmpfhvqzg2qp4ckkhwajljxvzckcc83pc23n")))

(define-public crate-max7219-0.3 (crate (name "max7219") (vers "0.3.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "04mi95qg3wq3fqky6ja7lbj3yx9adaq41x69ivw03m98pm6l05vw")))

(define-public crate-max7219-0.4 (crate (name "max7219") (vers "0.4.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0a9lkpbgd3jigbkaaxnq0w0hkc6yssrvfnx2dz25p63pj7asprmm")))

(define-public crate-max7219-0.4 (crate (name "max7219") (vers "0.4.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "070d80ic88qxjml4n7av0kkg933wlpmjh46gqfbf3rcn9gqkdbcn")))

(define-public crate-max7219-async-0.1 (crate (name "max7219-async") (vers "0.1.0") (deps (list (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-async") (req "^1.0") (default-features #t) (kind 0)))) (hash "1vyz8pjb0zklmq5g3bg9l8fglqhgv2cm6f9ljci9lm94vpbyc729") (v 2) (features2 (quote (("defmt-03" "embedded-hal-async/defmt-03" "dep:defmt"))))))

(define-public crate-max7219-async-0.1 (crate (name "max7219-async") (vers "0.1.1") (deps (list (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-async") (req "^1.0") (default-features #t) (kind 0)))) (hash "1slq37cq9zik33lpjjzw2c8db92wiyx5vrzkcm8srlk51p1zfbq9") (v 2) (features2 (quote (("defmt-03" "embedded-hal-async/defmt-03" "dep:defmt"))))))

(define-public crate-max7219-canvas-0.1 (crate (name "max7219-canvas") (vers "0.1.0") (deps (list (crate-dep (name "max7219") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "04x08gq085dy3dvffs72l603pc5yyrr0nrskh9y1hpcmma5gkq4f")))

(define-public crate-max7219-canvas-0.1 (crate (name "max7219-canvas") (vers "0.1.1") (deps (list (crate-dep (name "max7219") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0pg32c9rr4vx79h45dc3w4lzlp4sbii3xlx0wkz1bx0gicxqpgkc")))

(define-public crate-max7219-canvas-0.1 (crate (name "max7219-canvas") (vers "0.1.2") (deps (list (crate-dep (name "max7219") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0n7zfrw7ya2j5pcwmpfhwll55434pl5maymrg6gn3pcg6zsw8nlv")))

(define-public crate-max7219-driver-0.1 (crate (name "max7219-driver") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "16pqmjpgavcj8ak6bcxzmpvnndc0hx69nsqf6ijd5kjl7b9xfai7") (yanked #t)))

(define-public crate-max7219-driver-0.1 (crate (name "max7219-driver") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "02z7x4gakiqzjvh14zafws5vyxjh5rahamxv8a18h9l25hf9mfkh")))

(define-public crate-max7219-driver-0.2 (crate (name "max7219-driver") (vers "0.2.0") (deps (list (crate-dep (name "embedded-graphics") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.7") (default-features #t) (kind 0)))) (hash "05w7c9v2wcyz7qvmbm50fm9gckqnanblm52zilrb2gy96y2ykfyw") (yanked #t)))

(define-public crate-max7219-driver-0.2 (crate (name "max7219-driver") (vers "0.2.1") (deps (list (crate-dep (name "embedded-graphics") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0w0s5m448x9l93hd3qabqgnfbwd9zvhzcvvxcbqq5nzbl3wbs1w2")))

(define-public crate-max7219-driver-0.2 (crate (name "max7219-driver") (vers "0.2.2") (deps (list (crate-dep (name "embedded-graphics") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.7") (kind 0)))) (hash "13j44ylcqw5i456kr2amgilmcaj67rqbg6ahi5m52df9ykxmsbrh")))

(define-public crate-max7301-0.1 (crate (name "max7301") (vers "0.1.0") (deps (list (crate-dep (name "cortex-m") (req "^0.5.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.9") (default-features #t) (kind 2)))) (hash "06v8wksvbvqmz56gglaa6pw8w5x4hrcgavsh9qplsbaij30yrj5g") (features (quote (("unproven" "embedded-hal/unproven") ("std") ("default" "std" "unproven") ("cortexm" "cortex-m"))))))

(define-public crate-max7301-0.2 (crate (name "max7301") (vers "0.2.0") (deps (list (crate-dep (name "cortex-m") (req "^0.5.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.9") (default-features #t) (kind 2)))) (hash "1hwx5k2zlwn95927pnrsvrjkk9g580ymwzra2gh9yvb0cnjdj7i9") (features (quote (("unproven" "embedded-hal/unproven") ("std") ("default" "std" "unproven") ("cortexm" "cortex-m"))))))

(define-public crate-max7301-0.3 (crate (name "max7301") (vers "0.3.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.9") (default-features #t) (kind 2)))) (hash "0ixqzn9yr6bd17jb672n8r5fhdbmxhkha9pk31x25irqx9rd77h8") (features (quote (("unproven" "embedded-hal/unproven") ("std") ("default" "std" "unproven") ("cortexm" "cortex-m"))))))

(define-public crate-max7456-0.1 (crate (name "max7456") (vers "0.1.0") (deps (list (crate-dep (name "peripheral-register") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0lqfs8z6582wji64m441pl4ximm8xjpjlyjh7k2j6wmchyb42fdj")))

(define-public crate-max7456-0.1 (crate (name "max7456") (vers "0.1.1") (deps (list (crate-dep (name "peripheral-register") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "162qdm1ad5pgz98ki1mz327gf10z4v4110vj4x9aq5j8zchfp5nz")))

(define-public crate-max7456-0.1 (crate (name "max7456") (vers "0.1.2") (deps (list (crate-dep (name "peripheral-register") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0wf0vgvpnhhsxc85jp2hsl5121ip3rq7paldzjnizhzjmv4fcl46")))

(define-public crate-max7456-0.1 (crate (name "max7456") (vers "0.1.3") (deps (list (crate-dep (name "peripheral-register") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1jyr2dfd546np4w23j5g5nj05308gcwb70krb4blgqy8nmk4nsk4")))

(define-public crate-max7456-0.1 (crate (name "max7456") (vers "0.1.4") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "peripheral-register") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1mhkm15lw7ff7gx0y9jbcs6b3cmjldp88lckln5bvmgm7smzm24f")))

(define-public crate-max7456-0.1 (crate (name "max7456") (vers "0.1.5") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "peripheral-register") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "07s6f1cjh43wb1r0k91d1h84i6qagma7q5iph8n9kx0ng06rif55")))

(define-public crate-max7456-0.1 (crate (name "max7456") (vers "0.1.6") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "peripheral-register") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0af5099gmk5kx3742k3bxl82kn4sndn7ql1c3nrnmvl0fn08ls65")))

(define-public crate-max7456-0.1 (crate (name "max7456") (vers "0.1.7") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "peripheral-register") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0ir1fjmnx1qd0r3v3b60zvr7ma6wmmr7zvifq61c77nwyc86axkl")))

(define-public crate-max7456-0.1 (crate (name "max7456") (vers "0.1.8") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "peripheral-register") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1yiyxdm3v95a8274j6sag363jxx28liv39kvamwazfjr3gl74r29")))

(define-public crate-max7456-0.1 (crate (name "max7456") (vers "0.1.9") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "peripheral-register") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0a8mqyiss2j87p731hwy3vwg70jn9ssp7r212g2bb9l15vwi3z1k")))

(define-public crate-max7456-0.1 (crate (name "max7456") (vers "0.1.10") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "peripheral-register") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1w4jh3066bdjl6fnj8nv20sshd3ypdys4mhzq087k90vc1irx04c")))

(define-public crate-max7797x-driver-0.1 (crate (name "max7797x-driver") (vers "0.1.0") (deps (list (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-async") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "modular-bitfield") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "1sia6y0zf5skfkvc0mi9srkshlfajn6jxvam71yg6vg85shvhx7a") (features (quote (("default")))) (v 2) (features2 (quote (("defmt-03" "embedded-hal-async/defmt-03" "dep:defmt"))))))

(define-public crate-max7797x-driver-0.1 (crate (name "max7797x-driver") (vers "0.1.1") (deps (list (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-async") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "modular-bitfield") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "1ilf70cnpqqr6pw6j07hfh9982rkczljkf8zjzi4ixdylsrp4438") (features (quote (("default")))) (v 2) (features2 (quote (("defmt-03" "embedded-hal-async/defmt-03" "dep:defmt"))))))

(define-public crate-max78000-0.1 (crate (name "max78000") (vers "0.1.0") (hash "1m8x9zdahnpphy5sxy5bxzcsc5myj0ary597hsg21xpar08zz113")))

(define-public crate-max78000-hal-0.1 (crate (name "max78000-hal") (vers "0.1.0") (hash "05wczm6im4hynxnwy5rry9sa872bdcyijp1lw1gbgljw92wv46y9")))

