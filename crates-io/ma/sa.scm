(define-module (crates-io ma sa) #:use-module (crates-io))

(define-public crate-masala-0.1 (crate (name "masala") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "10ykb8qhaqnbdxxmdk5hx10wkzzk4pv15ai8sf55gnlmn48zn0fv")))

(define-public crate-masala-0.2 (crate (name "masala") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1mnjqipjrvm6cqs1s8909g7piwh4mngmhhkw6b61lcgic22wxh5i")))

(define-public crate-masala-0.2 (crate (name "masala") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1kxayy5c603lskjk7x5wf6r8k1v559vw3lx38mnj27yamharx5j1")))

