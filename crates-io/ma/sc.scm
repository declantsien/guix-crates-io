(define-module (crates-io ma sc) #:use-module (crates-io))

(define-public crate-mascheroni-0.1 (crate (name "mascheroni") (vers "0.1.0") (hash "1jc4dvn4qx2sxcgmdfxpvslpk5v53s1jpypspa0xmsr797n4cgml")))

(define-public crate-mascheroni-0.1 (crate (name "mascheroni") (vers "0.1.1") (hash "06higcj3vbkihj7vk5adlazdlyn6f0yc5fr30rl2h73wcaq1lggh")))

(define-public crate-mascheroni-0.1 (crate (name "mascheroni") (vers "0.1.2") (hash "1gk678nrnhzrdcgpkj9bkqw4ldf9mj7dl0p6451k9x7bgq01bb86")))

(define-public crate-mascheroni-0.2 (crate (name "mascheroni") (vers "0.2.0") (hash "063g8s40xy7lliw42snvw111v388pwmyyn0rgkziambfzpd9bg5m")))

(define-public crate-maschine-0.1 (crate (name "maschine") (vers "0.1.0") (deps (list (crate-dep (name "hidapi") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "147fzlakwrhq9rsajcx9vswc2w0scgrzj6zbh6j8xxmw6nh0p4hx")))

(define-public crate-mascot-rs-0.1 (crate (name "mascot-rs") (vers "0.1.0") (hash "0j864ckpwn5zi3w9vkggvpl96gamlkgghvm4dhpyl32gdwx0y24k")))

