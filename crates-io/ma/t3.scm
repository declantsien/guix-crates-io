(define-module (crates-io ma t3) #:use-module (crates-io))

(define-public crate-mat3-0.1 (crate (name "mat3") (vers "0.1.0") (deps (list (crate-dep (name "number_traits") (req "^0.1") (default-features #t) (kind 0)))) (hash "008ipn66l8kn2pdl1x7icz9l5f5l2fx3a8npdqlbazcrpp3pcp50")))

(define-public crate-mat3-0.1 (crate (name "mat3") (vers "0.1.1") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "07sr8gahix1rs62367fhx47g5hdyrchgqislsibzcys0d8v5lvcf")))

(define-public crate-mat3-0.1 (crate (name "mat3") (vers "0.1.2") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0dj5qljmparh6inzra4imygv8qwxkky3hbhv69mnfky9waya3n4s")))

(define-public crate-mat3-0.1 (crate (name "mat3") (vers "0.1.3") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0jpsbjgy59qif70rg2ppxqryfy2pf7m5s5dzi3whlfl5ykc0wlld")))

(define-public crate-mat3-0.1 (crate (name "mat3") (vers "0.1.4") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "09m1qxm0v1k1arc2gv0j8741kl1n3rd8rv6axm6dlhmbvbqr55qc")))

(define-public crate-mat3-0.2 (crate (name "mat3") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1k2xkn4k97qpf5k3pxxqqsk2r41rhg2k3c50djj9l8gd1qjfrpr1")))

(define-public crate-mat3-0.2 (crate (name "mat3") (vers "0.2.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0m2gzj3zgm0fpplbrg1v006dg1das0ryc2grn2fw5jhx5f0n04l1")))

(define-public crate-mat32-0.1 (crate (name "mat32") (vers "0.1.0") (deps (list (crate-dep (name "number_traits") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "vec2") (req "^0.1") (default-features #t) (kind 0)))) (hash "0rx3apgkz444pw74a2brp37wlcdgsbpna2r7wvpcgbqhqir9cync")))

(define-public crate-mat32-0.1 (crate (name "mat32") (vers "0.1.1") (deps (list (crate-dep (name "number_traits") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "vec2") (req "^0.1") (default-features #t) (kind 0)))) (hash "1c94xwn088x74ir0ha4jn5wmlm1fi9zik957ra133jn17mfddvc9")))

(define-public crate-mat32-0.1 (crate (name "mat32") (vers "0.1.2") (deps (list (crate-dep (name "number_traits") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "vec2") (req "^0.1") (default-features #t) (kind 0)))) (hash "0yh703qdiradvb78ff1jy3sfa0hdnkif5irds364r61pip3whpva")))

(define-public crate-mat32-0.1 (crate (name "mat32") (vers "0.1.3") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec2") (req "^0.1") (default-features #t) (kind 0)))) (hash "0p4yfm3v0s94bx4aby5hynmz4nkj4micyy5h86vbdnpkwi6ncq3h")))

(define-public crate-mat32-0.1 (crate (name "mat32") (vers "0.1.4") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec2") (req "^0.1") (default-features #t) (kind 0)))) (hash "0m2waaiyv0dkq0qkajcp7nyka4bi6m38h6qynjywlh3i90qb3mn9")))

(define-public crate-mat32-0.1 (crate (name "mat32") (vers "0.1.5") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec2") (req "^0.1") (default-features #t) (kind 0)))) (hash "0xzzs4wlnff2nakzc2axzjb401lifsyyj1iyy2v57phx3r7na8kk")))

(define-public crate-mat32-0.2 (crate (name "mat32") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec2") (req "^0.2") (default-features #t) (kind 0)))) (hash "0w4j45qwcq7k0gmbn9rhvfx9y3ia75zk3jrs262biz23ymcdvrhv")))

(define-public crate-mat32-0.2 (crate (name "mat32") (vers "0.2.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec2") (req "^0.2") (default-features #t) (kind 0)))) (hash "1mgzasa42ahqm5g0kjq24bggx9ffwm5hc3ld5d9mk4pxngafrkri")))

