(define-module (crates-io ma le) #:use-module (crates-io))

(define-public crate-male-11 (crate (name "male") (vers "11.2.0") (hash "0hnf5ra3xbn27akclh2cmq3lvxkzjwq1v6l23w6by789v96x2x0g")))

(define-public crate-malevolence-0.1 (crate (name "malevolence") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.18") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.171") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "13dazlx23bqj7w0jns2mbq9kx2vijwli4k1xj0s4qwv3d70dwj6n")))

