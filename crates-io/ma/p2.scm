(define-module (crates-io ma p2) #:use-module (crates-io))

(define-public crate-map2struct-0.1 (crate (name "map2struct") (vers "0.1.0") (deps (list (crate-dep (name "map2struct-derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "1bx3mkgx5mrchfqxafhxvv63a43kvm8m5faivcwz0z176q9bn9c3")))

(define-public crate-map2struct-derive-0.1 (crate (name "map2struct-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1vrvzs72aw1m76h9pw87k8zsy0ffq0lphafi3gpxjdisr4shhch7")))

