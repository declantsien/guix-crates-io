(define-module (crates-io ma ix) #:use-module (crates-io))

(define-public crate-maixnor-super-amazing-component-lib-0.1 (crate (name "maixnor-super-amazing-component-lib") (vers "0.1.0") (deps (list (crate-dep (name "gloo-net") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "yew") (req "^0.20") (features (quote ("csr"))) (default-features #t) (kind 0)))) (hash "1765ralvr812yspadnvb2zbdj0lv05vy5qgbsz3010nwqf2g52sq")))

