(define-module (crates-io ma #{73}#) #:use-module (crates-io))

(define-public crate-ma734-0.0.1 (crate (name "ma734") (vers "0.0.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "19rddj1a4hpfds53d470qkmqraxmhhnadnmdl59afyf4jwdh91ic")))

(define-public crate-ma734-0.0.2 (crate (name "ma734") (vers "0.0.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1izq3xfj5j2fginfga4wbdp65h5lw18pd27mi1xby8p07ph5r124")))

(define-public crate-ma734-0.1 (crate (name "ma734") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1yh6yxlz2qq6gncn5bp6pi5haksf3qrnnb13vy1clwnbn855h6sj")))

(define-public crate-ma734-0.1 (crate (name "ma734") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1zinafy4hbgz0mjc26i8q7c1hb5kfbblnnwda6s8cdm67s71y839")))

