(define-module (crates-io ma t4) #:use-module (crates-io))

(define-public crate-mat4-0.1 (crate (name "mat4") (vers "0.1.0") (deps (list (crate-dep (name "number_traits") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "vec3") (req "^0.1") (default-features #t) (kind 0)))) (hash "0lfl4szba571hjrbb98sd62kfj67xz4j913izwclclrsh4zgcffv")))

(define-public crate-mat4-0.1 (crate (name "mat4") (vers "0.1.1") (deps (list (crate-dep (name "number_traits") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "vec3") (req "^0.1") (default-features #t) (kind 0)))) (hash "0kapwr4yz9sg49lnqdh72ps3khpn3vyyy9vv4xz9c6qnm6zncaih")))

(define-public crate-mat4-0.1 (crate (name "mat4") (vers "0.1.2") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec3") (req "^0.1") (default-features #t) (kind 0)))) (hash "1plk9nwhqn9b2v63yin343al8dyndixv64785jk7i6cpn3lshdwr")))

(define-public crate-mat4-0.1 (crate (name "mat4") (vers "0.1.3") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec3") (req "^0.1") (default-features #t) (kind 0)))) (hash "1is0pfhd7gnh0mf8in7kfvwc3qkx4l6cqfb12sgma52q9wiczmcm")))

(define-public crate-mat4-0.1 (crate (name "mat4") (vers "0.1.4") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec3") (req "^0.1") (default-features #t) (kind 0)))) (hash "06q06ns2fvvcqsa9n42br43q3rlcijv6jzrip458j3np9f4pn6h5")))

(define-public crate-mat4-0.2 (crate (name "mat4") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec3") (req "^0.2") (default-features #t) (kind 0)))) (hash "0z1285kmy07s1fq6lhv6c1d60kzjbfrshcw9wn2an0lwgjz7sp3l")))

(define-public crate-mat4-0.2 (crate (name "mat4") (vers "0.2.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec3") (req "^0.2") (default-features #t) (kind 0)))) (hash "1b43djm9p6g0l40jbdz9faha62w5qmsybj5dik5m9y6c844w13lp")))

