(define-module (crates-io ma lh) #:use-module (crates-io))

(define-public crate-malhada-0.1 (crate (name "malhada") (vers "0.1.0") (hash "15bnr4132bjv2y4nz3j63d4hg2f9nnczcir2azkq2w1b2yx9afqv") (yanked #t)))

(define-public crate-malhada-0.1 (crate (name "malhada") (vers "0.1.2") (deps (list (crate-dep (name "hangul") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0d2qmqjbph2v0p6l2di7c8nbg4kz51rfd1nr62bsp9j745p5bisx") (yanked #t)))

