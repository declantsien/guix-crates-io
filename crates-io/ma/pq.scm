(define-module (crates-io ma pq) #:use-module (crates-io))

(define-public crate-mapquest-0.1 (crate (name "mapquest") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "17vzq3lsmy6kwj2nbwlz5nkfxvdm8dig9lrw9sdiwhdnvampxks3")))

