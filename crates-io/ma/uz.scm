(define-module (crates-io ma uz) #:use-module (crates-io))

(define-public crate-mauzi-0.0.1 (crate (name "mauzi") (vers "0.0.1") (deps (list (crate-dep (name "mauzi_macros") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1sdikla6la3z4ys54s9qr6vkgmvgwsv2dsdwvg5zdi6z84si0lqk")))

(define-public crate-mauzi-0.0.2 (crate (name "mauzi") (vers "0.0.2") (deps (list (crate-dep (name "mauzi_macros") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1aqws6wkb4v7cd84psn08sh1lzm8zr3ny8jdnid0d8ig6msn3168")))

(define-public crate-mauzi_macros-0.0.1 (crate (name "mauzi_macros") (vers "0.0.1") (deps (list (crate-dep (name "literalext") (req "^0.1") (features (quote ("proc-macro"))) (kind 0)))) (hash "0qs4jq6n37wxsbr5p2a30xhjhlnp63xgvvnrxxs3ndfx3m5qaml2")))

(define-public crate-mauzi_macros-0.0.2 (crate (name "mauzi_macros") (vers "0.0.2") (deps (list (crate-dep (name "literalext") (req "^0.1") (features (quote ("proc-macro"))) (kind 0)))) (hash "0147daxnya9yjhy1n3hm7wxx77qazl6fa6dkjg80ilkrc790adjp")))

