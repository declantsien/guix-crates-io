(define-module (crates-io ma l2) #:use-module (crates-io))

(define-public crate-mal2eng-0.1 (crate (name "mal2eng") (vers "0.1.0") (deps (list (crate-dep (name "fancy-regex") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)))) (hash "0212amsw606whdhzprsj1b1h4g1dlvgn16icambjcn6kh4kaaqg2")))

(define-public crate-mal2eng-0.1 (crate (name "mal2eng") (vers "0.1.1") (deps (list (crate-dep (name "fancy-regex") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)))) (hash "0cgiap3mvd8mpwn9zqamzq0npgaqxh1cn7n177sai8l5b9gr86iz")))

