(define-module (crates-io ma ir) #:use-module (crates-io))

(define-public crate-mairs-0.0.0 (crate (name "mairs") (vers "0.0.0") (hash "07z9g3zfbawz7bmqp70c80zcx3z5ydjrxdxj5lqy50akmj3x5x8k")))

(define-public crate-mairs-0.1 (crate (name "mairs") (vers "0.1.0") (hash "0474j11kqzs13vgbw8qq52x1ggdz4fpnsnbda3rx1lsvjka7akcq")))

(define-public crate-mairs-0.1 (crate (name "mairs") (vers "0.1.1") (hash "021bi9zqq5c197w48r302km02j6k590qmmm52crswi5lwjqi8qnv")))

(define-public crate-mairs-0.1 (crate (name "mairs") (vers "0.1.2") (hash "1bb85kmcx9yydjvk774zi5sphpdgh9n0g2mfgqrgm9j0qiyiv9gv")))

(define-public crate-mairs-0.1 (crate (name "mairs") (vers "0.1.3") (hash "03n4v015bqqmmkckhyiw3xzvs7y88r1aczz36zqi2iak5l9h26x5")))

(define-public crate-mairs-0.1 (crate (name "mairs") (vers "0.1.5") (hash "0fx2kyd4y8v224gxaxflgswrx9h6s28j8q3ljgvi5w6yc902dvrq")))

(define-public crate-mairs-0.1 (crate (name "mairs") (vers "0.1.6") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "0dy6qch9zg980bdilj0wxlr6zf4fpyi3l5c7rkag41faxp1yyl49")))

