(define-module (crates-io ma nb) #:use-module (crates-io))

(define-public crate-manber-fingerprinting-0.1 (crate (name "manber-fingerprinting") (vers "0.1.0") (hash "1p78h2vq08h80sn1gmbdqq5480kxfk1mv735ac1sv51zn2rd7irj")))

(define-public crate-manber-fingerprinting-0.2 (crate (name "manber-fingerprinting") (vers "0.2.0") (hash "1rz7cv5zmw3kd33wvgxnn3bzhw3y58v105bm41yhpm37y695zbbg")))

(define-public crate-manber-fingerprinting-0.3 (crate (name "manber-fingerprinting") (vers "0.3.0") (hash "086slmn2lifxkkax33ki9d73yd0900qwjx7z8a5a4291sxa1yvil")))

(define-public crate-manber-fingerprinting-0.4 (crate (name "manber-fingerprinting") (vers "0.4.0") (hash "1dyynf678mh6qnpvzdlx96yxddz92vy9kdv5wca8gzvf3i2rq3yl")))

(define-public crate-manber-fingerprinting-0.5 (crate (name "manber-fingerprinting") (vers "0.5.0") (hash "0bmn6qdgvzrg2j3qs4rbm2rwyblajnjrky2b7xn05jqg8g74h2ij")))

(define-public crate-manber-fingerprinting-0.6 (crate (name "manber-fingerprinting") (vers "0.6.0") (hash "1sd23i1v1j97rhlhi7zqw1fyzlbhxvpvkafkb1669k6snf84xvj8")))

