(define-module (crates-io ma x5) #:use-module (crates-io))

(define-public crate-max553x-0.1 (crate (name "max553x") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "1p8pxj2mjnvizxpd6z820dpbgba33a06kxx49fk9y5xpxsz6hifj")))

(define-public crate-max553x-0.1 (crate (name "max553x") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "0zp3xrqw88qs2h5dlvrj497hyxg3h4yx3m96xghkvjssrpz616v0")))

(define-public crate-max553x-0.2 (crate (name "max553x") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8") (default-features #t) (kind 2)))) (hash "0fd5xqvwa0zc2kvdpknlrcks60pm7svg2wjxxvaiv7jmnh1dpy8s")))

(define-public crate-max553x-0.3 (crate (name "max553x") (vers "0.3.0") (deps (list (crate-dep (name "embedded-hal") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.10") (default-features #t) (kind 2)))) (hash "17mmrfmafnmfdlgwvwsp93d9jyj4nl54cnpwwrybg6r5ly7fiiji")))

