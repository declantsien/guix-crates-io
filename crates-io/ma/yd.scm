(define-module (crates-io ma yd) #:use-module (crates-io))

(define-public crate-mayda-0.1 (crate (name "mayda") (vers "0.1.0") (deps (list (crate-dep (name "mayda_codec") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0p3sn0rqcyjcfaawik4wbcpry2yjipxqddi2f2ygg3mhdr76miwf")))

(define-public crate-mayda-0.1 (crate (name "mayda") (vers "0.1.1") (deps (list (crate-dep (name "mayda_codec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0l269ynbbbdv86klfccp9lx0ym50z3sw1n04py1b6ygsgink49yx")))

(define-public crate-mayda-0.2 (crate (name "mayda") (vers "0.2.0") (deps (list (crate-dep (name "mayda_codec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0r67vgd6jx27gx9zdkjp5hgbhdq874540c3a6ihs3rx393dkqbyp")))

(define-public crate-mayda-0.2 (crate (name "mayda") (vers "0.2.1") (deps (list (crate-dep (name "mayda_codec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "092f35nasgqxybq7cmqxsg9mddcm7lay3r0bqz3sx7lg6cna41cd")))

(define-public crate-mayda-0.2 (crate (name "mayda") (vers "0.2.2") (deps (list (crate-dep (name "mayda_codec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1x4dgj003p3qnrc2ckdmllnav6yhadigzp83cksmr0q6wdw728di")))

(define-public crate-mayda-0.2 (crate (name "mayda") (vers "0.2.3") (deps (list (crate-dep (name "mayda_codec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1x1mxqh4p5nb07ljarmv207mnbbl1zanhvfam9a104rsb6zgrhb8")))

(define-public crate-mayda-0.2 (crate (name "mayda") (vers "0.2.4") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "mayda_codec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "1xq9pwpj609b9zinp91fg0fri8sn3nq8hdq5x7ds3rsasvmgxdcp")))

(define-public crate-mayda-0.2 (crate (name "mayda") (vers "0.2.5") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "mayda_codec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "1dc6g7ygigal8s73rii3c2xvxa7jxaaipnrhxq8lh77qh73lhvql") (features (quote (("bench"))))))

(define-public crate-mayda_codec-0.1 (crate (name "mayda_codec") (vers "0.1.0") (deps (list (crate-dep (name "mayda_macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "simd") (req "^0.1") (default-features #t) (kind 0)))) (hash "14ppajfdkg363db1g197api6f47xggga4r0dclg202xz7dim9r55")))

(define-public crate-mayda_codec-0.1 (crate (name "mayda_codec") (vers "0.1.1") (deps (list (crate-dep (name "mayda_macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "simd") (req "^0.1") (default-features #t) (kind 0)))) (hash "10fq4nc842hzl3mwmipg5yn2z3m75pgjsbs3ppy3ls5nsrs3913m")))

(define-public crate-mayda_codec-0.1 (crate (name "mayda_codec") (vers "0.1.2") (deps (list (crate-dep (name "mayda_macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "simd") (req "^0.1") (default-features #t) (kind 0)))) (hash "14pa1814w27lqi51xkbqfv2k62jqsg2qw6prfsdk5n71plpnvfrk")))

(define-public crate-mayda_codec-0.1 (crate (name "mayda_codec") (vers "0.1.3") (deps (list (crate-dep (name "mayda_macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "simd") (req "^0.1") (default-features #t) (kind 0)))) (hash "0x4h2vd1ng484yh2ddix9il9ajijhcl020pirrb8kyyf4vsvni9h")))

(define-public crate-mayda_codec-0.1 (crate (name "mayda_codec") (vers "0.1.4") (deps (list (crate-dep (name "mayda_macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "simd") (req "^0.2") (default-features #t) (kind 0)))) (hash "1558ckfap2pqzjkxxgdi2vkakgai7pqk6sd3rvbbjc20r6ykh0n8")))

(define-public crate-mayda_macros-0.1 (crate (name "mayda_macros") (vers "0.1.0") (hash "0dd2gbgzaqnn5k9aghyjwy77hmwla4kq9qzans88x3s9j5zpgwz3")))

(define-public crate-mayda_macros-0.1 (crate (name "mayda_macros") (vers "0.1.1") (hash "1d6p1n0nmxba0ar2z2sc63c6bp4kw9lwqn9nxxbcfawv7ggncgzy")))

(define-public crate-mayda_macros-0.1 (crate (name "mayda_macros") (vers "0.1.2") (hash "1dy9rr6096p8vv0gwgl86pmw6nmp5igfhwfrzdlnpdzz63nvda0k")))

(define-public crate-mayda_macros-0.1 (crate (name "mayda_macros") (vers "0.1.3") (hash "18azi98l4cgqinsrgw24zfd5w0cirlyz7f80k7w9d1wwy1i095s9")))

(define-public crate-mayda_macros-0.1 (crate (name "mayda_macros") (vers "0.1.4") (hash "1mghv2jynvm56nf90r4wra94vjwrzh9nf47a2r9nna4ri06s553h")))

