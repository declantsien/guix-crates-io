(define-module (crates-io ma sq) #:use-module (crates-io))

(define-public crate-masque-0.0.0 (crate (name "masque") (vers "0.0.0") (hash "1dvnwrq57cxpnd5kn28p39hxxbff9b5qd86ihrx1x63nlydhmbs1")))

(define-public crate-masquerade-0.1 (crate (name "masquerade") (vers "0.1.0") (hash "0flh66yaqvk1cdlhy3gar08hx6r66ilnryhfhwv636f05al96jd8")))

(define-public crate-masquerade-proxy-0.1 (crate (name "masquerade-proxy") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "httparse") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "quiche") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "socks5-proto") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "test-log") (req "^0.2") (default-features #t) (kind 2)))) (hash "15gigpmrm9my8rf5sxciwmq0f9nhfd5bqasvk4vaf7r76yqmvp0g")))

