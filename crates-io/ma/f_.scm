(define-module (crates-io ma f_) #:use-module (crates-io))

(define-public crate-maf_cal-0.1 (crate (name "maf_cal") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1.2.2") (default-features #t) (kind 0)))) (hash "10llhdhq3bqpa3z6lq08bzx1avbx1fj407bjsxkcdqj1j4axjmsg")))

(define-public crate-maf_cal-0.1 (crate (name "maf_cal") (vers "0.1.1") (deps (list (crate-dep (name "csv") (req "^1.2.2") (default-features #t) (kind 0)))) (hash "15h3v3306ga76sylkfql2jrp0bdr88492faww683hlnpb01fap5x")))

(define-public crate-maf_cal-0.1 (crate (name "maf_cal") (vers "0.1.2") (deps (list (crate-dep (name "csv") (req "^1.2.2") (default-features #t) (kind 0)))) (hash "04vlyva0sm7469irm5i2w8h0yi5d16x721cldgvs6530b08kfz8y")))

