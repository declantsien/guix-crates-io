(define-module (crates-io ma g3) #:use-module (crates-io))

(define-public crate-mag3110-0.1 (crate (name "mag3110") (vers "0.1.1") (deps (list (crate-dep (name "cast") (req "^0.2.2") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1hw3r7rmrzdzdj3krpmlfpihh21dhvcnzy659gccrim07ymhk48m")))

(define-public crate-mag3110-0.1 (crate (name "mag3110") (vers "0.1.2") (deps (list (crate-dep (name "cast") (req "^0.2.2") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0dpyz1c8hhbzassnwrr0k3i2knlslzmznj3vpxjw7lhv4dkfkp6z")))

(define-public crate-mag3110-0.1 (crate (name "mag3110") (vers "0.1.3") (deps (list (crate-dep (name "cast") (req "^0.2.2") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0mznz17rnnk6mbd7mrxpc5m3pry2h1hq9jk8hgm8xjggx0s5c806")))

(define-public crate-mag3110-0.1 (crate (name "mag3110") (vers "0.1.4") (deps (list (crate-dep (name "cast") (req "^0.2.2") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "15gdxb16l1b4m0wdkg2p0wicnpc0qy54qf0qhykf961f2gjfnaz9")))

