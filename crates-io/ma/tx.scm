(define-module (crates-io ma tx) #:use-module (crates-io))

(define-public crate-matx-0.1 (crate (name "matx") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "188y7r9r6iqlr5yx1w1d3k1sabpk4g45bw5n0mx4sqabs40lddb8")))

(define-public crate-matx-0.2 (crate (name "matx") (vers "0.2.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "03znf3w6zj6ia3yx4z4hfz49shb6qk5s50frljxlrz3yvka2fci1")))

