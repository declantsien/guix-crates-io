(define-module (crates-io ma be) #:use-module (crates-io))

(define-public crate-mabel-0.0.1 (crate (name "mabel") (vers "0.0.1") (hash "0mq691y51ci181s6sgaisn8dw15nja9p3km9ggg5npf10rf90pv0")))

(define-public crate-mabel-0.1 (crate (name "mabel") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "color-art") (req "^0.3.8") (kind 0)) (crate-dep (name "mabel-eno") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17.13") (default-features #t) (kind 0)))) (hash "0qfxmyq8qjfj6zi40spjrmxsx34y86j47sr854sabx1vxxvw51x9")))

(define-public crate-mabel-0.2 (crate (name "mabel") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "color-art") (req "^0.3.8") (kind 0)) (crate-dep (name "mabel-aseprite") (req "^0.3.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mabel-eno") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17.13") (default-features #t) (kind 0)))) (hash "0b0kzvas7y3rajbbrpdkia3bgyybd8iyjy9rkf7gwbznqkdggmw0") (features (quote (("default" "aseprite")))) (v 2) (features2 (quote (("aseprite" "dep:mabel-aseprite"))))))

(define-public crate-mabel-aseprite-0.3 (crate (name "mabel-aseprite") (vers "0.3.9") (deps (list (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (kind 0)) (crate-dep (name "image") (req "^0.24") (features (quote ("png"))) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nohash") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req ">=0.7, <0.9") (default-features #t) (kind 2)) (crate-dep (name "rect_packer") (req "^0.2") (default-features #t) (kind 2)))) (hash "18n4zv2bbirsy61v3bs2jjxbgzxa83bqjlm48d3c8bjf78m9q9hh") (features (quote (("utils") ("default"))))))

(define-public crate-mabel-eno-0.4 (crate (name "mabel-eno") (vers "0.4.3") (deps (list (crate-dep (name "indoc") (req "^2.0.4") (default-features #t) (kind 2)))) (hash "1a28s4mccwzndxa1cfy1x8mbil68y85gq62lfs77jfvwjig1qbb9")))

(define-public crate-mabel-eno-0.4 (crate (name "mabel-eno") (vers "0.4.4") (deps (list (crate-dep (name "indoc") (req "^2.0.4") (default-features #t) (kind 2)))) (hash "0jgzfvlps0yf4gfmbbkgrwx5fxkykg86nl97dp9fnhhld19l23kv")))

