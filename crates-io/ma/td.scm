(define-module (crates-io ma td) #:use-module (crates-io))

(define-public crate-matdesign-color-0.1 (crate (name "matdesign-color") (vers "0.1.0") (deps (list (crate-dep (name "strum") (req "^0.20") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "04il8mxfqkw2wh2liqrdn57qkj0v2l1y526wvvb6b4sixcfzz7ph")))

(define-public crate-matdesign-color-0.1 (crate (name "matdesign-color") (vers "0.1.1") (deps (list (crate-dep (name "strum") (req "^0.20") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "13jj2cjxx9n1bm091mim56ixv2hv1n50r5hrszb7d2pdx8r1xcla")))

(define-public crate-matdesign-color-0.1 (crate (name "matdesign-color") (vers "0.1.2") (deps (list (crate-dep (name "strum") (req "^0.20") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "1cg360bdqjlya923sh6f1zvwnvws98c4308klwpazs2fj35ifcmp")))

