(define-module (crates-io ma hj) #:use-module (crates-io))

(define-public crate-mahjong-0.1 (crate (name "mahjong") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.1") (default-features #t) (kind 0)))) (hash "1395m1cvg6xd60mbb5v6dzjwwr3lqalx1v3n4y4virbyjjk0695r")))

