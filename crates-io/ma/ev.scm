(define-module (crates-io ma ev) #:use-module (crates-io))

(define-public crate-maeve-0.1 (crate (name "maeve") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "04bvicl8bph46yd1fgmnzkla7q76g4z4mclb3s960wk7zicg9ydk")))

