(define-module (crates-io ma dy) #:use-module (crates-io))

(define-public crate-mady-0.1 (crate (name "mady") (vers "0.1.0-beta") (deps (list (crate-dep (name "mady_macro") (req "^0.1.0-beta") (default-features #t) (kind 0)) (crate-dep (name "mady_math") (req "^0.1.0-beta") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "19a9alyl978avcfmshffllxyl780hi2pqqmmap9959l50nsx3vyc") (features (quote (("math") ("macro") ("default" "macro" "math"))))))

(define-public crate-mady_macro-0.1 (crate (name "mady_macro") (vers "0.1.0-beta") (deps (list (crate-dep (name "mady_macro_core") (req "^0.1.0-alpha") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits" "fold" "parsing"))) (default-features #t) (kind 0)))) (hash "013kxlyrg2dzwqzjd2ndrca2j70fab1h7kw5x8r3pb94mg9nklg1")))

(define-public crate-mady_macro_core-0.1 (crate (name "mady_macro_core") (vers "0.1.0-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits" "fold" "parsing"))) (default-features #t) (kind 0)) (crate-dep (name "tabbycat") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1kr1mnf6qb7kzzsfhldvjszgzz2ysvh6pi6cyis7j77f4akyv60y")))

(define-public crate-mady_math-0.1 (crate (name "mady_math") (vers "0.1.0-beta") (deps (list (crate-dep (name "nalgebra") (req "^0.31") (default-features #t) (kind 0)))) (hash "0arpfyn9w8bhlp1s9s4h8h2qbb8z976jb4zm41wpksyc6wx6bpz5")))

