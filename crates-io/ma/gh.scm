(define-module (crates-io ma gh) #:use-module (crates-io))

(define-public crate-maghemite-0.1 (crate (name "maghemite") (vers "0.1.0") (deps (list (crate-dep (name "iota") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "0zbs7cafk71cvf1sir63z639kvgxc65a5q6lzrbk2dwmjcycjh13")))

