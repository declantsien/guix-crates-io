(define-module (crates-io ma pr) #:use-module (crates-io))

(define-public crate-mapr-0.8 (crate (name "mapr") (vers "0.8.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("basetsd" "handleapi" "memoryapi" "minwindef" "std" "sysinfoapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "001mjzk4zavalz9nybrn7lvxsynm6f4hbi13y7vb41f0vdaqm8j6")))

(define-public crate-mapreduce-0.1 (crate (name "mapreduce") (vers "0.1.0") (hash "1gbcx4jnzyap5hfk1nl7fbah6mh0s1fm263xlm07kjrcgalc5psq")))

(define-public crate-mapreduce-rs-0.1 (crate (name "mapreduce-rs") (vers "0.1.0") (hash "0cgvdak37agswmvg8hmkj6vi5c9v9y8i9bdavywiagxfqp62f7jp")))

(define-public crate-mapro-0.1 (crate (name "mapro") (vers "0.1.0") (hash "1l67w25p33gcg6677n8ry5h0xcrxhxx84xly2ladhjh489gvc7lr")))

(define-public crate-mapro-0.1 (crate (name "mapro") (vers "0.1.1") (hash "1icjifakk3cpwwwvh3b6s0y268fym2329mn1dqhakqblcaqnzhdh")))

