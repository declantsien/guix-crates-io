(define-module (crates-io ma ud) #:use-module (crates-io))

(define-public crate-maud-0.1 (crate (name "maud") (vers "0.1.0") (hash "1wg6yps1n8b7adldggjkbznkdr4z6lq0jgv6ak1n0bkrd51ldvdk") (yanked #t)))

(define-public crate-maud-0.1 (crate (name "maud") (vers "0.1.1") (hash "0zxx4p6dhhpnvkjqwkd1ywyp0lmx732jvspxlq02qb5dk0ir8hbj")))

(define-public crate-maud-0.1 (crate (name "maud") (vers "0.1.2") (hash "0ggm1ym381in60yx3l0q99fp1s3vqm42sypy6vjm5y1lcdp63as2")))

(define-public crate-maud-0.2 (crate (name "maud") (vers "0.2.0") (hash "0gdqxkj1hd50qxy8dm96hzd7mb8qb42xrk3gdaj77w9c6zpdyk4f")))

(define-public crate-maud-0.3 (crate (name "maud") (vers "0.3.0") (hash "1k9p3iydb5ybjfsmaw5c6fqhkvz2fs1jhh0zfnamlywmbnb9cnq1")))

(define-public crate-maud-0.3 (crate (name "maud") (vers "0.3.1") (hash "1x9c3pps2x7mvpid5zydzbvbpifwd7v9fwi1zkm4854p4xxvgi9g")))

(define-public crate-maud-0.3 (crate (name "maud") (vers "0.3.2") (hash "0dayywm4gfnk3ggx23cr6cinx5hs2pjbs7n2j2p05i7r0q3g1zdm")))

(define-public crate-maud-0.3 (crate (name "maud") (vers "0.3.3") (hash "1mwmnsjsyj8x72y0zgl01wc3qb6bzdlslnnar053hkvpwcibvcj6")))

(define-public crate-maud-0.3 (crate (name "maud") (vers "0.3.4") (hash "1hady5p9knh2nwm481yf33fiax4dk5gills7s4655wim4qpcqk7s")))

(define-public crate-maud-0.4 (crate (name "maud") (vers "0.4.0") (hash "0djcw9iq271gfbxqnrzn6qlvsgwfrkkzsw8yngzdxg6y57xbr6vy") (yanked #t)))

(define-public crate-maud-0.4 (crate (name "maud") (vers "0.4.1") (hash "11kv5rkj040ddjc82r7pw99zihf4zwynap9fcp37x9v35fbqqh9j")))

(define-public crate-maud-0.4 (crate (name "maud") (vers "0.4.2") (hash "1x3c88w0hiij1wwhjqgz1s5prf14zb3ji2cng4ganwqwf4vi3c2k")))

(define-public crate-maud-0.4 (crate (name "maud") (vers "0.4.3") (hash "1c0ai488i4sr63hqyb14bm80rdx8y6wr7syna33rnwfdiwxrflwg")))

(define-public crate-maud-0.4 (crate (name "maud") (vers "0.4.4") (hash "0fisw0yv01m35ayqnlmdk9lqyg8dk474k24k9l3mrqvzfifq97g1")))

(define-public crate-maud-0.5 (crate (name "maud") (vers "0.5.0") (hash "1cp73vi0pjh12a9y8inm8793zxlcz8pnirmvs8018cbp971ni6c3")))

(define-public crate-maud-0.5 (crate (name "maud") (vers "0.5.1") (hash "1qsnmkf23glc9c7bfk8pscd26gbp0n4472h69m4vqg0samv0mgpj")))

(define-public crate-maud-0.6 (crate (name "maud") (vers "0.6.0") (hash "1drm42mj1gdj7ril73dh19fhk08ydvmln8bccgv9abaia516akah")))

(define-public crate-maud-0.6 (crate (name "maud") (vers "0.6.1") (hash "0ijzix2m8ybbai2cg324zwy18w7jp04lh580vszpa4naz7nwdgv2")))

(define-public crate-maud-0.6 (crate (name "maud") (vers "0.6.2") (hash "069iv1p8yzjgmn8ijsa0gsp6qsi6ghb8hzczkmpr7llxgq609jdx")))

(define-public crate-maud-0.7 (crate (name "maud") (vers "0.7.0") (hash "0w4n34qx9hmdizhcydfqk1svfz4czbc9rp315rk1czl7wjgp2vyq")))

(define-public crate-maud-0.7 (crate (name "maud") (vers "0.7.1") (hash "0icp8bqha8wqxwfzw8w4wvzmfdrdblvsvny78yv1cs7jl6jff7q4")))

(define-public crate-maud-0.7 (crate (name "maud") (vers "0.7.2") (hash "0dh3grjk8m5ajk2gqk58kl30c6pbway8b90qv2ix8lrk91fv61v6")))

(define-public crate-maud-0.7 (crate (name "maud") (vers "0.7.3") (hash "1fqqhp5p5jlhifayivk9hhab4k2fv0gjw3cfj4g54kpxz7kp9ydd")))

(define-public crate-maud-0.7 (crate (name "maud") (vers "0.7.4") (hash "0dmj2ddjvacnig0yarimn8f4zv73c5cvwjf2004dk423yqsg7r7v")))

(define-public crate-maud-0.8 (crate (name "maud") (vers "0.8.0") (hash "0chffm0irprdlh7s1p846bm6rf4y282g9pwfnxkprnpgqv7iz87n")))

(define-public crate-maud-0.8 (crate (name "maud") (vers "0.8.1") (hash "1zdij5qb4fdalqhhnkwcq855pb9m1vxwkj0gf3xr3gbim5hfpf2a")))

(define-public crate-maud-0.9 (crate (name "maud") (vers "0.9.0") (hash "057g8bwq81m3f4wa6264ldm87gdfvmv904hl4a1ab7cgny22sadq")))

(define-public crate-maud-0.10 (crate (name "maud") (vers "0.10.0") (hash "1x58fkfcvj6zidlw5931k6awv91zsmi12kqy4c1cqjdi6wlpcclk")))

(define-public crate-maud-0.11 (crate (name "maud") (vers "0.11.0") (deps (list (crate-dep (name "iron") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "0ckzz61al1qfl1nf1zgnvpda8m8ny1ccdx2qcwqqvzds0vpzmpa8")))

(define-public crate-maud-0.11 (crate (name "maud") (vers "0.11.1") (deps (list (crate-dep (name "iron") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "083jk5q7ljvggx822bz39lxahngwv52rlr7iqvarhflip1k1s7x6")))

(define-public crate-maud-0.12 (crate (name "maud") (vers "0.12.0") (deps (list (crate-dep (name "iron") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "19sp3g0s5s92rymjck62g81p01ci0j34lfpwiy17vsgkvqx7vqj0")))

(define-public crate-maud-0.13 (crate (name "maud") (vers "0.13.0") (deps (list (crate-dep (name "iron") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "03zrr4mgr6azxh4vvr4pblkbbchbf9d0wj9szgim7zdrc5966qv8")))

(define-public crate-maud-0.14 (crate (name "maud") (vers "0.14.0") (deps (list (crate-dep (name "iron") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "0p9gp44mfg2xyaby18138w551p277vvia271xi60csrmy88250rb")))

(define-public crate-maud-0.15 (crate (name "maud") (vers "0.15.0") (deps (list (crate-dep (name "iron") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)))) (hash "1dyqsq1jc7nmaj81kbjb5pgpjqdkshl2ah0cvi5wf74iiqlg3iwj")))

(define-public crate-maud-0.16 (crate (name "maud") (vers "0.16.0") (deps (list (crate-dep (name "iron") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)))) (hash "1m406mfcim2pyclfil25bq7b0kfmid85f5sda7iffllkbx5jlwb2")))

(define-public crate-maud-0.16 (crate (name "maud") (vers "0.16.1") (deps (list (crate-dep (name "iron") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rocket") (req ">= 0.1.2, < 0.3") (optional #t) (default-features #t) (kind 0)))) (hash "0vp49d7biszgjpr8j4z7hsdd9mi52a35s8iry13s3brd3ykkb6c5")))

(define-public crate-maud-0.16 (crate (name "maud") (vers "0.16.2") (deps (list (crate-dep (name "iron") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rocket") (req ">= 0.1.2, < 0.3") (optional #t) (default-features #t) (kind 0)))) (hash "0mrgz1hgr42l7gi4liy3b77m0i1ry8hqjd6drh41mhw659pl3xad")))

(define-public crate-maud-0.17 (crate (name "maud") (vers "0.17.0") (deps (list (crate-dep (name "iron") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maud_htmlescape") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "maud_macros") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "0hn6bypn2fq60m0hm4y8nibhby15gjiifbd9p8gd8cspbanwds69")))

(define-public crate-maud-0.17 (crate (name "maud") (vers "0.17.1") (deps (list (crate-dep (name "iron") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maud_htmlescape") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "maud_macros") (req "^0.17.1") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "09wlg90xqjdg0y2v4ry6lafvkijicbpnmx6pvf39viqjiw3gmbc0")))

(define-public crate-maud-0.17 (crate (name "maud") (vers "0.17.2") (deps (list (crate-dep (name "iron") (req ">= 0.5.1, < 0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maud_htmlescape") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "maud_lints") (req "^0.17.0") (default-features #t) (kind 2)) (crate-dep (name "maud_macros") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "0j1025vhif0ryg2spw749xni5s6w9r5dcak1md35jm2a9y0kq0n0")))

(define-public crate-maud-0.17 (crate (name "maud") (vers "0.17.3") (deps (list (crate-dep (name "iron") (req ">= 0.5.1, < 0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maud_htmlescape") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "maud_lints") (req "^0.17.0") (default-features #t) (kind 2)) (crate-dep (name "maud_macros") (req "^0.17.3") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "0qny7z2l1h3b6j38xm8vk5b95yha2gyinmy3w8rv4f273r1xc0q5")))

(define-public crate-maud-0.17 (crate (name "maud") (vers "0.17.4") (deps (list (crate-dep (name "iron") (req ">= 0.5.1, < 0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maud_htmlescape") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "maud_lints") (req "^0.17.0") (default-features #t) (kind 2)) (crate-dep (name "maud_macros") (req "^0.17.4") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "14i3j782d70xsac058wvlvj38vvv4q75256qr3aidvz6k3f1phpk")))

(define-public crate-maud-0.17 (crate (name "maud") (vers "0.17.5") (deps (list (crate-dep (name "iron") (req ">= 0.5.1, < 0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maud_htmlescape") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "maud_lints") (req "^0.17.0") (default-features #t) (kind 2)) (crate-dep (name "maud_macros") (req "^0.17.5") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "1lqz89zbrmlzh4f7bwi5j4py2bvi7bhzwwbvh83xlmsxm2cqjx6m")))

(define-public crate-maud-0.18 (crate (name "maud") (vers "0.18.0") (deps (list (crate-dep (name "actix-web") (req "^0.6.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "iron") (req ">= 0.5.1, < 0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maud_htmlescape") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "maud_lints") (req "^0.17.0") (default-features #t) (kind 2)) (crate-dep (name "maud_macros") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "1c761zlfd54q6xfbnsr59lncm0sv4vq94s9467mdzsfgxk5w2iy2")))

(define-public crate-maud-0.18 (crate (name "maud") (vers "0.18.1") (deps (list (crate-dep (name "actix-web") (req "^0.6.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "iron") (req ">= 0.5.1, < 0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maud_htmlescape") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "maud_lints") (req "^0.18.1") (default-features #t) (kind 2)) (crate-dep (name "maud_macros") (req "^0.18.1") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "0a2xli00kcs7r6sf28nng0gmiawqjhlz5mb9wlfmv1gsbd5a3q69")))

(define-public crate-maud-0.19 (crate (name "maud") (vers "0.19.0") (deps (list (crate-dep (name "actix-web") (req ">= 0.6.12, < 0.8.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "iron") (req ">= 0.5.1, < 0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maud_htmlescape") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "maud_macros") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "18xvpx1ziywqph3cm4945chklwxa6sz5f2vc7fzi2cqsv1mslgyy")))

(define-public crate-maud-0.20 (crate (name "maud") (vers "0.20.0") (deps (list (crate-dep (name "actix-web") (req ">= 0.6.12, < 0.8.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "compiletest_rs") (req "^0.3.15") (default-features #t) (kind 2)) (crate-dep (name "iron") (req ">= 0.5.1, < 0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maud_htmlescape") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "maud_macros") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req ">= 0.3, < 0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0fvrwn6jyhbid5ia3prkwc463p19nprjrndd1d2hk27z28jlnyrk")))

(define-public crate-maud-0.21 (crate (name "maud") (vers "0.21.0") (deps (list (crate-dep (name "actix-web") (req "^1.0.0") (optional #t) (kind 0)) (crate-dep (name "compiletest_rs") (req "^0.3.19") (features (quote ("stable"))) (default-features #t) (kind 2)) (crate-dep (name "iron") (req ">= 0.5.1, < 0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maud_htmlescape") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "maud_macros") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req ">= 0.3, < 0.5") (optional #t) (default-features #t) (kind 0)))) (hash "07f747a152q2r9f2cgxlw4vs3fa12mcnx9h57v6iw5aabb2ixr80")))

(define-public crate-maud-0.22 (crate (name "maud") (vers "0.22.0") (deps (list (crate-dep (name "actix-web-dep") (req "^2.0.0") (optional #t) (kind 0) (package "actix-web")) (crate-dep (name "compiletest_rs") (req "^0.3.19") (features (quote ("stable"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "iron") (req ">=0.5.1, <0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maud_htmlescape") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "maud_macros") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req ">=0.3, <0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1nszly79526lpixgxd9zn619xnm8h0drm6ykl8i2x787zh13fqc4") (features (quote (("actix-web" "actix-web-dep" "futures"))))))

(define-public crate-maud-0.22 (crate (name "maud") (vers "0.22.1") (deps (list (crate-dep (name "actix-web-dep") (req ">=2, <4") (optional #t) (kind 0) (package "actix-web")) (crate-dep (name "futures-util") (req "^0.3.0") (optional #t) (kind 0)) (crate-dep (name "iron") (req ">=0.5.1, <0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maud_htmlescape") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "maud_macros") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req ">=0.3, <0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.33") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0y8p7rkcrca3z1zrdc39bjgnqqmrfswfnxwkbzabadvqzc6xndkf") (features (quote (("default") ("actix-web" "actix-web-dep" "futures-util"))))))

(define-public crate-maud-0.22 (crate (name "maud") (vers "0.22.2") (deps (list (crate-dep (name "actix-web-dep") (req ">=2, <4") (optional #t) (kind 0) (package "actix-web")) (crate-dep (name "futures-util") (req "^0.3.0") (optional #t) (kind 0)) (crate-dep (name "iron") (req ">=0.5.1, <0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maud_htmlescape") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "maud_macros") (req "^0.22.2") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req ">=0.3, <0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.33") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1yk2a6gy6naq615qssy57yl18r4vvx4101y1cl1zh1hcif0881ys") (features (quote (("default") ("actix-web" "actix-web-dep" "futures-util"))))))

(define-public crate-maud-0.22 (crate (name "maud") (vers "0.22.3") (deps (list (crate-dep (name "actix-web-dep") (req ">=2, <4") (optional #t) (kind 0) (package "actix-web")) (crate-dep (name "axum") (req "^0.1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.0") (optional #t) (kind 0)) (crate-dep (name "iron") (req ">=0.5.1, <0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maud_htmlescape") (req "^0.17.1") (default-features #t) (kind 0)) (crate-dep (name "maud_macros") (req "^0.22.3") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req ">=0.3, <0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tide") (req "^0.16.0") (optional #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.33") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0z20q47jpfsmpdj7c0nvllzxz9x86kmgkfhc55dsxlg7gn84km2r") (features (quote (("default") ("actix-web" "actix-web-dep" "futures-util"))))))

(define-public crate-maud-0.23 (crate (name "maud") (vers "0.23.0") (deps (list (crate-dep (name "actix-web-dep") (req ">=2, <4") (optional #t) (kind 0) (package "actix-web")) (crate-dep (name "axum") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.0") (optional #t) (kind 0)) (crate-dep (name "maud_macros") (req "^0.23.0") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req ">=0.3, <0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tide") (req "^0.16.0") (optional #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.33") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "07hfn5pn8ys9j1y5ha8bkgkw5jj8dw56bcgs5v671jcg9yxzx13k") (features (quote (("default") ("actix-web" "actix-web-dep" "futures-util"))))))

(define-public crate-maud-0.24 (crate (name "maud") (vers "0.24.0") (deps (list (crate-dep (name "actix-web-dep") (req "^4") (optional #t) (kind 0) (package "actix-web")) (crate-dep (name "axum-core") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.0") (optional #t) (kind 0)) (crate-dep (name "http") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "itoa") (req "^0.4.8") (features (quote ("i128"))) (kind 0)) (crate-dep (name "maud_macros") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req ">=0.3, <0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tide") (req "^0.16.0") (optional #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.33") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0n0yyvfkyxkz6xp4yiyky4qzgc0k4mjh59pg57gr6jzb376z5bqr") (features (quote (("default") ("axum" "axum-core" "http") ("actix-web" "actix-web-dep" "futures-util"))))))

(define-public crate-maud-0.25 (crate (name "maud") (vers "0.25.0") (deps (list (crate-dep (name "actix-web-dep") (req "^4") (optional #t) (kind 0) (package "actix-web")) (crate-dep (name "axum-core") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.0") (optional #t) (kind 0)) (crate-dep (name "http") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "itoa") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "maud_macros") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req ">=0.3, <0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tide") (req "^0.16.0") (optional #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.33") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "006g7szmlhfhbfpc7zmyhjllx7gw7dwq3s23ib0y2zwaxyfb3fmh") (features (quote (("default") ("axum" "axum-core" "http") ("actix-web" "actix-web-dep" "futures-util"))))))

(define-public crate-maud-0.26 (crate (name "maud") (vers "0.26.0") (deps (list (crate-dep (name "actix-web-dep") (req "^4") (optional #t) (kind 0) (package "actix-web")) (crate-dep (name "axum-core") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.0") (optional #t) (kind 0)) (crate-dep (name "http") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "itoa") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "maud_macros") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tide") (req "^0.16.0") (optional #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.33") (features (quote ("diff"))) (default-features #t) (kind 2)) (crate-dep (name "warp") (req "^0.3.6") (optional #t) (default-features #t) (kind 0)))) (hash "1amw5r3w4v9z2grr2cf90aca9x1244gv18gzvp6qjhkb05sqnlfz") (features (quote (("default") ("axum" "axum-core" "http") ("actix-web" "actix-web-dep" "futures-util"))))))

(define-public crate-maud-live-view-0.24 (crate (name "maud-live-view") (vers "0.24.0") (deps (list (crate-dep (name "actix-web-dep") (req "^4") (optional #t) (kind 0) (package "actix-web")) (crate-dep (name "axum-core") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.0") (optional #t) (kind 0)) (crate-dep (name "http") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "itoa") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "maud-live-view-macros") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req ">=0.3, <0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tide") (req "^0.16.0") (optional #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.33") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0vsm8gqcsn5l1ly7x4pc6wj7lrv5vdvcicby3mlqyxx51lbs2hcx") (features (quote (("default") ("axum" "axum-core" "http") ("actix-web" "actix-web-dep" "futures-util"))))))

(define-public crate-maud-live-view-0.24 (crate (name "maud-live-view") (vers "0.24.1") (deps (list (crate-dep (name "actix-web-dep") (req "^4") (optional #t) (kind 0) (package "actix-web")) (crate-dep (name "axum-core") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.0") (optional #t) (kind 0)) (crate-dep (name "http") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "itoa") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "maud-live-view-macros") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req ">=0.3, <0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tide") (req "^0.16.0") (optional #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.33") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0b14idfy1wlp9vgzqgvpyhnib49714qxy872gq8f90w5fc1sq8cf") (features (quote (("default") ("axum" "axum-core" "http") ("actix-web" "actix-web-dep" "futures-util"))))))

(define-public crate-maud-live-view-0.24 (crate (name "maud-live-view") (vers "0.24.2") (deps (list (crate-dep (name "actix-web-dep") (req "^4") (optional #t) (kind 0) (package "actix-web")) (crate-dep (name "axum-core") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.0") (optional #t) (kind 0)) (crate-dep (name "http") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "itoa") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "maud-live-view-macros") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req ">=0.3, <0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tide") (req "^0.16.0") (optional #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.33") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "104vhfxyjx7093n7v0j1dmmm74c65afs7gffzylwabf80di4ig26") (features (quote (("default") ("axum" "axum-core" "http") ("actix-web" "actix-web-dep" "futures-util"))))))

(define-public crate-maud-live-view-0.24 (crate (name "maud-live-view") (vers "0.24.3") (deps (list (crate-dep (name "actix-web-dep") (req "^4") (optional #t) (kind 0) (package "actix-web")) (crate-dep (name "axum-core") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.0") (optional #t) (kind 0)) (crate-dep (name "http") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "itoa") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "maud-live-view-macros") (req "^0.24.3") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req ">=0.3, <0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tide") (req "^0.16.0") (optional #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.33") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0g2xsmzbyq3f4qyln20j9js1k8dxhwsa4743kljv683yq34pq8gk") (features (quote (("default") ("axum" "axum-core" "http") ("actix-web" "actix-web-dep" "futures-util"))))))

(define-public crate-maud-live-view-macros-0.24 (crate (name "maud-live-view-macros") (vers "0.24.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "0lc6qmv8m89ycrk3i262k1di3vlflarq22g5n12mrwwx9hrnmghv")))

(define-public crate-maud-live-view-macros-0.24 (crate (name "maud-live-view-macros") (vers "0.24.1") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "0nasvin9wm27dnzymmvrmnaq2ps9w9nb5kqp0z0amrfga3lgqk1j")))

(define-public crate-maud-live-view-macros-0.24 (crate (name "maud-live-view-macros") (vers "0.24.2") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "02cd6mdyigr93rp5dnfr7xnyqv1dd66s13n31yhqk66agd5di5vs")))

(define-public crate-maud-live-view-macros-0.24 (crate (name "maud-live-view-macros") (vers "0.24.3") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "02qmycqav5y6r1b1h7kaq5z4pf8dflmcmajk3b9vs7xs4gxx2ny4")))

(define-public crate-maud-pulldown-cmark-0.0.2 (crate (name "maud-pulldown-cmark") (vers "0.0.2") (deps (list (crate-dep (name "maud") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "1g8z05bclfnniy6nj23prhmkxwwal990m7j0dkkcbla4c4204a9l") (features (quote (("default"))))))

(define-public crate-maud-pulldown-cmark-0.0.3 (crate (name "maud-pulldown-cmark") (vers "0.0.3") (deps (list (crate-dep (name "maud") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "00ix40pv3m593k7sv3a9z8df7qx2npajqbq8vzfpj5vgx3hvy3pc") (features (quote (("default"))))))

(define-public crate-maud-pulldown-cmark-0.0.4 (crate (name "maud-pulldown-cmark") (vers "0.0.4") (deps (list (crate-dep (name "maud") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "0f8l9y2r23pzj583850fzn8x7lsl3prgri010wf4lq5bz0k4kzaz") (features (quote (("default"))))))

(define-public crate-maud-pulldown-cmark-0.0.5 (crate (name "maud-pulldown-cmark") (vers "0.0.5") (deps (list (crate-dep (name "maud") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "0ds6bk9yii6csi79flpbpvimycpxh97x587xrzcxdp1fhrqbamaf") (features (quote (("default"))))))

(define-public crate-maud-pulldown-cmark-0.0.6 (crate (name "maud-pulldown-cmark") (vers "0.0.6") (deps (list (crate-dep (name "maud") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "10c9iy0yh0d3239mgbrfzdxbl69z8srl5y108d34hi8ri7k4m10p") (features (quote (("default"))))))

(define-public crate-maud-pulldown-cmark-0.0.7 (crate (name "maud-pulldown-cmark") (vers "0.0.7") (deps (list (crate-dep (name "maud") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "09sp5j15vhq481vxqxi06gi95ldvkaa8l3ddgjsz5szyqzda1kwi") (features (quote (("default"))))))

(define-public crate-maud-pulldown-cmark-0.1 (crate (name "maud-pulldown-cmark") (vers "0.1.0") (deps (list (crate-dep (name "maud") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "037xqzx96kpbrcvpqpfj84g2v23vyiiyyfkx9dp8csxmqb14pd3l") (features (quote (("default"))))))

(define-public crate-maud-pulldown-cmark-0.2 (crate (name "maud-pulldown-cmark") (vers "0.2.2") (deps (list (crate-dep (name "maud") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.0.8") (default-features #t) (kind 0)))) (hash "0z7nva0fm3y1mzj7rd3pv2kshs7pw9b8is59dzdb0q5767y2cw6d")))

(define-public crate-maud-pulldown-cmark-0.3 (crate (name "maud-pulldown-cmark") (vers "0.3.0") (deps (list (crate-dep (name "maud") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "maud_macros") (req "^0.11.0") (default-features #t) (kind 2)) (crate-dep (name "pulldown-cmark") (req "^0.0.8") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.6") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.6") (default-features #t) (kind 2)))) (hash "18vv9dd6q9slv9k0y5lhpccaarc1mcif1ql17cf1z1acsxsqsnfh")))

(define-public crate-maud-pulldown-cmark-0.4 (crate (name "maud-pulldown-cmark") (vers "0.4.0") (deps (list (crate-dep (name "maud") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "maud_macros") (req "^0.12.0") (default-features #t) (kind 2)) (crate-dep (name "pulldown-cmark") (req "^0.0.8") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.6") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.6") (default-features #t) (kind 2)))) (hash "085v8pmi7pc2lw72h6n2k8mlxbgb436d2jp1sy52c59i47xv0x6m")))

(define-public crate-maud-pulldown-cmark-0.5 (crate (name "maud-pulldown-cmark") (vers "0.5.0") (deps (list (crate-dep (name "maud") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "maud_macros") (req "^0.14.0") (default-features #t) (kind 2)) (crate-dep (name "pulldown-cmark") (req "^0.0.8") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.6") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.6") (default-features #t) (kind 2)))) (hash "0nl7xcnwi504w9cl378qc7l8kagqgdzjs9r9rsn46rkdc7pqn5xw")))

(define-public crate-maud_htmlescape-0.17 (crate (name "maud_htmlescape") (vers "0.17.0") (hash "1dxzakhgpgbr7yy7z8vmywbi77d3yrwqdvg1s4m30hpwryy8byyh")))

(define-public crate-maud_htmlescape-0.17 (crate (name "maud_htmlescape") (vers "0.17.1") (hash "19kzv4b79qx1x6fr57jh3cvsmhk0nb3wvy7s0srfpdclgyixlm85")))

(define-public crate-maud_lints-0.17 (crate (name "maud_lints") (vers "0.17.0") (deps (list (crate-dep (name "if_chain") (req "^0.1") (default-features #t) (kind 0)))) (hash "0gh7vyxp4v3h7qqa7vaac7ph075y3jhynsp0fs4j86hxviwck50m")))

(define-public crate-maud_lints-0.17 (crate (name "maud_lints") (vers "0.17.4") (deps (list (crate-dep (name "if_chain") (req "^0.1") (default-features #t) (kind 0)))) (hash "063j7y3r5v7l00zysx1jrwffxbsf211w569gbpnx8zv38rp113hz")))

(define-public crate-maud_lints-0.18 (crate (name "maud_lints") (vers "0.18.1") (deps (list (crate-dep (name "if_chain") (req "^0.1") (default-features #t) (kind 0)))) (hash "0bhrsb9p85as03h2jz7sas604kfwfxbwn86ynykswi7lds7l86c5")))

(define-public crate-maud_macros-0.1 (crate (name "maud_macros") (vers "0.1.0") (deps (list (crate-dep (name "maud") (req "= 0.1.0") (default-features #t) (kind 0)))) (hash "0zhvc5r84d218v8x2xxdq8aid8pxy03lmafnn1rp29r9vg5mkh0q") (yanked #t)))

(define-public crate-maud_macros-0.1 (crate (name "maud_macros") (vers "0.1.1") (deps (list (crate-dep (name "maud") (req "= 0.1.1") (default-features #t) (kind 0)))) (hash "1xvixhl8kkf9ac06529gfxgwlznfxpp1cyd1p7lm3nvncxmawlf8")))

(define-public crate-maud_macros-0.1 (crate (name "maud_macros") (vers "0.1.2") (deps (list (crate-dep (name "maud") (req "= 0.1.2") (default-features #t) (kind 0)))) (hash "00nd13sr71bsjhws4cxicy68ap5kp63dri98qmch85lzh6igghv2")))

(define-public crate-maud_macros-0.2 (crate (name "maud_macros") (vers "0.2.0") (deps (list (crate-dep (name "maud") (req "= 0.2.0") (default-features #t) (kind 0)))) (hash "1mx3f5v3mna70rkxchfl3vgi4p6qc1lv840lci3f9zmvwl8ifz55")))

(define-public crate-maud_macros-0.3 (crate (name "maud_macros") (vers "0.3.0") (deps (list (crate-dep (name "maud") (req "= 0.3.0") (default-features #t) (kind 0)))) (hash "0fkm0s28015dh1igfkd4ldc1g2nbhnwv2m696c0rss86577z0r33")))

(define-public crate-maud_macros-0.3 (crate (name "maud_macros") (vers "0.3.1") (deps (list (crate-dep (name "maud") (req "= 0.3.1") (default-features #t) (kind 0)))) (hash "0z6s17gl1jwry7bzpxyxqc5i066jc5s7rljga48na7a5rqkd516z")))

(define-public crate-maud_macros-0.3 (crate (name "maud_macros") (vers "0.3.2") (deps (list (crate-dep (name "maud") (req "= 0.3.2") (default-features #t) (kind 0)))) (hash "1xylvpvlhg72hhiyhpkzrzfha94m6d7hv4ca7j2g0riqj8q91m1m")))

(define-public crate-maud_macros-0.3 (crate (name "maud_macros") (vers "0.3.3") (deps (list (crate-dep (name "maud") (req "= 0.3.3") (default-features #t) (kind 0)))) (hash "0py64frbylj9m0p6bpr589lkvczcs8yghrp9157g9dz3vjc37l9f")))

(define-public crate-maud_macros-0.3 (crate (name "maud_macros") (vers "0.3.4") (deps (list (crate-dep (name "maud") (req "= 0.3.4") (default-features #t) (kind 0)))) (hash "1svc18dh54bz8h5vdkd3zayb6z0baigm106w8sjlcrg9rnryf16w")))

(define-public crate-maud_macros-0.4 (crate (name "maud_macros") (vers "0.4.0") (deps (list (crate-dep (name "maud") (req "= 0.4.0") (default-features #t) (kind 0)))) (hash "0papq7zxz6dmix4vhd7rcy1ab6ncqyhscpxld1zwckxzp63ifx8y") (features (quote (("print-expansion")))) (yanked #t)))

(define-public crate-maud_macros-0.4 (crate (name "maud_macros") (vers "0.4.1") (deps (list (crate-dep (name "maud") (req "= 0.4.1") (default-features #t) (kind 0)))) (hash "1vbs6y2bdfs6h13hy5njzdfaghln6fjialgkaaxxdydddqsa7kbs")))

(define-public crate-maud_macros-0.4 (crate (name "maud_macros") (vers "0.4.2") (deps (list (crate-dep (name "maud") (req "= 0.4.2") (default-features #t) (kind 0)))) (hash "0yg8g4d3a0jzc79mqqa7a6fivg58kmaydrnhkw3cknqgaaczx6fp")))

(define-public crate-maud_macros-0.4 (crate (name "maud_macros") (vers "0.4.3") (deps (list (crate-dep (name "maud") (req "= 0.4.3") (default-features #t) (kind 0)))) (hash "1lwihc33gcycjgadgkaa5ycz28cm3f45d4cm19mywfppspsj1vvy")))

(define-public crate-maud_macros-0.4 (crate (name "maud_macros") (vers "0.4.4") (deps (list (crate-dep (name "maud") (req "= 0.4.4") (default-features #t) (kind 0)))) (hash "0qc7hg9bbh0a1ln61x4y08hmrn6pq4ij37nmrdn5kpyw6bwcckc1")))

(define-public crate-maud_macros-0.5 (crate (name "maud_macros") (vers "0.5.0") (deps (list (crate-dep (name "maud") (req "= 0.5.0") (default-features #t) (kind 0)))) (hash "1xypbwzndsps5r3ks1kdynm5hfdljhxfmirhiy0sn303ihsirg3z")))

(define-public crate-maud_macros-0.5 (crate (name "maud_macros") (vers "0.5.1") (deps (list (crate-dep (name "maud") (req "= 0.5.1") (default-features #t) (kind 0)))) (hash "0q215a5rdkpnxikfsmk9337xjx7n9vsb7361gpnx601ja3ijn2hy")))

(define-public crate-maud_macros-0.6 (crate (name "maud_macros") (vers "0.6.0") (deps (list (crate-dep (name "maud") (req "= 0.6.0") (default-features #t) (kind 0)))) (hash "02mzbi3d0ci050rpb1ixaki353sbc8y9ylsmhk4m3ijraw2iip08")))

(define-public crate-maud_macros-0.6 (crate (name "maud_macros") (vers "0.6.1") (deps (list (crate-dep (name "maud") (req "= 0.6.1") (default-features #t) (kind 0)))) (hash "12d32sj2p21gcp6lxpi9phb477s312na36w0k0p7q8449pvvz9r1")))

(define-public crate-maud_macros-0.6 (crate (name "maud_macros") (vers "0.6.2") (deps (list (crate-dep (name "maud") (req "= 0.6.2") (default-features #t) (kind 0)))) (hash "00nmgjqdsy9vq5v0v3qa0m059602a75yzbcj79dm7lx6g3rp43cz")))

(define-public crate-maud_macros-0.7 (crate (name "maud_macros") (vers "0.7.0") (deps (list (crate-dep (name "maud") (req "= 0.7.0") (default-features #t) (kind 0)))) (hash "07a1l4ap9vmahl2c8x6760i70m5hh506b1dc71i306fhc5vq92rj")))

(define-public crate-maud_macros-0.7 (crate (name "maud_macros") (vers "0.7.1") (deps (list (crate-dep (name "maud") (req "= 0.7.1") (default-features #t) (kind 0)))) (hash "1vr2bfpn7wbpw7ayvg6hypykd4mdxa1g83nwbcbp1n9714xf9xhk")))

(define-public crate-maud_macros-0.7 (crate (name "maud_macros") (vers "0.7.2") (deps (list (crate-dep (name "maud") (req "= 0.7.2") (default-features #t) (kind 0)))) (hash "0s9kp7b62f5s9zgn45vlr2w78vnplh63rv57pwqb5xah683xvhdr")))

(define-public crate-maud_macros-0.7 (crate (name "maud_macros") (vers "0.7.3") (deps (list (crate-dep (name "maud") (req "= 0.7.3") (default-features #t) (kind 0)))) (hash "0ng5jbs5x3rlg81nhldi0lp6hash52j7vxa3i3xh68jnfimvgb5c")))

(define-public crate-maud_macros-0.7 (crate (name "maud_macros") (vers "0.7.4") (deps (list (crate-dep (name "maud") (req "= 0.7.4") (default-features #t) (kind 0)))) (hash "13f3x1gfqj96vwyc3j5ww9c8g8k7rsbyfvxgb49y558aiij94cs0")))

(define-public crate-maud_macros-0.8 (crate (name "maud_macros") (vers "0.8.0") (deps (list (crate-dep (name "maud") (req "= 0.8.0") (default-features #t) (kind 0)))) (hash "06pin69v4in2gw08l94c6mv4wqhwjjv8xx3j0wlrls2w0zxxr48z")))

(define-public crate-maud_macros-0.8 (crate (name "maud_macros") (vers "0.8.1") (deps (list (crate-dep (name "maud") (req "= 0.8.1") (default-features #t) (kind 0)))) (hash "0cf83cdfzqd50galvqhk1rj38rlw6yrqjj769mff7s6id1n6h5rq")))

(define-public crate-maud_macros-0.9 (crate (name "maud_macros") (vers "0.9.0") (deps (list (crate-dep (name "maud") (req "= 0.9.0") (default-features #t) (kind 0)))) (hash "0ak51i4y2v9rp69mgix6rsqrxk4ygzawavsjmiqqhgf9yii81nbk")))

(define-public crate-maud_macros-0.9 (crate (name "maud_macros") (vers "0.9.1") (deps (list (crate-dep (name "maud") (req "= 0.9.0") (default-features #t) (kind 0)))) (hash "1fq88c2ci538cwvyh0cvbrybq3b8m8iqy6xgqjwl0b1dhdn1ffk0")))

(define-public crate-maud_macros-0.9 (crate (name "maud_macros") (vers "0.9.2") (deps (list (crate-dep (name "maud") (req "= 0.9.0") (default-features #t) (kind 0)))) (hash "08klb0dlwj2skfybd7bn94jgwlj6004lppzf5r2s74x3imwpgy1g")))

(define-public crate-maud_macros-0.10 (crate (name "maud_macros") (vers "0.10.0") (deps (list (crate-dep (name "maud") (req "= 0.10.0") (default-features #t) (kind 0)))) (hash "112nqyck9rqznfcnj1ifbv0gcw0b0n02yf2ry8d57ga04sg382y5")))

(define-public crate-maud_macros-0.11 (crate (name "maud_macros") (vers "0.11.0") (deps (list (crate-dep (name "maud") (req "= 0.11.0") (default-features #t) (kind 0)))) (hash "11f397yxa3svqgr775f14czrclxsgvb500cxf5pagyw6bni28jzz")))

(define-public crate-maud_macros-0.11 (crate (name "maud_macros") (vers "0.11.1") (deps (list (crate-dep (name "maud") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "1x5gqvpdqbfja5vsfc0z1pn0csm91yz3rpm2ylj6qa2jv2879jqp")))

(define-public crate-maud_macros-0.12 (crate (name "maud_macros") (vers "0.12.0") (deps (list (crate-dep (name "maud") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0cd5751z3cld8svg5xmami9112w45bqnczvf07bfzkq2wz3zydxa")))

(define-public crate-maud_macros-0.13 (crate (name "maud_macros") (vers "0.13.0") (deps (list (crate-dep (name "maud") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "07vv29ryly4sd004wnln24rs3mz8kpmbzzpmpr343jv3r7nds8xm")))

(define-public crate-maud_macros-0.14 (crate (name "maud_macros") (vers "0.14.0") (deps (list (crate-dep (name "maud") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "0wi6jvr77qr80f2flxrjzsgaqr90hqvjh65h1hlbv77hcjx6x09m")))

(define-public crate-maud_macros-0.15 (crate (name "maud_macros") (vers "0.15.0") (deps (list (crate-dep (name "if_chain") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "0i3ixj739ivifvq2w7991l2p84p940l7h65n8vfdfqqa90bv8yvg")))

(define-public crate-maud_macros-0.16 (crate (name "maud_macros") (vers "0.16.0") (deps (list (crate-dep (name "if_chain") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "1g3m5ghpg5zhzdx6hrr13g93ziw1mv2vwhz4ygmmd99ws15js0mz")))

(define-public crate-maud_macros-0.16 (crate (name "maud_macros") (vers "0.16.1") (deps (list (crate-dep (name "if_chain") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "1qz766c5hlxngm94alm44lykpg0pi14dhnhk9vs5h2185jvz8928")))

(define-public crate-maud_macros-0.16 (crate (name "maud_macros") (vers "0.16.2") (deps (list (crate-dep (name "if_chain") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "0g3l7nrv0g03nkqzcjl4vy0vazp5sqn5g8qp5bdxkhd8g9q38j1m")))

(define-public crate-maud_macros-0.16 (crate (name "maud_macros") (vers "0.16.3") (deps (list (crate-dep (name "if_chain") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "0fcabiam0m9f2dgcv9a4lgkp6qkw0hcygpaj6rwpf84gmp928d45")))

(define-public crate-maud_macros-0.17 (crate (name "maud_macros") (vers "0.17.0") (deps (list (crate-dep (name "literalext") (req "^0.1") (features (quote ("proc-macro"))) (kind 0)) (crate-dep (name "maud_htmlescape") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "1jbr96m9klcwbdida36nk2qi30v84vgyy8a1ndbwh91n51bpi7wd")))

(define-public crate-maud_macros-0.17 (crate (name "maud_macros") (vers "0.17.1") (deps (list (crate-dep (name "literalext") (req "^0.1") (features (quote ("proc-macro"))) (kind 0)) (crate-dep (name "maud_htmlescape") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "1wms41r5cwkzm04b0c19gish0ayy7gigfm2gdhj9nyr9262grpkl")))

(define-public crate-maud_macros-0.17 (crate (name "maud_macros") (vers "0.17.2") (deps (list (crate-dep (name "literalext") (req "^0.1") (features (quote ("proc-macro"))) (kind 0)) (crate-dep (name "maud_htmlescape") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "0mffkyyy4vliwhyp9b3hwiyqnk23bg8mjynni95h958rgkbpg0qd")))

(define-public crate-maud_macros-0.17 (crate (name "maud_macros") (vers "0.17.3") (deps (list (crate-dep (name "literalext") (req "^0.1") (features (quote ("proc-macro"))) (kind 0)) (crate-dep (name "maud_htmlescape") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "1lxzykg5n2xxzfx2x13yr05nq6grkip6vq4k4d2vhhvh7skvznqh")))

(define-public crate-maud_macros-0.17 (crate (name "maud_macros") (vers "0.17.4") (deps (list (crate-dep (name "literalext") (req "^0.1") (features (quote ("proc-macro"))) (kind 0)) (crate-dep (name "maud_htmlescape") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "1nq8vscy5mlqzaghflayc448jrdl8fas5s7yg8xr2bp7ds4px18j")))

(define-public crate-maud_macros-0.17 (crate (name "maud_macros") (vers "0.17.5") (deps (list (crate-dep (name "literalext") (req "^0.1") (features (quote ("proc-macro"))) (kind 0)) (crate-dep (name "matches") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "maud_htmlescape") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "02292vr97bnc8wsbp7jyzpfwkmy7kkln8hl26hnfp3iy6m9hzii1")))

(define-public crate-maud_macros-0.18 (crate (name "maud_macros") (vers "0.18.0") (deps (list (crate-dep (name "literalext") (req "^0.1") (features (quote ("proc-macro"))) (kind 0)) (crate-dep (name "matches") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "maud_htmlescape") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "02kclcijw1shh3znnr383d5ii2c3frz84yqkd9r5b5b9zvf0likl")))

(define-public crate-maud_macros-0.18 (crate (name "maud_macros") (vers "0.18.1") (deps (list (crate-dep (name "literalext") (req "^0.1") (features (quote ("proc-macro"))) (kind 0)) (crate-dep (name "matches") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "maud_htmlescape") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "0d9ds2c32w10skz694f5f460zw5g8jbaijyx3vpgfqb8p8nw8isf")))

(define-public crate-maud_macros-0.19 (crate (name "maud_macros") (vers "0.19.0") (deps (list (crate-dep (name "literalext") (req "^0.1") (features (quote ("proc-macro"))) (kind 0)) (crate-dep (name "matches") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "maud_htmlescape") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "0mx4v89glg37x04y23z8cd8psmk3vbyn1bqf4cmm1azapfnz751z")))

(define-public crate-maud_macros-0.20 (crate (name "maud_macros") (vers "0.20.0") (deps (list (crate-dep (name "literalext") (req "^0.1") (features (quote ("proc-macro"))) (kind 0)) (crate-dep (name "matches") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "maud_htmlescape") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "0hfazfcw7qljlx2pbhrf86ga9p48rmdhxrlccvffwybzv8f7an3g")))

(define-public crate-maud_macros-0.21 (crate (name "maud_macros") (vers "0.21.0") (deps (list (crate-dep (name "matches") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "maud_htmlescape") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.34") (default-features #t) (kind 0)))) (hash "0cfr781kq7s0szkybbnb1vvpbrfagwjqb3xggkanlyh5sz8hzbk2")))

(define-public crate-maud_macros-0.22 (crate (name "maud_macros") (vers "0.22.0") (deps (list (crate-dep (name "matches") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "maud_htmlescape") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "1kcvbjx36zdvzvi96l4j2cvj388cllzaxvxmjqbwpnjzq3f2ddjy")))

(define-public crate-maud_macros-0.22 (crate (name "maud_macros") (vers "0.22.1") (deps (list (crate-dep (name "maud_htmlescape") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "01qcjy2akhyv40a5gbzk5x4s8rh82xq4mr9fhf4hgv6d1lank2lw")))

(define-public crate-maud_macros-0.22 (crate (name "maud_macros") (vers "0.22.2") (deps (list (crate-dep (name "maud_htmlescape") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.19") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "07jvcn3d99xbplimb3la1f6xriz9fy2jrgfqlc3i8rylmdc0383k")))

(define-public crate-maud_macros-0.22 (crate (name "maud_macros") (vers "0.22.3") (deps (list (crate-dep (name "maud_htmlescape") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.19") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "0xldpc0y8005ym478ld86w3n93kh6figgwf0kawq1snwij76z2g6")))

(define-public crate-maud_macros-0.23 (crate (name "maud_macros") (vers "0.23.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "0m9kiqarn9ayyiv29jcmwz1pp2mxvqljjgz9gmni623l8sn30d22")))

(define-public crate-maud_macros-0.24 (crate (name "maud_macros") (vers "0.24.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "1szjhg2h92rf71xj88vpaxmza38lidxbhbsxlbags22bybv19hg5")))

(define-public crate-maud_macros-0.25 (crate (name "maud_macros") (vers "0.25.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "1lpfr53x82h69mpgqvr6mzn86yp1pbjmhq1177kgqkq2qdk5vs8b")))

(define-public crate-maud_macros-0.26 (crate (name "maud_macros") (vers "0.26.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.0") (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "062f4w3367x1la8j6wkvv5ga9hxmsdw5kz0idfps1391xhw34igs")))

