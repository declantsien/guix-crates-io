(define-module (crates-io gd sd) #:use-module (crates-io))

(define-public crate-gdsdk-0.1 (crate (name "gdsdk") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1dambqf0x001ii8876bbjb9mw7sz08ip1w8zm0a6hrxyxg87zpnx")))

