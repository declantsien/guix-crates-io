(define-module (crates-io gd ru) #:use-module (crates-io))

(define-public crate-gdrust-0.1 (crate (name "gdrust") (vers "0.1.0") (deps (list (crate-dep (name "gdnative") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "gdrust_macros") (req "=0.1.0") (default-features #t) (kind 0)))) (hash "1d6sm85k3ql2npaf6k4b0dvwg2isz1fxvw5041ij125xa498h8mq")))

(define-public crate-gdrust_macros-0.1 (crate (name "gdrust_macros") (vers "0.1.0") (deps (list (crate-dep (name "gdnative") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "heck") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1cs2p5i91wf0fkq2d2bi14fq7z2pk7wk06hyw0mdvf64jmn7nn9z")))

