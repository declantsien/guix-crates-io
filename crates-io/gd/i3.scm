(define-module (crates-io gd i3) #:use-module (crates-io))

(define-public crate-gdi32-sys-0.0.1 (crate (name "gdi32-sys") (vers "0.0.1") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "19ynzshvl1bl1n3j63ni2i38ri8asnvbk674bfv02ndpd5q29r4l")))

(define-public crate-gdi32-sys-0.0.2 (crate (name "gdi32-sys") (vers "0.0.2") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "1j3vfgyn53mxswdzqgrxr40cgmdn44in84k8y7ailcw0m01dxssl")))

(define-public crate-gdi32-sys-0.0.3 (crate (name "gdi32-sys") (vers "0.0.3") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "0mb1w09l7jsn498qvda7w1mdl2zrdrg7kf6hjl5vxfarcxi8rnr5")))

(define-public crate-gdi32-sys-0.0.4 (crate (name "gdi32-sys") (vers "0.0.4") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "0a2z8ljz8xbai81h9k32akzpwxw3ynyk5b9skjwmswwgqkfx0p35")))

(define-public crate-gdi32-sys-0.1 (crate (name "gdi32-sys") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "0ab50lp8g9a93qh328kns9xb5idmm8z639nvdwjhwpbc7ncg4ka6")))

(define-public crate-gdi32-sys-0.1 (crate (name "gdi32-sys") (vers "0.1.1") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "16nm7xd5mmc7aqgzy78bm656kq54asws677wbgqfd4i5vk26w9b5")))

(define-public crate-gdi32-sys-0.2 (crate (name "gdi32-sys") (vers "0.2.0") (deps (list (crate-dep (name "winapi") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "0605d4ngjsspghwjv4jicajich1gnl0aik9f880ajjzjixd524h9")))

(define-public crate-gdi32-sys-0.1 (crate (name "gdi32-sys") (vers "0.1.2") (deps (list (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "04bcwaia0q46k4rajwpivdvlfyc2gw5vnvkbz247hlh724nbjglf")))

