(define-module (crates-io gd k5) #:use-module (crates-io))

(define-public crate-gdk5-0.0.0 (crate (name "gdk5") (vers "0.0.0") (hash "1iqpc314lg2xkgzlp1n1yylbym37wkd7dz2jmhxyl0qpc9wdpxkw")))

(define-public crate-gdk5-sys-0.0.0 (crate (name "gdk5-sys") (vers "0.0.0") (hash "1my2p9vfgys6nfhlcyivn4bdfak037n2qjwlcrpsd87sxl4a2j5j")))

(define-public crate-gdk5-wayland-0.0.0 (crate (name "gdk5-wayland") (vers "0.0.0") (hash "0lzssw6rni7dipc57f2355cxi604c3bygpgnyyb4zb520nvisqnz")))

(define-public crate-gdk5-wayland-sys-0.0.0 (crate (name "gdk5-wayland-sys") (vers "0.0.0") (hash "015rn9p8ldg7m3j7hrs7j4gfr15cavb7chskwqijhi5p9997g1rd")))

(define-public crate-gdk5-x11-0.0.0 (crate (name "gdk5-x11") (vers "0.0.0") (hash "0410acaq75cg4mjbklvknjw4lbpqwkbg116azlqvc2qszr0ayaf8")))

(define-public crate-gdk5-x11-sys-0.0.0 (crate (name "gdk5-x11-sys") (vers "0.0.0") (hash "0yd7wqbg1qi03idn8gbn28n69n9zalpdz9g90p9izp7xwkljpq90")))

