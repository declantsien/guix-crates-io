(define-module (crates-io gd ex) #:use-module (crates-io))

(define-public crate-gdext-0.0.0 (crate (name "gdext") (vers "0.0.0") (hash "1rpcpnvaazmdr0ccjadcgkx3pxfwjxi94xfwn2cl3hs3sqyy9h5z")))

(define-public crate-gdext-egui-0.0.0 (crate (name "gdext-egui") (vers "0.0.0") (hash "1v08d4dzgzhxa75wnagrvgzxf4zd1mcx4g1cw1lchq77m4v1hd6f")))

(define-public crate-gdext-utils-0.0.0 (crate (name "gdext-utils") (vers "0.0.0") (hash "0hrcfyllyfd8g94yz2bm59wdsbcsm42340zjqg05s614a2had1mc")))

(define-public crate-gdextension-0.0.1 (crate (name "gdextension") (vers "0.0.1") (hash "1i17yl4nq9yxh0iidy5iwk3lanf906wifkd3nrfkb8vv9hcvzj9x")))

