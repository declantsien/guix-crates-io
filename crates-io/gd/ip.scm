(define-module (crates-io gd ip) #:use-module (crates-io))

(define-public crate-gdiplus-0.0.1 (crate (name "gdiplus") (vers "0.0.1") (hash "14pfi567lkwlakkrprqj0dzv3ismzd7032nhmzs2jwbdcv67gqp3")))

(define-public crate-gdiplus-0.0.2 (crate (name "gdiplus") (vers "0.0.2") (deps (list (crate-dep (name "gdiplus-sys2") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("windef"))) (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("impl-default" "libloaderapi" "winerror" "winuser"))) (default-features #t) (kind 2)))) (hash "0da9vq61cl1a0g1hg6q1xhklc2v97m2g6mxw352aid3hnzzfz6kb")))

(define-public crate-gdiplus-sys2-0.1 (crate (name "gdiplus-sys2") (vers "0.1.1") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("commctrl" "vcruntime" "windef" "wingdi" "wtypes"))) (default-features #t) (kind 0)))) (hash "1pg0kw7f0s9yszj8q18m25i86yawjc3yagg5x2zjmbzxv7k6wjqd")))

