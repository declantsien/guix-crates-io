(define-module (crates-io gd s-) #:use-module (crates-io))

(define-public crate-gds-sim-0.1 (crate (name "gds-sim") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1i6za29y9c7yvv5y6npmim5ckp0jczzj0j9kzad8zcy5iq5fg5fs")))

(define-public crate-gds-sim-0.1 (crate (name "gds-sim") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0q5q1dn9953f74zazmk7kqf91vckwsnphy205dvq059qkj0amayd")))

