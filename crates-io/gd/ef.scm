(define-module (crates-io gd ef) #:use-module (crates-io))

(define-public crate-gdeflate-0.1 (crate (name "gdeflate") (vers "0.1.0") (deps (list (crate-dep (name "cxx") (req "^1.0.102") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0.102") (default-features #t) (kind 1)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "1c2hmqkadpx1s8d15n25a4wryfm4gkw6fvb2vyz083k11yk708j7") (yanked #t)))

(define-public crate-gdeflate-0.1 (crate (name "gdeflate") (vers "0.1.1") (deps (list (crate-dep (name "cxx") (req "^1.0.102") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0.102") (default-features #t) (kind 1)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "1s4z2x9q4k7h3jn2h2y0fjffwv1r28wk2x7gdvd3wz0zv1k587vm")))

(define-public crate-gdeflate-0.1 (crate (name "gdeflate") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0.82") (default-features #t) (kind 1)) (crate-dep (name "cxx") (req "^1.0.104") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0.104") (default-features #t) (kind 1)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "1aic61rlvazncp283hjc5rbn8wxha9mpgw0v6ffr15g4gf7q4k80")))

(define-public crate-gdeflate-0.2 (crate (name "gdeflate") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0.90") (default-features #t) (kind 1)) (crate-dep (name "cxx") (req "^1.0.119") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0.119") (default-features #t) (kind 1)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "04wllsa6i5n96xz45bb6fc22rncf5cxg62abwwmzd6jmj5mmm6ab")))

(define-public crate-gdeflate-0.3 (crate (name "gdeflate") (vers "0.3.0") (deps (list (crate-dep (name "gdeflate-sys") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "0p89x835adm8v0k18wv4ynbq2l1933y0fvkmn0b5jh2699lgqm4s")))

(define-public crate-gdeflate-0.3 (crate (name "gdeflate") (vers "0.3.1") (deps (list (crate-dep (name "gdeflate-sys") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "1da8x7nbfryydd1z0di0jamskqp0axmabz9slsyk515qj63j3m7z") (features (quote (("default"))))))

(define-public crate-gdeflate-0.4 (crate (name "gdeflate") (vers "0.4.0") (deps (list (crate-dep (name "gdeflate-sys") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "0jjmjimjlpy7q3jpgs79jv55q74al5ky0brj9h6ybbl4wqgki059") (features (quote (("default"))))))

(define-public crate-gdeflate-sys-0.3 (crate (name "gdeflate-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.90") (default-features #t) (kind 1)))) (hash "1kw202vvd8wc1hm2ad9r17si8kd2l2bkvf10vy6jkgbflbm9m2iq")))

