(define-module (crates-io gd l-) #:use-module (crates-io))

(define-public crate-gdl-parser-0.0.1 (crate (name "gdl-parser") (vers "0.0.1") (deps (list (crate-dep (name "peg") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "04v1k3h0w8vq89wy0cpr3004r3hzj2pgiz08i2y2p8g96swp99p8")))

(define-public crate-gdl-parser-0.0.2 (crate (name "gdl-parser") (vers "0.0.2") (deps (list (crate-dep (name "peg") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1hxzlfswfrs93j6h79jhs3dj71wdchpw9jvdlbdx7yxjabv2lhxd")))

(define-public crate-gdl-parser-0.0.3 (crate (name "gdl-parser") (vers "0.0.3") (deps (list (crate-dep (name "peg") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "0n9ra52zdgy3p6h6vjb1s65pr7pf57570wrml8rv9y8kcvrhdzry")))

(define-public crate-gdl-parser-0.0.4 (crate (name "gdl-parser") (vers "0.0.4") (deps (list (crate-dep (name "peg") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1rbfysnnqibrfp9gp2c393bpj09qwdn1xsjal6g9vpjld8w7xaj5")))

(define-public crate-gdl-parser-0.0.5 (crate (name "gdl-parser") (vers "0.0.5") (deps (list (crate-dep (name "peg") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "0988rf4mhz77jdqyr42s0xj9qz9dqignph1fcj2b8kgvj10asli3")))

(define-public crate-gdl-parser-0.0.6 (crate (name "gdl-parser") (vers "0.0.6") (deps (list (crate-dep (name "peg") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1kgzdqx5h7pk8k888ymyjxkh9c8mq8lgifwiz43nv28a4mny6k2v")))

(define-public crate-gdl-parser-0.0.7 (crate (name "gdl-parser") (vers "0.0.7") (deps (list (crate-dep (name "peg") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1qvqgcq260ngp72wxqvvh539cww6al1d84blybzc56kpj05rymak")))

(define-public crate-gdl-parser-0.0.8 (crate (name "gdl-parser") (vers "0.0.8") (deps (list (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "04qfdc80p43daw9ybnham1s33fsahz5z48rgd82b9gdwz24k9z71")))

