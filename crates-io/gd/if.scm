(define-module (crates-io gd if) #:use-module (crates-io))

(define-public crate-gdiff-0.0.1 (crate (name "gdiff") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.34") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.18.2") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.13.0") (features (quote ("crossterm"))) (kind 0)))) (hash "0g719q181zn0hq1fdmfgsy6q1qf2x21bv67r1dlx9jyz20z63sw6")))

