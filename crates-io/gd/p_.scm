(define-module (crates-io gd p_) #:use-module (crates-io))

(define-public crate-gdp_rs-0.0.1 (crate (name "gdp_rs") (vers "0.0.1") (deps (list (crate-dep (name "frunk_core") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0qx8w4mllc4gmjykinyha8qismq5k3ywfrbkqp7ra9wbk7d1qzc6")))

(define-public crate-gdp_rs-0.0.2 (crate (name "gdp_rs") (vers "0.0.2") (deps (list (crate-dep (name "frunk_core") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "065yf743w8ivaps8dpq1fws2pr99912rkyvqmwx113xr373q31fw")))

(define-public crate-gdp_rs-0.0.3 (crate (name "gdp_rs") (vers "0.0.3") (deps (list (crate-dep (name "frunk_core") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1djxrlfwxn8li26m4489axjpwy12nah75i4j33652nz078jdblvv")))

(define-public crate-gdp_rs-0.1 (crate (name "gdp_rs") (vers "0.1.0") (deps (list (crate-dep (name "either") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "frunk_core") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.166") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.41") (default-features #t) (kind 0)))) (hash "1l58l0ph4qs70wms1dfr87vpiqis8jvgpk4sbxbkvsvnsqvr5zy8") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-gdp_rs-0.1 (crate (name "gdp_rs") (vers "0.1.1") (deps (list (crate-dep (name "either") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "frunk_core") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.166") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.41") (default-features #t) (kind 0)))) (hash "093fd484i8izpp0755xccmjf8v6bnm74md1nf83pw40mw1jkfsqs") (v 2) (features2 (quote (("serde" "dep:serde"))))))

