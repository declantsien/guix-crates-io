(define-module (crates-io gd va) #:use-module (crates-io))

(define-public crate-gdvariants-0.1 (crate (name "gdvariants") (vers "0.1.0-pre.0") (deps (list (crate-dep (name "gdnative") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1kxjssik821x5gcjw1d3mxgrzmgcm16qy9whzb9yhm1m3pbynh05") (yanked #t)))

(define-public crate-gdvariants-0.1 (crate (name "gdvariants") (vers "0.1.0-pre.1") (deps (list (crate-dep (name "gdnative") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0c3qx8z0qrvmxabygvs4fr363y0xg5527ykag9dvgvk50wswrahq") (yanked #t)))

(define-public crate-gdvariants-0.1 (crate (name "gdvariants") (vers "0.1.0-pre.2") (deps (list (crate-dep (name "gdnative") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0jzk4xs7z73klcf68vs80pj9xpwhb11542b179kyf5lfcyqaw35d") (yanked #t)))

(define-public crate-gdvariants-0.1 (crate (name "gdvariants") (vers "0.1.0") (deps (list (crate-dep (name "gdnative") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1i232c6dn53kpbj7saf9zb4qarx5c5pbmzaiad8d5d17l60mycm3")))

(define-public crate-gdvariants-0.2 (crate (name "gdvariants") (vers "0.2.0") (deps (list (crate-dep (name "gdnative") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "12dzw7laba4rsvfwivy14jcvfzlsdam3v10f5q5cw2p7qrg85315")))

(define-public crate-gdvariants-0.3 (crate (name "gdvariants") (vers "0.3.0") (deps (list (crate-dep (name "gdnative") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "11a3wksx3f6jmkaqv1lz3q360mib406ipkvhaqk51yra2r92l50h")))

(define-public crate-gdvariants-0.4 (crate (name "gdvariants") (vers "0.4.0") (deps (list (crate-dep (name "gdnative") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "13dncng618jqbxjffp7dfh1ncz8v0vnqkp0g1vg9m9mi3yxz9fbw")))

(define-public crate-gdvariants-0.5 (crate (name "gdvariants") (vers "0.5.0") (deps (list (crate-dep (name "gdnative") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "11rl53yljf7rzwk18wdxhyi86x6v1j2q0x470sga7m8pfwag532l")))

(define-public crate-gdvariants-0.6 (crate (name "gdvariants") (vers "0.6.0") (deps (list (crate-dep (name "gdnative") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "03q6imr3ifk5289s31d1zfphmgsm3c69ggb92nrx8mx2nhvb86bg")))

(define-public crate-gdvariants-0.7 (crate (name "gdvariants") (vers "0.7.0") (deps (list (crate-dep (name "gdnative") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1pzhaqdp435jbnbd4sf5iq2b25rvsbxfk1z5rpx6hzqi0d44bmdc")))

(define-public crate-gdvariants-1 (crate (name "gdvariants") (vers "1.0.0") (deps (list (crate-dep (name "gdnative") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0zwzj94wr0wkz7pzrm98j4n43hvz5p48jdl2prwdryzf4b1k1bb3")))

(define-public crate-gdvariants-1 (crate (name "gdvariants") (vers "1.1.0") (deps (list (crate-dep (name "gdnative") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0j9j67mlfimbxggwcgsa6xr62gxw144hydbr0mwnxwdqw79pynpx") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

