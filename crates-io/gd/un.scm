(define-module (crates-io gd un) #:use-module (crates-io))

(define-public crate-gdunsafe-0.1 (crate (name "gdunsafe") (vers "0.1.0") (deps (list (crate-dep (name "gdnative") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 2)))) (hash "055m4y6g6s4w142ikw59ynqznix5nbnkfslgfrdhllna4f16pns9")))

