(define-module (crates-io gd ht) #:use-module (crates-io))

(define-public crate-gdhttp-1 (crate (name "gdhttp") (vers "1.0.0") (hash "0wszvw0cnkn1fdvp7jwr8mfrzhdcl1q0xk94bsys095aj8di6aqn")))

(define-public crate-gdhttp-1 (crate (name "gdhttp") (vers "1.0.1") (hash "133ibph2b5i30pnibmn2ayqllbfrjg1sy9pnjzkwddv1llnykp93")))

(define-public crate-gdhttp-1 (crate (name "gdhttp") (vers "1.0.2") (hash "1zkrcjmrirwvq7pjf4r00i5qdfrjm3ka040x4sya20y4whmp6ms8")))

(define-public crate-gdhttp-1 (crate (name "gdhttp") (vers "1.0.3") (hash "1ks6zk062ycvb2v370aqjn7qrk28hydn332wkw6l2416jcxfrnzl")))

