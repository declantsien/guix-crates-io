(define-module (crates-io gd lo) #:use-module (crates-io))

(define-public crate-gdlogue-0.1 (crate (name "gdlogue") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^4.1.0") (default-features #t) (kind 0)))) (hash "1vp064dc90ycx0jprc1r2asvckagy58i9i78g2lw83h8pzsjy01z")))

