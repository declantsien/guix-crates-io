(define-module (crates-io gd b_) #:use-module (crates-io))

(define-public crate-gdb_breakpoint-0.1 (crate (name "gdb_breakpoint") (vers "0.1.0") (deps (list (crate-dep (name "nix") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "0k8rnsqcfgr5f6hgcg9fqdybxm4k3nw5f8bvqkljrxrk595826jn")))

(define-public crate-gdb_breakpoint-0.1 (crate (name "gdb_breakpoint") (vers "0.1.1") (deps (list (crate-dep (name "nix") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "10pfyrjq6x26ahqggy7316483bq9nivrxq6gjah5pllk8jhm6ga5")))

(define-public crate-gdb_breakpoint-0.1 (crate (name "gdb_breakpoint") (vers "0.1.2") (deps (list (crate-dep (name "nix") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "0vphc9wicbzfg9rv48gcg9by54fk9cgp778ic70yaljxall08crx")))

(define-public crate-gdb_breakpoint-0.1 (crate (name "gdb_breakpoint") (vers "0.1.3") (deps (list (crate-dep (name "nix") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "0d87xi3l5yki3wfdb0f8n7681h3ny7jyfw2wygkzl3xkq0b75lrn")))

(define-public crate-gdb_breakpoint-0.1 (crate (name "gdb_breakpoint") (vers "0.1.4") (deps (list (crate-dep (name "nix") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "0xnv98na4ll014smylz65gg1nbwxz6dx93npkcv54kmscmfksc9f")))

(define-public crate-gdb_breakpoint-0.1 (crate (name "gdb_breakpoint") (vers "0.1.5") (deps (list (crate-dep (name "nix") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "18qd08dyr51ljpi3h0jc9mr2q5b5sz54kkss67bhxg14wha19nhl")))

(define-public crate-gdb_breakpoint-0.1 (crate (name "gdb_breakpoint") (vers "0.1.6") (deps (list (crate-dep (name "nix") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "10gaf22ixrfwfv8hdy0gza71cbvc4r8v9jz492f1ciwxw4gshyj7")))

(define-public crate-gdb_breakpoint-0.1 (crate (name "gdb_breakpoint") (vers "0.1.7") (deps (list (crate-dep (name "nix") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "0qmw3zxsi3d0sp1p1s1wlxdj8pl46zz0hwnsyc6q6gaghgr7nnha")))

(define-public crate-gdb_breakpoint-0.1 (crate (name "gdb_breakpoint") (vers "0.1.8") (deps (list (crate-dep (name "nix") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "0yz1k0n6b1y2ycx27wsc1nmp6zwi9a62llf2hzwlhsxfkhm3cx8h")))

(define-public crate-gdb_breakpoint-0.1 (crate (name "gdb_breakpoint") (vers "0.1.9") (deps (list (crate-dep (name "nix") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "18a6rrkd35yhnsk9gi7xm3df7m747ky856d8a19nx4ck20xy7vxa")))

(define-public crate-gdb_breakpoint-0.2 (crate (name "gdb_breakpoint") (vers "0.2.0") (deps (list (crate-dep (name "nix") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "018hgn8iwki6q1f06xr91f1j95yxqcbfwi7hvyasi27rc0wwqbba")))

(define-public crate-gdb_mi-0.1 (crate (name "gdb_mi") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "0.3.*") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "0.11.*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "0.3.*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.*.*") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "1.*.*") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "1.*.*") (default-features #t) (kind 0)))) (hash "0rs8xnilxi4kzx43g6knkc6k29cijwkjb5567fvrx0qx1b6riww5")))

(define-public crate-gdb_mi-0.1 (crate (name "gdb_mi") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "0.4.*") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "0.11.*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "0.3.*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.*.*") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "1.*.*") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "1.*.*") (default-features #t) (kind 0)))) (hash "1z3674x7g6bq1hl1xa98pnfsspphwgcaacqsl6bwg463zxd1zpb8")))

(define-public crate-gdb_mi-0.1 (crate (name "gdb_mi") (vers "0.1.2") (deps (list (crate-dep (name "env_logger") (req "0.4.*") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "0.11.*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "0.3.*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.*.*") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "1.*.*") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "1.*.*") (default-features #t) (kind 0)))) (hash "0rx60ymc8l207mf1d4wbd5nb2wsddy5gnf5zysbq38vbqx0lx9sc")))

(define-public crate-gdb_mi-0.1 (crate (name "gdb_mi") (vers "0.1.3") (deps (list (crate-dep (name "env_logger") (req "0.4.*") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "0.11.*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "0.3.*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.*.*") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "1.*.*") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "1.*.*") (default-features #t) (kind 0)))) (hash "0l5adihbvb0zhscb5847zcf8b9j4bcn0cb2z520raa9yp9ll08rr")))

(define-public crate-gdb_probe-0.1 (crate (name "gdb_probe") (vers "0.1.0") (deps (list (crate-dep (name "nix") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "0s0pzqncpjgxp44giryhvi8wzci4xvdsm7q7f77sndi7w5l9l73q")))

(define-public crate-gdb_probe-0.1 (crate (name "gdb_probe") (vers "0.1.1") (deps (list (crate-dep (name "nix") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "1krv5zf10c3q7b9gb52ah97pxidhfavjnpcxbhsdfp85zz2ra3bn")))

(define-public crate-gdb_probe-0.1 (crate (name "gdb_probe") (vers "0.1.2") (deps (list (crate-dep (name "nix") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "0cmy02iyhc0hclbz1qlab44220jrflz4rd1w27w283l5did62cg2")))

