(define-module (crates-io gd bg) #:use-module (crates-io))

(define-public crate-gdbg-0.0.0 (crate (name "gdbg") (vers "0.0.0") (hash "0v8kzczjd2rd5h5fq49vp5i11zqr8c2lyskm04y3w74csjfbf3g9")))

(define-public crate-gdbg-0.0.1 (crate (name "gdbg") (vers "0.0.1") (hash "0iy3r8q2j9wai89i4arf52p3ah5d4mn820gd1p5dc67mrafwc79n")))

(define-public crate-gdbg-0.0.2 (crate (name "gdbg") (vers "0.0.2") (hash "06srd28axj7w66pvljqjq1z1gllxjlm1p20yznvj3khnmby4c0jq")))

(define-public crate-gdbg-0.1 (crate (name "gdbg") (vers "0.1.0") (hash "1yblwbncahn9ba97jp5i1sxhs693c42wwmgkayg72lc15yz4shcm")))

(define-public crate-gdbg-0.1 (crate (name "gdbg") (vers "0.1.1") (hash "1ajidv3wrqgb5g0j6swhsqmi0wa5gdppz4l2c092z6l0c413fgvc") (features (quote (("std"))))))

(define-public crate-gdbg-0.1 (crate (name "gdbg") (vers "0.1.2") (hash "091lv2s0cx7cxsp6m22k21xmv746qvh7sdn5h5f8wz1yw54wf2bi") (features (quote (("std"))))))

