(define-module (crates-io gd il) #:use-module (crates-io))

(define-public crate-gdilib-rs-0.1 (crate (name "gdilib-rs") (vers "0.1.0") (deps (list (crate-dep (name "random-number") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("winuser"))) (default-features #t) (kind 0)))) (hash "0pj7fiwgr1r9lzm59jg08slb1sirby2006gn1szxl9wvns1wplq8") (yanked #t)))

(define-public crate-gdilib-rs-0.1 (crate (name "gdilib-rs") (vers "0.1.1") (deps (list (crate-dep (name "random-number") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("winuser"))) (default-features #t) (kind 0)))) (hash "0qv5lii0p78yfcdni6v36645qnfldrlbd74nb48gzjxq976bc3qk") (yanked #t)))

(define-public crate-gdilib-rs-0.1 (crate (name "gdilib-rs") (vers "0.1.2") (deps (list (crate-dep (name "random-number") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("winuser"))) (default-features #t) (kind 0)))) (hash "0z3c5yk38lrsm9jj2krf0khnhnh6kqxnsibyig8y02w7inb2c4v3")))

(define-public crate-gdilib-rs-0.1 (crate (name "gdilib-rs") (vers "0.1.3") (deps (list (crate-dep (name "random-number") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("winuser"))) (default-features #t) (kind 0)))) (hash "07hak11mwfm1kbkaqg3yvcnar6gk4lnp2yb9wh0k981iv4jqk07g")))

