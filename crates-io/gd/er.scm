(define-module (crates-io gd er) #:use-module (crates-io))

(define-public crate-gderu-0.1 (crate (name "gderu") (vers "0.1.0") (hash "0n6rk1b9y61jf2355ci5ias4nf0xfrzj2imczl0lq7apn74rw94q")))

(define-public crate-gderu-0.2 (crate (name "gderu") (vers "0.2.0") (hash "0173xizkwaj491mhaar7p1n25a5afd95vcv73xrw7xi5a54hhqbp")))

