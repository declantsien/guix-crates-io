(define-module (crates-io ua vc) #:use-module (crates-io))

(define-public crate-uavcan-0.0.1 (crate (name "uavcan") (vers "0.0.1") (deps (list (crate-dep (name "uavcan-core") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0jdzpq5pqq3xx6pbcm7sv7912icy5ivqn65iici29wv9hmp59z9a")))

(define-public crate-uavcan-0.1 (crate (name "uavcan") (vers "0.1.0-preview0") (deps (list (crate-dep (name "bit_field") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "embedded_types") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "half") (req "^1.0.0") (kind 0)) (crate-dep (name "uavcan-derive") (req "^0.1.0-preview0") (default-features #t) (kind 0)) (crate-dep (name "ux") (req "^0.0.1") (kind 0)))) (hash "1rwgdgzcy485sc8ch842dvp5bf9kvw5n0kpak0l1748mggx8mmhy") (features (quote (("std" "ux/std" "half/std") ("default"))))))

(define-public crate-uavcan-core-0.0.1 (crate (name "uavcan-core") (vers "0.0.1") (deps (list (crate-dep (name "bit_field") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "uavcan-derive") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0nqrmv93zg3wymfj6jygfyw7jzzirxgdlxi9wfmx8ymkqv6gm58f")))

(define-public crate-uavcan-derive-0.0.1 (crate (name "uavcan-derive") (vers "0.0.1") (deps (list (crate-dep (name "quote") (req "^0.3.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.10.5") (default-features #t) (kind 0)))) (hash "0n43y4xhb1x5bqchc80narsxb1q4mqgzrv34s70wj7gsiwszs36h")))

(define-public crate-uavcan-derive-0.1 (crate (name "uavcan-derive") (vers "0.1.0-preview0") (deps (list (crate-dep (name "quote") (req "^0.3.10") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)))) (hash "149qc827piq8psrbzjpvkc3pp48bi10hfbpxfggcaj7b1zkwik83")))

