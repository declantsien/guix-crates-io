(define-module (crates-io ua bs) #:use-module (crates-io))

(define-public crate-uabs-1 (crate (name "uabs") (vers "1.0.0") (deps (list (crate-dep (name "signrel") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1qppax9qp7qz6vz9rf7qks0k0rc22aq1w89xl0c621qxf9y2yfxf")))

(define-public crate-uabs-2 (crate (name "uabs") (vers "2.0.0") (deps (list (crate-dep (name "signrel") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "13amml8dd3i1704s97s3wr7xayh2ny6jx9g7b6hr0v0ccxr54l06")))

(define-public crate-uabs-3 (crate (name "uabs") (vers "3.0.0") (deps (list (crate-dep (name "signrel") (req "^2.0") (default-features #t) (kind 0)))) (hash "1hm8gqs5j4k3fddli5sjd6261lk5147j7fl2ba6fwr54933igg0g")))

