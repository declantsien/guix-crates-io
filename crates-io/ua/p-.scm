(define-module (crates-io ua p-) #:use-module (crates-io))

(define-public crate-uap-rs-0.1 (crate (name "uap-rs") (vers "0.1.0") (deps (list (crate-dep (name "onig") (req "^4.3.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.88") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.88") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "168dl6bmhn5892vndpjvgmxvandjv3g33drgkwsq4rcr5qg2kimm") (yanked #t)))

(define-public crate-uap-rs-0.2 (crate (name "uap-rs") (vers "0.2.0") (deps (list (crate-dep (name "derive_more") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "onig") (req "^4.3.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.88") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.88") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "0qj5934wc2dnw64qdy5bq9xzi2isyn2wci5g38h2y11wpl8vazvi") (yanked #t)))

(define-public crate-uap-rs-0.2 (crate (name "uap-rs") (vers "0.2.1") (deps (list (crate-dep (name "derive_more") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "onig") (req "^4.3.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.88") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.88") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "04wjz1asm8888ywpdl96gy6z4qmjn69xhcjxwydqpnkfs9lj3f3w") (yanked #t)))

(define-public crate-uap-rs-0.2 (crate (name "uap-rs") (vers "0.2.2") (deps (list (crate-dep (name "derive_more") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "onig") (req "^4.3.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.88") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.88") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "0v2azy4fsjlkmw4jqpclhj4mqxgb7pcimvirv08825qin2lpl60i") (yanked #t)))

(define-public crate-uap-rs-0.0.0 (crate (name "uap-rs") (vers "0.0.0") (hash "1j4j4908nwnng3c7w54vswslw30gz01h0x9xjnm8piwc677lx68q")))

(define-public crate-uap-rust-0.0.1 (crate (name "uap-rust") (vers "0.0.1") (deps (list (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.3") (default-features #t) (kind 0)))) (hash "0fy5lnpx5wvha2y0bp39n3y912w8qwnc2lvvi2cpghxqax7887z3")))

(define-public crate-uap-rust-0.0.2 (crate (name "uap-rust") (vers "0.0.2") (deps (list (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.3") (default-features #t) (kind 0)))) (hash "1ha5c7lzdvk2h7097ryh9wiiiy8awwivl01kk1k7mfa6zakxx885")))

(define-public crate-uap-rust-0.0.3 (crate (name "uap-rust") (vers "0.0.3") (deps (list (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.3") (default-features #t) (kind 0)))) (hash "1srflkr6dimdmdq1b29vs5zdcf8ahrql5qvlnzqvg8q5sjy94iwc")))

(define-public crate-uap-rust-0.0.4 (crate (name "uap-rust") (vers "0.0.4") (deps (list (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.3") (default-features #t) (kind 0)))) (hash "0v8d89d5f5b8nzfmmpcydwbyqm5v52v1nd8hiwcxwbd2917s539f")))

