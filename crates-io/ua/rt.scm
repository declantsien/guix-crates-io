(define-module (crates-io ua rt) #:use-module (crates-io))

(define-public crate-uart-0.1 (crate (name "uart") (vers "0.1.0") (deps (list (crate-dep (name "bit_field") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "1j16arv7zhjds2xr6dnpjb9jx8vggfwwwpr6pxxr4fh6ca4wm4j7") (yanked #t)))

(define-public crate-uart-0.1 (crate (name "uart") (vers "0.1.1") (deps (list (crate-dep (name "bit_field") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "07v69fif2dyn3lcirjvdllyjni0xpnni7fyxfwq3mgkxgw4fm249") (yanked #t)))

(define-public crate-uart-0.1 (crate (name "uart") (vers "0.1.2") (deps (list (crate-dep (name "bit_field") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "02g3lfpvsr955vi62nsg1vivms868sw1nfycbcjj32yqgpq7qyfg")))

(define-public crate-uart16550-0.0.0 (crate (name "uart16550") (vers "0.0.0") (hash "1zvhmkn9r8q477lyhk0a4mc9yzx2in06k2hb0qm55rphd5nl3p4r")))

(define-public crate-uart16550-0.0.1 (crate (name "uart16550") (vers "0.0.1") (hash "16zvpw8ff75n66ra5g5igwwc080nxkr0dzd8zhzgw5fqraf6z7wk")))

(define-public crate-uart8250-0.1 (crate (name "uart8250") (vers "0.1.0") (deps (list (crate-dep (name "volatile-register") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ldvid4xcgikj39j83cy1xrjkd81lrgaxz9rq0xxxwd95zh7gyp8")))

(define-public crate-uart8250-0.2 (crate (name "uart8250") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "=1.0.0-alpha.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "volatile-register") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ss3asq5jxr26wvrvwra8zlszaaj1hncgjnvn5k00gkrc8cbpya9") (features (quote (("fmt") ("embedded" "embedded-hal" "nb") ("default"))))))

(define-public crate-uart8250-0.3 (crate (name "uart8250") (vers "0.3.0") (deps (list (crate-dep (name "embedded-hal") (req "=1.0.0-alpha.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "volatile-register") (req "^0.2") (default-features #t) (kind 0)))) (hash "1i3lgfilr6l92kw6bi91wa87kcymjxrd2jl61ny325dzrpvd897h") (features (quote (("fmt") ("embedded" "embedded-hal" "nb") ("default"))))))

(define-public crate-uart8250-0.3 (crate (name "uart8250") (vers "0.3.1") (deps (list (crate-dep (name "embedded-hal") (req "=1.0.0-alpha.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "volatile-register") (req "^0.2") (default-features #t) (kind 0)))) (hash "1jmg197j02c7ayzj9n65x0d9lp368ph60mk7abcamvh2wgg6jq00") (features (quote (("fmt") ("embedded" "embedded-hal" "nb") ("default"))))))

(define-public crate-uart8250-0.3 (crate (name "uart8250") (vers "0.3.2") (deps (list (crate-dep (name "embedded-hal") (req "=1.0.0-alpha.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "volatile-register") (req "^0.2") (default-features #t) (kind 0)))) (hash "0m0a87xxad591dvbajggs4y899l173cmv24zw8sqc26gjp2iqz3x") (features (quote (("fmt") ("embedded" "embedded-hal" "nb") ("default"))))))

(define-public crate-uart8250-0.4 (crate (name "uart8250") (vers "0.4.0") (deps (list (crate-dep (name "embedded-hal") (req "=1.0.0-alpha.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "volatile-register") (req "^0.2") (default-features #t) (kind 0)))) (hash "0dgv2jf4pxq7l1gy993279ng01m1k79ahsdrvkdjxmjqg12md9nq") (features (quote (("fmt") ("embedded" "embedded-hal" "nb") ("default"))))))

(define-public crate-uart8250-0.4 (crate (name "uart8250") (vers "0.4.1") (deps (list (crate-dep (name "embedded-hal") (req "=1.0.0-alpha.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "volatile-register") (req "^0.2") (default-features #t) (kind 0)))) (hash "12dibdmy62267vq7yylcmkf04vg1yl2gynq2psvnm2i4gg0wv70c") (features (quote (("fmt") ("embedded" "embedded-hal" "nb") ("default"))))))

(define-public crate-uart8250-0.4 (crate (name "uart8250") (vers "0.4.2") (deps (list (crate-dep (name "embedded-hal") (req "=1.0.0-alpha.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "volatile-register") (req "^0.2") (default-features #t) (kind 0)))) (hash "13d80h518s08mdzr4kp88lyv0hawbcb75b3rya6qw3gl5zhpfrs8") (features (quote (("fmt") ("embedded" "embedded-hal" "nb") ("default"))))))

(define-public crate-uart8250-0.5 (crate (name "uart8250") (vers "0.5.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "volatile-register") (req "^0.2") (default-features #t) (kind 0)))) (hash "140vacv5m0wmlbxk028spkxp1ca5f0n2s2042v2j43xyih17d7gp") (features (quote (("fmt") ("default"))))))

(define-public crate-uart8250-0.6 (crate (name "uart8250") (vers "0.6.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "volatile-register") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ikfcp46drngr04jj525p39hjf8wavjfnds39xnl3q8qhkzj1xnh") (features (quote (("fmt") ("embedded" "embedded-hal" "nb") ("default"))))))

(define-public crate-uart_16550-0.1 (crate (name "uart_16550") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.2.0-alpha-019") (default-features #t) (kind 0)))) (hash "0vlnrim7w68g5rg3nz4w845d30ry99xrqn2w0ry6y8p3ilyrb7r6")))

(define-public crate-uart_16550-0.2 (crate (name "uart_16550") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "1c4m9lrp2rdni9v4hk6ny5q8flji9zp1a2jfybw3pzii17v954sv")))

(define-public crate-uart_16550-0.2 (crate (name "uart_16550") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "12x52bgcgp5lgikarchgmfz7vzkrvp96da2plv0k5frdc35shgl0")))

(define-public crate-uart_16550-0.2 (crate (name "uart_16550") (vers "0.2.2") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0czb8cvz3vmbbx893ysndcvw3xpn8x5rrkn4x0v19sqvzz40pz2q")))

(define-public crate-uart_16550-0.2 (crate (name "uart_16550") (vers "0.2.3") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.9") (default-features #t) (kind 0)))) (hash "0wzyjd6p7rn8qk3r150apak9dlkif8n4g95f5140pcn7p7s7kmam")))

(define-public crate-uart_16550-0.2 (crate (name "uart_16550") (vers "0.2.4") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.9.3") (kind 0)))) (hash "0cbi7mpzz66bmjw8rg1843arlli6883f3bbqbg0zpc42rcq0yjyl") (features (quote (("stable" "x86_64/stable") ("nightly" "x86_64/nightly") ("default" "nightly"))))))

(define-public crate-uart_16550-0.2 (crate (name "uart_16550") (vers "0.2.5") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.9.3") (kind 0)))) (hash "0lmfyzglgqnjd25d5nbfhc1cy1mp3xszqshsyaiwjgl7f20ni0z9") (features (quote (("stable" "x86_64/stable") ("nightly" "x86_64/nightly") ("default" "nightly"))))))

(define-public crate-uart_16550-0.2 (crate (name "uart_16550") (vers "0.2.6") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.10.2") (kind 0)))) (hash "15xiwkh0flhm51k73iqqbcprqvc71m4r9f8agfjilpmmg5xmwpib") (features (quote (("stable" "x86_64/stable") ("nightly" "x86_64/nightly") ("default" "nightly"))))))

(define-public crate-uart_16550-0.2 (crate (name "uart_16550") (vers "0.2.7") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.11.0") (kind 0)))) (hash "1sfzyf91wpm0h1d29yr416b4kar15z5dhyqakgy689kiq46w93z5") (features (quote (("stable" "x86_64/stable") ("nightly" "x86_64/nightly") ("default" "nightly"))))))

(define-public crate-uart_16550-0.2 (crate (name "uart_16550") (vers "0.2.8") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.12.1") (features (quote ("instructions"))) (kind 0)))) (hash "0indvzhdbcdi7ihdsq7dy431pwhzjbvswh34zijc07labriaijvl") (features (quote (("stable" "x86_64/external_asm") ("nightly" "x86_64/nightly") ("default" "x86_64/default"))))))

(define-public crate-uart_16550-0.2 (crate (name "uart_16550") (vers "0.2.9") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.12.2") (features (quote ("instructions"))) (kind 0)))) (hash "0gbn78fqsmss7hljsc4hvfyv2fdl75h12x3r5xvzijqmjacpfxy0") (features (quote (("stable" "x86_64/external_asm") ("nightly" "x86_64/nightly") ("default" "x86_64/default"))))))

(define-public crate-uart_16550-0.2 (crate (name "uart_16550") (vers "0.2.10") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.12.2") (features (quote ("instructions"))) (kind 0)))) (hash "037pmj9sydm2ck7fsqv5gklcf1q5khgxmzlcxg2bzb68lhb0pc5g") (features (quote (("stable" "x86_64/external_asm") ("nightly" "x86_64/nightly") ("default" "nightly"))))))

(define-public crate-uart_16550-0.2 (crate (name "uart_16550") (vers "0.2.11") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.12.2") (features (quote ("instructions"))) (kind 0)))) (hash "15xy71wqnkfhc9vja4i181b1w76163hd82662xhscnllfzwcvf53") (features (quote (("stable" "x86_64/external_asm") ("nightly" "x86_64/nightly") ("default" "nightly"))))))

(define-public crate-uart_16550-0.2 (crate (name "uart_16550") (vers "0.2.12") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.13.2") (features (quote ("instructions"))) (kind 0)))) (hash "1nbnp7sd5cqga537jnwkkll5ahb0s4v3qayh6myvmj8x334s6icd") (features (quote (("stable" "x86_64/external_asm") ("nightly" "x86_64/nightly") ("default" "nightly"))))))

(define-public crate-uart_16550-0.2 (crate (name "uart_16550") (vers "0.2.13") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.14.0") (features (quote ("instructions"))) (kind 0)))) (hash "0yr6jhvbw5wnpf8inhddny87r4mskcwdwzlxym0ampmvkna376p0") (features (quote (("stable" "x86_64/external_asm") ("nightly" "x86_64/inline_asm") ("default" "nightly"))))))

(define-public crate-uart_16550-0.2 (crate (name "uart_16550") (vers "0.2.14") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.14.0") (features (quote ("instructions"))) (kind 0)))) (hash "1p85aklqmcl14yr8v9ljgic0jsqpq0hd2qp6hmwsinl2dl76qfjh") (features (quote (("stable" "x86_64/external_asm") ("nightly" "x86_64/inline_asm") ("default" "nightly"))))))

(define-public crate-uart_16550-0.2 (crate (name "uart_16550") (vers "0.2.15") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.14.0") (features (quote ("instructions"))) (target "cfg(target_arch = \"x86_64\")") (kind 0)))) (hash "0x7k8kc3j5yvh54kk0clchbz6z6qkhjnlvvdwvzzhpzgh2a03bb5") (features (quote (("stable" "x86_64/external_asm") ("nightly" "x86_64/inline_asm") ("default" "nightly"))))))

(define-public crate-uart_16550-0.2 (crate (name "uart_16550") (vers "0.2.16") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.14.0") (features (quote ("instructions"))) (target "cfg(target_arch = \"x86_64\")") (kind 0)))) (hash "0nya1ds2xyg6351xmyb5i4n8zl9gj601yf4qc7vb1iskka5490dg") (features (quote (("stable" "x86_64/external_asm") ("nightly" "x86_64/inline_asm") ("default" "nightly"))))))

(define-public crate-uart_16550-0.2 (crate (name "uart_16550") (vers "0.2.17") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.14.0") (features (quote ("instructions"))) (target "cfg(target_arch = \"x86_64\")") (kind 0)))) (hash "109jrzad0fffz3z5qdbnm2dkdy6r44f6kdb5b2dc7a9qpdfkj5in") (features (quote (("stable" "x86_64/external_asm") ("nightly" "x86_64/inline_asm") ("default" "nightly"))))))

(define-public crate-uart_16550-0.2 (crate (name "uart_16550") (vers "0.2.18") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.14.9") (features (quote ("instructions"))) (target "cfg(target_arch = \"x86_64\")") (kind 0)))) (hash "1nb32gf757z6byvfcj5r3gvmlqjisklc0af5fkfrx55d029ynx5h") (features (quote (("stable") ("nightly") ("default"))))))

(define-public crate-uart_16550-0.2 (crate (name "uart_16550") (vers "0.2.19") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.14.9") (features (quote ("instructions"))) (target "cfg(target_arch = \"x86_64\")") (kind 0)))) (hash "0bmbair385wl8cqgd2zdbk8bl2wpi9cnh8kjfi1vvm40g2lg4kv1") (features (quote (("stable") ("nightly") ("default"))))))

(define-public crate-uart_16550-0.3 (crate (name "uart_16550") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86") (req "^0.52") (default-features #t) (target "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (kind 0)))) (hash "11w6nzi7ila0d4w05b02368gkny4x4splfc5gks72v3gg5209h3d") (features (quote (("stable") ("nightly") ("default"))))))

(define-public crate-uart_xilinx-0.1 (crate (name "uart_xilinx") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "volatile-register") (req "^0.2") (default-features #t) (kind 0)))) (hash "0zniv0bac4ch7qw0ijninps38h7dmrisc0qqbqm17agm4i4x6p9q") (features (quote (("fmt") ("default"))))))

(define-public crate-uart_xilinx-0.2 (crate (name "uart_xilinx") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "volatile-register") (req "^0.2") (default-features #t) (kind 0)))) (hash "0akz8s3lrm4cycmdmm5apf06gjqk5vw2kpyiw2cpv5qlzf99sknc") (features (quote (("fmt") ("default"))))))

