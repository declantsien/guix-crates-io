(define-module (crates-io ak ka) #:use-module (crates-io))

(define-public crate-akkariin-0.1 (crate (name "akkariin") (vers "0.1.0") (hash "1njbj032v210x2qppqlc7a6zsfai40iihp7n4vm10l5b8262nwhy")))

(define-public crate-akkarin-0.1 (crate (name "akkarin") (vers "0.1.0") (hash "1k7q922b4s4k03c25sc79czlbjydvfdvjjxwcxyr6idip4ajjngv")))

