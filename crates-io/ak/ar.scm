(define-module (crates-io ak ar) #:use-module (crates-io))

(define-public crate-akari-0.1 (crate (name "akari") (vers "0.1.0") (hash "1vp0sid0s1mpjqlq09y5lbd6qprapd62gr6jbzi464kl7a1kjr4s")))

(define-public crate-akarin-0.1 (crate (name "akarin") (vers "0.1.0") (hash "19lia57p336lnv8wgris9b8fsi1swld7zaicklsvb23549gl0g0q")))

