(define-module (crates-io ak af) #:use-module (crates-io))

(define-public crate-akafugu_twidisplay-0.1 (crate (name "akafugu_twidisplay") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.2") (default-features #t) (kind 2)))) (hash "1xlbgycyhpabbr5idbk6fxvs6jndgn2r3fbncv87n90p91arm255")))

