(define-module (crates-io ak _m) #:use-module (crates-io))

(define-public crate-ak_macros-0.1 (crate (name "ak_macros") (vers "0.1.0") (deps (list (crate-dep (name "crypto") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "08fj6wj7510nnmci53c371wpk0zk9ivp7isy6a7f9ib4w6kx782p") (yanked #t)))

(define-public crate-ak_macros-0.1 (crate (name "ak_macros") (vers "0.1.1") (deps (list (crate-dep (name "crypto") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "191vyz6s6gpwxr6fi7c655igv4awffsc5xp32b43qx93nzxlbgrb") (yanked #t)))

(define-public crate-ak_macros-0.1 (crate (name "ak_macros") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "11m325nw1hz728lj96sdjbpfggfwblm51rvsbbbgark9w3d4gdvr") (yanked #t)))

(define-public crate-ak_macros-0.1 (crate (name "ak_macros") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "104zk28xb5jwl44sjm0fm9hmzv6r8faclgmr0q2ncxvhik1gq91m") (yanked #t)))

(define-public crate-ak_macros-0.1 (crate (name "ak_macros") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "zip-extract") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0lw26dsyzvp8jb21syhvf0cnb85xjjkr1l6zgh0rf346fcd7n0z4") (yanked #t)))

(define-public crate-ak_macros-0.1 (crate (name "ak_macros") (vers "0.1.5") (hash "1c50x7c9jmvah1hn4dz6njai5sxr7msk507jazgds1m1affz6xv4") (yanked #t)))

(define-public crate-ak_macros-0.1 (crate (name "ak_macros") (vers "0.1.6") (hash "13r4gi8nrxvzr0h9c91inwxrnn29ypxbs1knx4lziijjcxn2vly1") (yanked #t)))

(define-public crate-ak_macros-0.1 (crate (name "ak_macros") (vers "0.1.7") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "zip-extract") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1zw6zk1n13n9lrxzjhxqd9m9p1m3d8v92s5cxq4zl9k6y1svymhg")))

(define-public crate-ak_macros-0.1 (crate (name "ak_macros") (vers "0.1.8") (hash "194q5ma56lq6ry9cgbfwyymsf6xj79dbrx0j1cyx2xlzs5z1jpnn")))

(define-public crate-ak_macros-0.1 (crate (name "ak_macros") (vers "0.1.9") (hash "0v3yifrrnh5saqp7ah386ki709b3gk2w6qpknswy17hjsl33agb7")))

(define-public crate-ak_macros-0.2 (crate (name "ak_macros") (vers "0.2.0") (hash "0jvlbdgqgwghnsfysh203f0a5mzw7y32bcij36lfw8s7zn4l84a7")))

(define-public crate-ak_macros-0.2 (crate (name "ak_macros") (vers "0.2.1") (hash "1rfm2av51sbi01l37x9rlagqcrnbbcj14mag6l5q3cqkb0x11br3")))

(define-public crate-ak_macros-0.2 (crate (name "ak_macros") (vers "0.2.2") (hash "0539jgbdzkz9hhqh5k132xxksxx8bb5ayn6c9f4xihjcssxa02c9")))

(define-public crate-ak_macros-0.2 (crate (name "ak_macros") (vers "0.2.3") (hash "07542mq2wk2kn4phbb6cpkz8djwxkczsq3r2fsgn1b94j8dih08q")))

(define-public crate-ak_macros-0.2 (crate (name "ak_macros") (vers "0.2.4") (hash "1jyayxa4i8s1y3ch9x7d3nqn884fv1091scbwk82wsik5qij65c0")))

(define-public crate-ak_macros-0.2 (crate (name "ak_macros") (vers "0.2.5") (hash "1w4j2j2lpclfxqzdq4n85ci64l55y77phy5b7yr0hwydzxn51cnm")))

(define-public crate-ak_macros-0.2 (crate (name "ak_macros") (vers "0.2.6") (hash "0fmnyscrznjny18bni6j24fwmhx57cfg6dp02h4fbp30i2zi2saq")))

(define-public crate-ak_macros-0.2 (crate (name "ak_macros") (vers "0.2.7") (hash "14487qdlzi3zlv57vkf4arrxwllsyx7y0d2qzywfrb99ik55d2y6")))

(define-public crate-ak_macros-0.2 (crate (name "ak_macros") (vers "0.2.8") (hash "1kgj44i22z525mh33j4kcjbfp4fx4fv2xl5pzcz0jjaj9w3bany0")))

