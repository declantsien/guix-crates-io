(define-module (crates-io ak ai) #:use-module (crates-io))

(define-public crate-akai-0.1 (crate (name "akai") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "1n7hrgcrwj3prl3zrng4pcfs5pb2l64ag1bb848wvlwd4izkhj7r")))

(define-public crate-akai-0.1 (crate (name "akai") (vers "0.1.1") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "0n87vwy57bbcxiyy5i9n1ff86rx9bl861gxa13lcqv06xwin4nkk")))

