(define-module (crates-io ak -r) #:use-module (crates-io))

(define-public crate-ak-rt-0.1 (crate (name "ak-rt") (vers "0.1.0") (hash "1nr54g5k3w92wqlslg738yzbiylqaksbb9ggmdy9gbkw46q2fbv8")))

(define-public crate-ak-rt-0.1 (crate (name "ak-rt") (vers "0.1.1") (deps (list (crate-dep (name "tokio") (req "^0.2.0-alpha.6") (default-features #t) (kind 0)) (crate-dep (name "tokio-executor") (req "^0.2.0-alpha.6") (default-features #t) (kind 0)))) (hash "0qq1330m2gbyzlnbcv2wf1cg1ffhhhrl4qp60ibgm2sfvl7i0da9")))

