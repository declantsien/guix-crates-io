(define-module (crates-io ak -c) #:use-module (crates-io))

(define-public crate-ak-codegen-0.1 (crate (name "ak-codegen") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1aq71l3rl6ky4nzhlaxc5z2vhhkg84f56fprqd0ap6hyxrdrmn3b")))

(define-public crate-ak-codegen-0.1 (crate (name "ak-codegen") (vers "0.1.1") (deps (list (crate-dep (name "darling") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "0ki234n7d3rhy8sd38552j2dhphhrdm1ki029xchnvl20lb36cbd")))

