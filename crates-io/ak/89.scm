(define-module (crates-io ak #{89}#) #:use-module (crates-io))

(define-public crate-ak8963-1 (crate (name "ak8963") (vers "1.0.0") (deps (list (crate-dep (name "byteorder") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "00mbjxj7y3jrv07c1irxmvdp3zjzbp02njf6zrlkc6k8n5vb6pi2")))

(define-public crate-ak8963-1 (crate (name "ak8963") (vers "1.0.1") (deps (list (crate-dep (name "byteorder") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "0sg8nfzyypi8ylrii4f6w8alnlfxl5q0dh2g9dinj8y02nz9qvyr")))

(define-public crate-ak8963-1 (crate (name "ak8963") (vers "1.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.12") (default-features #t) (kind 0)))) (hash "1wpa6zyi036libgwp7avbzgfd1g4ipqv35ax2b3xmjrhah45nbvh")))

