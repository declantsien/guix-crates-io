(define-module (crates-io ak sh) #:use-module (crates-io))

(define-public crate-akshually-0.1 (crate (name "akshually") (vers "0.1.0") (hash "0pijb4x0rxxad1r7m5hxkw254nw6cffdm5pf30xvkhs8ci0n0d1z") (yanked #t)))

(define-public crate-akshually-0.1 (crate (name "akshually") (vers "0.1.1") (hash "1arq3m6j2a4gqydxwrjycqm8gxjqa1rq56amv3n3s4d3p9pbal1l") (yanked #t)))

(define-public crate-akshually-0.1 (crate (name "akshually") (vers "0.1.2") (hash "180knbnplnwy0qzvgl2mkal3jnyxamfxf57lkac7hd38dvsirngz")))

(define-public crate-akshually-0.1 (crate (name "akshually") (vers "0.1.3") (hash "0i2ychjf3cqp4r0yxrwv26ia1ax0cnj7czddmvm385pwb20f5dqr")))

(define-public crate-akshually-0.2 (crate (name "akshually") (vers "0.2.0") (hash "0is1cymd6g2zks9q7zkbingswh7dyx6jalbq49h4injld4i6dik5")))

(define-public crate-akshually-0.2 (crate (name "akshually") (vers "0.2.1") (hash "1abf0svgbhl6j3nxgkzjx6rchyjnjbq06ddiaxn7awdpff07kaa7")))

(define-public crate-akshually-0.2 (crate (name "akshually") (vers "0.2.2") (hash "1jy0664lg830ni23ss0ln321dk1naq89g5h4q0m34zqr0a1gwaam")))

(define-public crate-akshually-0.2 (crate (name "akshually") (vers "0.2.3") (hash "0x3x00a1wsl88ilwzhmqilpazbkspcc9mh9f0hqws6xdmpnslx85")))

