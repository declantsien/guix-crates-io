(define-module (crates-io ak um) #:use-module (crates-io))

(define-public crate-akuma-0.1 (crate (name "akuma") (vers "0.1.0") (hash "1p7s9hxmn5dhbxln61k2gii5d488h32p94av919g5xsc7mazqx50")))

(define-public crate-akumi-0.1 (crate (name "akumi") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.9.6") (default-features #t) (kind 0)))) (hash "0spfah41927yvnbn06nabikw00wq0qbyp981aawh4i8lvw9v6a5b")))

