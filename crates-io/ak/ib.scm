(define-module (crates-io ak ib) #:use-module (crates-io))

(define-public crate-akibisuto-stylus-0.1 (crate (name "akibisuto-stylus") (vers "0.1.0") (deps (list (crate-dep (name "warp") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "17h14rp2sr5fla2wkwfa877b3knlrrgk5cfrsjdvg5mqnqzphr81") (features (quote (("default" "warp"))))))

(define-public crate-akibisuto-stylus-0.1 (crate (name "akibisuto-stylus") (vers "0.1.1") (deps (list (crate-dep (name "warp") (req "^0.2") (optional #t) (kind 0)))) (hash "0rxwfvg8jcnb7s8qi9mq3bidcg5ag9p9jz0qsannrb3j1jllblzw") (features (quote (("default" "warp"))))))

(define-public crate-akibisuto-stylus-0.1 (crate (name "akibisuto-stylus") (vers "0.1.2") (deps (list (crate-dep (name "warp") (req "^0.2") (optional #t) (kind 0)))) (hash "1awjdpil47cpz9ayy0yvicqxssrx4zq0jhgn62ih86kf835ica3h") (features (quote (("default" "warp"))))))

(define-public crate-akibisuto-stylus-0.1 (crate (name "akibisuto-stylus") (vers "0.1.3") (deps (list (crate-dep (name "warp") (req "^0.2") (optional #t) (kind 0)))) (hash "067ac9hb9m6kbd4hsq33zln4p9rv9nj0j0zprs0anl9hwj6xk3rn") (features (quote (("default" "warp"))))))

(define-public crate-akibisuto-stylus-0.1 (crate (name "akibisuto-stylus") (vers "0.1.4") (deps (list (crate-dep (name "warp") (req "^0.2") (optional #t) (kind 0)))) (hash "1ajvn7i3v51cv0ckn676mawkb1ps37438s2xp5jz2kcad4dy3adn") (features (quote (("default" "warp"))))))

(define-public crate-akibisuto-stylus-0.1 (crate (name "akibisuto-stylus") (vers "0.1.5") (deps (list (crate-dep (name "warp") (req "^0.2") (optional #t) (kind 0)))) (hash "0apw4ljn89v10119fjlyp4wjlh5g246gl7fsdlgi8hg5jfm4dp6h") (features (quote (("default" "warp"))))))

(define-public crate-akibisuto-stylus-0.1 (crate (name "akibisuto-stylus") (vers "0.1.6") (deps (list (crate-dep (name "warp") (req "^0.2") (optional #t) (kind 0)))) (hash "032j1dffn6y87h46lw3lnl3ldn6g6bkg1pmlg5zxc2xiyvbsv5h5") (features (quote (("default" "warp"))))))

(define-public crate-akibisuto-stylus-0.1 (crate (name "akibisuto-stylus") (vers "0.1.7") (deps (list (crate-dep (name "warp") (req "^0.2") (optional #t) (kind 0)))) (hash "1wm4fvmch1j9hj13h4fghr8gym3p4p95qnz6r8kibkj5qfb0apza") (features (quote (("default" "warp"))))))

(define-public crate-akibisuto-stylus-0.1 (crate (name "akibisuto-stylus") (vers "0.1.8") (deps (list (crate-dep (name "warp") (req "^0.2") (optional #t) (kind 0)))) (hash "05a62gf2wf0vp4sjw9lkb9mwzrj7cyf5186ffdhj1ckjjz3z0slz") (features (quote (("default" "warp"))))))

(define-public crate-akibisuto-stylus-0.1 (crate (name "akibisuto-stylus") (vers "0.1.9") (deps (list (crate-dep (name "warp") (req "^0.2") (optional #t) (kind 0)))) (hash "1yrjgi058b2wr77ghgf0h7sk3184f2bs5g87r9gdcfr62dmsawnq") (features (quote (("default" "warp"))))))

(define-public crate-akibisuto-stylus-0.1 (crate (name "akibisuto-stylus") (vers "0.1.10") (deps (list (crate-dep (name "warp") (req "^0.2") (optional #t) (kind 0)))) (hash "01xr11vx9cwaxgir5wyymz1c0iiylywcwpfhqsddkdg4xadwk0js") (features (quote (("default" "warp"))))))

(define-public crate-akibisuto-stylus-0.1 (crate (name "akibisuto-stylus") (vers "0.1.11") (deps (list (crate-dep (name "warp") (req "^0.2") (optional #t) (kind 0)))) (hash "0pcdw6fk687m8dwfxhylm6vk1adkzhzdcdvb4wbsa092yxjnl1fv") (features (quote (("default" "warp"))))))

(define-public crate-akibisuto-stylus-0.1 (crate (name "akibisuto-stylus") (vers "0.1.12") (deps (list (crate-dep (name "warp") (req "^0.2") (optional #t) (kind 0)))) (hash "0z7jspkwbhihngqpwdxk72zdcqaqsz7npib6qy12kf7x2h1y1xd3") (features (quote (("default" "warp"))))))

