(define-module (crates-io t6 #{7x}#) #:use-module (crates-io))

(define-public crate-t67xx-0.1 (crate (name "t67xx") (vers "0.1.0") (deps (list (crate-dep (name "device-driver") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "num_enum") (req "^0.5.1") (kind 0)))) (hash "09q000lppbaxz22i7m924g064hqamrlbr4djj9wljaq9bhc98d4c")))

(define-public crate-t67xx-0.1 (crate (name "t67xx") (vers "0.1.1") (deps (list (crate-dep (name "device-driver") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "num_enum") (req "^0.5.1") (kind 0)))) (hash "1zb9x33d0fiqbm1wpsfx9k66w283qkraiz1m9y90fbdwrqvwk1i4")))

