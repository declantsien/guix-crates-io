(define-module (crates-io l1 #{9_}#) #:use-module (crates-io))

(define-public crate-l19_terminal_tictactoe-0.1 (crate (name "l19_terminal_tictactoe") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1lysa7idfjh12f69diswpgyldkqz6kj769i17f5q2kni4q6qqm1h")))

(define-public crate-l19_terminal_tictactoe-0.1 (crate (name "l19_terminal_tictactoe") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0xnd8z4disaxmb6dfkjmlcgsddr0b5f5zxm0zr5vngw45jzhwh08")))

(define-public crate-l19_terminal_tictactoe-0.1 (crate (name "l19_terminal_tictactoe") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1f71kqqwm19nh4z3zpa83yrjxwjg5ih7p891zrvgks6yxn934vj4")))

