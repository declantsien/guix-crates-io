(define-module (crates-io l1 ir) #:use-module (crates-io))

(define-public crate-l1ir-rust-0.0.1 (crate (name "l1ir-rust") (vers "0.0.1") (deps (list (crate-dep (name "l1_ir") (req "^0.0.50") (default-features #t) (kind 0)))) (hash "0a2h5k8icapinndpyy666wjpwz973syx5r1nlm6s3b1ash4wj28c") (yanked #t)))

