(define-module (crates-io l1 _s) #:use-module (crates-io))

(define-public crate-l1_solver-0.1 (crate (name "l1_solver") (vers "0.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.30.1") (default-features #t) (kind 0)))) (hash "1z8rylddpqgzqin79vf7d8xvs707g2irn250v1ql2rhppmcawg98")))

